[![Build Status](https://app.travis-ci.com/nomercy_entertainment/nomercymediaserver.svg?branch=master)](https://app.travis-ci.com/nomercy_entertainment/nomercymediaserver)

# NoMercy MediaServer.

Import all your movies and tv shows and play them on all your devices.

## Installation

You can install the package with by following these steps:

1. Install [NodeJS](https://nodejs.org/en/download).
1. Install [Git](https://git-scm.com/downloads).
1. Create an account on [NoMercy.tv](https://nomercy.tv/register)
1. Run the following commands in the commandline at the desired root folder.

```bash
git clone https://gitlab.com/nomercy_entertainment/nomercymediaserver.git nomercymediaserver
cd nomercymediaserver
```
## Usage

``` bash
npm start
```

#### Navigate to https://app.nomercy.tv.
## Enjoy!

<br>

<!-- ### Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information what has changed recently. -->

## Warning

#### This Package is in active development, as such this package may break at any time.<br>Update with caution.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

<!-- ### Security

If you discover any security related issues, please email stoney@nomercy.tv instead of using the issue tracker. -->


## Credits

- [Stoney Eagle](https://gitlab.com/Stoney_Eagle)
- [All Contributors](CONTRIBUTORS.md)

## License

See the [LICENSE](LICENSE.md) file for license rights and limitations 