module.exports = {
	roots: [
		"<rootDir>",
	],
	modulePaths: [
		"<rootDir>",
	],
	moduleDirectories: ["node_modules", "src"],
	setupFilesAfterEnv: [
		"<rootDir>/src/setuptests.ts",
	],
}