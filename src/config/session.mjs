import { keycloak_key } from './keycloak.mjs';

export const session_config = {
	secret: keycloak_key,
	resave: false,
	saveUninitialized: true,
	cookie: {
		sameSite: 'None',
		domain: 'nomercy.tv',
		httpOnly: true,
		secure: true,
		expires: new Date(31 * 24 * 60 * 60 * 1000),
		maxAge: 31 * 24 * 60 * 60 * 1000,
	},
	user: {},
};

export default {
	session_config,
};
