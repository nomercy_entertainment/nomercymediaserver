import request from 'sync-request';

const realm = 'https://auth.nomercy.tv/auth/realms/NoMercyTV';
const res = request('GET', realm);
export const info = JSON.parse(res.getBody());

export const key = `-----BEGIN PUBLIC KEY-----\n${info.public_key}\n-----END PUBLIC KEY-----`;

export const keycloak_key = 'vbz8elvBVVTIWsX7tKB2LmI5FzDbItbN';

export default {
	realm: 'NoMercyTV',
	'bearer-only': true,
	'auth-server-url': 'https://auth.nomercy.tv/auth/',
	'ssl-required': 'all',
	resource: 'nomercy-server',
	'verify-token-audience': true,
	'use-resource-role-mappings': true,
	'confidential-port': 0,
	public_key: key,
};
