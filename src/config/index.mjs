import fs from 'fs';
import i18next from 'i18next';
import { key } from './keycloak.mjs';
import path from 'path';
import { prismaMedia } from './database.mjs';
import socketCors from './socket.mjs';

const __dirname = path.resolve();

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

let errorLog = __dirname + '/errorlog.txt';

const config = async function () {
	const configFile = __dirname + '/config/config.json';
	if (!fs.existsSync(configFile)) {
		fs.writeFileSync(configFile, JSON.stringify({}));
	}
	let { server_id } = JSON.parse(fs.readFileSync(configFile));

	const languagesFile = __dirname + '/config/languages.json';
	let languages = JSON.parse(fs.readFileSync(languagesFile));

	const conf = await prismaMedia.configuration.findMany({
		select: {
			key: true,
			value: true,
			modified_by: true,
		},
		orderBy: {
			key: 'asc',
		},
	});

	let configuration = {
		i18next,
		key,
		errorLog,
		socketCors,
		languages,
		server_id,
	};

	conf.map((c) => {
		try {
			return (configuration[c.key] = JSON.parse(c.value));
		} catch (error) {
			return (configuration[c.key] = c.value);
		}
	});

	global.config = configuration;
	return configuration;
};

export default config;
