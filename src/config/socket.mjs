export default {
	cors: {
		origin: '*',
		methods: 'GET,PUT,POST,DELETE,OPTIONS'.split(','),
		credentials: true,
		exposedHeaders: ['Content-Length', 'Range'],
		acceptRanges: 'byes',
	},
};
