import { lstatSync, readdirSync } from 'fs';

import FsBackend from 'i18next-node-fs-backend';
import {XMLHttpRequest} from 'xmlhttprequest';
import i18next from 'i18next';
import { join } from 'path';
import languageDetector from 'i18next-browser-languagedetector';
import middleware from 'i18next-http-middleware';
import path from 'path';

const __dirname = path.resolve();

i18next
	.use(FsBackend)
	.use(languageDetector)
	.use(middleware.LanguageDetector)
	.init({
		fallbackLng: 'en',
		saveMissing: true,
		debug: false,
		backend: {
			ajax: XMLHttpRequest,
			loadPath: __dirname + '/src/resources/lang/{{lng}}/{{ns}}.json',
			addPath: __dirname + '/src/resources/lang/{{lng}}/{{ns}}.missing.json',
		},
		preload: readdirSync(__dirname + '/src/resources/lang').filter((fileName) => {
			const joinedPath = join(__dirname + '/src/resources/lang', fileName);
			const isDirectory = lstatSync(joinedPath).isDirectory();
			return isDirectory;
		}),
		ns: readdirSync(__dirname + '/src/resources/lang/en').map((fileName) => {
			return fileName.replace('.json', '');
		}),
	});

export default i18next;
