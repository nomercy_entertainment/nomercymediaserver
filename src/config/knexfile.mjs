// Update with your config settings.
import path from 'path';
const __dirname = path.resolve();

const config = {
	content: {
		client: 'sqlite3',
		connection: {
			filename: path.resolve(__dirname, '..', '..', 'databases', 'database.sqlite3'),
		},
		useNullAsDefault: true,
		migrations: {
			stub: path.resolve(__dirname, 'database', 'migrations', 'default.stub'),
			directory: path.resolve(__dirname, 'database', 'migrations', 'main'),
		},
		seeds: {
			directory: path.resolve(__dirname, 'database', 'seeds', 'main'),
		},
		// debug: true,
		pool: {
			min: 1,
			max: 1,
			propagateCreateError: false,
		},
	},
	session: {
		client: 'sqlite3',
		connection: {
			filename: path.resolve(__dirname, '..', '..', 'databases', 'session.sqlite3'),
		},
		useNullAsDefault: true,
		pool: {
			min: 1,
			max: 1,
			propagateCreateError: false,
		},
	},
	sync: {
		client: 'sqlite3',
		connection: {
			filename: path.resolve(__dirname, '..', '..', 'databases', 'sync.sqlite3'),
		},
		migrations: {
			stub: path.resolve(__dirname, 'database', 'migrations', 'default.stub'),
			directory: path.resolve(__dirname, 'database', 'migrations', 'sync'),
		},
		seeds: {
			directory: path.resolve(__dirname, 'database', 'seeds', 'sync'),
		},
		useNullAsDefault: true,
		pool: {
			min: 1,
			max: 1,
			propagateCreateError: false,
		},
	},
	development: {
		client: 'sqlite3',
		connection: {
			filename: path.resolve(__dirname, '..', '..', 'databases', 'database.sqlite3'),
		},
		useNullAsDefault: true,
		debug: true,
		migrations: {
			stub: path.resolve(__dirname, 'database', 'migrations', 'default.stub'),
			directory: path.resolve(__dirname, 'database', 'migrations', 'main'),
		},
		seeds: {
			directory: path.resolve(__dirname, 'database', 'seeds', 'main'),
		},
		pool: {
			min: 1,
			max: 1,
			propagateCreateError: false,
		},
	},
};

export default config;
