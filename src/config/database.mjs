import path from 'path';
import prisma from '@prisma/client';

const __dirname = path.resolve();

const { PrismaClient } = prisma;
/**
 * 
 * @param {import('@prisma/client')} prismaMedia 
 */
export const prismaMedia = global.prismaMedia || new PrismaClient({
	datasources: {
		db: {
			url: `file:${__dirname}/databases/media.db?socket_timeout=10&connection_limit=1&timeout=5000`,
		},
	},
	errorFormat: 'pretty',
	log: [
		{
			emit: 'stdout',
			level: 'error',
		},
	],
});

if (process.env.NODE_ENV === 'development') global.prismaMedia = prismaMedia;

prismaMedia.$on('error', (e) => {
	console.log(e);
});
