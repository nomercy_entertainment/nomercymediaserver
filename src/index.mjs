import cdn from './loaders/cdn.mjs';
import configuration from './config/index.mjs';
import env from 'dotenv';
import express from 'express';
import fs from 'fs';
import loaders from './loaders/index.mjs';
import logo from './loaders/logo.mjs';
import migrations from './loaders/migrations.mjs';
import path from 'path';
import register from './loaders/register.mjs';
import seed from './loaders/seed.mjs';
const __dirname = path.resolve();
global.__dirname = __dirname + '/src';
env.config();

async function startServer() {
	if (!fs.existsSync(__dirname + '/.env')) {
		fs.copyFileSync(__dirname + '/.env.example', __dirname + '/.env');
		env.config();
	}

	await cdn();
	await logo();

	await migrations();
	await seed();

	await configuration();

	await register();

	const app = express();

	await loaders(app);
}

startServer();
