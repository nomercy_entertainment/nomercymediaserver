import cdn from './loaders/cdn.mjs';
import migrations from './loaders/migrations.mjs';
import { prismaMedia } from './config/database.mjs';
import seed from './loaders/seed.mjs';

describe('Startup tests', () => {
	beforeEach(() => {
		jest.setTimeout(60000);
	});

	afterAll((done) => {
		prismaMedia.$disconnect();
		done();
	});

	// test('Migrations can be done', async () => {
	// 	await migrations();

	// 	const genre = await prismaMedia.movie_genre.findFirst();
	// 	expect(genre).toEqual({ id: 28, name: 'Action' });
	// 	prismaMedia.$disconnect();
	// }, 30000);

	// test('Seeds can be done', async () => {
	// 	await seed();

	// 	const configuration = await prismaMedia.configuration.findFirst();

	// 	expect(configuration).toMatchObject({ id: expect.any(Number), key: expect.any(String), updated_at: expect.any(Date), value: expect.any(String) });
	// 	prismaMedia.$disconnect();
	// }, 30000);

	test('Can get data from CDN', async () => {
		let data = await cdn();

		expect(data).toMatchObject({
			state: expect.any(String),
			version: expect.any(String),
			copyright: expect.any(String),
			licence: expect.any(String),
			contact: { homepage: expect.any(String), email: expect.any(String), dmca: expect.any(String), languages: expect.any(String) },
			git: 'https://gitlab.com/nomercy_entertainment/nomercymediaserver.git',
			keys: { makemkv_key: expect.any(String) },
			quote: expect.any(String),
			colors: expect.any(Array),
		});
	});

	// test('Can Setup the server configurations ', async () => {
	// 	import config from './config/index.mjs';

	// 	let data = await config();
	// 	delete data.i18next;
	// 	data.server_id = data.server_id + '';
	// 	data.owner = data.owner + '';

	// 	expect(data).toMatchObject({
	// 		key: expect.any(String),
	// 		errorLog: expect.any(String),
	// 		socketCors: {
	// 			cors: {
	// 				origin: expect.any(String),
	// 				methods: expect.any(Array),
	// 				credentials: expect.any(Boolean),
	// 				exposedHeaders: expect.any(Array),
	// 				acceptRanges: expect.any(String),
	// 			},
	// 		},
	// 		languages: expect.any(Array),
	// 		server_id: expect.any(String),
	// 		allowAdult: expect.any(Boolean),
	// 		allowedUsers: expect.any(Array),
	// 		alwaysCreatePlaylist: expect.any(Boolean),
	// 		assToVtt: expect.any(Boolean),
	// 		channelOrder: expect.any(Object),
	// 		debug: expect.any(Boolean),
	// 		defaults: expect.any(Array),
	// 		deleteInput: expect.any(Boolean),
	// 		encodingQualities: expect.any(Array),
	// 		external_port: expect.any(Number),
	// 		folderRoots: expect.any(Array),
	// 		internal_port: expect.any(Number),
	// 		keepOriginal: {
	// 			audio: expect.any(Boolean),
	// 			subtitles: expect.any(Boolean),
	// 		},
	// 		loglevel: expect.any(String),
	// 		maxParts: expect.any(Number),
	// 		mediaRoot: expect.any(String),
	// 		minlength: expect.any(Number),
	// 		openServer: expect.any(Boolean),
	// 		owner: expect.any(String),
	// 		port_forward: expect.any(Boolean),
	// 		prefferedOrder: expect.any(Object),
	// 		queue: expect.any(Boolean),
	// 		ripper: expect.any(Boolean),
	// 		ripperFolder: expect.any(String),
	// 		ripperOutFolder: expect.any(String),
	// 		segment: expect.any(Array),
	// 		segmentTime: expect.any(Number),
	// 		updateInterval: expect.any(Number),
	// 		workerName: expect.any(String),
	// 		workers: expect.any(Number),
	// 	});
	// });
});
