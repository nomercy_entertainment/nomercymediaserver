const { app, BrowserWindow, screen, Tray, Menu } = require('electron');
const path = require('path');
require('dotenv').config();
const axios = require('axios');
const https = require('https');
const axiosRetry = require('axios-retry');

const axiosInstance = axios.create({
	httpsAgent: new https.Agent({
		rejectUnauthorized: false,
	}),
});

axiosRetry(axiosInstance, {
	retries: 3,
	retryDelay: () => {
		return 2000;
	},
});

let win, tray, splash;
const trayIcon = path.join(__dirname, '/logo-white.png');
const mainIcon = path.join(__dirname, '/logo-encoder-white.png');

function splashWindow() {
	splash = new BrowserWindow({
		width: 600,
		height: 300,
		frame: false,
		center: true,
		icon: trayIcon,
		webPreferences: {
			contextIsolation: true,
			nodeIntegration: true,
			sandbox: true,
		},
		// alwaysOnTop: true
	});
	splash.loadURL(`file://${__dirname}/splash.html`);

	splash.on('ready-to-show', () => {
		splash.focus();
		splash.show();
	});
}

function mainWindow() {
	const size = screen.getPrimaryDisplay().workAreaSize;
	win = new BrowserWindow({
		show: false,
		title: 'NoMercy MediaServer',
		icon: mainIcon,
		width: size.width - 100,
		height: size.height - 100,
		minWidth: 1281,
		minHeight: 720,
		resizable: true,
		roundedCorners: true,
		center: true,
		icon: trayIcon,
		autoHideMenuBar: true,
		useContentSize: true,
		webPreferences: {
			nodeIntegration: true,
			autoplayPolicy: 'no-user-gesture-required',
			sandbox: true,
			contextIsolation: false,
			webSecurity: false,
			secure: true,
			bypassCSP: false,
			allowServiceWorkers: true,
			supportFetchAPI: true,
			corsEnabled: false,
		},
	});

	win.loadURL(`https://app.nomercy.tv`);

	win.on('closed', () => {
		win = null;
	});

	win.on('ready-to-show', async () => {
		splash.destroy();
		win.focus();
		win.show();
	});

	win.on('close', function(event) {
		if (!app.isQuiting) {
			event.preventDefault();
			win.hide();
		}
		return false;
	});

	app.on('browser-window-created', function(e, window) {
		window.setMenu(null);
	});

	app.on('window-all-closed', function() {
		if (process.platform !== 'darwin') {
			app.quit();
		}
	});
}

function trayMenu() {
	const trayMenu = Menu.buildFromTemplate([
		{
			label: 'Show App',
			click: function() {
				win.show();
			},
		},
		{
			label: 'Quit',
			click: function() {
				app.isQuiting = true;
				app.quit();
			},
		},
	]);
	tray = new Tray(trayIcon);
	tray.setContextMenu(trayMenu);
	tray.setToolTip('NoMercy Media Server');

	tray.on('click', function() {
		win.show();
	});
}

async function createApp() {
	splashWindow();

	app.allowRendererProcessReuse = true;

	const gotTheLock = app.requestSingleInstanceLock();

	if (!gotTheLock) {
		app.quit();
	} else {
		app.on('second-instance', (event, commandLine, workingDirectory) => {
			if (win) {
				if (win.isMinimized()) win.restore();
				win.focus();
			}
		});

		app.on('activate', () => {
			BrowserWindow.getAllWindows().map(win => win.show());
		});
	}

	axiosInstance
		.get(`https://localhost:${process.env.SECURE_INTERNAL_PORT}/api/ping`)
		.catch(error => {
			if (error.response.data.message.includes('token')) {
				mainWindow();
				trayMenu();
			}
		});
}

app.on('ready', () => {
	createApp();
});
