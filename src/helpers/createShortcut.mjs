import createDesktopShortcut from 'create-desktop-shortcuts';
import { execSync } from 'child_process';
import { getPlatform } from './system.mjs';
import path from 'path';

export const createShortcut = () => createDesktopShortcut({
  onlyCurrentOS: true,
  verbose: false,
  windows: {
    filePath: path.resolve('./NoMercy MediaServer.vbs'),
    name: 'NoMercy MediaServer',
    comment: 'The Effortless Encoder',
    icon: path.resolve('./src/resources/favicon.ico'),
		WindowStyle: 'hidden',
  },
  // linux: {
  //   // REQUIRED: Path must exist
  //   filePath: '/home/path/to/executable',
  //   // OPTIONAL: Defaults to the Desktop of the current user
  //   outputPath: '/home/some/folder',
  //   // OPTIONAL: defaults to the filePath file's name (without the extension)
  //   name: 'My App Name',
  //   // OPTIONAL
  //   description: 'My app description',
  //   // OPTIONAL: File must exist and be PNG or ICNS
  //   icon: '/home/path/to/file.png',
  //   // OPTIONAL: 'Application', 'Directory', or 'Link' (must logically match filePath)
  //   type: 'Application',
  //   // OPTIONAL: defaults to false
  //   terminal: false,
  //   // OPTIONAL: defaults to true
  //   chmod: true,
  //   // OPTIONAL
  //   arguments: '--my-argument -f'
  // },
  // osx: {
  //   // REQUIRED: Path must exist
  //   filePath: '/Applications/My App.app',
  //   // OPTIONAL: Defaults to the Desktop of the current user
  //   outputPath: '/home/some/folder',
  //   // OPTIONAL: defaults to the filePath file's name (without the extension)
  //   name: 'My App Name',
  //   // OPTIONAL: defaults to false
  //   overwrite: false
  // }
});
