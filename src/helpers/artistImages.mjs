import { prismaMedia } from '../config/database.mjs';
import axios from 'axios';
import cheerio from 'cheerio';
import fs from 'fs';

export const artistImages = async function (artist) {
	const baseUrl = 'https://www.last.fm';

	const url = `${baseUrl}/music/${artist
		.replace(/[\s]/g, '+')
		.replace(/[áÁåä]/gi, 'a')
		.replace(/[ç]/gi, 'c')
		.replace(/[éëèê]/gi, 'e')
		.replace(/[íîï]/gi, 'i')
		.replace(/[Øöó]/gi, 'o')
		.replace(/[ý]/gi, 'y')
		.replace(/[®]/gi, '')
	}/+images`;

	const urls = [];

	const promisses = [];

	try {
		await axios
			.get(url)
			.then(({ data }) => {
				const $ = cheerio.load(data);
				$('.image-list')
					.children()
					.each((i, el) => {
						const item = $(el).children().closest('a').attr('href');
						urls.push({
							page: `${baseUrl}${item}`,
							url: 'https://lastfm.freetls.fastly.net/i/u/avatar300s/' + item.replace(/.*\//, ''),
							up: 0,
							down: 0,
							average: 0,
							type: 'unknown',
						});
					});
			})
			.catch((e) => {
				console.log(e);
			})
			.then(() => {
				for (let i = 0; i < urls.length; i += 1) {
					promisses.push(
						axios
							.get(urls[i].page)
							.then(async ({ data }) => {
								const $ = cheerio.load(data);

								$('.gallery-image-votes').each((j, el) => {
									const a = $(el).find('span');
									const b = a.parent().contents();
									const text = parseInt(
										$(
											b[
												Array.prototype.findIndex.call(b, function (elem) {
													return $(elem).is(a);
												}) + 1
											]
										)
											.text()
											.trim()
									);
									const direction = a.text();

									if (direction.includes('Up')) {
										urls[i].up = text;
									} else if (direction.includes('Down')) {
										urls[i].down = text;
									}
								});
								urls[i].average = (urls[i].up / (urls[i].down + urls[i].up)) * 100;

								const res = await axios.head(urls[i].url);
								urls[i].type = res.headers['content-type'];
							})
							.catch((e) => {})
					);
				}
			});
		await Promise.all(promisses).catch((e) => {});
	} catch (e) {}

	return urls
		.filter(async (u) => (await axios.head(u.url).catch(() => ({status: 500}))).status < 400)
		.filter((a) => !a.type.includes('gif'))
		.filter((a) => a.average > 40)
		.sort((a, b) => b.up - a.up)
		.concat(
			urls
				.filter((a) => a.average <= 40)
				.filter((a) => !a.type.includes('gif'))
				.sort((a, b) => b.up - a.up)
		)
		.concat(urls.filter((a) => a.type.includes('gif')).sort((a, b) => b.average - a.average));
};

export const storageArtistImageInDatabase = async function (name, storagePath) {
	const artist = await prismaMedia.artist.findFirst({
		where: {
			name: name,
		},
	});
	if (!artist?.name) return;

	await prismaMedia.artist.update({
		where: {
			id: artist.id,
		},
		data: {
			cover: '/Music/' + storagePath,
		},
	});
};
export const downloadImage = async function (url, destination) {
	return new Promise(async (resolve, reject) => {
		const folder = destination.replace(/[\/\\][^\\\/]+$/, '');
		try {
			if (!fs.existsSync(folder)) {
				fs.mkdirSync(folder, { recursive: true });
			}
			const writer = fs.createWriteStream(destination);

			const response = await axios({
				url,
				method: 'GET',
				responseType: 'stream',
			});

			response.data.pipe(writer);

			writer.on('finish', resolve);
			writer.on('error', reject);
		} catch (error) {}
	});
};

export const getBestArtistImag = async function (artist, storagePath) {
	const image = (await artistImages(artist))[0];

	let result = false;

	if (image) {
		await downloadImage(image.url, storagePath)
			.then(() => (result = true))
			.catch((e) => {});
	}

	return result ? storagePath : null;
};
