import fs from 'fs';
import { pad } from './stringArray.mjs';

export const convertToSeconds = function (time) {
	if (!time) return null;
	let t = time.split('.')[0];
	let a = t.split(':');
	return +a[0] * 60 * 60 + +a[1] * 60 + +a[2];
};

export const convertToHuman = function (time) {
	time = parseInt(time);
	let days = parseInt(time / (3600 * 24));

	let hours = pad(parseInt((time % 86400) / 3600), 2);

	let minutes = parseInt((time % 3600) / 60);
	let seconds = parseInt(time % 60);
	if (('' + minutes).length === 1) {
		minutes = '0' + minutes;
	}
	if (('' + seconds).length === 1) {
		seconds = '0' + seconds;
	}
	if (days !== 0) {
		days = '' + days + ':';
	} else {
		days = '00:';
	}
	if (hours !== 0) {
		hours = '' + hours + ':';
	} else {
		hours = '00:';
	}
	if (minutes !== 0) {
		minutes = '' + minutes + ':';
	} else {
		minutes = '00:';
	}
	const current = days + hours + minutes + seconds;
	return current.replace('NaN:NaN:NaN:NaN', '00:00');
};

export const humanTime = function (time) {
	time = parseInt(time);
	let hours = pad(parseInt((time % 86400) / 3600 + ""), 2);

	let minutes = parseInt((time % 3600) / 60 + "");
	let seconds = parseInt((time % 60) + "");
	if (("" + minutes).length === 1) {
		minutes = "0" + minutes;
	}
	if (("" + seconds).length === 1) {
		seconds = "0" + seconds;
	}
	if (hours !== 0) {
		hours = "" + hours + ":";
	} else {
		hours = "00:";
	}
	if (minutes !== 0) {
		minutes = "" + minutes + ":";
	} else {
		minutes = "00:";
	}
	const current = hours + minutes + seconds;
	return current.replace("NaN:NaN:NaN", "00:00");
};

export const convertToHis = function (time) {
	let hours = pad(parseInt((time % 86400) / 3600), 2);

	let minutes = parseInt((time % 3600) / 60);
	let seconds = parseInt(time % 60);
	if (('' + minutes).length === 1) {
		minutes = '0' + minutes;
	}
	if (('' + seconds).length === 1) {
		seconds = '0' + seconds;
	}
	if (hours !== 0) {
		hours = '' + hours + ':';
	} else {
		hours = '00:';
	}
	if (minutes !== 0) {
		minutes = '' + minutes + ':';
	} else {
		minutes = '00:';
	}
	const current = hours + minutes + seconds;
	return current.replace('NaN:NaN:NaN', '00:00');
};

export const sleep = async function (ms) {
	let start = new Date().getTime();
	let expire = start + ms;
	while (new Date().getTime() < expire) {}
	return;
};

export const fileChangedAt = function (file) {
	const fileTime = fs.statSync(file).mtime;
	return fileTime;
};

export const parseYear = function (date) {
	return new Date(date).getFullYear();
};

/**
 * * Return file change time in hours ago.
 * @param {File} file Full path to file.
 * @param {string} notation Days, Hours (default), Minutes, Seconds.
 * @param {string} method Floor (default), Ceil, Round.
 */
export const fileChangedAgo = function (file, notation = 'hours', method = 'floor') {
	let time;
	switch (notation) {
		case 'days':
			time = 24 * 60 * 60 * 1000;
			break;
		case 'hours':
			time = 60 * 60 * 1000;
			break;
		case 'minutes':
			time = 60 * 1000;
			break;
		case 'seconds':
			time = 1000;
			break;
		default:
			time = 1;
			break;
	}

	const fileTime = fs.statSync(file).mtime;
	const now = new Date();

	const result = Math.abs((now - fileTime) / time);

	if (method == 'ceil') return Math.ceil(result);
	if (method == 'round') return Math.found(result);
	if (method == 'floor') return Math.floor(result);

	return result;
};

/**
 * * Return time in hours ago.
 * @param {date} date Full path to file.
 * @param {string} notation Days, Hours (default), Minutes, Seconds.
 * @param {string} method Floor (default), Ceil, Round.
 */
export const dateAgo = function (date, notation = 'hours', method = 'floor') {
	let time;
	switch (notation) {
		case 'days':
			time = 24 * 60 * 60 * 1000;
			break;
		case 'hours':
			time = 60 * 60 * 1000;
			break;
		case 'minutes':
			time = 60 * 1000;
			break;
		case 'seconds':
			time = 1000;
			break;
		default:
			time = 1;
			break;
	}

	const now = new Date();
	const result = Math.abs((now - date) / time);

	if (method == 'ceil') return Math.ceil(result);
	if (method == 'round') return Math.found(result);
	if (method == 'floor') return Math.floor(result);

	return result;
};

/**
 * * Return Array of times starting at 00:00:00.000
 * * createTimeInterval(310,10) Ends at: 00:05:10.000
 * @param {Number} duration Number in seconds
 * @param {Number} interval time between new time
 */
export const createTimeInterval = function (duration, interval) {
	var d = new Date();
	d.setHours(0, 0, 0, 0);

	var date = d.getDate();
	var timeArr = [];
	while (date == d.getDate()) {
		let hours = pad(d.getHours(), 2);
		let minute = pad(d.getMinutes(), 2);
		let second = pad(d.getSeconds(), 2);
		let miliSecond = pad(d.getMilliseconds(), 3);

		timeArr.push(hours + ':' + minute + ':' + second + '.' + miliSecond);

		d.setSeconds(d.getSeconds() + interval);
	}

	return timeArr.splice(0, duration / interval);
};

export const dateFormat = (function () {
	var token = /d{1,4}|m{1,4}|yy(?:yy)?|([HhMsTt])\1?|[LloSZ]|"[^"]*"|'[^']*'/g,
		timezone = /\b(?:[PMCEA][SDP]T|(?:Pacific|Mountain|Central|Eastern|Atlantic) (?:Standard|Daylight|Prevailing) Time|(?:GMT|UTC)(?:[-+]\d{4})?)\b/g,
		timezoneClip = /[^-+\dA-Z]/g,
		pad = function (val, len) {
			val = String(val);
			len = len || 2;
			while (val.length < len) val = '0' + val;
			return val;
		};

	// Regexes and supporting functions are cached through closure
	return function (date, mask, utc) {
		var dF = dateFormat;

		// You can't provide utc if you skip other args (use the "UTC:" mask prefix)
		if (arguments.length == 1 && Object.prototype.toString.call(date) == '[object String]' && !/\d/.test(date)) {
			mask = date;
			date = undefined;
		}

		// Passing date through Date applies Date.parse, if necessary
		date = date ? new Date(date) : new Date();
		if (isNaN(date)) throw SyntaxError('invalid date');

		mask = String(dF.masks[mask] || mask || dF.masks['default']);

		// Allow setting the utc argument via the mask
		if (mask.slice(0, 4) == 'UTC:') {
			mask = mask.slice(4);
			utc = true;
		}

		var _ = utc ? 'getUTC' : 'get',
			d = date[_ + 'Date'](),
			D = date[_ + 'Day'](),
			m = date[_ + 'Month'](),
			y = date[_ + 'FullYear'](),
			H = date[_ + 'Hours'](),
			M = date[_ + 'Minutes'](),
			s = date[_ + 'Seconds'](),
			L = date[_ + 'Milliseconds'](),
			o = utc ? 0 : date.getTimezoneOffset(),
			flags = {
				d: d,
				dd: pad(d),
				ddd: dF.i18n.dayNames[D],
				dddd: dF.i18n.dayNames[D + 7],
				m: m + 1,
				mm: pad(m + 1),
				mmm: dF.i18n.monthNames[m],
				mmmm: dF.i18n.monthNames[m + 12],
				yy: String(y).slice(2),
				yyyy: y,
				h: H % 12 || 12,
				hh: pad(H % 12 || 12),
				H: H,
				HH: pad(H),
				M: M,
				MM: pad(M),
				s: s,
				ss: pad(s),
				l: pad(L, 3),
				L: pad(L > 99 ? Math.round(L / 10) : L),
				t: H < 12 ? 'a' : 'p',
				tt: H < 12 ? 'am' : 'pm',
				T: H < 12 ? 'A' : 'P',
				TT: H < 12 ? 'AM' : 'PM',
				Z: utc ? 'UTC' : (String(date).match(timezone) || ['']).pop().replace(timezoneClip, ''),
				o: (o > 0 ? '-' : '+') + pad(Math.floor(Math.abs(o) / 60) * 100 + (Math.abs(o) % 60), 4),
				S: ['th', 'st', 'nd', 'rd'][d % 10 > 3 ? 0 : (((d % 100) - (d % 10) != 10) * d) % 10],
			};

		return mask.replace(token, function ($0) {
			return $0 in flags ? flags[$0] : $0.slice(1, $0.length - 1);
		});
	};
})();

// Some common format strings
dateFormat.masks = {
	default: 'ddd mmm dd yyyy HH:MM:ss',
	shortDate: 'm/d/yy',
	mediumDate: 'mmm d, yyyy',
	longDate: 'mmmm d, yyyy',
	fullDate: 'dddd, mmmm d, yyyy',
	shortTime: 'h:MM TT',
	mediumTime: 'h:MM:ss TT',
	longTime: 'h:MM:ss TT Z',
	isoDate: 'yyyy-mm-dd',
	isoTime: 'HH:MM:ss',
	isoDateTime: "yyyy-mm-dd'T'HH:MM:ss",
	isoUtcDateTime: "UTC:yyyy-mm-dd'T'HH:MM:ss'Z'",
};

// Internationalization strings
dateFormat.i18n = {
	dayNames: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
	monthNames: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
};

// For convenience...
Date.prototype.format = function (mask, utc) {
	return dateFormat(this, mask, utc);
};

export default {
	convertToSeconds,
	convertToHuman,
	convertToHis,
	sleep,
	fileChangedAt,
	fileChangedAgo,
	parseYear,
	createTimeInterval,
	dateFormat,
};
