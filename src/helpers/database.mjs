import { prismaMedia } from '../config/database.mjs';

export const getConfig = async (key) => {
	return await prismaMedia.configuration.findFirst({
		where: {
			key: key.toString(),
		},
	});
};

export const setConfig = async (key, value) => {

	if(key == 'owner') {
		value = value?.sub_id || value;
	}
	await prismaMedia.configuration.upsert({
		where: {
			key: key.toString(),
		},
		update: {
			key: key.toString(),
			value: JSON.stringify(value)?.replace(/^"|"$/g, ''),
		},
		create: {
			key: key.toString(),
			value: JSON.stringify(value)?.replace(/^"|"$/g, ''),
		},
	});
};

export default {
	getConfig,
	setConfig,
};
