import Logger from '../loaders/logger.mjs';
import fs from 'fs';
import unzipper from 'unzipper';

export default async ({folder, name}) => {
	
	Logger.log({
		level: 'info',
		name: 'unzip',
		color: 'blueBright',
		message: 'Unpacking: ' + name,
	});

	const buffer = fs.readFileSync(folder + '/' + name);
	const directory = await unzipper.Open.buffer(buffer);

	const folders = directory.files.filter(f => f.type === 'Directory');
	for(const f of folders) {
		fs.mkdirSync(folder + '/' + f.path, {recursive: true});
	}
	
	const files = directory.files.filter(f => f.type === 'File');
	for(const f of files) {
		const content = await f.buffer();
		fs.writeFileSync(folder + '/' + f.path, content);
		fs.chmodSync(folder + '/' + f.path, 711);
	}
	
	fs.rmSync(folder + '/' + name);
}