export class ErrorHandler extends Error {
	statusCode;
	message;

	constructor(statusCode, message) {
		super();
		this.statusCode = statusCode;
		this.message = message;
	}
}

export const handleError = (err, res) => {
	const { statusCode, message } = err;

	global.io.on('connection', (socket) => {
		sendNotificationTo(config.owner, 'notification', {
			title: `Server error ${statusCode}`,
			body: message,
		});
	});

	res.status(statusCode).json({
		status: 'error',
		statusCode,
		message,
	});
};

export default {
	ErrorHandler,
	handleError,
};
