import jwt_decode from 'jwt-decode';

export function tokenParser(req) {
	let token = req.header('authorization').split('Bearer ')[1];
	let data = jwt_decode(token);
	return data;
}

export default tokenParser;
