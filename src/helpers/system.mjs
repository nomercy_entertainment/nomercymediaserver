import d from 'diskinfo';
import { fileURLToPath } from 'url';
import fs from 'fs';
import path from 'path';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export const update_env = (k, v) => {
	let file = fs.readFileSync(__dirname + '/../.env').toString();

	let original = [];
	let keys = [];

	file.split(/[\r\n]/).forEach((line) => {
		keys.push(line.split('=')[0]);
	});

	if (!keys.includes(k)) {
		original.push(k + '=' + v);
	}

	file.split(/[\r\n]/).forEach((line) => {
		let str = line.split('=');
		let key = str[0];
		let val;

		if (typeof k != 'undefined' && k == key) {
			val = v;
		} else if (typeof str[1] != 'undefined') {
			val = str[1];
		}
		if (typeof str[1] != 'undefined') {
			original.push(key + '=' + val);
		}
	});

	let updated = original.sort().join('\n');

	fs.writeFileSync('.env', updated);

	process.env[k] = v;

	return updated.split(/[\r\n]/);
};

export const convertPath = (path)  =>{
	if (process.platform === 'win32') {
		return path.replace(/\//g, '\\');
	}
	return path;
};

export const convertPathDoubleSlash = (path)  =>{
	if (process.platform === 'win32') {
		return path.replace(/\\/g, '\\\\').replace(/\//g, '\\\\');
	}
	return path;
};


export const convertCubePath = (path) => {
	if (process.platform === 'win32') {
		return path.replace(/\\/g, '/').replace(/(\w)\:\//g, '$1\\:/');
	}
	return path;
}

export const getDrives = async () => {

	return new Promise((resolve, Reject) => {
		
		d.getDrives(function(err, aDrives) {

			let list = {
				localDrives: aDrives.filter(d => d.filesystem == 'Local Fixed Disk'),
				opticalDrives: aDrives.filter(d => d.filesystem == 'CD-ROM Disc'),
				networkDrives: aDrives.filter(d => d.filesystem == 'Network Connection'),
			};

			return resolve(list);
		});

	});
}

export const getPlatform = () => {
	let platform;

switch (process.platform) {
	case 'win32':
		platform = 'windows';
		break;
	case 'darwin':
		platform = 'mac';
		break;
	case 'linux':
		platform = 'linux';
		break;
	default:
		platform = 'unknown';
		break;
	}
	return platform;
}

export default {
	update_env,
	convertPath,
	getDrives,
	getPlatform,
};
