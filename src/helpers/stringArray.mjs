/**
 * * Return only unique objects by key,
 * @param {string} value
 * @param {string} index
 * @param {string} self
 */
export const distinct = function (value, index, self) {
	return self.indexOf(value) === index;
};

/**
 * * Return only unique objects by key,
 * @param {Array} array Array
 * @param {string} key Unique key
 */
export const unique = function (array, key) {
	if (!array) return [];
	return array?.filter((obj, pos, arr) => {
		return arr.map((mapObj) => mapObj[key]).indexOf(obj[key]) === pos;
	});
};

/**
 * * FilterCallback.
 */
export function Unique(value, index, self) {
	return self.indexOf(value) === index;
}

/**
 * * Sort Array of objects by key,
 * @param {Array} array Array
 * @param {string} key Group key
 * @param {string} direction Order direction, default: ascending
 */
export const sort_by = function (array, key, direction = 'asc') {
	return array.sort(function (a, b) {
		var x = a[key];
		var y = b[key];
		if (direction == 'desc') {
			return x > y ? -1 : x < y ? 1 : 0;
		}

		return x < y ? -1 : x > y ? 1 : 0;
	});
};

/**
 * * ZeroPad a number to set length of characters.
 * * returns a string.
 * @param {number} number number
 * @param {number} length length
 */
export const pad = function (number, length) {
	let str = '' + number;
	while (str.length < length) {
		str = '0' + str;
	}
	return str;
};

/**
 * * Generate a random string.
 * * returns only aphanumeric characters.
 * @param {number} length Lenght
 */
export const random_string = function (length) {
	let result = '';
	let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	let charactersLength = characters.length;
	for (let i = 0; i < length; i++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return result;
};

/**
 * Generates a random string containing numbers and letters
 * @param  {number} length The length of the string
 * @return {string} The generated string
 */
 export const generateRandomString = (length) => {
	let text = '';
	const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

	for (let i = 0; i < length; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
};


/**
 * * Shuffle array,
 * @param {Array} array Array
 */
export const shuffle = function (array) {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		[array[i], array[j]] = [array[j], array[i]];
	}
	return array;
};

/**
 * * Group Array of objects by key,
 * * Sort by key (optional),
 * @param {Array} array Array
 * @param {string} key Group key
 * @param {string} key Sort key
 */
 export function sortByPriority(arr, prefferedOrder) {
	let result = [];
	let i;
	let j;
	for (i = 0; i < prefferedOrder.length; i++) {
		while (-1 != (j = arr.indexOf(prefferedOrder[i]))) {
			result.push(arr.splice(j, 1)[0]);
		}
	}
	return result.concat(arr);
}

/**
 * * Create Enum from an array of values.
 * @param {Array} array Array
 */
 export function createEnumFromArray(array) {
	return array.reduce((res, key, index) => {
		res[key] = index + 1;
		return res;
	}, {});
}

/**
 * * Group Array of objects by key.
 * * Sort by key (optional)
 * @param {Array} array Array
 * @param {string} key Group key
 * @param {string} key Sort key
 */
export const groupBy = function (array, key, sort) {
	const list = {};

	if (sort) {
		array = sort_by(array, sort);
	}

	array.map((el) => {
		list[el[key]] = array.filter((e) => e[key] == el[key]);
	});

	return list;
};

/**
 * * SortCallback.
 * ** Deprecated,
 * Use "sort_updated" with direction
 */
export const sort_updated_asc = function (a, b) {
	var keyA = new Date(a.updated_at);
	var keyB = new Date(b.updated_at);
	if (keyA > keyB) return -1;
	if (keyA < keyB) return 1;
	return 0;
};

/**
 * * SortCallback.
 * ** Deprecated,
 * Use "sort_updated" with direction
 */
export const sort_updated_desc = function (a, b) {
	var keyA = new Date(a.updated_at);
	var keyB = new Date(b.updated_at);
	if (keyA < keyB) return -1;
	if (keyA > keyB) return 1;
	return 0;
};

/**
 * * SortCallback.
 * * Sort Array of objects by Updated at,
 * @param {Array} array Array
 * @param {string} key Group key
 * @param {string} direction Sort direction
 */
export const sort_updated = function (a, b, direction = 'asc') {
	var keyA = new Date(a.updated_at);
	var keyB = new Date(b.updated_at);
	if (keyA < keyB) return direction == 'desc' ? -1 : 1;
	if (keyA > keyB) return direction == 'desc' ? 1 : -1;
	return 0;
};

export const trackSort = function(a, b) {          
	if (a.disc === b.disc) {
		return pad(a.track, 2) - pad(b.track,2);
	}
	return pad(a.track, 2) > pad(b.track, 2) ? 1 : -1;
}

/**
 * * SortCallback.
 * * Sort Array of objects by a priority list.
 * @param {Array} array Array
 * @param {Array} sortingOrder Sorting Order
 * @param {string} key Sort key
 * @param {string} key Sort direction
 */
 export function sortByPriorityKeyed(sortingOrder, key, order = 'desc') {
	if (Array.isArray(sortingOrder)) {
		sortingOrder = createEnumFromArray(sortingOrder);
	}
	return function (a, b) {
		if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
			return 0;
		}

		if (!a[key]) {
			return;
		}

		const first = a[key].toString().toLowerCase() in sortingOrder ? sortingOrder[a[key]] : Number.MAX_SAFE_INTEGER;
		const second = b[key].toString().toLowerCase() in sortingOrder ? sortingOrder[b[key]] : Number.MAX_SAFE_INTEGER;

		let result = 0;
		if (first > second) result = -1;
		else if (first < second) result = 1;
		return order === 'desc' ? ~result : result;
	};
}

export function chunk(a, n) {
	if (n < 2) return [a];

	var len = a.length,
		out = [],
		i = 0,
		size;

	if (len % n === 0) {
		size = Math.floor(len / n);
		while (i < len) {
			out.push(a.slice(i, (i += size)));
		}
	} else {
		while (i < len) {
			size = Math.ceil((len - i) / n--);
			out.push(a.slice(i, (i += size)));
		}
	}

	return out;
}

export function uniqBy(a, key) {
	return [...new Map(a.map(item =>
		[item[key], item])).values()];
}

export const limitSentenceByCharacters = (str) => {

	let arr = str.substring(0, 360).split('.');
	arr.pop(arr.length)
	return arr.join('.') + '.';
	
}

export const sortByPosterAlfabetized = (data, sort = 'name', uniqued = null) => {

	if(uniqued){
		data = unique(data, uniqued);
	}

	let postered = data.filter((d) => d.profile_path != null).sort((a, b) => +(a[sort] > b[sort]) || -(a[sort] < b[sort]));
	let nulled = data.filter((d) => d.profile_path == null).sort((a, b) => +(a[sort] > b[sort]) || -(a[sort] < b[sort]));

	return postered.concat(nulled);
}

import crypto from 'crypto';
import hexToUuid from 'hex-to-uuid';

export const javaHash = (input) => {
    var md5Bytes = crypto.createHash('md5').update(input).digest()
    md5Bytes[6] &= 0x0f;  // clear version        
    md5Bytes[6] |= 0x30;  // set to version 3     
    md5Bytes[8] &= 0x3f;  // clear variant        
    md5Bytes[8] |= 0x80;  // set to IETF variant  
    return hexToUuid(md5Bytes.toString('hex'))
}

export const mathPercentage = (strA, strB) => {
	let result = 0;
	// eslint-disable-next-line no-cond-assign
	for (let i = strA.length; i -= 1;) {
		if (typeof strB[i] == 'undefined' || strA[i] == strB[i]) {
			//
		} else if (strA[i].toLowerCase() == strB[i].toLowerCase()) {
			result += 1;
		} else { result += 4; }
	}
	return (100 - (result + 4 * Math.abs(strA.length - strB.length)) / (2 * (strA.length + strB.length)) * 100);
};

/**
 * * Sort Array of objects by the percentage matched,
 * @param {Array} array Array
 * @param {string} key Group key
 * @param {string} match Match to
 */
export const sortByMathPercentage = (array,	key, match) => {
	return array.sort((a, b) => {
		const x = mathPercentage(match, a[key]);
		const y = mathPercentage(match, b[key]);
		return x > y ? -1 : x < y ? 1 : 0;
	});
};



export default {
	distinct,
	unique,
	limitSentenceByCharacters,
	sortByPosterAlfabetized,
	Unique,
	uniqBy,
	sort_by,
	sort_updated_asc,
	sort_updated_desc,
	sort_updated,
	generateRandomString,
	pad,
	trackSort,
	random_string,
	shuffle,
	sortByPriority,
	sortByPriorityKeyed,
	createEnumFromArray,
	groupBy,
	chunk,
};
