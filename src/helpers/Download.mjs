import Logger from '../loaders/logger.mjs';
import fs from 'fs';
import path from 'path';
import request from 'request';

export default async (uri, filename, cb) => {

	const name = path.basename(uri);
	const folder = filename.replace(name, '');

	Logger.log({
		level: 'info',
		name: 'download',
		color: 'blueBright',
		message: 'Downloading: ' + name,
	});

	return new Promise(async (resolve) => {
		await request.head(uri, () => {
			request(uri)
				.pipe(fs.createWriteStream(filename + name)
				.on('finish', async () => {
					if(typeof cb === 'function' && fs.existsSync(filename + name)){

						await cb({folder, name})
					}
					
					resolve();
				}));
		})
	});
};
