export default function () {
	setTimeout(function () {
		process.on('exit', function () {
			require('child_process').spawn(process.argv.shift(), process.argv, {
				cwd: process.cwd(),
				detached: true,
				silent: true,
				windowsHide: true,
				shell: false,
				stdio: 'inherit',
			});
		});
		process.exit();
	}, 5000);
};
