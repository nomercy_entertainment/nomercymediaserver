import Logger from '../../../loaders/logger.mjs';
import moment from 'moment';
import tmdbApiClient from './tmdbApiClient.mjs';

export async function movie(id) {
	let append = ['alternative_titles', 'release_dates', 'credits', 'keywords', 'recommendations', 'similar', 'translations', 'videos', 'images', 'watch/providers'];

	let params = {
		params: {
			append_to_response: append.join(','),
			include_image_language: 'en,null',
		},
	};

	let response = await tmdbApiClient.get(`movie/${id}`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	if (response?.data) {
		let data = response?.data;
		data.media_type = 'movie';
		return data;
	}
}

export async function movieChanges(id, daysBack = 1) {
	if (daysBack > 14) {
		daysBack = 14;
	}

	let params = {
		start_date: moment().subtract(daysBack, 'days').format('md'),
		end_date: moment().format('md'),
	};

	let response = await tmdbApiClient.get(`movie/${id}/changes`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return response?.data.changes;
}

export async function movieNowPlaying() {
	let response = await tmdbApiClient.get(`movie/now_playing`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return response?.data.results;
}

export async function movieLatest() {
	let response = await tmdbApiClient.get(`movie/latest`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return response?.data.results;
}

export async function moviePopular() {
	let response = await tmdbApiClient.get(`movie/popular`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return response?.data.results;
}

export async function movieRecommendations(id, limit = 10) {
	let data = [];

	let base = await tmdbApiClient.get(`movie/${id}/recommendations`, { params: { page: 1 } }).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	for (let j = 0; j < base.data.results.length; j++) {
		data.push(base.data.results[j]);
	}

	for (let i = 1; i < base.data.total_pages && i < limit && i < 1000; i++) {
		let res = await tmdbApiClient.get(`movie/${id}/recommendations`, { params: { page: i + 1 } }).catch((err) => {
			Logger.log({
				level: 'error',
				name: 'MovieDB',
				color: 'red',
				message: err,

			});
		});

		for (let j = 0; j < res?.data.results.length; j++) {
			data.push(res.data.results[j]);
		}
	}

	return data;
}

export async function discoverByNetwork(network = 213, limit = 10) {

	let data = [];

	let base = await tmdbApiClient.get(`discover/movie`, { params: { page: 1, with_networks: network, append_to_response: 'images' } }).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	for (let j = 0; j < base.data.results.length; j++) {
		data.push(base.data.results[j]);
	}

	for (let i = 1; i < base.data.total_pages && i < limit && i < 1000; i++) {
		let res = await tmdbApiClient.get(`discover/movie`, { params: { page: i + 1, with_networks: network, append_to_response: 'images' } }).catch((err) => {
			Logger.log({
				level: 'error',
				name: 'MovieDB',
				color: 'red',
				message: err,

			});
		});

		for (let j = 0; j < res?.data.results.length; j++) {
			data.push(res.data.results[j]);
		}
	}

	return data;
}

export async function movieSimilar(id, limit = 10) {
	let data = [];

	let base = await tmdbApiClient.get(`movie/${id}/similar`, { params: { page: 1 } }).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	for (let j = 0; j < base.data.results.length; j++) {
		data.push(base.data.results[j]);
	}

	for (let i = 1; i < base.data.total_pages && i < limit && i < 1000; i++) {
		let res = await tmdbApiClient.get(`movie/${id}/similar`, { params: { page: i + 1 } }).catch((err) => {
			Logger.log({
				level: 'error',
				name: 'MovieDB',
				color: 'red',
				message: err,

			});
		});

		for (let j = 0; j < res?.data.results.length; j++) {
			data.push(res.data.results[j]);
		}
	}

	return data;
}

export async function movieTopRated() {
	let response = await tmdbApiClient.get(`movie/top_rated`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return response?.data.results;
}

export async function movieUpcomming() {
	let response = await tmdbApiClient.get(`movie/upcoming`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return response?.data.results;
}

export async function movieVideos(id) {
	let params = {
		params: {
			language: 'null',
		},
	};

	let response = await tmdbApiClient.get(`movie/${id}/videos`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return response?.data.results;
}

export async function movieImages(id) {
	let params = {
		params: {
			language: 'null',
		},
	};

	let response = await tmdbApiClient.get(`movie/${id}/images`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return response?.data;
}

export default {
	movie,
	movieChanges,
	movieImages,
	movieLatest,
	movieNowPlaying,
	moviePopular,
	movieRecommendations,
	movieSimilar,
	movieTopRated,
	movieUpcomming,
	movieVideos,
};
