import Logger from '../../../loaders/logger.mjs';
import moment from 'moment';
import tmdbApiClient from './tmdbApiClient.mjs';

export async function tv(id) {
	let append = [
		// 'aggregate_credits', 
		'alternative_titles', 
		'content_ratings', 
		'credits', 
		'external_ids', 
		'images', 
		'keywords', 
		'recommendations', 
		'similar', 
		'translations', 
		'videos', 
		'watch/providers'
	];

	let params = {
		params: {
			append_to_response: append.join(','),
			include_image_language: 'en,null',
		},
	};

	let response = await tmdbApiClient.get(`tv/${id}`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	if (response?.data) {
		return response.data;
	}
}

export async function tvChanges(id, daysBack = 1) {
	if (daysBack > 14) {
		daysBack = 14;
	}

	let params = {
		start_date: moment().subtract(daysBack, 'days').format('md'),
		end_date: moment().format('md'),
	};

	let { data } = await tmdbApiClient.get(`tv/${id}/changes`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	return data.changes;
}

export async function tvNowPlaying() {
	let { data } = await tmdbApiClient.get(`tv/now_playing`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	return data.results;
}

export async function tvLatest() {
	let { data } = await tmdbApiClient.get(`tv/latest`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	return data.results;
}

export async function tvPopular() {
	let { data } = await tmdbApiClient.get(`tv/popular`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	return data.results;
}

export async function tvRecommendations(id, limit = 10) {
	Logger.log({
		level: 'info',
		name: 'MovieDB',
		color: 'blue',
		message: 'Searching recommendations',
	});

	let data = [];

	let base = await tmdbApiClient.get(`tv/${id}/recommendations`, { params: { page: 1 } }).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	for (let j = 0; j < base.data.results.length; j++) {
		data.push(base.data.results[j]);
	}

	for (let i = 1; i < base.data.total_pages && i < limit && i < 1000; i++) {
		let res = await tmdbApiClient.get(`tv/${id}/recommendations`, { params: { page: i + 1 } }).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

		for (let j = 0; j < res?.data.results.length; j++) {
			data.push(res.data.results[j]);
		}
	}

	return data;
}

export async function discoverByNetwork(network = 213, limit = 10) {

	let data = [];

	let base = await tmdbApiClient.get(`discover/tv`, { params: { page: 1, with_networks: network, append_to_response: 'images' } }).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	for (let j = 0; j < base.data.results.length; j++) {
		data.push(base.data.results[j]);
	}

	for (let i = 1; i < base.data.total_pages && i < limit && i < 1000; i++) {
		let res = await tmdbApiClient.get(`discover/tv`, { params: { page: i + 1, with_networks: network, append_to_response: 'images' } }).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

		for (let j = 0; j < res?.data.results.length; j++) {
			data.push(res.data.results[j]);
		}
	}

	return data;
}

export async function tvSimilar(id, limit = 10) {
	Logger.log({
		level: 'info',
		name: 'MovieDB',
		color: 'blue',
		message: 'Searching similars',
	});
	let data = [];

	let base = await tmdbApiClient.get(`tv/${id}/similar`, { params: { page: 1 } }).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	for (let j = 0; j < base.data.results.length; j++) {
		data.push(base.data.results[j]);
	}

	for (let i = 1; i < base.data.total_pages && i < limit && i < 1000; i++) {
		let res = await tmdbApiClient.get(`tv/${id}/similar`, { params: { page: i + 1 } }).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

		for (let j = 0; j < res?.data.results.length; j++) {
			data.push(res.data.results[j]);
		}
	}

	return data;
}

export async function tvTopRated() {
	let { data } = await tmdbApiClient.get(`tv/top_rated`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	return data.results;
}

export async function tvUpcomming() {
	let { data } = await tmdbApiClient.get(`tv/upcoming`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	return data.results;
}

export async function tvVideos(id) {
	Logger.log({
		level: 'info',
		name: 'MovieDB',
		color: 'blue',
		message: 'Searching videos',
	});

	let params = {
		params: {
			language: 'null',
		},
	};

	let { data } = await tmdbApiClient.get(`tv/${id}/videos`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	return data.results;
}

export async function tvImages(id) {
	Logger.log({
		level: 'info',
		name: 'MovieDB',
		color: 'blue',
		message: 'Searching images',
	});

	let params = {
		params: {
			language: 'null',
		},
	};

	let { data } = await tmdbApiClient.get(`tv/${id}/images`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	return data;
}

export default {
	tv,
	tvChanges,
	tvLatest,
	tvImages,
	tvNowPlaying,
	tvPopular,
	tvRecommendations,
	tvSimilar,
	tvTopRated,
	tvUpcomming,
	tvVideos,
};
