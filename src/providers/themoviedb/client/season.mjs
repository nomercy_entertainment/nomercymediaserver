import Logger from '../../../loaders/logger.mjs';
import moment from 'moment';
import tmdbApiClient from './tmdbApiClient.mjs';

export async function season(id, season) {
	let append = ['credits', 'external_ids', 'images', 'translations'];

	let params = {
		params: {
			append_to_response: append.join(','),
			include_image_language: 'en,null',
		},
	};

	let { data } = await tmdbApiClient.get(`tv/${id}/season/${season}`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	data.media_type = 'season';

	return data;
}

export async function seasonChanges(id, season, daysback = 1) {
	let params = {
		start_date: moment().subtract(daysback, 'days').format('md'),
		end_date: moment().format('md'),
	};

	let { data } = await tmdbApiClient.get(`tv/${id}/season/${season}/changes`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	data.media_type = 'tv';

	return data;
}

export async function seasons(id, seasons = []) {
	let data = [];

	let append = ['aggregate_credits', 'credits', 'external_ids', 'images', 'translations'];

	for (let i = 0; i < seasons.length; i++) {
		let params = {
			params: {
				append_to_response: append.join(','),
			},
		};
		let res = await tmdbApiClient.get(`tv/${id}/season/${seasons[i]}`, params).catch((err) => {
			Logger.log({
				level: 'error',
				name: 'MovieDB',
				color: 'red',
				message: err,

			});
		});

		if (res?.data) {
			data.push(res.data);
		}
	}

	return data;
}

export default {
	season,
	seasonChanges,
	seasons,
};
