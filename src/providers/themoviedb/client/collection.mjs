import Logger from '../../../loaders/logger.mjs';
import tmdbApiClient from './tmdbApiClient.mjs';

export async function collection(id) {
	let append = ['content_ratings', 'credits', 'external_ids', 'images', 'translations'];

	let params = {
		params: {
			append_to_response: append.join(','),
		},
	};

	let { data } = await tmdbApiClient.get(`collection/${id}`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
			
		});
	});

	return data;
}

export default collection;
