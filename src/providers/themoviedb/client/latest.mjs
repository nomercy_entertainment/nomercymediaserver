import Logger from '../../../loaders/logger.mjs';
import tmdbApiClient from './tmdbApiClient.mjs';

export async function getLatestMovie() {
	let { data } = await tmdbApiClient.get(`movie/latest`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	data.media_type = 'movie';

	return data;
}

export async function getLatestTv() {
	let { data } = await tmdbApiClient.get(`tv/latest`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	data.media_type = 'tv';

	return data;
}

export async function getLatest() {
	let movies = await getLatestMovie();
	let tvs = await getLatestTv();

	let data = movies.concat(tvs);

	return data;
}

export default {
	getLatestMovie,
	getLatestTv,
	getLatest,
};
