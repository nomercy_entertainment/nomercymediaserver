import Logger from '../../../loaders/logger.mjs';
import tmdbApiClient from './tmdbApiClient.mjs';

export async function watchProviderMovie() {
	let { data } = await tmdbApiClient.get(`watch/providers/movie`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	data.results.media_type = 'movie';

	return data.results;
}

export async function watchProviderRegions() {
	let { data } = await tmdbApiClient.get(`watch/providers/regions`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return data.results;
}

export async function watchProviderTv() {
	let { data } = await tmdbApiClient.get(`watch/providers/tv`).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return data.results;
}

export default {
	watchProviderMovie,
	watchProviderRegions,
	watchProviderTv,
};
