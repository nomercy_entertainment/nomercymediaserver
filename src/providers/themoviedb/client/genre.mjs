import Logger from '../../../loaders/logger.mjs';
import i18next from '../../../config/i18next.mjs';
import tmdbApiClient from './tmdbApiClient.mjs';
import { unique } from '../../../helpers/stringArray.mjs';

export async function genreMovie() {
	let params = {
		params: {
			language: i18next.language,
		},
	};

	let { data } = await tmdbApiClient.get(`genre/movie/list`, params).catch((err) => {
				Logger.log({
					level: 'error',
					name: 'MovieDB',
					color: 'red',
					message: err,
					
				});
			});

	return data.genres;
}

export async function genreTv() {
	let params = {
		params: {
			language: i18next.language,
		},
	};

	let { data } = await tmdbApiClient.get(`genre/tv/list`, params).catch((err) => {
				Logger.log({
					level: 'error',
					name: 'MovieDB',
					color: 'red',
					message: err,
					
				});
			});

	return data.genres;
}

export async function genres() {
	let movies = await genreMovie();
	let tvs = await genreTv();

	let data = unique(movies.concat(tvs), 'id');

	return data;
}

export default {
	genreMovie,
	genreTv,
	genres,
};
