import Logger from '../../../loaders/logger.mjs';
import moment from 'moment';
import tmdbApiClient from './tmdbApiClient.mjs';

export async function episode(id, season, episode) {
	let append = ['credits', 'external_ids', 'images', 'translations'];

	let params = {
		params: {
			append_to_response: append.join(','),
		},
	};

	let { data } = await tmdbApiClient.get(`tv/${id}/season/${season}/episode/${episode}`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
		});
	});

	return data;
}

export async function episodeChanges(id, season, episode, daysback = 1) {
	let params = {
		start_date: moment().subtract(daysback, 'days').format('md'),
		end_date: moment().format('md'),
	};

	let { data } = await tmdbApiClient.get(`tv/${id}/season/${season}/episode/${episode}/changes`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	data.media_type = 'tv';

	return data;
}

export async function episodes(id, season, episodes = []) {
	let data = [];

	let append = ['aggregate_credits', 'credits', 'external_ids', 'images', 'translations'];

	for (let i = 0; i < episodes.length; i++) {
		let params = {
			params: {
				append_to_response: append.join(','),
			},
		};

		try {
			let res = await tmdbApiClient.get(`tv/${id}/season/${season}/episode/${episodes[i]}`, params).catch((err) => {
				Logger.log({
					level: 'error',
					name: 'MovieDB',
					color: 'red',
					message: err,

				});
			});

			data.push(res?.data);

		} catch (error) {

		}
	}

	return data;
}

export default {
	episode,
	episodeChanges,
	episodes,
};
