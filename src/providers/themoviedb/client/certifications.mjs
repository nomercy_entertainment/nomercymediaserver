import Logger from '../../../loaders/logger.mjs';
import i18next from '../../../config/i18next.mjs';
import tmdbApiClient from './tmdbApiClient.mjs';
import { unique } from '../../../helpers/stringArray.mjs';

export async function certificationMovie() {
	let params = {
		params: {
			language: i18next.language,
		},
	};

	let { data } = await tmdbApiClient.get(`certification/movie/list`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	const arr = [];

	for (let [key, value] of Object.entries(data.certifications)) {
		value.map(r => {
			arr.push({
				...r,
				iso_3166_1: key,
			});
		});
	}

	return arr;
}

export async function certificationTv() {
	let params = {
		params: {
			language: i18next.language,
		},
	};

	let { data } = await tmdbApiClient.get(`certification/tv/list`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	const arr = [];

	for (let [key, value] of Object.entries(data.certifications)) {
		value.map(r => {
			arr.push({
				...r,
				iso_3166_1: key,
			});
		});
	}

	return arr;
}

export async function certifications() {
	let movies = await certificationMovie();
	let tvs = await certificationTv();

	let data = movies.concat(tvs);

	return data;
}

export default {
	certificationMovie,
	certificationTv,
	certifications,
};
