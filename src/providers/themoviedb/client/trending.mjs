import Logger from '../../../loaders/logger.mjs';
import tmdbApiClient from './tmdbApiClient.mjs';

export async function trending(type = 'all', window = 'day', limit = 1000) {
	let data = [];

	let base = await tmdbApiClient.get(`trending/${type}/${window}`, { page: 1 }).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	for (let j = 0; j < base.data.results.length; j++) {
		data.push(base.data.results[j]);
	}

	for (let i = 1; i < base.data.total_pages && i < limit; i++) {
		let res = await tmdbApiClient.get(`trending/${type}/${window}`, { page: i + 1 }).catch((err) => {
			Logger.log({
				level: 'error',
				name: 'MovieDB',
				color: 'red',
				message: err,

			});
		});

		for (let j = 0; j < res.data.results.length; j++) {
			data.push(res.data.results[j]);
		}
	}

	return data;
}

export default trending;
