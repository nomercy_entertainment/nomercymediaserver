import Logger from '../../../loaders/logger.mjs';
import tmdbApiClient from './tmdbApiClient.mjs';

export async function Person(id) {

	let append = ['details', 'movie_credits', 'tv_credits', 'external_ids', 'images', 'translations'];

	let params = {
		params: {
			append_to_response: append.join(','),
		},
	};

	let response = await tmdbApiClient.get(`person/${id}`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,
			
		});
	});

	return response?.data ?? null;
}

export default Person;
