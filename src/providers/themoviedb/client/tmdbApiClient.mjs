import Logger from '../../../loaders/logger.mjs';
import axios from 'axios';
import axiosRetry from 'axios-retry';

const tmdbApiClient = axios.create();
axiosRetry(tmdbApiClient, { 
	retries: 3 
});

tmdbApiClient.defaults.baseURL = 'https://api.themoviedb.org/3/';
tmdbApiClient.defaults.timeout = 10000;

tmdbApiClient.interceptors.request.use(
	async (conf) => {
		conf.params = {
			api_key: process.env.TMDB_API_KEY,
			// language: i18next.language || process.env.LANGUAGE,
			...conf.params,
		};
		conf.timeout = 10000;

		return new Promise((resolve) => {
			conf.headers['Accept'] = 'application/json';
			Logger.log({
				level: 'verbose',
				name: 'MovieDB',
				color: 'blue',
				message:
					conf.baseURL +
					conf.url +
					'?' +
					Object.keys(conf.params)
						.map((p) => {
							return p + '=' + conf.params[p];
						})
						.join('&'),
				
			});
			resolve(conf);
		});
	},
	function (error) {
		// console.log(error.response.message);
		return Promise.reject(error.response.message);
	}
);

export default tmdbApiClient;
