import { sortByMathPercentage } from '../../../helpers/stringArray.mjs';
import Logger from '../../../loaders/logger.mjs';
import tmdbApiClient from './tmdbApiClient.mjs';

export async function searchCollection(query) {
	let params = {
		params: {
			query: query.replace(/[\s\.]{1,}and[\s\.]{1,}/, '&').replace(/\s/g, '%20'),
			include_adult: config.allowAdult,
		},
	};

	let response = await tmdbApiClient.get(`search/collection`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	response.data.results.media_type = 'collection';

	return response.data.results || [];
}

export async function searchCompany(query) {
	let params = {
		params: {
			query: query.replace(/\s/g, '%20'),
			include_adult: config.allowAdult,
		},
	};

	let response = await tmdbApiClient.get(`search/company`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	response.data.results.media_type = 'company';

	return response.data.results || [];
}

export async function searchMovie(query, year = null) {
	query = query.replace(/[\s\.]{1,}and[\s\.]{1,}/, '&').replace(/[\s\.]/g, ' ');

	Logger.log({
		level: 'info',
		name: 'MovieDB',
		color: 'blue',
		message: 'Searching for: ' + query + ' ' + year,

	});

	let params = {
		params: {
			query: query,
			primary_release_year: year,
			include_adult: config.allowAdult,
		},
	};

	let response = await tmdbApiClient.get(`search/movie`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return sortByMathPercentage(response.data.results?.filter(
		d =>
			!d.title.includes('OVA')
			|| !d.title.includes('ova')
			|| !d.title.includes('Making of')
			|| !d.title.includes('making of')
	) || [], 'title', query);
}

export async function searchMulti(query, year = null) {
	Logger.log({
		level: 'info',
		name: 'MovieDB',
		color: 'blue',
		message: 'Searching for: ' + query.replace(/[\s\.]/g, ' ') + ' ' + year,

	});

	let params = {
		params: {
			query: query.replace(/[\s\.]/g, ' '),
			primary_release_year: year,
			include_adult: config.allowAdult,
		},
	};

	let response = await tmdbApiClient.get(`search/multi`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return response?.data.results || [];
}

export async function searchPerson(query) {
	Logger.log({
		level: 'info',
		name: 'MovieDB',
		color: 'blue',
		message: 'Searching for: ' + query.replace(/\./g, ' '),

	});

	let params = {
		params: {
			query: query.replace(/[\s\.]/g, ' '),
			include_adult: config.allowAdult,
		},
	};

	let response = await tmdbApiClient.get(`search/person`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	response.data.results.media_type = 'person';

	return response.data.results || [];
}

export async function searchTv(query, year = null) {
	query = query.replace(/[\s\.]*and[\s\.]*/, '&').replace(/[\s\.]/g, ' ');

	Logger.log({
		level: 'info',
		name: 'MovieDB',
		color: 'blue',
		message: 'Searching for: ' + query + ' ' + year,

	});

	let params = {
		params: {
			query,
			first_air_date_year: year,
		},
	};

	let response = await tmdbApiClient.get(`search/tv`, params).catch((err) => {
		Logger.log({
			level: 'error',
			name: 'MovieDB',
			color: 'red',
			message: err,

		});
	});

	return sortByMathPercentage(response.data.results?.filter(
		d =>
			!d.name.includes('OVA')
			|| !d.name.includes('ova')
	) || [], 'name', query);
}

export default {
	searchCollection,
	searchCompany,
	searchMovie,
	searchMulti,
	searchPerson,
	searchTv,
};
