import {certifications} from '../client/certifications.mjs';
import { prismaMedia } from '../../../config/database.mjs';

export default async function (id) {

	const certification = await certifications(id);

	const transaction = [];

	certification.map(async (cr) => {

		const certificationsInsert = {
			iso_3166_1: cr.iso_3166_1,
			rating: cr.certification,
			meaning: cr.meaning,
			order: cr.order,
		};

		transaction.push(prismaMedia.certification.upsert({
			where: { 
				certification_rating_iso_3166_1_unique: {
					iso_3166_1: cr.iso_3166_1,
					rating: cr.certification,
				},
			},
			update: certificationsInsert,
			create: certificationsInsert,
		}));
	});
	
	await prismaMedia.$transaction(transaction);
	
};
