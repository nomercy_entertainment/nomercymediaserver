import person from "./person.mjs";
import { prismaMedia } from "../../../config/database.mjs";
import { unique } from "../../../helpers/stringArray.mjs";

export default async function (req, transaction, castArray, persons) {

	for (const cast of unique((req?.aggregate_credits?.cast ?? req?.credits?.cast), 'credit_id')) {
		if(!cast.credit_id) continue;
		
		const castsInsert = {
			person_id: parseInt(cast.id),
			adult: cast.adult,
			character: cast.character,
			creditId: cast.credit_id,
			gender: cast.gender,
			known_for_department: cast.known_for_department,
			name: cast.name,
			order: cast.order,
			original_name: cast.original_name,
			popularity: cast.popularity,
			profile_path: cast.profile_path,
		};
		transaction.push(
			prismaMedia.cast.upsert({
				where: {
					creditId: cast.credit_id,
				},
				update: castsInsert,
				create: castsInsert,
			})
		);
		
		await person(cast, 'cast', transaction, persons);

		castArray.push({
			where: {
				creditId: cast.credit_id,
			},
			create: {
				creditId: cast.credit_id,
			},
		});
	}

}