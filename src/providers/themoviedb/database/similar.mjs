import { createTitleSort } from "../../../controllers/file/filenameParser.mjs";
import { prismaMedia } from "../../../config/database.mjs";
import { unique } from "../../../helpers/stringArray.mjs";

export default async function (req, transaction, table) {

	for (const similar of unique(req.similar.results, 'id')) {
		
		const similarInsert = {
			backdrop: similar.backdrop_path,
			media_id: similar.id,
			media_type: table,
			overview: similar.overview,
			poster: similar.poster_path,
			similarable_id: req.id,
			similarable_type: table,
			title: similar.name ?? similar.title,
			title_sort: createTitleSort(similar.name ?? similar.title),
		};

		transaction.push(
			prismaMedia.similar.upsert({
				where: {
					similar_unique: {
						media_id: similar.id,
						media_type: similar.media_type,
						similarable_id: req.id,
						similarable_type: table,
					},
				},
				update: similarInsert,
				create: similarInsert,
			})
		);
	}
}