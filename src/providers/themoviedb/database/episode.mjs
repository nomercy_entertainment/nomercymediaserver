import cast from './cast.mjs';
import crew from './crew.mjs';
import { episodes } from '../client/episode.mjs';
import guest_star from './guest_star.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import translation from './translation.mjs';

async function episode(id, s, transaction, persons) {
	const allEpisodes = await episodes(
		id,
		s.season_number,
		s.episodes.map((episode) => episode.episode_number)
	);

	for (const e of allEpisodes) {
		if (!e) continue;
		const guestStarInsert = [];
		const crewInsert = [];
		const castInsert = [];

		await guest_star(e, transaction, guestStarInsert, persons);
		await cast(e, transaction, castInsert, persons);
		await crew(e, transaction, crewInsert, persons);
		await translation(e, transaction, 'episode');

		if (e?.still_path != '' && episode.still_path != null) {
			const mediaInsert = {
				mediaable_id: e.id,
				mediaable_type: 'episode',
				src: e.still_path,
				type: 'still',
			};

			transaction.push(
				prismaMedia.media.upsert({
					where: {
						media_unique: {
							mediaable_id: e.id,
							mediaable_type: 'episode',
							src: e.still_path,
						},
					},
					update: mediaInsert,
					create: mediaInsert,
				})
			);
		}

		const episodesInsert = {
			air_date: e.air_date,
			episode_number: e.episode_number,
			id: parseInt(e.id),
			title: e.name,
			overview: e.overview,
			production_code: e.production_code,
			season_number: e.season_number,
			still: e.still_path,
			vote_average: e.vote_average,
			vote_count: e.vote_count,
			seasonId: parseInt(s.id),
			tvId: parseInt(id),
			imdbId: e.externalIds?.imdbId,
			tvdbId: e.externalIds?.tvdbId,

			episode_guest_star: {
				connectOrCreate: guestStarInsert,
			},
			cast_episode: {
				connectOrCreate: castInsert,
			},
			crew_episode: {
				connectOrCreate: crewInsert,
			},
		};

		transaction.push(
			prismaMedia.episode.upsert({
				where: {
					id: e.id,
				},
				update: episodesInsert,
				create: episodesInsert,
			})
		);
	}
}

export default episode;
