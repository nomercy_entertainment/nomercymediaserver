import { searchMovie, searchTv } from '../client/search.mjs';

import Logger from '../../../loaders/logger.mjs';
import { cleanFileName } from '../../../controllers/file/filenameParser.mjs';
import movie from '../database/movie.mjs';
import { movieChanges } from '../client/movie.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import tv from '../database/tv.mjs';
import { tvChanges } from '../client/tv.mjs';

export const addTv = async function (t, force = false) {
	let result = await searchTv(t.title, t.year);

	let data = result.filter((d) => !d.name.includes('OVA') || !d.name.includes('ova'));
	if (data.length > 0) {
		let first =
			data.find((d) => d.name.toLowerCase() == t.title.toLowerCase() || d.name.replace(':', '').replace('?', '').toLowerCase() == t.title.toLowerCase() || d.name.replace(':', '').replace('?', '').toLowerCase() == t.title.toLowerCase()) || data[0];

		let database = await prismaMedia.tv.findFirst({
			where: {
				id: first.id,
			},
		});

		let changes = await tvChanges(first.id);
		let updated = changes.length > 0;

		if (database && !force && !updated) {
			Logger.log({
				level: 'warn',
				name: 'App',
				color: 'magentaBright',
				message: 'Skipping: ' + t.title.replace(/[\s\.]/g),

			});
			return database.id;
		} else {
			if (data.length == 1) {
				await tv(data[0].id, t.folder);
				return data[0].id;
			} else if (data.length > 1) {
				await tv(first.id, t.folder);
				return first.id;
			} else {
				Logger.log({
					level: 'warn',
					name: 'App',
					color: 'red',
					message: data.map((d) => d.name),

				});
			}
		}
	} else {
		Logger.error('Nothing found for: ' + t.title);
		return;
	}
};

export const addMovie = async function (t, force = false) {
	let result = await searchMovie(t.title, t.year);

	let data = result.filter((d) => !d.title.includes('OVA') || !d.title.includes('ova') || !d.title.includes('Making of') || !d.title.includes('making of'));
	if (data.length > 0) {
		let first =
			data.find(
				(d) =>
					d.title.toLowerCase() == t.title.toLowerCase() ||
					cleanFileName(d.title.toLowerCase()) == cleanFileName(t.title.toLowerCase()) ||
					d.title.replace(':', '').replace('?', '').toLowerCase() == t.title.toLowerCase() ||
					d.title.replace(':', '').replace('?', '').toLowerCase() == t.title.toLowerCase()
			) || data[0];

		let database = await prismaMedia.movie.findFirst({
			where: {
				id: first.id,
			},
		});

		let changes = await movieChanges(first.id, 1);
		let updated = changes.length > 0;
		if (database && !force && !updated) {
			Logger.log({
				level: 'warn',
				name: 'App',
				color: 'magentaBright',
				message: 'Skipping: ' + t.title.replace(/[\s\.]/g, '+'),

			});
			return database.id;
		} else {
			if (data.length == 1) {
				await movie(data[0].id, t.folder);
				return data[0].id;
			} else if (data.length > 1) {
				await movie(first.id, t.folder);
				return first.id;
			} else {
				Logger.log({
					level: 'warn',
					name: 'App',
					color: 'red',
					message: data.map((d) => d.title),

				});
				return;
			}
		}
	} else {
		Logger.error('Nothing found for: ' + t.title);
		return;
	}
};

export default {
	addMovie,
	addTv,
};
