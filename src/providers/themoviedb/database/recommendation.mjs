import { createTitleSort } from "../../../controllers/file/filenameParser.mjs";
import { prismaMedia } from "../../../config/database.mjs";
import { unique } from "../../../helpers/stringArray.mjs";

export default async function (req, transaction, table) {

	for (const recommendation of unique(req.recommendations.results, 'id')) {
		
		const recommendationInsert = {
			backdrop: recommendation.backdrop_path,
			media_id: recommendation.id,
			media_type: recommendation.media_type,
			overview: recommendation.overview,
			poster: recommendation.poster_path,
			recommendationable_id: req.id,
			recommendationable_type: table,
			title: recommendation.name ?? recommendation.title,
			title_sort: createTitleSort(recommendation.name ?? recommendation.title),
		};

		transaction.push(
			prismaMedia.recommendation.upsert({
				where: {
					recommendation_unique: {
						media_id: recommendation.id,
						media_type: recommendation.media_type,
						recommendationable_id: req.id,
						recommendationable_type: table,
					},
				},
				update: recommendationInsert,
				create: recommendationInsert,
			})
		);
	}

}