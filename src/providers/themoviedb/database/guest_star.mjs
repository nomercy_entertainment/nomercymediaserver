import person from "./person.mjs";
import { prismaMedia } from "../../../config/database.mjs";
import { unique } from "../../../helpers/stringArray.mjs";

export default async function (req, transaction, guestStarArray, persons) {

	for (const guest_star of unique(req?.credits?.guest_stars, 'credit_id')) {
		const guestStarInsert = {
			person_id: parseInt(guest_star.id),
			adult: guest_star.adult,
			character: guest_star.character,
			creditId: guest_star.credit_id,
			gender: guest_star.gender,
			known_for_department: guest_star.known_for_department,
			name: guest_star.name,
			order: guest_star.order,
			original_name: guest_star.original_name,
			popularity: guest_star.popularity,
			episodeId: req.id,
			profile_path: guest_star.profile_path,
		};

		transaction.push(
			prismaMedia.guest_star.upsert({
				where: {
					creditId: guest_star.credit_id,
				},
				update: guestStarInsert,
				create: guestStarInsert,
			})
		);

		await person(guest_star, 'guest_star', transaction, persons);

		guestStarArray.push({
			where: {
				creditId: guest_star.credit_id,
			},
			create: {
				creditId: guest_star.credit_id,
			},
		});
	};
}