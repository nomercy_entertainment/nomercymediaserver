import { unique } from "../../../helpers/stringArray.mjs";

export default async function (req,  genresArray, table) {

	for (const genre of unique(req.genres, 'id')) {
		genresArray.push({
			where: {
				[`genre_${table}_unique`]: {
					genreId: parseInt(genre.id),
					[`${table}Id`]: parseInt(req.id),
				},
			},
			create: {
				genreId: parseInt(genre.id),
			},
		});
	}
}