import Logger from '../../../loaders/logger.mjs';
import alternative_title from './alternative_title.mjs';
import cast from './cast.mjs';
import collection from './collection.mjs';
import { createTitleSort } from '../../../controllers/file/filenameParser.mjs';
import crew from './crew.mjs';
import genre from './genre.mjs';
import image from './image.mjs';
import keyword from './keyword.mjs';
import { movie } from '../client/movie.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import recommendation from './recommendation.mjs';
import similar from './similar.mjs';
import translation from './translation.mjs';
import { unique } from '../../..//helpers/stringArray.mjs';

export default async function (id, folder) {
	const m = await movie(id);

	Logger.log({
		level: "info",
		name: "App",
		color: "magentaBright",
		message: 'Adding ' + m.title,

	});

	const alternativeTitlesInsert = [];
	const castInsert = [];
	const collectionInsert = [];
	const crewInsert = [];
	const genresInsert = [];
	const keywordsInsert = [];
	const transaction = [];
	const persons = [];

	if (m.belongs_to_collection) {
		await collection(m, transaction, collectionInsert);
	}

	await genre(m, genresInsert, 'movie');
	await alternative_title(m, transaction, alternativeTitlesInsert);
	await cast(m, transaction, castInsert, persons);
	await crew(m, transaction, crewInsert, persons);
	await keyword(m, transaction, keywordsInsert, 'movie');

	await image(m, transaction, 'backdrop', 'movie');
	await image(m, transaction, 'logo', 'movie');
	await image(m, transaction, 'poster', 'movie');

	await recommendation(m, transaction, 'movie');
	await similar(m, transaction, 'movie');
	await translation(m, transaction, 'movie');

	const movieInsert = {
		adult: m.adult,
		backdrop: m.backdrop_path,
		budget: m.budget,
		homepage: m.homepage,
		id: parseInt(m.id),
		imdb_id: m.external_ids?.imdb_id,
		tvdb_id: m.external_ids?.tvdb_id,
		original_language: m.original_language,
		original_title: m.original_title,
		overview: m.overview,
		popularity: m.popularity,
		poster: m.poster_path,
		release_date: m.release_date,
		revenue: m.revenue,
		runtime: m.runtime,
		status: m.status,
		tagline: m.tagline,
		title: m.title,
		title_sort: createTitleSort(m.title),
		video: m.video.toString(),
		vote_average: m.vote_average,
		vote_count: m.vote_count,
		folder: folder?.replace(/[\/\\]$/, '').replace(/\/{1,}/g, '/'),
		genre_movie: {
			connectOrCreate: genresInsert,
		},
		alternative_titles: {
			connectOrCreate: alternativeTitlesInsert,
		},
		cast_movie: {
			connectOrCreate: castInsert,
		},
		crew_movie: {
			connectOrCreate: crewInsert,
		},
		keyword_movie: {
			connectOrCreate: keywordsInsert,
		},
		collection_movie: {
			connectOrCreate: collectionInsert
		},
	};

	transaction.push(
		prismaMedia.movie.upsert({
			where: {
				id: parseInt(id),
			},
			create: movieInsert,
			update: movieInsert,
		})
	);


	for (const video of unique(m.videos.results, 'key')) {
		const mediaInsert = {
			iso_639_1: video.iso_639_1,
			mediaable_id: m.id,
			mediaable_type: 'movie',
			name: video.name,
			site: video.site,
			size: video.size,
			src: video.key,
			type: video.type,
		};

		transaction.push(
			prismaMedia.media.upsert({
				where: {
					media_unique: {
						mediaable_id: m.id,
						mediaable_type: 'movie',
						src: video.key,
					},
				},
				update: mediaInsert,
				create: mediaInsert,
			})
		);
	}

	for (const rating of unique(m.release_dates?.results, 'id')) {

		const cert = await prismaMedia.certification.findFirst({
			where: {
				iso_3166_1: rating.iso_3166_1,
				rating: rating.release_dates.certification,
			},
		});

		if (cert) {

			const movieRatingsInsert = {
				iso_3166_1: rating.iso_3166_1,
				movieId: parseInt(m.id),
				certificationId: cert.id,
			};

			transaction.push(
				prismaMedia.certification_movie.upsert({
					where: {
						certification_movie_unique: {
							iso_3166_1: rating.iso_3166_1,
							movieId: parseInt(m.id),
						}
					},
					create: movieRatingsInsert,
					update: movieRatingsInsert,
				})
			);
		}
	}

	// TODO: this
	// const productionCompaniesInsert = m.production_companies.map(p => {
	//   return {
	//     id: p.id,
	// 		logo: p.logo_path,
	// 		name: p.name,
	// 		origin_country: p.origin_country,
	//   }
	// });
	// transaction.push(prismaMedia.company.upsert({
	//   where: { id },
	//   update: productionCompaniesInsert,
	//   create: productionCompaniesInsert,
	// });)

	// const productionCountriesInsert = m.production_countries.map(p => {
	//   return {
	//     iso_3166_1: p.iso_3166_1,
	//   }
	// });

	// transaction.push(prismaMedia.productionCountries.upsert({
	//   where: { id },
	//   update: productionCountriesInsert,
	//   create: productionCountriesInsert,
	// }));

	// try {
	// 	await prismaMedia.$transaction(transaction);
	// } catch (error) {
	// 	try {
	// 		await prismaMedia.$transaction(transaction);
	// 	} catch (error) {
			// try {
				await prismaMedia.$transaction(transaction);
			// } catch (error) {

			// 	Logger.log({
			// 		level: 'error',
			// 		name: 'MovieDB',
			// 		color: 'red',
			// 		message: JSON.stringify(error),
			// 	});

			// 	return {
			// 		success: false,
			// 		message: 'Something went wrong adding ' + m.name,
			// 		data: m,
			// 	};
			// }
	// 	}
	// }

	return {
		success: true,
		message: 'Movie ' + m.title + ' added successfully',
		data: m,
	};
};
