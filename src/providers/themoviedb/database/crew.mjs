import person from "./person.mjs";
import { prismaMedia } from "../../../config/database.mjs";
import { unique } from "../../../helpers/stringArray.mjs";

export default async function (req, transaction, crewArray, persons) {

	for (const crew of unique((req?.aggregate_credits?.cast ?? req?.credits?.cast), 'credit_id')) {
		if(!crew.credit_id) continue;
		
		const crewsInsert = {
			person_id: parseInt(crew.id),
			adult: crew.adult,
			creditId: crew.credit_id,
			department: crew.department,
			gender: crew.gender,
			job: crew.job,
			known_for_department: crew.known_for_department,
			name: crew.name,
			original_name: crew.original_name,
			popularity: crew.popularity,
			profile_path: crew.profile_path,
		};

		transaction.push(
			prismaMedia.crew.upsert({
				where: {
					creditId: crew.credit_id,
				},
				update: crewsInsert,
				create: crewsInsert,
			})
		);

		await person(crew, 'crew', transaction, persons);

		crewArray.push({
			where: {
				creditId: crew.credit_id,
			},
			create: {
				creditId: crew.credit_id,
			},
		});
	}

}