import Logger from '../../../loaders/logger.mjs';
import Person from '../client/person.mjs';
import alternative_title from './alternative_title.mjs';
import axios from 'axios';
import cast from './cast.mjs';
import { createTitleSort } from '../../../controllers/file/filenameParser.mjs';
import crew from './crew.mjs';
import genre from './genre.mjs';
import image from './image.mjs';
import keyword from './keyword.mjs';
import person from './person.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import recommendation from './recommendation.mjs';
import season from '../database/season.mjs';
import similar from './similar.mjs';
import translation from './translation.mjs';
import { tv } from '../client/tv.mjs';
import { unique } from '../../../helpers/stringArray.mjs';

export default async function (id, folder) {
	const t = await tv(id);

	const transaction = [];

	const castInsert = [];
	const crewInsert = [];
	const createdByInsert = [];
	const keywordsInsert = [];
	const alternativeTitlesInsert = [];
	const genresInsert = [];
	const persons = [];
	let Type = t.media_type;

	Logger.log({
		level: 'info',
		name: 'App',
		color: 'magentaBright',
		message: 'Adding ' + t.name,

	});

	await genre(t, genresInsert, 'tv');
	await cast(t, transaction, castInsert, persons);
	await crew(t, transaction, crewInsert, persons);
	await alternative_title(t, alternativeTitlesInsert, 'tv');
	await keyword(t, transaction, keywordsInsert, 'tv');

	await image(t, transaction, 'backdrop', 'tv');
	await image(t, transaction, 'logo', 'tv');
	await image(t, transaction, 'poster', 'tv');

	await recommendation(t, transaction, 'tv');
	await similar(t, transaction, 'tv');
	await translation(t, transaction, 'tv');

	let duration = null;
	let Duration = Math.round(t.episode_run_time.reduce((ert, c) => ert + c, 0) / t.episode_run_time.length);

	if (Duration != 'NaN' && t.episode_run_time.length > 0) {
		duration = Duration;
	}

	if (folder) {
		const reg = /.*[\\\/](?<type>[\w\.]*)[\\\/](?<direction>[\w\.]*)[\\\/].*\(\d{4}\)$/;
		const regResult = reg.exec(folder);
		if (regResult?.groups) {
			Type = regResult.groups.type.toLowerCase().split('.')[0];
		}
	}

	for (const created_by of unique(t.created_by || [], 'credit_id')) {
		const createdInsert = {
			creditId: created_by.credit_id,
			person_id: parseInt(created_by.id),
			name: created_by.name,
			gender: created_by.gender,
			profile_path: created_by.profile_path,
		};

		transaction.push(
			prismaMedia.creator.upsert({
				where: {
					creditId: created_by.credit_id,
				},
				update: createdInsert,
				create: createdInsert,
			})
		);
		createdByInsert.push({
			where: {
				creditId: created_by.credit_id,
			},
			create: {
				creditId: created_by.credit_id,
			},
		});
	};

	
	const tvInsert = {
		backdrop: t.backdrop_path,
		budget: t.budget,
		duration: duration,
		first_air_date: t.first_air_date,
		homepage: t.homepage,
		id: parseInt(t.id),
		imdb_id: t.external_ids?.imdb_id,
		tvdb_id: t.external_ids?.tvdb_id,
		in_production: t.in_production,
		last_episode_to_air: t.last_episode_to_air?.id,
		media_type: Type,
		next_episode_to_air: t.next_episode_to_air?.id,
		number_of_episodes: t.number_of_episodes,
		number_of_seasons: t.number_of_seasons,
		origin_country: t.origin_country.join(', '),
		original_language: t.original_language,
		original_title: t.original_title,
		overview: t.overview,
		popularity: t.popularity,
		poster: t.poster_path,
		revenue: t.revenue,
		runtime: t.runtime,
		spoken_languages: t.spoken_languages.map((sl) => sl.iso_639_1).join(', '),
		status: t.status,
		tagline: t.tagline,
		title: t.name,
		title_sort: createTitleSort(t.name),
		type: t.type,
		vote_average: t.vote_average,
		vote_count: t.vote_count,
		folder: folder?.replace(/[\/\\]$/, '').replace(/\/{1,}/g, '/'),

		genre_tv: {
			connectOrCreate: genresInsert,
		},
		alternative_titles: {
			connectOrCreate: alternativeTitlesInsert,
		},
		cast_tv: {
			connectOrCreate: castInsert,
		},
		crew_tv: {
			connectOrCreate: crewInsert,
		},
		keyword_tv: {
			connectOrCreate: keywordsInsert,
		},
		creator_tv: {
			connectOrCreate: createdByInsert,
		},
		// content_ratings: {
		//   connectOrCreate: contentRatingsInsert
		// },
	};

	transaction.push(
		prismaMedia.tv.upsert({
			where: {
				id: parseInt(t.id),
			},
			update: tvInsert,
			create: tvInsert,
		})
	);
	await season(id, t, transaction, persons);

	for (const video of unique(t.videos.results, 'key')) {
		const mediaInsert = {
			iso_639_1: video.iso_639_1,
			mediaable_id: t.id,
			mediaable_type: 'tv',
			name: video.name,
			site: video.site,
			size: video.size,
			src: video.key,
			type: video.type,
		};

		transaction.push(
			prismaMedia.media.upsert({
				where: {
					media_unique: {
						mediaable_id: t.id,
						mediaable_type: 'tv',
						src: video.key,
					},
				},
				update: mediaInsert,
				create: mediaInsert,
			})
		);
	}
	for (const content_rating of t.content_ratings?.results || []) {

		const cert = await prismaMedia.certification.findFirst({
			where: {
				iso_3166_1: content_rating.iso_3166_1,
				rating: content_rating.rating,
			},
		});

		if (cert) {

			const tvRatingsInsert = {
				iso_3166_1: content_rating.iso_3166_1,
				tvId: parseInt(t.id),
				certificationId: cert.id,
			};

			transaction.push(
				prismaMedia.certification_tv.upsert({
					where: {
						certification_tv_unique: {
							iso_3166_1: content_rating.iso_3166_1,
							tvId: parseInt(t.id),
						}
					},
					create: tvRatingsInsert,
					update: tvRatingsInsert,
				})
			);
		}
	}

	try {
		await prismaMedia.$transaction(transaction);
	} catch (error) {
		try {
			await prismaMedia.$transaction(transaction);
		} catch (error) {
			try {
				await prismaMedia.$transaction(transaction);
			} catch (error) {

				Logger.log({
					level: 'error',
					name: 'MovieDB',
					color: 'red',
					message: JSON.stringify(error),
				});

				return {
					success: false,
					message: 'Something went wrong adding ' + t.name,
					data: t,
				};
			}
		}
	}

	return {
		success: true,
		message: 'Added ' + t.name + ' successfully',
		data: t,
	};
};
