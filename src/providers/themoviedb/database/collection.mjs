import collectionClient from '../client/collection.mjs';
import { createTitleSort } from '../../../controllers/file/filenameParser.mjs';
import { prismaMedia } from '../../../config/database.mjs';

async function collection(req, transaction, collectionMovieInsert) {
	const collection = await collectionClient(req.belongs_to_collection.id);

	const collectionInsert = {
		backdrop: collection.backdrop_path,
		id: parseInt(collection.id),
		overview: collection.overview,
		parts: collection.parts.length,
		poster: collection.poster_path,
		title: collection.name,
		title_sort: createTitleSort(collection.name),
	};
	transaction.push(
		prismaMedia.collection.upsert({
			where: {
				id: parseInt(collection.id),
			},
			update: collectionInsert,
			create: collectionInsert,
		})
	);

	collection.parts.map(async (p) => {
		const genresCollectionInsert = p.genre_ids.map((g) => {
			return {
				where: {
					genre_movie_unique: {
						genreId: g,
						movieId: parseInt(p.id),
					},
				},
				create: {
					genreId: g,
				},
			};
		});

		const movieCollectionInsert = {
			adult: p.adult,
			backdrop: p.backdrop_path,
			id: parseInt(p.id),
			original_language: p.original_language,
			original_title: p.original_title,
			overview: p.overview,
			popularity: p.popularity,
			poster: p.poster_path,
			release_date: p.release_date,
			tagline: p.tagline,
			title: p.title,
			title_sort: createTitleSort(p.title),
			video: p.video.toString(),
			vote_average: p.vote_average,
			vote_count: p.vote_count,

			genre_movie: {
				connectOrCreate: genresCollectionInsert,
			},
		};

		transaction.push(
			prismaMedia.movie.upsert({
				where: {
					id: parseInt(p.id),
				},
				create: movieCollectionInsert,
				update: movieCollectionInsert,
			})
		);
	});

	collection.translations.translations.map(async (tr) => {
		const collectionTranslationsInsert = {
			english_name: tr.english_name,
			homepage: tr.homepage,
			iso_3166_1: tr.iso_3166_1,
			iso_639_1: tr.iso_639_1,
			name: tr.name,
			overview: tr.data && tr.data?.overview ? tr.data?.overview : null,
			title: tr.data && tr.data?.title ? tr.data?.title : null,
			translationable_id: collection.id,
			translationable_type: 'collection',
		};

		transaction.push(
			prismaMedia.translation.upsert({
				where: {
					translation_unique: {
						translationable_id: collection.id,
						translationable_type: 'collection',
						iso_639_1: tr.iso_639_1,
					},
				},
				update: collectionTranslationsInsert,
				create: collectionTranslationsInsert,
			})
		);
	});

	collectionMovieInsert.push({
		where: {
			collectionId_movieId: {
				collectionId: parseInt(collection.id),
				movieId: parseInt(req.id),
			},
		},
		create: {
			collectionId: parseInt(collection.id),
		},
	});

}

export default collection;
