import { unique } from "../../../helpers/stringArray.mjs";

export default async function (req, alternative_titleArray, table) {

	for (const alternative_title of unique(req.alternative_titles?.results, 'id')) {
		alternative_titleArray.push({
			where: {
				[`alternative_titles_${table}_unique`]: {
					[`${table}Id`]: parseInt(req.id),
					iso_3166_1: alternative_title.iso_3166_1,
				},
			},
			create: {
				iso_3166_1: alternative_title.iso_3166_1,
				title: alternative_title.title,
			},
		});
	}

}