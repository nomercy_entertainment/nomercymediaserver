import { prismaMedia } from "../../../config/database.mjs";
import { unique } from "../../../helpers/stringArray.mjs";

export default async function (req, transaction, keywordArray, type) {

	for (const keyword of unique(req.keywords?.keywords, 'id')) {
		const keywordInsert = {
			keyword_id: req.id,
			name: keyword.name,
		};

		transaction.push(
			prismaMedia.keyword.upsert({
				where: {
					keyword_id: req.id,
				},
				update: keywordInsert,
				create: keywordInsert,
			})
		);

		keywordArray.push({
			where: {
				[`keyword_${type}_unique`]: {
					[`${type}Id`]: parseInt(req.id),
					keyword_id: req.id,
				},
			},
			create: {
				keyword_id: req.id,
			},
		});
	};

}