import Person from '../client/person.mjs';
import { dateAgo } from '../../../helpers/dateTime.mjs';
import { prismaMedia } from '../../../config/database.mjs';

export default async function (req, type, transaction, persons) {

	let person = await prismaMedia.person.findFirst({
		where: {
			id: parseInt(req.id),
		}
	});

	if (person?.id) {
		person.also_known_as = JSON.parse(person.also_known_as);
	}

	if ((!person || dateAgo(person.updated_at, 'days') > 30) && (!person || !persons.some(person => person.id == req.id))) {
		person = await Person(req.id);
		persons.push(person);
	}
	
	if (!person?.id && persons.some(person => person.id == req.id)) {
		person = persons.find(person => person.id == req.id)
	}

	const personInsert = {
		adult: person.adult,
		also_known_as: JSON.stringify(person.also_known_as),
		biography: person.biography,
		birthday: person.birthday,
		deathday: person.deathday,
		gender: person.gender,
		homepage: person.homepage,
		imdb_id: person.imdb_id,
		known_for_department: person.known_for_department,
		name: person.name,
		id: parseInt(person.id),
		place_of_birth: person.place_of_birth,
		popularity: person.popularity,
		profile_path: person.profile_path,
		cast_person: {
			connectOrCreate: {
				where: {
					creditId: req.credit_id,
				},
				create: {
					creditId: req.credit_id,
				},
			},
		},
		crew_person: {
			connectOrCreate: {
				where: {
					creditId: req.credit_id,
				},
				create: {
					creditId: req.credit_id,
				},
			},
		},
		guest_star_person: {
			connectOrCreate: {
				where: {
					creditId: req.credit_id,
				},
				create: {
					creditId: req.credit_id,
				},
			},
		},
	};

	if (type == 'crew') {
		delete personInsert.cast_person;
		delete personInsert.guest_star_person;
	}
	else if (type == 'cast') {
		delete personInsert.crew_person;
		delete personInsert.guest_star_person;
	}
	else if (type == 'guest_star') {
		delete personInsert.cast_person;
		delete personInsert.crew_person;
	}

	transaction.push(
		prismaMedia.person.upsert({
			where: {
				id: parseInt(person.id),
			},
			update: personInsert,
			create: personInsert,
		})
	);

};
