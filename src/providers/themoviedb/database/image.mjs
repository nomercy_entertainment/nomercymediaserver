import axios from "axios";
import { prismaMedia } from "../../../config/database.mjs";
import { unique } from "../../../helpers/stringArray.mjs";

export default async function (req, transaction, type, table) {

	for (const image of unique(req.images[type + 's'], 'file_path')) {
		if ((await axios.head('https://image.tmdb.org/t/p/w92' + image.file_path, { validateStatus: false })).status == 200) {
			const mediaInsert = {
				aspect_ratio: image.aspect_ratio,
				height: image.height,
				iso_639_1: image.iso_639_1,
				mediaable_id: req.id,
				mediaable_type: table,
				src: image.file_path,
				type: type,
				vote_average: image.vote_average,
				vote_count: image.vote_count,
				width: image.width,
			};

			transaction.push(
				prismaMedia.media.upsert({
					where: {
						media_unique: {
							mediaable_id: req.id,
							mediaable_type: table,
							src: image.file_path,
						},
					},
					update: mediaInsert,
					create: mediaInsert,
				})
			);
		}
	};
}