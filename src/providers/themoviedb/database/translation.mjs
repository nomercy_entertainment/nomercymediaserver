import person from "./person.mjs";
import { prismaMedia } from "../../../config/database.mjs";
import { unique } from "../../../helpers/stringArray.mjs";

export default async function (req, transaction, table) {

	for (const translation of unique(req.translations?.results, 'id')) {
		
		const movieTranslationsInsert = {
			english_name: translation.english_name,
			homepage: translation.homepage,
			iso_3166_1: translation.iso_3166_1,
			iso_639_1: translation.iso_639_1,
			name: translation.name,
			overview: translation.data?.overview,
			title: translation.data?.title || translation.data?.name,
			translationable_id: req.id,
			translationable_type: table,
		};

		transaction.push(
			prismaMedia.translation.upsert({
				where: {
					translation_unique: {
						translationable_id: req.id,
						translationable_type: table,
						iso_639_1: translation.iso_639_1,
					},
				},
				update: movieTranslationsInsert,
				create: movieTranslationsInsert,
			})
		);
	}
}