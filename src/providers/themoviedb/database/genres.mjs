import { genres } from '../client/genre.mjs';
import { prismaMedia } from '../../../config/database.mjs';

export default async function (id) {

	const genre = await genres(id);

	genre.map(async (g) => {
		const genreInsert = {
			id: g.id,
			name: g.name,
		};

		await prismaMedia.movie_genre.upsert({
			where: { 
				id: g.id 
			},
			update: genreInsert,
			create: genreInsert,
		});
	});
};
