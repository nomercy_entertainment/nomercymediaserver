import axios from 'axios';
import cast from './cast.mjs';
import crew from './crew.mjs';
import episode from './episode.mjs';
import image from './image.mjs';
import person from './person.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import { seasons } from '../client/season.mjs';
import translation from './translation.mjs';
import { unique } from '../../../helpers/stringArray.mjs';

async function season(id, t, transaction, persons) {

	const allSeasons = await seasons(
		id,
		t.seasons.map((season) => season.season_number)
	);

	for (let i = 0; i < allSeasons.length; i++) {
		const s = allSeasons[i];

		const castInsert = [];
		const crewInsert = [];

		await cast(s, transaction, castInsert, []);
		await crew(s, transaction, crewInsert, []);
		await translation(s, transaction, 'season');

		await image(s, transaction, 'backdrop', 'season');
		await image(s, transaction, 'poster', 'season');

		const seasonsInsert = {
			air_date: s.air_date,
			episode_count: t.seasons[i].episode_count,
			id: parseInt(s.id),
			overview: s.overview,
			poster: s.poster_path,
			season_number: s.season_number,
			title: s.name,
			tvId: parseInt(id),

			cast_season: {
				connectOrCreate: castInsert,
			},
			crew_season: {
				connectOrCreate: crewInsert,
			},
		};

		transaction.push(
			prismaMedia.season.upsert({
				where: {
					id: parseInt(s.id),
				},
				update: seasonsInsert,
				create: seasonsInsert,
			})
		);
		
		await episode(id, s, transaction, []);
	}
}

export default season;
