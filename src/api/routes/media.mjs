import Continue from '../media/continue.mjs';
import Logger from '../../loaders/logger.mjs';
import anime from '../media/anime.mjs';
import collection from '../media/collection.mjs';
import collections from '../media/collections.mjs';
import express from 'express';
import { fileURLToPath } from 'url';
import fonts from '../media/fonts.mjs';
import forkit from 'node-forkit';
import home from '../media/home.mjs';
import index from '../media/index.mjs';
import movie from '../media/movie/index.mjs';
import movieInfo from '../media/movie/info.mjs';
import moviePlaylist from '../media/movie/playlist.mjs';
import path from 'path';
import { prismaMedia } from '../../config/database.mjs';
import requestCountry from 'request-country';
import search from '../media/search.mjs';
import select from '../media/select.mjs';
import special from '../media/special/get.mjs';
import specialCreate from '../media/special/create.mjs';
import specialParse from '../media/special/parse.mjs';
import specialPaylist from '../media/special/playlist.mjs';
import specials from '../media/special/index.mjs';
import { tokenParser } from '../../helpers/index.mjs';
import tv from '../media/tv/index.mjs';
import tvInfo from '../media/tv/info.mjs';
import tvMissing from '../media/tv/missing.mjs';
import tvPlaylist from '../media/tv/playlist.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const router = express.Router();

router.post('/', index);
router.post('/home', home);
router.post('/anime', anime);
router.post('/select', select);

router.post('/movies', movie);
router.post('/movie/:id/info', movieInfo);
router.post('/movie/:id/playlist', async (req, res) => {
	const user_id = tokenParser(req)?.sub;
	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
	const country = requestCountry(req, 'US');
	let data = await moviePlaylist({ id: req.params.id, servers: req.body.servers, user_id, language, country, server_id: config.server_id }).catch((err) => {
			Logger.log({
				level: 'error',
				name: 'App',
				color: 'red',
				message: err,
			});
		});
	return res.json(data.filter((s) => s.id));
});

router.post('/tv', tv);
router.post('/tvs', tv);
router.post('/tv/:id/info', tvInfo);
router.post('/tv/:id/missing', tvMissing);
router.post('/tv/:id/playlist', async (req, res) => {
	const user_id = tokenParser(req)?.sub;
	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
	const country = requestCountry(req, 'US');
	let data = await tvPlaylist({ id: req.params.id, servers: req.body.servers, user_id, language, country, server_id: config.server_id }).catch((err) => {
			Logger.log({
				level: 'error',
				name: 'App',
				color: 'red',
				message: err,
			});
		});
	return res.json(data.filter((s) => s.id));
});

router.post('/collections', collections);
router.post('/collection/:id', collection);

router.post('/continue_watching', Continue);

router.get('/fonts', fonts);
router.get('/search', search);

router.post('/specials', specials);
router.post('/special', specialCreate);
router.post('/special/parse', specialParse);
router.post('/special/:id', special);
router.post('/special/:id/playlist', async (req, res) => {
	const user_id = tokenParser(req)?.sub;
	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
	const country = requestCountry(req, 'US');
	let data = await specialPaylist({ id: req.params.id, servers: req.body.servers, user_id, language, country, server_id: config.server_id }).catch((err) => {
			Logger.log({
				level: 'error',
				name: 'App',
				color: 'red',
				message: err,
			});
		});
	return res.json(data);
});


router.post('/playlist', async (req, res) => {
	
  const user_id = tokenParser(req)?.sub;

  const progress = await prismaMedia.video_progress.findMany({
    where: {
      profile_id: user_id, 
    },
    orderBy: {
      updated_at: 'desc',
    },
  });

	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
	const country = requestCountry(req, 'US');
	let data;

	if(progress[0].type == 'tv'){
		data = await tvPlaylist({ id: progress[0].tmdb_id, servers: req.body.servers, user_id, language, country, server_id: config.server_id }).catch((err) => {
			Logger.log({
				level: 'error',
				name: 'App',
				color: 'red',
				message: err,
			});
		});
	}
	else if(progress[0].type == 'movie'){ 
		data = await moviePlaylist({ id: progress[0].tmdb_id, servers: req.body.servers, user_id, language, country, server_id: config.server_id }).catch((err) => {
			Logger.log({
				level: 'error',
				name: 'App',
				color: 'red',
				message: err,
			});
		});
	}
	else{ 
		data = await specialPaylist({ id: progress[0].tmdb_id, servers: req.body.servers, user_id, language, country, server_id: config.server_id }).catch((err) => {
			Logger.log({
				level: 'error',
				name: 'App',
				color: 'red',
				message: err,
			});
		});
	}

	return res.json(data);
});

export default router;
