import add from '../dashboard/add.mjs';
import addMusic from '../dashboard/addMusic.mjs';
import cpu from '../dashboard/cpu.mjs';
import directorytree from '../dashboard/directorytree.mjs';
import encodeFolder from '../dashboard/encodeFolder.mjs';
import express from 'express';
import filelist from '../dashboard/filelist.mjs';
import folderlist from '../dashboard/folderlist.mjs';
import getConfiguration from '../dashboard/getConfiguration.mjs';
import getrippedfiles from '../dashboard/getrippedfiles.mjs';
import info from '../dashboard/info.mjs';
import messages from '../dashboard/messages.mjs';
import notifications from '../dashboard/notifications.mjs';
import processFiles from '../dashboard/processFiles.mjs';
import queue from '../dashboard/queue.mjs';
import rename from '../dashboard/rename.mjs';
import restart from '../../helpers/restart.mjs';
import saveConfiguration from '../dashboard/saveConfiguration.mjs';
import updateShow from '../dashboard/updateShow.mjs';
import { prismaMedia } from '../../config/database.mjs';

const router = express.Router();

router.get('/info', info);
router.get('/monitor/queue', queue);
router.get('/monitor/cpu/usage', cpu);

router.get('/manage/folderlist', folderlist);
router.get('/manage/filelist', filelist);
router.get('/manage/processFiles', processFiles);
router.get('/manage/updateShow', updateShow);

router.get('/manage/directorytree', directorytree);
router.get('/manage/encodeFolder', encodeFolder);
router.get('/manage/getrippedfiles', getrippedfiles);

router.post('/manage/rename', rename);
router.post('/manage/add', add);
router.get('/manage/getconfiguration', getConfiguration);
router.post('/manage/saveconfiguration', saveConfiguration);
router.get('/manage/restart', (req, res) => {
	res.json({
		success: true,
		message: 'restarting server...',
	});
	restart();
});

router.get('/user/messages', messages);
router.post('/user/messages', messages);
router.put('/user/messages/:id', messages);
router.patch('/user/messages/:id', messages);
router.delete('/user/messages/:id', messages);

router.get('/user/notifications', notifications);
router.post('/user/notifications', notifications);
router.put('/user/notifications/:id', notifications);
router.patch('/user/notifications/:id', notifications);
router.delete('/user/notifications/:id', notifications);

router.post('/manage/add-music', async (req, res) => {

	const resetTable = req.body.resetTable;
	// const resetTable = true;

	if (resetTable) {
		
		await prismaMedia.artist.deleteMany();
		await prismaMedia.$executeRaw`DELETE FROM sqlite_sequence WHERE name="artist";`;

		await prismaMedia.track.deleteMany();
		await prismaMedia.$executeRaw`DELETE FROM sqlite_sequence WHERE name="track";`;

		await prismaMedia.album.deleteMany();
		await prismaMedia.$executeRaw`DELETE FROM sqlite_sequence WHERE name="album";`;

		// await prismaMedia.playlist.deleteMany();
		// await prismaMedia.$executeRaw`DELETE FROM sqlite_sequence WHERE name="playlist";`;

		// await prismaMedia.playlistTrack.deleteMany();
		// await prismaMedia.$executeRaw`DELETE FROM sqlite_sequence WHERE name="playlistTrack";`;
	}

	const result = await addMusic();
	return res.json(result);
});

export default router;
