import configuration from '../../config/index.mjs';
import { isOwner } from '../middlewares/permissions.mjs';
import { prismaMedia } from '../../config/database.mjs';
import queueLoader from '../../loaders/queue.mjs';
import restart from '../../helpers/restart.mjs';
import { tokenParser } from '../../helpers/index.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	var token = req.kauth.grant.access_token;

	if (!isOwner(token)){
		return res.status(403).json({
			success: false,
			message: 'You can not modify the configuration',
		});
	}

	let transaction = [];
	let needRestart = false;
	let updateQueue = false;
	let enableQueue = true;

	for (let [key, value] of Object.entries(req.body)) {

		if(key == 'owner') {
			value = value?.sub_id || value;
		}
		
		transaction.push(
			prismaMedia.configuration.upsert({
				where: {
					key: key.toString(),
				},
				update: {
					key: key.toString(),
					value: JSON.stringify(value)?.replace(/^"|"$/g, ''),
					modified_by: tokenParser(req).sub,
				},
				create: {
					key: key.toString(),
					value: JSON.stringify(value)?.replace(/^"|"$/g, ''),
					modified_by: tokenParser(req).sub,
				},
			})
		);
		if (key == 'folderRoots') {
			needRestart = true;
		}
		if (key == 'Queue') {
			updateQueue = true;
			enableQueue = value;
		}
		if (key == 'workers') {
			updateQueue = true;
			enableQueue = config.queue;
		}
	}

	if (needRestart) {
		console.log('Restarting...')
		// restart();
	}

	let result
	
	try {
		result = await prismaMedia.$transaction(transaction);
	} catch (error) {
		try {
			retult = await prismaMedia.$transaction(transaction);
		} catch (error) {
			try {
				result = await prismaMedia.$transaction(transaction);
			} catch (error) {
				//
			}
		}
	}
	
	await configuration();

	if(updateQueue) {
		
		if(config.workers == 0 || !enableQueue){
			for (let i = 0; i < config.workers; i++) {
				queue[i].kill('SIGINT');
			}
		}
		else {
			await queueLoader();
		}
	}

	return res.json(result);
};
