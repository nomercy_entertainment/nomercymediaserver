import appApiClient from '../../loaders/axios.mjs';
import { prismaMedia } from '../../config/database.mjs';
import { sendMessageTo } from '../../loaders/socket.mjs';
import { tokenParser } from '../../helpers/index.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async (req, res) => {
	let body;
	let id = req.params.id;

	const Message = prismaMedia.messages;

	if (req.method == 'GET') {
		let messages = await Message.findMany({
			where: {
				type: 'message',
				to: {
					in: [tokenParser(req).sub, tokenParser(req).email, '*'],
				},
			},
		});

		body = messages.map((b) => {
			return {
				...b,
				created_at: Math.floor(new Date(b.created_at).getTime()),
				updated_at: Math.floor(new Date(b.updated_at).getTime()),
				method: 'add',
			};
		});
	} else if (req.method == 'POST') {
		let response = await Message.create({
			data: {
				notify: req.body.notify || true,
				read: false,
				from: req.body.from,
				to: req.body.to,
				image: req.body.image,
				title: req.body.title,
				body: req.body.body,
				type: 'message',
			},
		});

		response.created_at = Math.floor(new Date(response.created_at).getTime());
		response.updated_at = Math.floor(new Date(response.updated_at).getTime());
		(response.method = 'add'), sendMessageTo(response.to, response);

		body = response;
	} else if (req.method == 'PUT') {
		let message = await Message.findFirst({
			where: {
				id: parseInt(id),
			},
		});

		if (!message) {
			return res.status(400).json({
				success: false,
				message: 'This resource does not exist.',
			});
		}

		let response = await Message.update({
			where: {
				id: parseInt(id),
			},
			update: {
				notify: req.body.notify || true,
				read: false,
				from: req.body.from,
				image: req.body.image,
				title: req.body.title,
				body: req.body.body,
				type: 'message',
			},
		});

		response.created_at = Math.floor(new Date(response.created_at).getTime());
		response.updated_at = Math.floor(new Date(response.updated_at).getTime());

		(response.method = 'update'), sendMessageTo(response.to, response);

		body = response;
	} else if (req.method == 'PATCH') {
		let message = await Message.findFirst({
			where: {
				id: parseInt(id),
			},
		});

		if (!message) {
			return res.status(400).json({
				success: false,
				message: 'This resource does not exist.',
			});
		}

		let response = await Message.update({
			where: {
				id: parseInt(id),
			},
			data: {
				...message,
				...req.body,
				type: 'message',
			},
		});

		response.created_at = Math.floor(new Date(response.created_at).getTime());
		response.updated_at = Math.floor(new Date(response.updated_at).getTime());

		(response.method = 'update'), sendMessageTo(response.to, response);

		body = response;
	} else if (req.method == 'DELETE') {
		let message = await Message.findFirst({
			where: {
				id: parseInt(id),
			},
		});

		if (!message) {
			return res.status(400).json({
				success: false,
				message: 'This resource does not exist.',
			});
		}

		let response = await Message.delete({
			where: {
				id: parseInt(id),
			},
		});

		response.method = 'delete';

		sendMessageTo(response.to, response);

		body = response;
	}

	return res.json(body);
};
