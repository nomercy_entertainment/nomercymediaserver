import appApiClient from '../../loaders/axios.mjs';
import { get_ip } from '../../loaders/networking.mjs';
import { prismaMedia } from '../../config/database.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const internal_ip = get_ip();

	const config = await prismaMedia.configuration.findMany({
		orderBy: {
			key: 'asc',
		},
	});

	let configuration = {};

	config.map((c) => {
		try {
			return (configuration[c.key] = JSON.parse(c.value));
		} catch (error) {
			return (configuration[c.key] = c.value);
		}
	});
	
	const response = await appApiClient.get(`https://api.nomercy.tv/getUserDetails?users=${configuration.allowedUsers.concat(config.find(c => c.key == 'owner').value).join(',')}&internal_ip=${internal_ip}`).catch((e) => console.warn(e));

	configuration.owner = response?.data?.find((u) => u.sub_id == configuration.owner) ?? [];
	
	configuration.allowedUsers = configuration.allowedUsers.map((b) => {
		let meta = data?.find((u) => u.sub_id == b);
		return meta;
	});

	return res.json(configuration);
};
