import { findEpisodeFiles, findMovieFiles, getFileList } from '../../controllers/file/fileController.mjs';

import { createFFmpegCommand } from '../../controllers/encoder/ffmpeg/ffmpeg.mjs';
import { sendTo } from '../../loaders/socket.mjs';
import { sort_by } from '../../helpers/stringArray.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	let folder = req.query.folder;

	let results = [];
	let message;

	let list = (await getFileList(folder, ['.mp4', '.mkv', '.avi', '.ogv', '.NoMercy.m3u8'])).filter(f => !f.includes('original'));

	for (let i = 0; i < list.length; i++) {
		results.push(await createFFmpegCommand(list[i]));
	}

	results = sort_by(results, 'fileName');

	let data = results
		.filter((f) => f.ffmpegCommands != null)
		.map((d) => {
			return {
				ffmpegCommands: d.ffmpegCommands,
				'x-videoMap': d.videoMap.join(' '),
				'y-audioMap': d.audioMap.join(' '),
				'z-subtitleMap': d.subtitleMap.join(' '),
				'z-thumbMap': d.thumbMap.join(' '),
				outputFileName: d.OutputFileName,
				duration: d.duration,
				hdr: d.hdr,
				_file: d.fileIn,
				outputFolder: d.outputFolder,
				fileName: d.fileName,
				_type: d.type,
				dbType: d.dbtype,
				_Id: d.dbindex,
			};
		});
		
	if(data[0] && !folder.includes('Films') && !folder.includes('Movies')){
		let t = { folder: data[0].outputFolder.replace(/\/[\w\d\s\.'"’(),_\[\]@#$!&+-]+\/$/, '')};
		await findEpisodeFiles(data[0].dbindex, t);
	}
	else if(data[0]) {
		let t = { folder: data[0].outputFolder};
		await findMovieFiles(data[0].dbindex, t);
	}

	if (data.length == 0 && results.length > 0) {
		message = 'All files are processed';
		
	}
	if (typeof socket !== 'undefined') {
		sendTo(config.owner, 'update_content');
	}

	return res.json({
		status: 'ok',
		message: message,
		data: data,
	});
};
