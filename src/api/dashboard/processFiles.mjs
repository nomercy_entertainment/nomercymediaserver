import Logger from '../../loaders/logger.mjs';
import addMusic from './addMusic.mjs';
import { fileURLToPath } from 'url';
import {fork} from 'child_process';
import path from 'path';
import { prismaMedia } from '../../config/database.mjs';
import { sendTo } from '../../loaders/socket.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const child = fork(__dirname + '/../../controllers/forks/processAllFiles.mjs',  {
		windowsHide: true,		
	});
	
	let folder = req.query.folder?.replace(/\\/g, '/');
	const force = req.query.force || false;
	const id = parseInt(req.query.id);
	const type = req.query.type;
	const reversed = req.query.reversed;

	if (!folder && id && type) {
		if (type === 'tv' || type === 'anime') {
			let tv = await prismaMedia.tv.findFirst({
				where: {
					id: id,
				},
			});

			folder = tv?.folder;
		} else if (type === 'movie') {
			let movie = await prismaMedia.movie.findFirst({
				where: {
					id: id,
				},
			});

			folder = movie.folder;
		} else {
			folder = null;
		}
	}

	Logger.info(force + ' ' + folder);

	child.once('message', async (msg) => {		
		child.send({force,folder, reversed});
		
		child.on('message', (msg) => {
			sendTo(config.owner, 'update_content');
		});
	});

	return res.json({
		success: true,
		message: 'Updated successfully'
	});
};
