import { messages } from '../data.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const body = messages;

	return res.json(body);
};
