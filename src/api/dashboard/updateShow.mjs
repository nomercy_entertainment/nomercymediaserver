import { fork } from 'child_process';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const folder = req.query.folder?.replace(/\\/g, '/');
	const force = req.query.force || false;

	const child = fork('./src/controllers/forks/processAllFiles.mjs', {
	});
	child.send({ force, folder });

	child.on('message', (data) => {
		return res.json({
			message: 'ok',
			data: data,
		});
	});
};
