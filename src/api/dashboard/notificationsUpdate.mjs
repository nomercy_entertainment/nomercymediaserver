import notifications from './notifications.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const body = notifications;

	return res.json(body);
};
