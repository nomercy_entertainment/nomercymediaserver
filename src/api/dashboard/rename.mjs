import Logger from '../../loaders/logger.mjs';
import { fileURLToPath } from 'url';
import forkit from 'node-forkit';
import fs from 'fs';
import fsextra from 'fs-extra';
import path from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	req.body.map(async (item, index) => {
		if (!fs.existsSync(item.baseFolder.to)) {
			Logger.log({
				level: 'info',
				name: 'App',
				color: 'magentaBright',
				message: 'Moving: ' + item.baseFolder.from + ' to: ' + item.baseFolder.to,
				
			});
			fsextra.moveSync(item.baseFolder.from, item.baseFolder.to, { recursive: true });

			item.files.forEach((f) => {
				if (fs.existsSync(f.from) && !fs.existsSync(f.to)) {
					fsextra.moveSync(f.from, f.to);
				}
			});
		} else {
			Logger.log({
				level: 'info',
				name: 'App',
				color: 'magentaBright',
				message: item.baseFolder.from + ' already moved.',
				
			});
		}

		if (index == req.body.length - 1) {
			const data = await forkit(__dirname + '/../../controllers/forks/processAllFiles.mjs', {
				force: true,
				folder: item.baseFolder.to,
			});

				return res.json({
					success: true,
					message: 'Updated successfully',
					data: data,
				});
		}
	});

	return res.json({
		status: 'ok',
	});
};
