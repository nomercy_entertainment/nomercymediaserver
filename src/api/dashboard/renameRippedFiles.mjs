import Logger from '../../loaders/logger.mjs';
import fs from 'fs';
import fsextra from 'fs-extra';

// import { processAllFiles } from '../../controllers/file';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	req.body.map((item, index) => {
		if (!fs.existsSync(item.baseFolder.to)) {
			Logger.log({
				level: 'info',
				name: 'App',
				color: 'magentaBright',
				message: 'Moving: ' + item.baseFolder.from + ' to: ' + item.baseFolder.to,
				
			});
			fsextra.moveSync(item.baseFolder.from, item.baseFolder.to, { recursive: true });

			item.files.forEach((f) => {
				if (fs.existsSync(f.from) && !fs.existsSync(f.to)) {
					fsextra.moveSync(f.from, f.to);
				}
			});
		} else {
			Logger.log({
				level: 'info',
				name: 'App',
				color: 'magentaBright',
				message: item.baseFolder.from + ' already moved.',
				
			});
		}
		if (index == req.body.length - 1) {
			const child = fork('./src/controllers/forks/processAllFiles.mjs', {
			});
			child.send({ force: true, folder: item.baseFolder.to.replace(/[\\\/][\w\.]*[\\\/]$/, '') });

			child.on('message', (data) => {
				return res.json({
					success: true,
					message: 'Updated successfully',
					data: data,
				});
			});
		}
	});

	return res.json({
		status: 'ok',
	});
};
