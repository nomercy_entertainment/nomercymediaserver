import { cleanFileName } from '../../controllers/file/filenameParser.mjs';
import { createFFmpegCommand } from '../../controllers/encoder/ffmpeg/ffmpeg.mjs';
import { fork } from 'child_process';
import fs from 'fs';
import { getFileList } from '../../controllers/file/fileController.mjs';
import movie from '../../providers/themoviedb/database/movie.mjs';
import { parseYear } from '../../helpers/dateTime.mjs';
import { sendTo } from '../../loaders/socket.mjs';
import tv from '../../providers/themoviedb/database/tv.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	let type;
	let name;
	let response;
	let folderName;
	let baseFolderIn;
	let baseFolderOut;

	switch (req.body.type) {
		case 'anime':
			response = await tv(req.body.id);
			name = response.data.name;
			folderName = response.data.name + '.(' + parseYear(response.data.first_air_date) + ')';
			baseFolderIn = config.folderRoots.find((r) => r.path.includes('Anime') && r.direction == 'in').path + '/';
			baseFolderOut = config.folderRoots.find((r) => r.path.includes('Anime') && r.direction == 'out').path + '/';
			type = 'Anime';

			break;

		case 'tv':
			response = await tv(req.body.id);
			name = response.data.name;
			folderName = response.data.name + '.(' + parseYear(response.data.first_air_date) + ')';

			baseFolderIn = config.folderRoots.find((r) => r.path.includes('TV.Shows') && r.direction == 'in').path + '/';
			baseFolderOut = config.folderRoots.find((r) => r.path.includes('TV.Shows') && r.direction == 'out').path + '/';
			type = 'TV.Show';

			break;
		case 'movie':
			response = await movie(req.body.id ?? req.body.if);
			name = response.data.title;
			folderName = response.data.title + '.(' + parseYear(response.data.release_date) + ')';

			baseFolderIn = config.folderRoots.find((r) => (r.path.includes('Films') || r.path.includes('Movies')) && r.direction == 'in').path + '/';
			baseFolderOut = config.folderRoots.find((r) => (r.path.includes('Films') || r.path.includes('Movies')) && r.direction == 'out').path + '/';
			type = baseFolderIn.includes('Films') ? 'Films' : 'Movies';

			break;

		default:
			break;
	}

	let outputFolderIn = cleanFileName(baseFolderIn + folderName);
	let outputFolderrOut = cleanFileName(baseFolderOut + folderName);

	if (!fs.existsSync(outputFolderrOut)) {
		fs.mkdirSync(outputFolderrOut, {recursive: true});
	}

	let list = await getFileList(outputFolderIn);
	await Promise.all(
		list.map(async (fileIn) => {
			await createFFmpegCommand(fileIn);
		})
	);

	const child = fork('./src/controllers/forks/processAllFiles.mjs', {
		windowsHide: true,
	});

	child.send({ force: true, folder: outputFolderrOut });

	child.on('message', (data) => {
		sendTo(config.owner, 'update_content');
	});

	return res.json({
		status: 'ok',
		message: name + ' added successfully',
	});
};
