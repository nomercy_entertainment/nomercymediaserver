import { verifyMp4, verifyPlaylist } from '../../controllers/encoder/ffmpeg/verify.mjs';

import fs from 'fs';
import { getFileList } from '../../controllers/file/fileController.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	let files = await getFileList(config.ripperOutFolder);

	const newFiles = files.map((file) => {
		let fileOk = false;

		if (file.endsWith('.m3u8')) {
			if (verifyPlaylist(file)) {
				fileOk = true;
			}
		}

		if (file.endsWith('.mp4')) {
			if (verifyMp4(file)) {
				fileOk = true;
			}
		}

		if (fileOk) {
			let url = file.replace(config.ripperOutFolder.replace(/\//g, '\\'), '/Ripper/').replace(/\\/g, '/');
			let name = file.replace(/\.NoMercy.*$/, '').replace(/.*[\\\/]/g, '');
			let date = file.replace(/_.*\.NoMercy.*$/, '').replace(/.*[\\\/]/g, '');
			let filename = file.replace(/.*[\\\/]/g, '');
			let folder = file
				.replace(config.ripperOutFolder.replace(/\//g, '\\'), '')
				.replace(/\\/g, '/')
				.replace('/' + filename, '');

			let textTracks = (fs.existsSync(config.ripperOutFolder + folder + '/subtitles') && fs.readdirSync(config.ripperOutFolder + folder + '/subtitles')) || [];

			return {
				date,
				share: 'Ripper',
				folder,
				filename,
				textTracks,
				origin: config.server_id,
				name,
				url,
			};
		}
	});
	
	return res.json({
		data: {
			ripper_folder: config.ripperOutFolder,
			folder_roots: config.folderRoots.filter((r) => !r.path.includes('Download')),
			files: newFiles.filter(f => f != null),
		},
	});
};
