import { getParsedFileList } from '../../controllers/file/fileController.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const folder = req.query.folder?.replace(/\\/g, '/');
	const data = await getParsedFileList(folder);
	const body = { message: 'ok', data: data };

	return res.json(body);
};
