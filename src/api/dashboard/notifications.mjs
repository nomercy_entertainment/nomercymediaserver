import appApiClient from '../../loaders/axios.mjs';
import { prismaMedia } from '../../config/database.mjs';
import { sendNotificationTo } from '../../loaders/socket.mjs';
import { tokenParser } from '../../helpers/index.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	let body;
	let id = req.params.id;

	const Notification = prismaMedia.messages;

	// let notifications = await Notification.findMany();

	// let { data } = await appApiClient.get(`https://api.nomercy.tv/getUserDetails?users=${notifications.map((m) => m.from).join(',')}`).catch((e) => console.warn(e));

	if (req.method == 'GET') {
		let notifications = await Notification.findMany({
			where: {
				type: 'notification',
				to: {
					in: [tokenParser(req).sub, tokenParser(req).email, '*'],
				},
			},
		});

		body = notifications
		// .map((b) => {
		// 	// let meta = data?.find((u) => u.email == b.from || u.sub_id == b.from);

		// 	return {
		// 		...b,
		// 		created_at: Math.floor(new Date(b.created_at).getTime()),
		// 		updated_at: Math.floor(new Date(b.updated_at).getTime()),
		// 		method: 'add',
		// 		image: meta?.avatar || b.image,
		// 		from: meta?.name || b.from,
		// 	};
		// });
	} else if (req.method == 'POST') {
		let response = await Notification.create({
			data: {
				notify: req.body.notify || true,
				read: false,
				from: req.body.from,
				to: req.body.to,
				image: req.body.image,
				title: req.body.title,
				body: req.body.body,
				type: 'notification',
			},
		});

		let meta = data?.find((u) => u.email == response.from || u.sub_id == response.from);

		response.created_at = Math.floor(new Date(response.created_at).getTime());
		response.updated_at = Math.floor(new Date(response.updated_at).getTime());
		(response.method = 'add'), (response.from = meta?.name || response.from);
		response.image = meta?.avatar || response.image;

		sendNotificationTo(response.to, response);

		body = response;
	} else if (req.method == 'PUT') {
		let notification = await Notification.findFirst({
			where: {
				id: parseInt(id),
			},
		});

		if (!notification) {
			return res.json({
				success: false,
				notification: 'This resource does not exist.',
			});
		}

		let response = await Notification.update({
			where: {
				id: parseInt(id),
			},
			update: {
				notify: req.body.notify || true,
				read: false,
				from: req.body.from,
				image: req.body.image,
				title: req.body.title,
				body: req.body.body,
				type: 'notification',
			},
		});

		let meta = data?.find((u) => u.email == response.from || u.sub_id == response.from);

		response.created_at = Math.floor(new Date(response.created_at).getTime());
		response.updated_at = Math.floor(new Date(response.updated_at).getTime());
		response.from = meta?.name || response.from;
		response.image = meta?.avatar || response.image;

		(response.method = 'update'), sendNotificationTo(response.to, response);

		body = response;
	} else if (req.method == 'PATCH') {
		let notification = await Notification.findFirst({
			where: {
				id: parseInt(id),
			},
		});

		if (!notification) {
			return res.status(400).json({
				success: false,
				notification: 'This resource does not exist.',
			});
		}

		let response = await Notification.update({
			where: {
				id: parseInt(id),
			},
			data: {
				...notification,
				...req.body,
				type: 'notification',
			},
		});

		let meta = data?.find((u) => u.email == response.from || u.sub_id == response.from);

		response.created_at = Math.floor(new Date(response.created_at).getTime());
		response.updated_at = Math.floor(new Date(response.updated_at).getTime());
		response.from = meta?.name || response.from;
		response.image = meta?.avatar || response.image;

		(response.method = 'update'), sendNotificationTo(response.to, response);

		body = response;
	} else if (req.method == 'DELETE') {
		let notification = await Notification.findFirst({
			where: {
				id: parseInt(id),
			},
		});

		if (!notification) {
			return res.status(400).json({
				success: false,
				notification: 'This resource does not exist.',
			});
		}

		let response = await Notification.delete({
			where: {
				id: parseInt(id),
			},
		});

		response.method = 'delete';

		sendNotificationTo(response.to, response);

		body = response;
	}

	return res.json(body);
};
