import { isModerator, isOwner } from '../../api/middlewares/permissions.mjs';

import { execSync } from 'child_process';
import { fileURLToPath } from 'url';
import fs from 'fs';
import path from 'path';
import { platform } from 'os-utils';
import { sort_by } from '../../helpers/stringArray.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const token = req.kauth.grant.access_token;

	if (!isOwner(token) && !isModerator(token)) {
		return res.status(401).json({
			status: 'error',
			message: 'No permission to perform this action.',
		});
	}

	let path = req.query.path;
	if (platform() == 'win32') {
		path = path.replace(/^\//, '');
	}

	if (!path || path == 'null' || path == 'undefined') {
		if (platform() == 'win32') {
			let wmic = execSync('wmic logicaldisk get name').toString();
			path = wmic
				.split('\r\r\n')
				.filter((value) => /[A-Za-z]:/.test(value))
				.map((value) => value.trim() + '/');
		}
		else if (token.content.email == 'test@nomercy.tv') {
			path = __dirname + '/../public/Media/';
		}
		else {
			path = process.env.SHARE_ROOT + '/';
		}
	}
	
	if(path == '/usr/src/app/src/api/dashboard/../../../public/' && token.content.email == 'test@nomercy.tv'){
		path = '/usr/src/app/src/api/dashboard/../../../public/Media/';
	}

	let array = [];

	if (Array.isArray(path)) {
		array = path.map((f) => createFolderObject('', f));
	}

	try {
		if (!Array.isArray(path) && fs.statSync(path).isDirectory()) {
			if (!fs.statSync(path).mode & 0x92) {
				return res.status(400).json({
					status: 'error',
					message: 'No permission to access this path.',
				});
			}

			let folders = fs.readdirSync(path.replace('null', '').replace('undefined', ''));
			
			array = sort_by(
				folders
					.filter((f) => !f.includes('$'))
					.filter((f) => !f.startsWith('.'))
					.map((f) => createFolderObject(path, f)),
				'type',
				'desc'
			);
		}

		if (path) {
			return res.json({
				status: 'success',
				array: array.filter((f) => f != null),
			});
		}
	} catch (error) {
		return res.status(400).json({
			status: 'error',
			message: 'Specified path is not a directory.',
		});
	}

	return res.json(body);
};

const createFolderObject = function (parent, path) {
	let fullPath = parent ? parent + path + '/' : path + '/';
		
	try {

		let stats;
		if(fs.existsSync(fullPath)){
			stats = fs.statSync(fullPath);
		}
		else{
			stats = fs.statSync(fullPath.replace(/[\/\\]$/,''));
		}
		
		return {
			path: path.match(/\w:/) ? path : path + '/',
			mode: stats.mode,
			size: stats.size,
			type: stats.isDirectory() ? 'folder' : 'file',
			parent: parent.replace(/[/]{1,}$/, '').replace(/[\w.\s\d-_?,()$]*[\\/]*$/g, ''),
			fullPath: fullPath.replace(/[/]{2,}$/, '/'),
		};
	} catch (error) {
		return;
	}
};
