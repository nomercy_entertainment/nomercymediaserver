import { humanTime, fileChangedAgo } from '../../helpers/dateTime.mjs';

import Logger from '../../loaders/logger.mjs';
import { execSync } from 'child_process';
import { fileURLToPath } from 'url';
import fs from 'fs';
import { getAudioInfo } from '../../controllers/encoder/ffmpeg/ffprobe.mjs';
import { getFileList } from '../../controllers/file/fileController.mjs';
import { javaHash } from '../../helpers/stringArray.mjs';
import path from 'path';
import { prismaMedia } from '../../config/database.mjs';
import { getBestArtistImag } from '../../helpers/artistImages.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

function parse(str) {
	if (!/^(\d){8}$/.test(str)) return null;
	var y = str.substr(0, 4),
		m = str.substr(4, 2) - 1,
		d = str.substr(6, 2);
	return new Date(y, m, d);
}

export default async function () {
	const folder = process.env.SHARE_ROOT + '/Music/';
	let music = [];

	const folderFile = path.join(__dirname, '..', '..', '..', 'cache', folder.replace(/[\/\\]/g, '_').replace(':', '_') + '.json');

	Logger.log({
		level: 'info',
		name: 'App',
		color: 'magentaBright',
		message: 'Searching music files',
	});

	if (fs.existsSync(folderFile) && fileChangedAgo(folderFile, 'minutes') < 50) {
		music = JSON.parse(fs.readFileSync(folderFile, 'utf-8'));
	} else if (fs.existsSync(folder)) {
		music = await getFileList(folder, ['.mp3', 'flac']);

		fs.writeFileSync(folderFile, JSON.stringify(music));
	}

	const cache = [];

	const result = [];

	for (let i = 0; i < music.length; i++) {
		let fileIn = music[i];

		let ffprobe = await getAudioInfo(fileIn).catch((reason) => console.log(reason));
		if (ffprobe) {
			// const regex = /(?<host_folder>.*)[\\\/](?<root>Music)[\\\/]((?<firstLetter>\w{1})[\\\/])?(?<artist>[\w\d\s\.'"’(),_\[\]@#$!&+-]+)[\\\/]([\w\d\s\.'"’(),_\[\]@#$!&+-]+\))?(?<albumFolder>(\[(?<year>\d{4})\] )?(?<album>[\w\d\s\.'"’(),_\[\]@#$!&+-\[\]]+))[\\\/](?<fileName>(((?<disc>\d)-)?(?<track>\d{2})|\[\d{4}\])[\s\.]+(?<title>[\w\d\s\.'"’(),_\[\]@#$!&+-]+).(?<ext>m4a|flac|mp3))/;

			const regex =
				/(?<host_folder>.*)[\\\/](?<root>Music)[\\\/]((?<firstLetter>\w{1})[\\\/])?(?<artist>[\w\d\s\.'"’(),_\[\]@#$!&+-]+)[\\\/]([\w\d\s\.'"’(),_\[\]@#$!&+-]+\))?(?<albumFolder>(\[(?<year>\d{4})\] )?(?<album>[\w\d\s\.'"’(),_\[\]@#$!&+-\[\]]+))[\\\/](?<fileName>((((?<disc>\d{1,2})[\s-])?(?<track>\d{1,2}))|\[\d{4}\])[\s\.]*(?<title>[\w\d\s\.'"’(),_\[\]@#$!&+-]+).(?<ext>m4a|flac|mp3))/;

			const match = regex.exec(ffprobe.format.filename);

			if (match?.groups && ffprobe.tags) {
				const albumName = ffprobe.tags.album || match.groups.album;
				const albumId = ffprobe.tags.MusicBrainz_album_id || javaHash(albumName);
				const artsitName = ffprobe.tags.album_artist || ffprobe.tags.artist;
				const originalyear = ffprobe.tags.originalyear;

				const disc = ffprobe.tags.disc;
				const track = ffprobe.tags.track;
				const title = ffprobe.tags.title || match.groups.title;

				const artistIds = ffprobe.tags.MusicBrainz_artist_ids?.map((a) => {
					return {
						artistId: a,
					};
				}) || [javaHash(artsitName)];

				const genres = ffprobe.tags.genre?.map((a) => {
					return {
						name: a.split('/')?.[0]?.trim() || a,
					};
				});

				const artists = ffprobe.tags.ARTISTS || [ffprobe.tags.artist];

				const file = match.groups.fileName;
				const folder = `${match.groups.root}/${match.groups.firstLetter ? match.groups.firstLetter + '/' : ''}${match.groups.artist}`;
				const rootFolder = `${match.groups.host_folder}/${folder}`;

				let albumImage;
				if (fs.existsSync(`${rootFolder}/${match.groups.albumFolder}/Cover.jpg`)) {
					albumImage = `${folder}/${match.groups.albumFolder}/Cover.jpg`;
				} else {
					const img = fs.readdirSync(`${match.groups.host_folder}/${folder}/${match.groups.albumFolder}`).find((a) => a.endsWith('.jpg'));

					albumImage = img ? `${folder}/${match.groups.albumFolder}/` + img : null;
				}

				let trackImage = null;
				if (fs.existsSync(fileIn.replace(/\.\w{3,4}$/, '.jpg'))) {
					trackImage = `${file.replace(/\.\w{3,4}$/, '.jpg')}`;
				}

				if (ffprobe.format.duration == 'N/A') {
					const oldFile = fileIn.replace('.mp3', '.old.mp3');

					fs.renameSync(fileIn, oldFile);

					execSync(`ffmpeg -i "${oldFile}" -c:a mp3 "${fileIn}" -y`);

					ffprobe = await getAudioInfo(fileIn).catch((reason) => console.log(reason));

					if (ffprobe.format.duration != 'N/A') {
						fs.rmSync(oldFile);
					}
				}

				if (genres && artists) {
					genres.map(async (genre) => {
						await prismaMedia.music_genre.upsert({
							where: {
								name: genre.name,
							},
							create: {
								name: genre.name,
							},
							update: {
								name: genre.name,
							},
						});
					});
				}

				if (artistIds && artists) {
					artistIds.map(async (artist, index) => {
						let artistImage = undefined;
						if (fs.existsSync(`${match.groups.host_folder}/${folder}/${artists[index]}.jpg`)) {
							artistImage = `${folder}/${artists[index]}.jpg`;
						}
						if (fs.existsSync(process.env.SHARE_ROOT + '/Music/' + artists[index][0] + '/' + artists[index] + '/' + artists[index] + '.jpg')) {
							artistImage = '/Music/' + artists[index][0] + '/' + artists[index] + '/' + artists[index] + '.jpg';
						}
						if (fs.existsSync(`${match.groups.host_folder}/${folder}/${artists[index]}.webp`)) {
							artistImage = `${folder}/${artists[index]}.webp`;
						}

						if (!artistImage && !cache.some((i) => i.id == artistIds[0])) {
							const i = await prismaMedia.artist.findFirst({ where: { artistId: artist.artistId ?? artistIds[0] } });

							if (i?.cover) {
								cache.push({
									id: artistIds[0],
									image: i.cover,
								});
							} else {
								try {
									const x = await getBestArtistImag(artists[index], process.env.SHARE_ROOT + '/Music/' + artists[index][0] + '/' + artists[index] + '/' + artists[index] + '.jpg');
									if (x) {
										artistImage = '/Music/' + artists[index][0] + '/' + artists[index] + '/' + artists[index] + '.jpg';
										cache.push({
											id: artistIds[0],
											image: '/Music/' + artists[index][0] + '/' + artists[index] + '/' + artists[index] + '.jpg',
										});
									}
								} catch (error) {}
							}
						} else if (!artistImage && cache.some((i) => i.id == artistIds[0])) {
							albumImage = cache.find((i) => i.id == artistIds[0])?.image;
						}

						await prismaMedia.artist.upsert({
							where: {
								artistId: artist.artistId ?? artistIds[0],
							},
							create: {
								name: artists[index],
								cover: artistImage ?? undefined,
								folder: "/Music/" + artists[index][0] + '/' +  artists[index],
								artistId: artist.artistId ?? artistIds[0],
							},
							update: {
								name: artists[index],
								cover: artistImage,
								folder: "/Music/" + artists[index][0] + '/' +  artists[index],
								artistId: artist.artistId ?? artistIds[0],
							},
						});
					});
				}

				const albumData = {
					name: albumName,
					albumId: albumId,
					cover: albumImage,
					description: artsitName + (originalyear ? ', (' + originalyear + ')' : ''),
				};

				if (!artistIds) {
					delete albumData.artist;
				}

				await prismaMedia.album.upsert({
					where: {
						name: albumName,
					},
					create: albumData,
					update: albumData,
				});

				if (new Date(ffprobe.tags.date) == 'Invalid Date') {
					ffprobe.tags.date = parse(ffprobe.tags.date);
				}

				const quality = ffprobe.audio.bit_rate != 'NaN' ? Math.floor(ffprobe.audio.bit_rate / 1000) : null;

				const data = {
					name: title,
					track: track,
					cover: trackImage,
					disc: disc,
					date: new Date(ffprobe.tags.date),
					folder: `${folder}/${match.groups.albumFolder}`,
					filename: file,
					duration: humanTime(ffprobe.format.duration).replace(/^00:/, ''),
					host_folder: rootFolder,
					quality: isNaN(quality) ? null : quality,
					music_genre: {
						connect: genres,
					},
					album: {
						where: {
							name: albumName,
						},
						create: {
							name: albumName,
							albumId: albumId,
							cover: albumImage,
							description:
								artsitName +
								(originalyear ? ", (" + originalyear + ")" : ""),
							artist: {
								connect: {
									artistId: artistIds[0]?.artistId,
								}
							}
						},
						update: {
							name: albumName,
							albumId: albumId,
							cover: albumImage,
							description:
								artsitName +
								(originalyear ? ", (" + originalyear + ")" : ""),
							artist: {
								connect: {
									artistId: artistIds[0]?.artistId,
								}
							}
						}
					},
					artist: {
						connect: artistIds,
					},
				};

				if (!ffprobe.tags.genre) {
					delete data.music_genre;
				}
				if (!albumId) {
					delete data.album;
				}
				if (!artistIds) {
					delete data.artist;
				}

				if (data.name) {
					let createdtrack;
					try {
						createdtrack = await prismaMedia.track.upsert({
							where: {
								track_unique: {
									filename: data.filename,
									host_folder: data.host_folder,
								},
							},
							create: data,
							update: data,
						});
					} catch (error) {}

					result.push(createdtrack);
				}
			}
		}
	}

	Logger.log({
		level: 'info',
		name: 'App',
		color: 'magentaBright',
		message: 'Music library updated, found ' + result.length + ' tracks',
	});

	return result;
}
