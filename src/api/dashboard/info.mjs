import { convertToHuman, convertToSeconds } from '../../helpers/dateTime.mjs';

import { prismaMedia } from '../../config/database.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async (req, res) => {
	
	const video_files = await prismaMedia.video_file.findMany({
		where: {
			duration: {
				not: '00',
			}
		},
	});

	const tv = await prismaMedia.tv.findMany({
    where: {
      have_episodes: {
        gt: 0
      }
    },
    select: {
			id: true,
			media_type: true,
      have_episodes: true,
      number_of_episodes: true,
    }
	});

	const movie = await prismaMedia.movie.findMany({
    select: {
			id: true,
      video_file: {
				select:{
					id: true,
				},
			},
    },
	});
	const collection = await prismaMedia.collection.findMany({
    select: {
			id: true,
      collection_movie: {
				select:{
					movieId: true,
				},
			},
    },
	});
	
	let length = 0;
	for (let i = 0; i < video_files.length; i++) {
		length+= convertToSeconds(video_files[i].duration);
	}
	
	const totalTime = convertToHuman(length);

	return res.json({
		// tv,
		// movie,
		// collection,
		anime: tv.filter(a => a.media_type === 'anime').length,
		tv_shows: tv.filter(a => a.media_type === 'tv').length,
		movies: movie.filter(m => m.video_file.length > 0).length,
		collections: collection.filter(c => c.collection_movie.length > 0).length,
		totalTime,
	});

}
