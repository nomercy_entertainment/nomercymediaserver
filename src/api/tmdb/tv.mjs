import tv from '../../providers/themoviedb/database/tv.mjs';

export default async function (req, res) {
	const data = await tv(req.params.id);

	return res.json(data);
};
