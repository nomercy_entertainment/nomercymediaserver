import movie from '../../providers/themoviedb/database/movie.mjs';

export default async function (req, res) {
	const data = await movie(req.params.id);

	return res.json(data);
};
