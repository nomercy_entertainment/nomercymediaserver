import getCollection from '../../providers/themoviedb/client/collection.mjs';

export default async function (req, res) {
	const data = await getCollection(req.params.id);

	return res.json(data);
};
