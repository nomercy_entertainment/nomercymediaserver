import { groupBy } from '../../helpers/index.mjs';
import trending from '../../providers/themoviedb/client/trending.mjs';

export default async function (req, res) {
	const all = await trending('all', 'day', 10);
	const person = await trending('person', 'day', 2);

	let data = all.concat(person);

	let results = groupBy(data, 'media_type');

	return res.json(results);
};
