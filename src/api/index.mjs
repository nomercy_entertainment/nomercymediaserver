import dashboard from './routes/dashboard.mjs';
import express from 'express';
import media from './routes/media.mjs';
import music from './routes/music.mjs';

const router = express.Router();

router.use('/', media);
router.use('/dashboard', dashboard);
router.use('/music', music);

router.get('/me', (req, res) => {
	var token = req.kauth.grant.access_token;
	return res.json(token.content);
});

export default router;
