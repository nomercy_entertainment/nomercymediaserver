import AudioFile from '../../../Models/AudioFile.mjs';
import AudioFile from '../../../Models/AudioFile.mjs';
import AudioFile from '../../../Models/AudioFile.mjs';
import LoggerInstance from '../../loaders/logger.mjs';
import Playlist from '../../../Models/Playlist.mjs';
import { convertPath } from '../../helpers/index.mjs';
import dataController from '../content/data.mjs';
import db from '../../database/db.mjs';
import fs from 'fs';
import mime from 'mime-types';
const youtubeDl = require('../encoder/youtube-dl');


export default async function (socket, io) {
	socket.on('music-download', (url, audioOnly, folder = 'mnt/media/Youtube') => {
		LoggerInstance.info('download', url, audioOnly ? 'audio only' : 'video');

		youtubeDl.download(url, audioOnly, folder, (e) => {
			socket.emit('music-download', e);
			Logger.info('done');
			let artist = folder.split('/')[folder.split('/').length - 1];
			dataController.musicCount(artist);
		});
	});

	socket.on('music-load', async (name) => {
		let playlist = [];

				const list = await AudioFile.query().where({ artist: name }).orderBy('title').then();

		list.forEach((file) => {
			playlist.push(createPlaylistItem(file));
		});

		socket.emit(
			'music-load',
			playlist.sort((a, b) => (a.title > b.title ? 1 : -1))
		);
	});

	socket.on('music-folders', async () => {
		let list = [];

				list = await AudioFile.query().distinct('artist').whereNotNull('artist').orderBy('artist').then();

		socket.emit('music-folders', list);
	});

	socket.on('music-search', async (search) => {
		var playlist = [];

				let allFiles = await AudioFile.query().then();

		allFiles
			.filter((file) => {
				const re = new RegExp(search, 'igm');

				if (file.title && file.title.match(re)) {
					return file;
				} else if (file.artist && file.artist.match(re)) {
					return file;
				} else if (file.album && file.album.match(re)) {
					return file;
				} else if (file.tags && file.tags.match(re)) {
					return file;
				} else if (file.file && file.file.match(re)) {
					return file;
				} else if (file.playlist && file.playlist.match(re)) {
					return file;
				} else if (file.description && file.description.match(re)) {
					return file;
				}
			})
			.sort((a, b) => (a.title > b.title ? 1 : -1))
			.forEach((item) => {
				playlist.push(createPlaylistItem(item));
			});

		socket.emit('music-search', playlist);
	});

	socket.on('music-playlists', async (name) => {
		let playlist = await db('playlist').orderBy('title').then();

		socket.emit('music-playlists', playlist);
	});

	socket.on('music-playlist', async (name) => {
		let playlist = [];

				const list = await Playlist.query().withGraphFetched('[audio_file]').where({ title: name }).then();

		if (list.audio_file) {
			list.audio_file.forEach((file) => {
				playlist.push(createPlaylistItem(file));
			});
		}

		socket.emit('music-playlist', playlist);
	});

	socket.on('music-create-playlist', async (data) => {
		// Logger.info(('music-create-playlist', data);

		db.raw(
			db('playlist')
				.insert({
					title: data.title,
					description: data.description,
					image: data.image,
				})
				.toQuery()
				.replace('insert into', 'insert or replace into')
		).then(async () => {
			let playlist = await db('playlist').orderBy('title').then();
			socket.emit('music-playlists', playlist);
		});
	});

	socket.on('music-count', async (folder) => {
		Logger.info('stating counting of existing episodes');
		await dataController.musicCount(folder);
	});
};

function createPlaylistItem(data) {
	return {
		id: data.id,
		title: data.title,
		description: data.artist,
		image: config.externamHost + (data.image || data.file.replace(config.mediaRoot, '/Media').replace(/\.\w{3,4}$/, '.jpg')),
		sources: [
			{
				src: config.externamHost + data.file.replace(config.mediaRoot, '/Media'),
				type: mime.lookup(data.file) == 'audio/x-flac' ? 'audio/flac' : mime.lookup(data.file),
			},
		],
	};
}
