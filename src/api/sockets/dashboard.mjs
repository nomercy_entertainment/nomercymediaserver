import { fileURLToPath } from 'url';
import fs from 'fs';
import i18next from 'i18next';
import path from 'path';
import progressMjs from '../../controllers/encoder/ffmpeg/progress.mjs';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const watchDir = __dirname + '/public/encoder/working/';

export default function (socket, io) {
	socket.on('servers', () => {
		progressMjs(socket, io);

		fs.writeFileSync(watchDir + 'update.json', JSON.stringify(Date.now()));

		let workerName = config.workerName;
		let status = i18next.t('inactive');
		socket.emit('servers', {
			workerName,
			status,
		});
	});
};
