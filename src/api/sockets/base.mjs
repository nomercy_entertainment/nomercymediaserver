import Logger from '../../loaders/logger.mjs';
import content from './content.mjs';
import cpuStats from '../../controllers/monitor/cpu.mjs';
import dashboard from './dashboard.mjs';
import music from './music.mjs';
import progress from '../../controllers/encoder/ffmpeg/progress.mjs';
import video from './video.mjs';

export default function (socket, io, myClientList) {
		
	socket.on('notification', (data) => {
		Logger.log({
			level: 'http',
			name: 'notification',
			color: 'yellow',
			message: `${socket.decoded_token.name}: ${data}`,
		});
		socket.nsp.to(socket.decoded_token.sub).emit('notification', data);
	});

	socket.on('command', (data) => {
		Logger.log({
			level: 'http',
			name: 'command',
			color: 'yellow',
			message: `${socket.decoded_token.name}: ${data}`,
		});
		socket.nsp.to(socket.decoded_token.sub).emit('command', data);
	});

	socket.on('log', (data) => {		
		Logger.log({
			level: 'http',
			name: 'log',
			color: 'yellow',
			message: `${socket.decoded_token.name}: ${data}`,
		});
	});

	music(socket, io, myClientList);
	content(socket, io);
	dashboard(socket, io);
	progress(socket, io);
	video(socket, io, myClientList);
	cpuStats(socket, io);
};

const sendTo = (myClientList, to, event, message = null) => {
	myClientList.filter((c) => c.sub == to).forEach((c) => c.socket.emit(event, message));
};