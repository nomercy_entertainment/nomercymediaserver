
export default function (socket, io, updatedList) {

	io.sockets.emit('clientList', updatedList(socket));

	socket.on('get_devices', () => {
		socket.nsp
			.to(socket.decoded_token.sub)
			.emit('get_devices', updatedList(socket));
	});

	socket.on('getCurrentDevice', () => {
		socket.nsp.to(socket.decoded_token.sub).emit('getCurrentDevice', { value: updatedList(socket)[0].id });
	});

	socket.on('switch_device', (data) => {
		socket.to(socket.decoded_token.sub).emit('switch_device', data);
	});

	socket.on('update_state', (data) => {
		socket.to(socket.decoded_token.sub).emit('update_state', data);
	});

	socket.on('playState', (data) => {
		socket.to(socket.decoded_token.sub).emit('playState', data);
	});
	socket.on('mutedState', (data) => {
		socket.to(socket.decoded_token.sub).emit('mutedState', data);
	});
	socket.on('volumeState', (data) => {
		socket.to(socket.decoded_token.sub).emit('volumeState', data);
	});
	socket.on('positionState', (data) => {
		socket.to(socket.decoded_token.sub).emit('positionState', data);
	});
	socket.on('seek', (data) => {
		socket.to(socket.decoded_token.sub).emit('seek', data);
	});
	socket.on('durationState', (data) => {
		socket.to(socket.decoded_token.sub).emit('durationState', data);
	});

	socket.on('currentPlaylist', (data) => {
		socket.to(socket.decoded_token.sub).emit('currentPlaylist', data);
	});
	socket.on('currentItem', (data) => {
		socket.to(socket.decoded_token.sub).emit('currentItem', data);
	});

	socket.on('lyrics', (data) => {
		socket.to(socket.decoded_token.sub).emit('lyrics', data);
	});

	socket.on('artists', (data) => {
		socket.to(socket.decoded_token.sub).emit('artists', data);
	});
	socket.on('albums', (data) => {
		socket.to(socket.decoded_token.sub).emit('albums', data);
	});
	socket.on('playlists', (data) => {
		socket.to(socket.decoded_token.sub).emit('playlists', data);
	});
	socket.on('userPlaylists', (data) => {
		socket.to(socket.decoded_token.sub).emit('userPlaylists', data);
	});
}