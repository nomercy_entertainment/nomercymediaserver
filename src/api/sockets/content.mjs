import Logger from '../../loaders/logger.mjs';
// import connection from '../../database/db.mjs';
// import dataController from '../content/data.mjs';
// import sync from '../../../../database/sync.mjs';
// import youtubeDl from '../../controllers/encoder/youtube-dl';

export default function (socket, io) {
	// socket.on('search', async function (search) {
	// 	let year_regex = /\d{4}/g;
	// 	let hasYear = year_regex.test(search);
	// 	let year;
	// 	let tv;
	// 	let movie;
	// 	if (hasYear) {
	// 		year = search.match(year_regex)[0];
	// 		search = search.replace(year, '');
	// 	}
	// 	search = search.replace(/\s/g, '%');

	// 	if (hasYear) {
	// 		tv = await connection('tv').where('title', 'like', `%${search}%`).where('first_air_date', 'like', `%${year}%`).then();
	// 		movie = await connection('movie').where('title', 'like', `%${search}%`).where('release_date', 'like', `%${year}%`).then();
	// 	} else if (search.length > 3) {
	// 		tv = await connection('tv').where('title', 'like', `%${search}%`).then();
	// 		movie = await connection('movie').where('title', 'like', `%${search}%`).then();
	// 	} else {
	// 		tv = await connection('tv').where('title', 'like', `${search}%`).then();
	// 		movie = await connection('movie').where('title', 'like', `${search}%`).then();
	// 	}
	// 	tv.map(function (t) {
	// 		t.type = 'tv';
	// 	});
	// 	movie.map(function (t) {
	// 		t.type = 'movie';
	// 	});

	// 	let data = tv.concat(movie);
	// 	data.sort((a, b) => (a.title_sort > b.title_sort ? 1 : -1));

	// 	socket.emit('search', data.slice(0, parseInt(config.searchLimit)));
	// });

	// socket.on('load_index', async () => {
	// 	const data = {
	// 		data: await dataController.index(),
	// 	};

	// 	socket.emit('load_index', data);
	// });

	socket.on('get_trailer', async (id) => {
		// console.log(id);
		let trailer = null;

		if (id) {
			trailer = await youtubeDl.link(id);
		}

		socket.emit('get_trailer', trailer);
	});

	// socket.on('load_data', async (data) => {
	// 	let response;
	// 	switch (data.type) {
	// 		case 'anime':
	// 			response = await dataController.anime();
	// 			break;
	// 		case 'movies':
	// 			response = await dataController.movies();
	// 			break;
	// 		case 'tv':
	// 			response = await dataController.tv();
	// 			break;
	// 		case 'collections':
	// 			response = await dataController.collections();
	// 			break;
	// 		case 'collection':
	// 			response = await await dataController.collection(data.id);
	// 			response = response.movies;
	// 			break;
	// 		case 'anime':
	// 			response = await dataController.anime();
	// 			break;

	// 		default:
	// 			break;
	// 	}
	// 	socket.emit('load_data', response);
	// });

	// socket.on('load_info', async (d) => {
	// 	let data;
	// 	switch (d.type) {
	// 		case 'movie':
	// 			data = await dataController.movie_info(d.id);
	// 			break;
	// 		case 'tv':
	// 			data = await dataController.tv_info(d.id);
	// 			break;
	// 		case 'collection':
	// 			data = await dataController.collection(d.id);
	// 			break;

	// 		default:
	// 			break;
	// 	}

	// 	// Logger.debug((typeof data);

	// 	if (typeof data != 'undefined' && data.trailer) {
	// 		let trailer = data.trailer;
	// 		data.trailer = await youtubeDl.link(`https://www.youtube.com/watch?v=${trailer}`);
	// 	}
	// 	socket.emit('load_info', data);
	// });

	socket.on('count', async () => {
		Logger.log({
			level: 'info',
			name: 'Socket',
			color: 'yellow',
			message: 'stating counting of existing episodes',
		});
		await dataController.count();
	});
};
