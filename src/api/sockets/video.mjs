import { prismaMedia } from '../../config/database.mjs';

export default function (socket, io, myClientList) {
	socket.on("setTime", async (time) => {
		if (time.tmdb_id && time.video_id) {
			let progressInsert = {
				profile_id: socket.decoded_token.sub,
				video_id: parseInt(time.video_id),
				tmdb_id: parseInt(time.tmdb_id),
				type: time.type,
				audio: time.audio + "",
				subtitle_type: time.subtitle_type,
				subtitle: time.subtitle + "",
				time: parseInt(time.time),
			};
			// console.log('setTime', progressInsert);

			try {
				await prismaMedia.video_progress.upsert({
					where: {
						video_id: parseInt(time.video_id),
					},
					update: progressInsert,
					create: progressInsert,
				});
			} catch (error) {}
		}
	});
	socket.on("addToList", async (data) => {
		// console.log(data);
		let id = Math.floor(Math.random() * 1000000);

		let progressInsert = {
			profile_id: socket.decoded_token.sub,
			tmdb_id: parseInt(data.tmdb_id),
			type: data.type,
			time: 0,
			video_id: id,
		};

		try {
			await prismaMedia.video_progress.upsert({
				where: {
					video_id: id,
				},
				update: progressInsert,
				create: progressInsert,
			});
		} catch (error) {}

		socket.emit("addToList", true);
	});
	socket.on("load", async (data) => {
		// console.log("load",data);
		if (data.tmdb_id) {
			const start = await prismaMedia.video_progress.findFirst({
				where: {
					profile_id: socket.decoded_token.sub,
					tmdb_id: parseInt(data.tmdb_id),
					type: data.type,
				},
				orderBy: {
					updated_at: "desc",
				},
			});
			// console.log('load', start);
			socket.emit("load", start);
		}
	});

	socket.on("getTime", async (data) => {
		// console.log("getTime",data);
		if (data.tmdb_id) {
			const start = await prismaMedia.video_progress.findFirst({
				where: {
					profile_id: socket.decoded_token.sub,
					tmdb_id: parseInt(data.tmdb_id),
					type: data.type,
					subtitle_type: data.subtitle_type,
					video_id: parseInt(data.video_id),
				},
				orderBy: {
					updated_at: "desc",
				},
			});

			if (start == null) {
				// console.log('getTime', {time: 0});
				socket.emit("getTime", {
					time: 0,
				});
			} else {
				// console.log('getTime', start);
				socket.emit("getTime", start);
			}
		}
	});

	socket.on("remove-watched", async (data) => {
		// console.log("remove-watched",data);
		if (data.tmdb_id && data.type) {
			let video = await prismaMedia.video_progress.findMany({
				where: {
					profile_id: socket.decoded_token.sub,
					tmdb_id: parseInt(data.tmdb_id),
					type: data.type,
				},
			});
			if (video.length > 0) {
				await prismaMedia.video_progress.deleteMany({
					where: {
						id: {
							in: video.map((v) => v.id),
						},
					},
				});
				socket.emit("remove-watched", data);
			}
		}
	});

	socket.on("log", (data) => {
		// console.log("log",data);
		console.log(data);
	});

	socket.on("join", (data) => {
		// console.log("join",data);
		socket.to(socket.decoded_token.sub).emit("join", data);
	});
	socket.on("joinTime", (data) => {
		// console.log("joinTime",data);
		socket.to(socket.decoded_token.sub).emit("joinTime", data);
	});
	socket.on("joinPlay", (data) => {
		// console.log("joinPlay",data);
		socket.to(socket.decoded_token.sub).emit("joinPlay", data);
	});
	socket.on("joinPause", (data) => {
		// console.log("joinPause",data);
		socket.to(socket.decoded_token.sub).emit("joinPause", data);
	});
	socket.on("joinSeek", (data) => {
		// console.log("joinSeek",data);
		socket.to(socket.decoded_token.sub).emit("joinSeek", data);
	});
	socket.on("joinPlaylistitem", (data) => {
		// console.log("joinPlaylistitem",data);
		socket.to(socket.decoded_token.sub).emit("joinPlaylistitem", data);
	});
	socket.on("get_caster", () => {
		// console.log("get_caster");
		socket.to(socket.decoded_token.sub).emit("get_caster");
	});
	socket.on("send_caster", (data) => {
		// console.log("send_caster",data);
		socket.to(socket.decoded_token.sub).emit("send_caster", data);
	});
	socket.on("set_remote_id", (data) => {
		// console.log("set_remote_id",data);
		socket.to(socket.decoded_token.sub).emit("set_remote_id", data);
	});

	socket.on("remote_active_audio", (data) => {
		// console.log("remote_active_audio",data);
		socket.to(socket.decoded_token.sub).emit("remote_active_audio", data);
	});
	socket.on("remote_active_quality", (data) => {
		// console.log("remote_active_quality",data);
		socket.to(socket.decoded_token.sub).emit("remote_active_quality", data);
	});
	socket.on("remote_active_subtitle", (data) => {
		// console.log("remote_active_subtitle",data);
		socket.to(socket.decoded_token.sub).emit("remote_active_subtitle", data);
	});
	socket.on("remote_audio_list", (data) => {
		// console.log("remote_audio_list",data);
		socket.to(socket.decoded_token.sub).emit("remote_audio_list", data);
	});
	socket.on("remote_backward", (data) => {
		// console.log("remote_backward",data);
		socket.to(socket.decoded_token.sub).emit("remote_backward", data);
	});

	socket.on("remote_change_audio", (data) => {
		// console.log("remote_change_audio",data);
		socket.to(socket.decoded_token.sub).emit("remote_change_audio", data);
	});
	socket.on("remote_change_quality", (data) => {
		// console.log("remote_change_quality",data);
		socket.to(socket.decoded_token.sub).emit("remote_change_quality", data);
	});
	socket.on("remote_change_subtitle", (data) => {
		// console.log("remote_change_subtitle",data);
		socket.to(socket.decoded_token.sub).emit("remote_change_subtitle", data);
	});
	socket.on("remote_current_time", (data) => {
		// console.log("remote_current_time",data);
		socket.to(socket.decoded_token.sub).emit("remote_current_time", data);
	});
	socket.on("remote_duration", (data) => {
		// console.log("remote_duration",data);
		socket.to(socket.decoded_token.sub).emit("remote_duration", data);
	});

	socket.on("remote_forward", (data) => {
		// console.log("remote_forward",data);
		socket.to(socket.decoded_token.sub).emit("remote_forward", data);
	});
	socket.on("remote_kill", (data) => {
		// console.log("remote_kill",data);
		socket.to(socket.decoded_token.sub).emit("remote_kill", data);
	});

	socket.on("remote_load", (data) => {
		// console.log("remote_load",data);
		socket.to(socket.decoded_token.sub).emit("remote_load", data);
	});
	socket.on("remote_metadata", (data) => {
		// console.log("remote_metadata",data);
		socket.to(socket.decoded_token.sub).emit("remote_metadata", data);
	});

	socket.on("remote_mute", (data) => {
		// console.log("remote_mute",data);
		socket.to(socket.decoded_token.sub).emit("remote_mute", data);
	});
	socket.on("remote_next", (data) => {
		// console.log("remote_next",data);
		socket.to(socket.decoded_token.sub).emit("remote_next", data);
	});
	socket.on("remote_pause", (data) => {
		// console.log("remote_pause",data);
		socket.to(socket.decoded_token.sub).emit("remote_pause", data);
	});
	socket.on("remote_play", (data) => {
		// console.log("remote_play",data);
		socket.to(socket.decoded_token.sub).emit("remote_play", data);
	});
	socket.on("remote_playlist", (data) => {
		// console.log("remote_playlist",data);
		socket.to(socket.decoded_token.sub).emit("remote_playlist", data);
	});

	socket.on("remote_playlist_item", (data) => {
		// console.log("remote_playlist_item",data);
		socket.to(socket.decoded_token.sub).emit("remote_playlist_item", data);
	});

	socket.on("remote_prev", (data) => {
		// console.log("remote_prev",data);
		socket.to(socket.decoded_token.sub).emit("remote_prev", data);
	});
	socket.on("remote_quality_list", (data) => {
		console.log("r/emote_quality_list",data);
		socket.to(socket.decoded_token.sub).emit("remote_quality_list", data);
	});
	socket.on("remote_restart", (data) => {
		// console.log("remote_restart",data);
		socket.to(socket.decoded_token.sub).emit("remote_restart", data);
	});
	socket.on("remote_stop", (data) => {
		// console.log("remote_stop",data);
		socket.to(socket.decoded_token.sub).emit("remote_stop", data);
	});
	socket.on("remote_subtitle_list", (data) => {
		// console.log("remote_subtitle_list",data);
		socket.to(socket.decoded_token.sub).emit("remote_subtitle_list", data);
	});
	socket.on("remote_time", (data) => {
		// console.log("remote_time",data);
		socket.to(socket.decoded_token.sub).emit("remote_time", data);
	});
	socket.on("remote_volume", (data) => {
		// console.log("remote_volume",data);
		socket.to(socket.decoded_token.sub).emit("remote_volume", data);
	});
}
