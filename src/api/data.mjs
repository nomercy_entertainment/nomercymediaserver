import appApiClient from "../loaders/axios.mjs";

const date = Date.now();

const notifications = [
	//   {
	//   id: 1,
	//   notify: true,
	//   read: false,
	//   from: 'MediaServer',
	//   image: 'https://cdn.nomercy.tv/img/favicon.ico',
	//   title: 'Title',
	//   body: 'Test notification from your MediaServer',
	//   date,
	// }, {
	//   id: 2,
	//   notify: true,
	//   read: false,
	//   from: 'Api',
	//   image: 'https://cdn.nomercy.tv/img/bot.svg',
	//   title: 'Title',
	//   body: 'Test notification from your the Api',
	//   date,
	// }, {
	//   id: 3,
	//   notify: true,
	//   read: false,
	//   from: 'NoMercy',
	//   image: 'https://cdn.nomercy.tv/img/favicon.ico',
	//   title: 'Title',
	//   body: 'Test notification from NoMercy TV',
	//   date,
	// }
];

const messages = [
	{
		id: 1,
		notify: true,
		read: false,
		from: 'Stoney_Eagle',
		image: 'https://www.gravatar.com/avatar/d34d156085c5ebf680bb24fe595c694b?d=monsterid&r=pg&s=50',
		title: 'Welcome to NoMercy TV',
		body: 'This app is still in alpha stage, feel free to test it but no guarantees are given.',
		date,
	},
];

export const serverData = (servers, route, body = {}) => {

	return new Promise(async (resolve, reject) => {
		const external = [];
		const promisses = [];

		for (let i = 0; i < servers?.length ?? 0; i += 1) {
			promisses.push(appApiClient.post(`${servers[i]}/api/${route}`, body)
				.catch(err => null).then((response) => response ? response.length > 0 ? external.push(...response) : external.push(response) : null))
		}

		return Promise.all(promisses).then(() => resolve(external));

	});

};

export default {
	serverData,
	messages,
	notifications,
};
