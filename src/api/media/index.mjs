import { shuffle, unique } from '../../helpers/stringArray.mjs';

import { prismaMedia } from '../../config/database.mjs';
import { serverData } from '../data.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
	const servers = req.body.servers?.filter(s => !s.includes(config.server_id)) ?? [];

	const external = [];
	const movies = [];
	const tvs = [];
	const translation = [];
	const genres = [];
	
	
	await Promise.all([
		serverData(servers, 'home')
			.then(data => external.push(...data)),
		prismaMedia.movie.findMany(movieQuery)
			.then(data => movies.push(...data)),
		prismaMedia.tv.findMany(tvQuery)
			.then(data => tvs.push(...data)),
		prismaMedia.movie_genre.findMany({
			orderBy: {
				name: 'asc',
			}
		})
			.then(data => genres.push(...data))
	]);
	
	await Promise.all([
		prismaMedia.translation.findMany(translationQuery({tvs, movies, language}))
			.then(data => translation.push(...data)),
	]);

	const data = unique(shuffle([
		...external,
		...tvs.map(tv => {
			const title = translation.find((t) => t.translationable_type == 'tv' && t.translationable_id == tv.id)?.title || tv.title;
			const overview = translation.find((t) => t.translationable_type == 'tv' && t.translationable_id == tv.id)?.overview || tv.overview;
			return {
				...tv,
				title: title[0].toUpperCase() + title.slice(1),
				overview: overview || tv.overview,
				type: 'tv'
			};
		}),
		...movies.map(movie => {
			const title = translation.find((t) => t.translationable_type == 'movie' && t.translationable_id == movie.id)?.title || movie.title;
			const overview = translation.find((t) => t.translationable_type == 'movie' && t.translationable_id == movie.id)?.overview || movie.overview;
			return {
				...movie,
				title: title[0].toUpperCase() + title.slice(1),
				overview: overview || tv.overview,
				type: 'movie'
			};
		}),
	]), 'id');

	let body = {};

	genres.map(g => {
		body[g.name] = data.filter(d => (d.genre_movie && d.genre_movie.map(g => g.genreId).includes(g.id)) || (d.genre_tv && d.genre_tv.map(g => g.genreId).includes(g.id)))
			.map(d => {
				return {
					id: d.id,
					poster: d.poster,
					backdrop: d.backdrop,
					title: d.title,
					overview: d.overview,
					type: d.type,
				};
			}).slice(0, 35);
		return;
	});

	return res.json(body);
};

const movieQuery = {
	where: {
		folder: {
			not: null,
		},
	},
	select: {
		id: true,
		poster: true,
		backdrop: true,
		genre_movie: true,
		title: true,
		overview: true,
	}
};

const tvQuery = {
	where: {
		have_episodes: {
			gt: 0
		}
	},
	select: {
		id: true,
		poster: true,
		backdrop: true,
		title: true,
		overview: true,
		genre_tv: true,
	}
};

const translationQuery = ({tvs, movies, language}) => {
	return {
		where: {
			translationable_id: {
				in: [
					...tvs.map((t) => t.id),
					...movies.map((t) => t.id)
				],
			},
			iso_639_1: language,
			translationable_type: {
				in: ['movie', 'tv'],
			},
		},
	}
}