import { sort_by, unique } from '../../helpers/stringArray.mjs';

import { createTitleSort } from '../../controllers/file/filenameParser.mjs';
import { prismaMedia } from '../../config/database.mjs';
import { serverData } from '../data.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';

	const translation = [];
	const tvIds = [];
	const movieIds = [];
	const specialIds = [];
	const items = [];

	await Promise.all([

		prismaMedia.tv.findMany(tvQuery(req.body.tv))
			.then(data => {
				tvIds.push(...data.map(d => d.id));
				items.push(...data.map(d => {
					return {
						...d,
						type: 'tv',
					};
				}));
			})
			.finally(async () => {
				await prismaMedia.translation.findMany(translationQuery({ tvIds, language }))
					.then(data => translation.push(...data));
			}),

		prismaMedia.movie.findMany(movieQuery(req.body.movie))
			.then(data => {
				movieIds.push(...data.map(d => d.id));
				items.push(...data.map(d => {
					return {
						...d,
						type: 'movie',
					};
				}));
			})
			.finally(async () => {
				await prismaMedia.translation.findMany(translationQuery({ movieIds, language }))
					.then(data => translation.push(...data));
			}),

		prismaMedia.special.findMany(specialQuery(req.body.special))
			.then(data => {
				specialIds.push(...data.map(d => d.id));
				items.push(...data.map(d => {
					return {
						...d,
						type: 'special',
						title_sort: createTitleSort(d.title),
					};
				}));
			})
			.finally(async () => {
				await prismaMedia.translation.findMany(translationQuery({ specialIds, language }))
					.then(data => translation.push(...data));
			})
	]);
    
	return res.json(items);
}

const tvQuery = (ids) => {
	return {
		where: {
			id: { in: ids }
		},
		select: {
			id: true,
			poster: true,
			backdrop: true,
			title: true,
			title_sort: true,
		}
	};
};
const movieQuery = (ids) => {
	return {
		where: {
			id: { in: ids }
		},
		select: {
			id: true,
			poster: true,
			backdrop: true,
			title: true,
			title_sort: true,
		}
	};
};
const specialQuery = (ids) => {
	return {
		where: {
			id: { in: ids }
		},
		select: {
			id: true,
			poster: true,
			backdrop: true,
			title: true,
		}
	};
};

const translationQuery = ({ ids, language }) => {
	return {
		where: {
			translationable_id: {
				in: ids
			},
			iso_639_1: language,
		}
	};
};