import { sort_by, unique } from '../../helpers/stringArray.mjs';

import appApiClient from '../../loaders/axios.mjs';
import { prismaMedia } from '../../config/database.mjs';
import { serverData } from '../data.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
	const servers = req.body.servers?.filter(s => !s.includes(config.server_id));

	const external = [];
	const translation = [];
	let collection = [];

	await Promise.all([

		serverData(servers, `collection/${req.params.id}`)
			.then(data => external.push(...data)),

		prismaMedia.collection.findFirst(collectionQuery({ id: req.params.id }))
			.then(async data => {
				collection = data;
				const ids = [
					parseInt(req.params.id),
					...data?.collection_movie.map(t => t.movieId) || [],
				]

				await prismaMedia.translation.findMany(translationQuery({ ids, language }))
					.then(data => translation.push(...data));
			})
		]);

	let body;
	
	if (collection) {
		collection.media_type = 'movie';
		collection.type = 'movie';
		collection.title = translation.find(t => t.translationable_id == req.params.id)?.title || collection.title;
		collection.overview = translation.find(t => t.translationable_id == req.params.id)?.overview || collection.overview;
		const movies = collection.collection_movie.map(c => c.movie || c).map(m => {

			let title = translation.find(t => t.translationable_id == m.id)?.title || m.title;
			let overview = translation.find(t => t.translationable_id == m.id)?.overview || m.overview;

			return {
				...m,
				title: title || m.title,
				overview: overview || m.overview,
				type: 'movie',
			};
		})

		collection.collection_movie = undefined;
		collection.movies = unique([
			...movies,
			...external.flat(Infinity)?.[0]?.movies || []
		], 'id');

		body = collection;

	}
	else {
		body = unique(external, 'id')?.[0];
	}

	return res.json(body);

}

const collectionQuery = ({ id }) => {
	return {
		where: {
			id: parseInt(id),
		},
		include: {
			collection_movie: {
				include: {
					movie: {
						select: {
							id: true,
							backdrop: true,
							poster: true,
							overview: true,
							title: true,
							title_sort: true,
						},
					}
				}

			}
		},
	};
};

const translationQuery = ({ ids, language }) => {
	return {
		where: {
			translationable_id: {
				in: ids,
			},
			iso_639_1: language,
			translationable_type: {
				in: ['movie', 'collection'],
			},
		}
	}
}