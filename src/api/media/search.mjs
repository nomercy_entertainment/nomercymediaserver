import { parseYear } from '../../helpers/dateTime.mjs';
import { searchMulti } from '../../providers/themoviedb/client/search.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
  let year;

  if(req.query.query.match(/\d{4}/)){
    year = req.query.query.match(/\d{4}/)[0]
  }

  let query = req.query.query.replace(year, '');

  let results = await searchMulti(query, year);

  results = results.map(r => {
    return {
      ...r,
      year: parseYear(r.release_date || r.first_air_date),
    }
  });

  return res.json(results);
};