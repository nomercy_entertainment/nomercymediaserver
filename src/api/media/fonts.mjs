import { fileURLToPath } from 'url';
import fs from 'fs';
import path from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

  let fontsFolder = __dirname + '/public/fonts';

  if(!fs.existsSync(fontsFolder)){
    fs.mkdirSync(fontsFolder);
  }

  let list = fs.readdirSync(fontsFolder);
  
  const body = list.map(f => {
    return '/fonts/' + f;
  })

  return res.json(body);
};