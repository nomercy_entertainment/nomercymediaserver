import { convertToHuman, convertToSeconds } from '../../../helpers/dateTime.mjs';

import fs from 'fs';
import i18next from '../../../config/i18next.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import { serverData } from '../../data.mjs';

const playlist = async ({ id, servers, language, country, user_id, server_id }) => {

	const external = [];

	await Promise.all([
		await serverData(servers, `tv/${id}/playlist`)
			.then(data => external.push(...data.filter(d => !d?.status))),
	])

	await Promise.all([

		prismaMedia.tv.findFirst({
			where: {
				id: parseInt(id),
			},
			select: { id: true }
		})
			.catch(async err => {
				if (external?.[0]) {
					await TV(id);
				}
			})

	]);

	const translation = [];
	const media = [];
	const progress = [];
	const files = [];
	let length = 0;
	let navigationY = 0;

	return prismaMedia.tv.findFirst(tvQuery({ id, language, country }))
		.then(async (tv) => {

			if (!tv || !tv.season || tv?.season?.map(s => s.episodes?.map(e => e.video_file).flat()).flat().flat().length == 0 && external?.[0]) {
				if (external.length > 0 ){
					return external;
				}
				else {
					return [];
				}
			}

			const seasonIds = tv.season.map((s) => s.id);
			const episodeIds = tv.season.map((s) => s.episode.map((e) => e.id)).flat();

			await Promise.all([
				prismaMedia.media.findMany(
					mediaQuery(id))
					.then((data) => media.push(...data)),

				prismaMedia.translation.findMany(
					translationQuery({ id, seasonIds, episodeIds, language }))
					.then((data) => translation.push(...data)),

				prismaMedia.video_progress.findMany(
					progressQuery({ id, user_id }))
					.then((data) => progress.push(...data)),
			]);

			tv.season.map(function (season) {
				navigationY = 0;

				const seasonTranslations = translation.find(t => t.translationable_type == 'season' && t.translationable_id == season.id);

				season?.episode.map(function (episode) {

					const externalFile = external?.find(e => e.id == episode.id && e.duration != null);

					if ((episode.video_file[0] != null && episode.video_file[0].duration != null) || externalFile) {

						episode.video_file = episode.video_file?.[0] || externalFile;

						if (episode.video_file) {
							length += convertToSeconds(episode.video_file?.duration);
						}

						if (!episode.video_file[0] && externalFile) {
							files.push(externalFile);
							return;
						}

						const episodeTranslations = translation.find(t => t.translationable_type == 'episode' && t.translationable_id == episode.id);

						const overview = episodeTranslations?.overview || episode.overview;
						const title = episodeTranslations?.title || episode.title;

						const data = {};
						data.id = episode.id;
						data.title = title;
						data.navigationY = navigationY;
						data.description = overview;
						data.image = episode.still;
						data.duration = episode.video_file.duration;

						data.poster = tv.poster
							? tv.poster
							: null;
						data.backdrop = tv.backdrop
							? tv.backdrop
							: null;

						data.image = (data.still || tv.poster)
							? data.still || tv.poster
							: null,

							data.season_image = season.poster;
						data.year = tv.first_air_date.split('-')[0];
						data.image = episode.still
							? episode.still
							: null;
						data.season_image = data.season_image
							? data.season_image
							: null;
						data.video_type = tv.media_type;
						data.tmdbid = tv.id;
						data.production = tv.status == 'Ended' ? false : true;
						data.season = episode.season_number;
						data.episode = episode.episode_number;
						data.season_title = seasonTranslations?.title || season.title;
						data.season_overview = seasonTranslations?.overview || season.overview;
						data.season_id = season.id;
						data.episode_id = episode.id;
						data.origin = server_id;
						data.uuid = tv.id + '' + episode.id;
						data.tmdbid = tv.id;
						data.show = translation.find(t => t.translationable_type == 'tv')?.title || tv.title;
						data.video_type = 'tv';
						data.playlist_type = 'tv';
						data.playlist_id = tv.id;
						data.logo = media[0]?.src || null;
						data.rating = tv.certification_tv.map(cr => {
							return {
								country: cr.iso_3166_1,
								rating: cr.certification.rating,
								meaning: cr.certification.meaning,
								image: `/${cr.iso_3166_1}/${cr.iso_3166_1}_${cr.certification.rating}.svg`
							};
						})?.[0] || {};

						data.progress = (progress.find(p => p.video_id == tv.id + '' + episode.id)?.time / convertToSeconds(episode.video_file.duration)) * 100 || null;

						data.textTracks = [];

						// if (fs.existsSync(episode.video_file.host_folder + 'subtitles')) {
						//   data.textTracks = fs.readdirSync(episode.video_file.host_folder + 'subtitles').filter(f => !f.match(/-\w{5,}\.\w{3}$/)).filter(f => f.endsWith('.ass') || f.endsWith('.vtt')).map(function (t) {

						//     const reg = /.*?[\\/]?(?<fileName>.*(?<lang>\w{3}).(?<type>\w{3,4}).(?<ext>\w{3}))$/.exec(t);
						//     if (reg) {
						//       return {
						//         label:
						//           i18next.t(`languages:${reg.groups.lang}`) +
						//           (reg.groups.type !== "full" ? ` ${reg.groups.type}` : "") + (reg.groups.ext === 'ass' ? ' (S)' : ''),

						//         src: `/${episode.video_file.share}/${episode.video_file.folder}subtitles/${t}`,

						//         srclang: i18next.t(`languages:${reg.groups.lang}`),
						//         language: reg.groups.lang,
						//         kind: "subtitles",
						//       }
						//     }
						//   });
						// }

						let search = false;

						episode.video_file.subtitles.split(',').filter(s => !s.match(/\d/)).forEach(sub => {
							const [lang, type, ext] = sub.split('.');
							if (lang) {
								if (ext == 'ass') {
									search = true;
								}
								data.textTracks.push({
									label: type,
									type,
									src: `/${episode.video_file.share}/${episode.video_file.folder}subtitles/${episode.video_file.filename.replace(/\.mp4|\.m3u8/, '')}.${lang}.${type}.${ext}`,
									srclang: i18next.t(`languages:${lang}`),
									ext: ext,
									language: lang,
									kind: "subtitles",
								});
							}
						});

						data.fonts = [];
						data.fontsfile = '';

						if (search && fs.existsSync(episode.video_file.folder + 'fonts.json')) {
							data.fonts = JSON.parse(fs.readFileSync(episode.video_file.folder + 'fonts.json', 'utf8')).map(f => {
								return {
									...f,
									file: `/${video_file?.share}/${video_file?.folder}fonts/${f.file}`,
								};
							});
							data.fontsfile = `/${episode.video_file.share}/${episode.video_file.folder}fonts.json`;
						}

						data.sources = [{
							src: `/${episode.video_file.share}/${episode.video_file.folder}${episode.video_file.filename}`,
							type: episode.video_file.filename.includes('.mp4') ? 'video/mp4' : 'application/x-mpegURL',
						}];

						data.tracks = [
							{
								file: `/${episode.video_file.share}/${episode.video_file.folder}previews.vtt`,
								kind: "thumbnails"
							},
							{
								file: `/${episode.video_file.share}/${episode.video_file.folder}chapters.vtt`,
								kind: "chapters"
							},
							{
								file: `/${episode.video_file.share}/${episode.video_file.folder}sprite.webp`,
								kind: "sprite"
							},
							{
								file: `/${episode.video_file.share}/${episode.video_file.folder}fonts.json`,
								kind: "fonts"
							}
						];

						files.push(data);
						navigationY += 1;
					}
					return;
				});
			});

			// console.log(convertToHuman(length));

			return files.filter(f => f.season != 0)
				.concat(...files.filter(f => f.season == 0)).filter((s) => s.id);
		});
};

export default playlist;

const tvQuery = ({ id, language, country }) => {
	return {
		where: {
			id: parseInt(id),
		},
		include: {
			certification_tv: {
				where: {
					iso_3166_1: {
						in: [language.toUpperCase(), country]
					}
				},
				include: {
					certification: true,
				},
			},
			season: {
				orderBy: {
					season_number: 'asc',
				},
				include: {
					episode: {
						orderBy: {
							episode_number: 'asc',
						},
						include: {
							video_file: true
						}
					},
				}
			}
		}
	};
};

const translationQuery = ({ id, seasonIds, episodeIds, language }) => {
	return {
		where: {
			OR: [
				{
					translationable_type: 'tv',
					iso_639_1: language,
					translationable_id: parseInt(id),
				},
				{
					translationable_type: 'season',
					iso_639_1: language,
					translationable_id: {
						in: seasonIds,
					},
				},
				{
					translationable_type: 'episode',
					iso_639_1: language,
					translationable_id: {
						in: episodeIds,
					},
				},
			],
		},
		select: {
			title: true,
			overview: true,
			iso_639_1: true,
			translationable_type: true,
			translationable_id: true,
		}
	};
};

const mediaQuery = (id) => {
	return {
		where: {
			mediaable_id: parseInt(id),
			mediaable_type: 'tv',
			type: 'logo',
		}
	};
};

const progressQuery = ({ id, user_id }) => {
	return {
		where: {
			profile_id: user_id,
			type: "tv",
			tmdb_id: parseInt(id),
		},
	};
};