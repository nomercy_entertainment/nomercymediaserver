import appApiClient from '../../../loaders/axios.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import { serverData } from '../../data.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const external = [];
	const files = [];
	const servers = req.body.servers?.filter(s => !s.includes(config.server_id)) ?? [];

	await	serverData(servers, `tv/${req.params.id}/missing`)
			.then(data => external.push(...data)),

	prismaMedia.tv.findFirst(tvQuery(req.params.id))
		.then(async tv => {

			tv.season.map(function (season) {
				season.episode.map(function (episode) {
					if (episode.video_file.length == 0) {
						files.push({
							id: episode.id,
							season_number: episode.season_number,
							episode_number: episode.episode_number,
							title: episode.title,
							air_date: episode.air_date,
							still: episode.still,
							seasonId: episode.seasonId,
							tvId: episode.tvId,
						});
					}
				});
			});

			const data = [
				...external,
				...files,
			];

			return res.json(data);

		})
		.catch(err => {
			return res.status(500).json({
				error: err.message
			});
		});
};


const tvQuery = (id) => {
	return {
		where: {
			id: parseInt(id),
		},
		include: {
			season: {
				orderBy: {
					season_number: 'asc',
				},
				include: {
					episode: {
						orderBy: {
							episode_number: 'asc',
						},
						include: {
							video_file: true,
						},
					},
				},
			},
		},
	};
};