import { Unique, groupBy, sortByPosterAlfabetized, unique } from '../../../helpers/stringArray.mjs';

import TV from '../../../providers/themoviedb/database/tv.mjs';
import appApiClient from '../../../loaders/axios.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import requestCountry from 'request-country';
import { serverData } from '../../data.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
	const servers = req.body.servers?.filter(s => !s.includes(config.server_id)) ?? [];
	const country = requestCountry(req, 'US');

	const external = [];
	const guests = [];
	const languages = [];
	const subtitles = [];
	const media = [];
	const translation = [];
	const similar = [];
	const recommendation = [];

	const haveTv = await prismaMedia.tv.findFirst({
		where: {
			id: parseInt(req.params.id),
		},
		include: {
			season: {
				include: {
					episode: {
						include: {
							video_file: true,
						}
					}
				}
			}
		}
	})
		.catch(async err => null);

	await Promise.all([
		await serverData(servers, req.url)
			.then(data => external.push(...data.filter(e => e.seasons?.map(s => s.episodes.map(e => e.video_file).flat()).flat().flat().length > 0))),
	]);

	if (!haveTv?.season?.map(s => s.episode.map(e => e.video_file).flat()).flat().flat().length > 0 && external?.[0]) {
		return res.json(external[0]);
	}
	if (!haveTv) {
		await TV(req.params.id);
	}

	prismaMedia.tv.findFirst(tvQuery(req.params.id))
		.then(async (tv) => {

			const seasonIds = tv.season.map((s) => s.id);
			const episodeIds = tv.season.map((s) => s.episode.map((e) => e.id)).flat();
			const cast = tv.cast_tv.map((c) => {
				const deceased = !!c.cast.cast_person[0]?.person?.deathday ?? false;
				delete c.cast.cast_person;
				return {
					...c.cast,
					deceased
				}
			});
			const crew = tv.crew_tv.filter((c) => c.crew.known_for_department && !c.crew.known_for_department.toLowerCase().includes('acting')).map((c) => {
				const deceased = !!c.crew.crew_person[0]?.person?.deathday ?? false;
				delete c.crew.crew_person;
				return {
					...c.crew,
					deceased
				}
			});
			
			const genres = tv.genre_tv.map((c) => c.movie_genre.name);

			await Promise.all([
				await prismaMedia.media.findMany(
					mediaQuery({ id: req.params.id, seasonIds, episodeIds, country, language }))
					.then((data) => media.push(...data)),

				await prismaMedia.translation.findMany(
					translationQuery({ id: req.params.id, seasonIds, episodeIds, language }))
					.then((data) => translation.push(...data)),

				await prismaMedia.similar.findMany(
					similarQuery(req.params.id))
					.then((data) => similar.push(...data)),

				await prismaMedia.recommendation.findMany(
					recommendationQuery(req.params.id))
					.then((data) => recommendation.push(...data))
			]);

			const shows = await prismaMedia.tv.findMany({
				where: {
					id: {
						in: recommendation.concat(similar).map((s) => s.media_id)
					}
				},
			})

			const medias = groupBy(media, 'type');

			let have_episodes = tv.have_episodes;

			tv.season.map(function (season) {
				season.title = translation.find((t) => t.translationable_type == 'season' && t.translationable_id == season.id)?.title || season.title;
				season.overview = translation.find((t) => t.translationable_type == 'season' && t.translationable_id == season.id)?.overview || season.overview;

				cast.push(...season.cast_season.map((c) => {
					const deceased = !!c.cast.cast_person[0]?.person.deathday ?? false;
					delete c.cast.cast_person;
					return {
						...c.cast,
						deceased
					}
				}));
				crew.push(...season.crew_season.filter((c) => c.crew.known_for_department && !c.crew.known_for_department.toLowerCase().includes('acting')).map((c) => {
					const deceased = !!c.crew.crew_person[0]?.person.deathday ?? false;
					delete c.crew.crew_person;
					return {
						...c.crew,
						deceased
					}
				}));
				delete season.cast_season;
				delete season.crew_season;

				season.episode.map(function (episode) {

					cast.push(...episode.cast_episode.map((c) => {
						const deceased = !!c.cast.cast_person[0]?.person.deathday ?? false;
						delete c.cast.cast_person;
						return {
							...c.cast,
							deceased
						}
					}));
					crew.push(...episode.crew_episode.filter((c) => c.crew.known_for_department && !c.crew.known_for_department.toLowerCase().includes('acting')).map((c) => {
						const deceased = !!c.crew.crew_person[0]?.person.deathday ?? false;
						delete c.crew.crew_person;
						return {
							...c.crew,
							deceased
						}
					}));
					guests.push(...episode.episode_guest_star.map((c) => {
						const deceased = !!c.guest_star.guest_star_person[0]?.person.deathday ?? false;
						delete c.guest_star.guest_star_person;
						return {
							...c.guest_star,
							deceased
						}
					}));

					const externalFile = external.find(e => e?.episodeId == episode.id && e.duration != null) || null;

					if ((episode.video_file[0] != null && episode.video_file[0].duration != null) || externalFile) {

						episode.video_file = episode.video_file?.[0] || externalFile;

						if (!episode.video_file?.[0] && externalFile) {
							have_episodes += 1;
							return externalFile;
						}

						episode.video_file.languages
							.split(',')
							.filter((l) => l !== '')
							.map((l) => languages.push(l));

						episode.video_file.subtitles
							.split(',')
							.filter((l) => l !== '')
							.map((l) => subtitles.push(l.split('.')[0]));

						episode.title = translation.find((t) => t.translationable_type == 'episode' && t.translationable_id == episode.id)?.title || episode.title;
						episode.overview = translation.find((t) => t.translationable_type == 'episode' && t.translationable_id == episode.id)?.overview || episode.overview;
					}

					delete episode.cast_episode;
					delete episode.crew_episode;
					delete episode.episode_guest_star;

				});
				season.episodes = season.episode;
				delete season.episode;
			});

			tv.seasons = tv.season;
			delete tv.season;

			tv.title = translation.find((t) => t.translationable_type == 'tv' && t.translationable_id == req.params.id)?.title || tv.title;
			tv.overview = translation.find((t) => t.translationable_type == 'tv' && t.translationable_id == req.params.id)?.overview || tv.overview;

			tv.backdrops = medias.backdrop ? medias.backdrop.map((t) => t.src).filter(Unique) : [];
			tv.posters = medias.poster ? medias.poster.map((t) => t.src).filter(Unique) : [];
			tv.videos = medias.Trailer || [];

			tv.cast = sortByPosterAlfabetized(cast, 'order', 'name');
			tv.crew = sortByPosterAlfabetized(crew, 'name', 'name');
			tv.guests = sortByPosterAlfabetized(guests, 'name', 'name');

			tv.lead_actors = tv.cast_tv.slice(0, 9).map((c) => c.cast.name);
			tv.creators = tv.creator_tv.map((c) => c.creator.name);
			
			tv.directors = tv.crew_tv.filter((c) => c.crew.job && c.crew.job.toLowerCase().includes('director')).map((c) => c.crew.name);
			tv.producers = tv.crew_tv.filter((c) => c.crew.job && c.crew.job.toLowerCase().includes('executive producer')).map((c) => c.crew.name);

			tv.genres = genres;
			tv.languages = languages.filter(Unique);
			tv.subtitles = subtitles.filter(Unique).filter((s) => !s.match(/\d/));
			
			tv.recommendations = recommendation.slice(0, 63).map(r => {
				return {
					...r,
					have_episodes: shows.find(s => s.id == r.media_id)?.have_episodes ?? null,
					number_of_episodes: shows.find(s => s.id == r.media_id)?.number_of_episodes ?? null,
				}
			});
			tv.similar = similar.slice(0, 63).map(r => {
				return {
					...r,
					have_episodes: shows.find(s => s.id == r.media_id)?.have_episodes ?? null,
					number_of_episodes: shows.find(s => s.id == r.media_id)?.number_of_episodes ?? null,
				}
			});

			tv.media_type = 'tv';
			tv.type = 'tv';
			tv.have_episodes = have_episodes;
			tv.duration = tv.duration;

			delete tv.cast_tv;
			delete tv.crew_tv;
			delete tv.creator_tv;
			delete tv.genre_tv;

			return res.json(tv);
		})
		.catch((err) => {
			console.log(err);
			return res.status(500).json({
				success: false,
				error: err
			});
		});
};

const tvQuery = (id) => {
	return {
		where: {
			id: parseInt(id),
		},
		include: {
			season: {
				orderBy: {
					season_number: 'asc',
				},
				include: {
					cast_season: {
						include: {
							cast: {
								include: {
									cast_person: {
										include: {
											person: true,
										},
									},
								},
							},
						},
					},
					crew_season: {
						include: {
							crew: {
								include: {
									crew_person: {
										include: {
											person: true,
										},
									},
								},
							},
						},
					},
					episode: {
						orderBy: {
							episode_number: 'asc',
						},
						include: {
							cast_episode: {
								include: {
									cast: {
										include: {
											cast_person: {
												include: {
													person: true,
												},
											},
										},
									},
								},
							},
							episode_guest_star: {
								include: {
									guest_star: {
										include: {
											guest_star_person: {
												include: {
													person: true,
												},
											},
										},
									},
								},
							},
							crew_episode: {
								include: {
									crew: {
										include: {
											crew_person: {
												include: {
													person: true,
												},
											},
										},
									},
								},
							},
							video_file: true,
						},
					},
				},
			},
			cast_tv: {
				include: {
					cast: {
						include: {
							cast_person: {
								include: {
									person: true,
								},
							},
						},
					},
				},
			},
			crew_tv: {
				include: {
					crew: { 
						include: {
							crew_person: {
								include: {
									person: true,
								},
							},
						},
					},
				},
			},
			creator_tv: {
				include: {
					creator:  true,
				},
			},
			genre_tv: {
				include: {
					movie_genre: true,
				},
			},
		},
	};
};

const mediaQuery = ({ id, seasonIds, episodeIds, country, language }) => {
	return {
		where: {
			OR: [
				{
					mediaable_type: 'tv',
					mediaable_id: parseInt(id),
				},
				{
					mediaable_type: 'season',
					mediaable_id: {
						in: seasonIds,
					},
				},
				{
					mediaable_type: 'episode',
					mediaable_id: {
						in: episodeIds,
					},
				},
			],
			// iso_639_1: {
			// 	in: [country, language.toUpperCase(), 'null', 'US'],
			// }
		},
		select: {
			id: true,
			src: true,
			type: true,
			name: true,
			mediaable_type: true,
			mediaable_id: true,
			iso_639_1: true,
		},
		orderBy: {
			mediaable_id: 'asc',
		}
	};
};

const translationQuery = ({ id, seasonIds, episodeIds, language }) => {
	return {
		where: {
			OR: [
				{
					translationable_type: 'tv',
					iso_639_1: language,
					translationable_id: parseInt(id),
				},
				{
					translationable_type: 'season',
					iso_639_1: language,
					translationable_id: {
						in: seasonIds,
					},
				},
				{
					translationable_type: 'episode',
					iso_639_1: language,
					translationable_id: {
						in: episodeIds,
					},
				},
			],
		},
		select: {
			title: true,
			overview: true,
			iso_639_1: true,
			translationable_type: true,
			translationable_id: true,
		}
	};
};

const similarQuery = (id) => {
	return {
		where: {
			similarable_type: 'tv',
			similarable_id: parseInt(id),
		},
	};
};

const recommendationQuery = (id) => {
	return {
		where: {
			recommendationable_type: 'tv',
			recommendationable_id: parseInt(id),
		},
	};
};