import { sort_by, unique } from '../../helpers/stringArray.mjs';

import appApiClient from '../../loaders/axios.mjs';
import { prismaMedia } from '../../config/database.mjs';
import { serverData } from '../data.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
  let servers = req.body.servers?.filter(s => !s.includes(config.server_id));
  
  let external = [];
  // if(servers){
  //   await Promise.all(
  //     servers.map(async (s) => {
  //       let response = await appApiClient.post(`${s}/api/collections`)
  //       .catch(err => {
					
	// 			});
  //       if(response){
  //         external.push(...response);
  //       }
  //     }));
  // }
	await Promise.all([
		await serverData(servers, `collections`)
			.then(data => external.push(...data)),
	]);

  let collections = await prismaMedia.collection.findMany({
    include: {
      collection_movie: true,
    },
  });
		
	const translation = await prismaMedia.translation.findMany({
		where: {
			translationable_id: {
				in: collections.map(t => t.id),
			},
			iso_639_1: language,
			translationable_type: 'collection',
		}
	});
		
  collections = collections.map(c => {
		let title = translation.find(t => t.translationable_id == c.id)?.title || c.title;

    return {
      id: c.id,
      title: title[0].toUpperCase() + title.slice(1) || c.title,
      title_sort: c.title_sort[0] ? c.title_sort[0].toUpperCase() + c.title_sort.slice(1) : c.title_sort,
      poster: c.poster,
      have_parts: c.collection_movie.length,
      media_type: 'collection',
      type: 'collection',
    }
  })
  
  collections = unique([
    ...collections,
    ...external
  ], 'id');

  let body = sort_by(collections, 'title_sort');

  return res.json(body);

};