import { sort_by, unique } from '../../helpers/stringArray.mjs';

import { createTitleSort } from '../../controllers/file/filenameParser.mjs';
import { prismaMedia } from '../../config/database.mjs';
import { serverData } from '../data.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
	const servers = req.body.servers?.filter(s => !s.includes(config.server_id)) ?? [];

	const external = [];
	const translation = [];
	const ids = [];
	let tvs = [];

	await Promise.all([

		await serverData(servers, 'anime')
			.then(data => data ? external.push(...data.flat()) : null),

		prismaMedia.tv.findMany(tvQuery)
			.then(data => tvs.push(...data)),

		prismaMedia.tv.findMany({
			where: {
				media_type: 'anime',
				folder: {
					not: null,
				},
			},
			select: { id: true }
		})
			.then(data => ids.push(...data.map(d => d.id)))
			.finally(async () => {
				await prismaMedia.translation.findMany(translationQuery({ ids, language }))
					.then(data => translation.push(...data));
			})
	]);

	const data = tvs.map(tv => {

		const title = translation.find(t => t.translationable_type == 'tv' && t.translationable_id == tv.id)?.title || tv.title;

		const files = [
			...tv.season.filter(t => t.season_number > 0).map(s => s.episode.map(e => e.video_file).flat()).flat().map(f => f.episodeId),
			...external.find(t => t.id == tv.id && t.files)?.files ?? []
		]
			.filter((v, i, a) => a.indexOf(v) === i);

		// delete tv.season;

		return {
			id: tv.id,
			poster: tv.poster,
			title: title[0].toUpperCase() + title.slice(1),
			title_sort: createTitleSort(title[0].toUpperCase() + title.slice(1)),
			type: 'tv',
			media_type: 'tv',
			have_episodes: files.length,
			number_of_episodes: tv.number_of_episodes,
			files: servers?.length > 0 ? undefined : files,
		};
	});

	tvs = unique([
		...data,
		...external
	], 'id');

	const body = sort_by(tvs, 'title_sort');

	return res.json(body);
}

const tvQuery = {
	where: {
		media_type: 'anime',
		have_episodes: {
			gt: 0
		}
	},
	include: {
		season: {
			orderBy: {
				season_number: 'asc',
			},
			include: {
				episode: {
					orderBy: {
						episode_number: 'asc',
					},
					where: {
						video_file: {
							some: {
								duration: {
									not: null,
								}
							}
						}
					},
					include: {
						video_file: true
					}
				},
			}
		}
	}
};

const translationQuery = ({ id, language }) => {
	return {
		where: {
			translationable_id: id,
			iso_639_1: language,
			translationable_type: 'tv',
		}
	};
};