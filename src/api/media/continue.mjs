import { sort_updated_asc, unique } from '../../helpers/stringArray.mjs';

import appApiClient from '../../loaders/axios.mjs';
import e from 'cors';
import { prismaMedia } from '../../config/database.mjs';
import { serverData } from '../data.mjs';
import { tokenParser } from '../../helpers/tokenParser.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const servers = req.body.servers?.filter(s => !s.includes(config.server_id)) ?? [];
	const user_id = tokenParser(req)?.sub;

	const external = [];
	const movie_progress = [];
	const tv_progress = [];
	const special_progress = [];

	const tvs = [];
	const movies = [];
	const specials = [];

	await Promise.all([
		prismaMedia.video_progress.findMany(movieProgressQuery(user_id))
			.then(data => movie_progress.push(...data)),
		prismaMedia.video_progress.findMany(tvProgressQuery(user_id))
			.then(data => tv_progress.push(...data)),
		prismaMedia.video_progress.findMany(specialProgressQuery(user_id))
			.then(data => special_progress.push(...data)),
	]);

	const tvIds = unique(tv_progress, "tmdb_id")
		.map(p => parseInt(p.tmdb_id));
	const movieIds = unique(movie_progress, "tmdb_id")
		.map(p => parseInt(p.tmdb_id));
	const specialIds = unique(special_progress, "tmdb_id")
		.map(p => parseInt(p.tmdb_id));

	await Promise.all([
		serverData(servers, `select`, {
			tv: tvIds,
			movie: movieIds,
			special: specialIds
		})
			.then(data => external.push(...data)),
	]);

	await Promise.all([

		prismaMedia.tv.findMany(tvQuery(tvIds))
			.then(tv => {
				tvs.push(...[
					...external.filter(e => e.type == 'tv'),
					...tv,
				]);
			}),

		prismaMedia.movie.findMany(movieQuery(movieIds))
			.then(movie => {
				movies.push(...[
					...external.filter(e => e.type == 'movie'),
					...movie,
				]);
			}),

		prismaMedia.special.findMany(specialQuery(specialIds))
			.then(special => {
				specials.push(...[
					...external.filter(e => e.type == 'special'),
					...special,
				]);
			})
	]);

	const body = unique([
		...movies.map(i => {
			return {
				...i,
				type: 'movie',
				updated_at: movie_progress.find(p => p.tmdb_id == i.id)?.updated_at ?? i.updated_at
			};
		}),

		...tvs.map(i => {
			return {
				...i,
				type: 'tv',
				updated_at: tv_progress.find(p => p.tmdb_id == i.id)?.updated_at ?? i.updated_at
			};
		}),

		...specials.map(i => {
			return {
				...i,
				type: 'special',
				title_sort: i.title,
				updated_at: special_progress.find(p => p.tmdb_id == i.id)?.updated_at ?? i.updated_at
			};
		})
	].sort(sort_updated_asc),
		"id"
	);

	return res.json(body);

};

const movieProgressQuery = (user_id) => {
	return {
		where: {
			profile_id: user_id,
			type: "movie"
		},
		orderBy: {
			updated_at: 'desc',
		},
	};
};
const tvProgressQuery = (user_id) => {
	return {
		where: {
			profile_id: user_id,
			type: "tv"
		},
		orderBy: {
			updated_at: 'desc',
		},
	};
};
const specialProgressQuery = (user_id) => {
	return {
		where: {
			profile_id: user_id,
			type: "special"
		},
		orderBy: {
			updated_at: 'desc',
		},
	};
};

const tvQuery = (ids) => {
	return {
		where: {
			id: { in: ids }
		},
		select: {
			id: true,
			poster: true,
			backdrop: true,
			title: true,
			title_sort: true,
		}
	};
};
const movieQuery = (ids) => {
	return {
		where: {
			id: { in: ids }
		},
		select: {
			id: true,
			poster: true,
			backdrop: true,
			title: true,
			title_sort: true,
		}
	};
};
const specialQuery = (ids) => {
	return {
		where: {
			id: { in: ids }
		},
		select: {
			id: true,
			poster: true,
			backdrop: true,
			title: true,
		}
	};
};