import { prismaMedia } from '../../config/database.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';

	let movies = await prismaMedia.movie.findMany({
		where: {
			folder: {
				not: null,
			},
		},
		select: {
			id: true,
			poster: true,
			backdrop: true,
			genre_movie: true,
			title: true,
			title_sort: true,
			overview: true,
			trailer: true,
		},
	});
	let tvs = await prismaMedia.tv.findMany({
		where: {
			have_episodes: {
				gt: 0,
			},
		},
		select: {
			id: true,
			poster: true,
			backdrop: true,
			title: true,
			title_sort: true,
			overview: true,
			trailer: true,
			genre_tv: true,
		},
	});

	const translation = await prismaMedia.translation.findMany({
		where: {
			translationable_id: {
				in: [
					...tvs.map((t) => t.id), 
					...movies.map((t) => t.id)
				],
			},
			iso_639_1: language,
			translationable_type: {
				in: ['movie', 'tv'],
			},
		},
	});

	let data = [
		...tvs.map((tv) => {
			let title = translation.find((t) => t.translationable_type == 'tv' && t.translationable_id == tv.id)?.title || tv.title;
			let overview = translation.find((t) => t.translationable_type == 'tv' && t.translationable_id == tv.id)?.overview || tv.overview;

			return {
				...tv,
				title: title[0].toUpperCase() + title.slice(1),
				overview: overview || tv.overview,
				type: 'tv',
			};
		}),
		...movies.map((movie) => {
			let title = translation.find((t) => t.translationable_type == 'movie' && t.translationable_id == movie.id)?.title || movie.title;
			let overview = translation.find((t) => t.translationable_type == 'movie' && t.translationable_id == movie.id)?.overview || movie.overview;

			return {
				...movie,
				title: title[0].toUpperCase() + title.slice(1),
				overview: overview || movie.overview,
				type: 'movie',
			};
		}),
	];

	return res.json(data);
};
