import { sort_by, unique } from '../../../helpers/stringArray.mjs';

import appApiClient from '../../../loaders/axios.mjs';
import { createTitleSort } from '../../../controllers/file/filenameParser.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import { serverData } from '../../data.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
	const servers = req.body.servers?.filter(s => !s.includes(config.server_id)) ?? [];

	const external = [];
	const translation = [];
	const ids = [];


	await Promise.all([
		
		await serverData(servers, 'movies')
			.then(data => external.push(...data)),

		prismaMedia.movie.findMany({
			where: {
				folder: {
					not: null,
				},
			},
			select: { id: true }
		})
		.then(data => ids.push(...data.map(d => d.id)))
		.finally(async () => {
			await prismaMedia.translation.findMany(translationQuery({ids, language}))
				.then(response => translation.push(...response));
		})

	]);

	prismaMedia.movie.findMany(movieQuery)
		.then(async movies => {

			movies = movies
				.filter(m => m.video_file.length > 0)
				.map(movie => {

					const title = translation.find(t => t.translationable_id == movie.id)?.title2 || movie.title;

					return {
						id: movie.id,
						poster: movie.poster,
						title: title[0].toUpperCase() + title.slice(1),
						title_sort: createTitleSort(title[0].toUpperCase() + title.slice(1)),
						type: 'movie',
						media_type: 'movie',
					};
				});

			movies = unique([
				...movies,
				...external
			], 'id');

			let body = sort_by(movies, 'title_sort');

			return res.json(body);

		});
}

const movieQuery = {
	where: {
		folder: {
			not: null,
		},
	},
	include: {
		video_file: true,
	},
};

const translationQuery = ({ ids, language }) => {
	return {
		where: {
			translationable_id: {
				in: ids
			},
			iso_639_1: language,
			translationable_type: 'movie',
		},
		select: {
			title: true,
		}
	};
};