import { Unique, groupBy, sortByPosterAlfabetized } from '../../../helpers/stringArray.mjs';

import MOVIE from '../../../providers/themoviedb/database/movie.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import requestCountry from 'request-country';
import { serverData } from '../../data.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	const language = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
	const country = requestCountry(req, 'US');

	const servers = req.body.servers?.filter(s => !s.includes(config.server_id));
	const external = [];
	const media = [];
	const similar = [];
	const recommendation = [];
	const translation = [];
	let haveTv;
	let response;
	
	let movie = await prismaMedia.movie.findFirst({
		where: {
			id: parseInt(req.params.id),
		},
		include: {
			video_file: true,
		},
	});

	if (!movie || movie.video_file.length == 0 && servers) {

		await Promise.all([
			serverData(servers, `movie/${req.params.id}/info`)
				.then(data => {
					response = data.find(r => r.video?.filename);
				})
				.catch(err => null)
		]);
		if (response) {
			return res.json(response);
		}
	}
	if(!movie){
		await MOVIE(req.params.id);
	}

	await Promise.all([

		prismaMedia.movie.findFirst({
			where: {
				id: parseInt(req.params.id),
			},
			select: { id: true, video_file: true }
		})
			.catch(err => null)
			.then(data => haveTv = data),

		prismaMedia.media.findMany(mediaQuery({ id: req.params.id, country, language }))
			.then(data => media.push(...data)),

		prismaMedia.similar.findMany(similarQuery(req.params.id))
			.then(data => similar.push(...data)),

		prismaMedia.recommendation.findMany(recommendationQuery(req.params.id))
			.then(data => recommendation.push(...data)),

		prismaMedia.translation.findMany(translationQuery({ id: req.params.id, language }))
			.then(data => translation.push(...data)),
	]);

	prismaMedia.movie.findFirst(movieQuery(req.params.id))
		.then(movie => {

			
			const cast = movie.cast_movie.map((c) => {
				const deceased = !!c.cast.cast_person[0]?.person.deathday ?? false;
				delete c.cast.cast_person;
				return {
					...c.cast,
					deceased
				}
			});
			const crew = movie.crew_movie.map((c) => {
				const deceased = !!c.crew.crew_person[0]?.person.deathday ?? false;
				delete c.crew.crew_person;
				return {
					...c.crew,
					deceased
				}
			});

			const collection = movie.collection_movie.map((c) => c.collection);
			const genres = movie.genre_movie.map((c) => c.movie_genre.name);

			const medias = groupBy(media, 'type');

			movie.title = translation.find((t) => t.translationable_type == 'movie')?.title || movie.title;
			movie.overview = translation.find((t) => t.translationable_type == 'movie')?.overview || movie.overview;
			movie.backdrops = medias.backdrop ? medias.backdrop.map((m) => m.src).filter(Unique) : [];
			movie.posters = medias.poster ? medias.poster.map((m) => m.src).filter(Unique) : [];
			movie.videos = media.filter((m) => m.type == 'Trailer' || m.type == 'Clip' || m.type == 'Teaser' || m.type == 'Opening Credits').filter(Unique) || [];
			movie.trailers = medias.Trailer ? medias.Trailer.map((m) => m.src).filter(Unique) : [];

			movie.cast = sortByPosterAlfabetized(cast, 'order', 'name');
			movie.crew = sortByPosterAlfabetized(crew, 'name', 'name');
			movie.collection = collection;
			movie.genres = genres;

			movie.lead_actors = cast.slice(0, 9).map((c) => c.name);
			movie.directors = crew.filter((c) => c.department && (c.department.includes('Directing') || c.department.includes('directing'))).map((c) => c.name);

			movie.recommendations = recommendation.slice(0, 63);
			movie.similar = similar.slice(0, 63);
			movie.media_type = 'movie';
			movie.type = 'movie';
			movie.duration = movie.runtime;

			if (movie?.video_file?.length > 0) {
				movie.video = movie.video_file?.[0];
				movie.subtitles = movie.video_file?.[0]?.subtitles
					.split(',')
					.filter((s) => !s.match(/\d/))
					.map((s) => s.split('.')[0]);
				movie.languages = movie.video_file?.[0]?.languages.split(',');
			}

			delete movie.cast_movie;
			delete movie.crew_movie;
			delete movie.collection_movie;
			delete movie.genre_movie;

			let body = movie;

			return res.json(body);
		});
}


const movieQuery = (id) => {
	return {
		where: {
			id: parseInt(id),
		},
		include: {
			cast_movie: {
				include: {
					cast: {
						include: {
							cast_person: {
								include: {
									person: true,
								},
							},
						},
					},
				},
			},
			crew_movie: {
				include: {
					crew: { 
						include: {
							crew_person: {
								include: {
									person: true,
								},
							},
						},
					},
				},
			},
			collection_movie: {
				include: {
					collection: true,
				},
			},
			genre_movie: {
				include: {
					movie_genre: true,
				},
			},
			video_file: true,
		},
	};
};

const mediaQuery = ({ id, country, language }) => {
	return {
		where: {
			mediaable_type: 'movie',
			mediaable_id: parseInt(id),
			// iso_639_1: {
				// in: [country, language.toUpperCase(), 'null', 'US'],
			// }
		},
		select: {
			id: true,
			src: true,
			type: true,
			name: true,
			mediaable_type: true,
			mediaable_id: true,
			iso_639_1: true,
		},
		orderBy: {
			mediaable_id: 'asc',
		}
	};
};

const translationQuery = ({ id, language }) => {
	return {
		where: {
			translationable_type: 'movie',
			iso_639_1: language,
			translationable_id: parseInt(id),

		},
		select: {
			title: true,
			overview: true,
			iso_639_1: true,
			translationable_type: true,
			translationable_id: true,
		}
	};
};

const similarQuery = (id) => {
	return {
		where: {
			similarable_type: 'movie',
			similarable_id: parseInt(id),
		},
	};
};

const recommendationQuery = (id) => {
	return {
		where: {
			recommendationable_type: 'movie',
			recommendationable_id: parseInt(id),
		},
	};
};