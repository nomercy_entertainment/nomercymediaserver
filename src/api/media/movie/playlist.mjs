import appApiClient from '../../../loaders/axios.mjs';
import { convertToSeconds } from '../../../helpers/dateTime.mjs';
import fs from 'fs';
import i18next from '../../../config/i18next.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import { serverData } from '../../data.mjs';

const playlist = async ({ id, servers, user_id, language, country, server_id }) => {

	let movie = await prismaMedia.movie.findFirst({
		where: {
			id: parseInt(id),
		},
		include: {
			video_file: true,
		},
	});

	let response;
	if (!movie || movie.video_file.length == 0 && servers) {

		await Promise.all([
			serverData(servers, `movie/${id}/playlist`)
				.then(data => {
					response = data.find(r => r.sources);
				})
				.catch(err => null)
		]);
		if (response) {
			return [response];
		}
	}

	return prismaMedia.movie.findFirst(movieQuery({ id, language, country }))
		.then(async (movie) => {
			const translation = [];
			const media = [];
			const progress = [];

			await Promise.all([
				prismaMedia.media.findMany(
					mediaQuery(id))
					.then((data) => media.push(...data)),

				prismaMedia.translation.findMany(
					translationQuery({ id, language }))
					.then((data) => translation.push(...data)),

				prismaMedia.video_progress.findMany(
					progressQuery({ id, user_id }))
					.then((data) => progress.push(...data)),
			]);

			movie.title = translation.find((t) => t.translationable_type == 'movie')?.title || movie.title;
			movie.description = translation.find((t) => t.translationable_type == 'movie')?.overview || movie.overview;
			movie.show = translation.find((t) => t.translationable_type == 'movie')?.title || movie.title;
			movie.origin = server_id;
			movie.uuid = movie.id;
			movie.duration = movie.video_file.duration;
			movie.tmdbid = movie.id;
			movie.video_type = 'movie';
			movie.playlist_type = 'movie';
			movie.playlist_id = id;
			movie.year = movie.release_date.split('-')[0];
			movie.logo = media[0]?.src || null;
			movie.rating = movie.certification_movie?.map(cr => {
				return {
					country: cr.iso_3166_1,
					rating: cr.certification.rating,
					meaning: cr.certification.meaning,
					image: `/${cr.iso_3166_1}/${cr.iso_3166_1}_${cr.certification.rating}.svg`
				};
			})?.[0] || {};

			movie.progress = (progress.find((p) => p.video_id == movie.id)?.time / convertToSeconds(movie.video_file.duration)) * 100 || null;

			(movie.poster = movie.poster ? movie.poster : null),
				(movie.backdrop = movie.backdrop ? movie.backdrop : null),
				(movie.image = movie.poster || movie.backdrop ? movie.poster || movie.backdrop : null),
				(movie.sources = [
					{
						src: `/${movie.video_file[0]?.share}/${movie.video_file[0]?.folder}${movie.video_file[0]?.filename}`,
						type: movie.video_file[0]?.filename.includes('.mp4') ? 'video/mp4' : 'application/x-mpegURL',
					},
				]);

			movie.fonts = [];
			if (fs.existsSync(movie.video_file?.[0]?.host_folder + 'fonts.json')) {
				movie.fonts = JSON.parse(fs.readFileSync(movie.video_file?.[0]?.host_folder + 'fonts.json', 'utf8'));
			}

			(movie.fontsfile = `/${movie.video_file?.[0]?.share}/${movie.video_file?.[0]?.folder}fonts.json`),
				(movie.tracks = [
					{
						file: `/${movie.video_file[0]?.share}/${movie.video_file[0]?.folder}previews.vtt`,
						kind: 'thumbnails',
					},
					{
						file: `/${movie.video_file[0]?.share}/${movie.video_file[0]?.folder}chapters.vtt`,
						kind: 'chapters',
					},
					{
						file: `/${movie.video_file[0]?.share}/${movie.video_file[0]?.folder}sprite.webp`,
						kind: 'sprite',
					},
					{
						file: `/${movie.video_file[0]?.share}/${movie.video_file[0]?.folder}fonts.json`,
						kind: 'fonts',
					},
				]);

			// let subtitles = [];
			// if (fs.existsSync(movie.video_file?.[0]?.host_folder + 'subtitles')) {
			//   fs.readdirSync(movie.video_file?.[0]?.host_folder + 'subtitles').filter(f => f.endsWith('.ass') || f.endsWith('.vtt')).filter(f => !f.match(/-\w{5,}\.\w{3}$/)).filter(f => !f.includes('#')).forEach(function (f) {
			//     subtitles.push(f);
			//   });
			// };

			// movie.textTracks = subtitles.map(t => {
			//   const reg = /.*?[\\/]?(?<fileName>.*(?<lang>\w{3}).(?<type>\w{3,4}).(?<ext>\w{3}))$/.exec(t);
			//   return {
			//       label: i18next.t(`languages:${reg.groups?.lang}`) + (reg.groups?.type !== "full" ? ` ${reg.groups?.type}` : "") + (reg.groups?.ext === 'ass' ? ' (S)' : ''),
			//       src: `/${movie.video_file[0]?.share}/${movie.video_file[0]?.folder}subtitles/${t}`,
			//       srclang: i18next.t(`languages:${reg.groups?.lang}`),
			//       language: reg.groups.lang,
			//       kind: "subtitles",
			//   }
			// });

			movie.fonts = [];
			movie.textTracks = [];
			movie.fontsfile = '';

			movie.video_file?.[0]?.subtitles
				.split(',')
				.filter((s) => !s.match(/\d/))
				.forEach((sub) => {
					const [lang, type, ext] = sub.split('.');

					movie.textTracks.push({
						label: type,
						src: `/${movie.video_file?.[0].share}/${movie.video_file?.[0].folder}subtitles/${movie.video_file?.[0].filename.replace(/\.mp4|\.m3u8/, '')}.${lang}.${type}.${ext}`,
						srclang: i18next.t(`languages:${lang}`),
						ext: ext,
						language: lang,
						kind: 'subtitles',
					});
				});

			delete movie.video_file;
			delete movie.certification_movie;

			return [movie];
		});
};
export default playlist;



const movieQuery = ({ id, language, country }) => {
	return {
		where: {
			id: parseInt(id),
		},
		include: {
			certification_movie: {
				where: {
					iso_3166_1: {
						in: [country, language.toUpperCase()]
					}
				},
				include: {
					certification: true,
				},
			},
			video_file: true,
		},
	};
}

const translationQuery = ({ id, language }) => {
	return {
		where: {
			translationable_type: 'movie',
			iso_639_1: language,
			translationable_id: parseInt(id),
		},
		select: {
			title: true,
			overview: true,
			iso_639_1: true,
			translationable_type: true,
			translationable_id: true,
		}
	};
};

const mediaQuery = (id) => {
	return {
		where: {
			mediaable_id: parseInt(id),
			mediaable_type: 'movie',
			type: 'logo',
		},
		orderBy: {
			vote_average: 'asc',
		}
	};
};

const progressQuery = ({ id, user_id }) => {
	return {
		where: {
			profile_id: user_id,
			type: "movie",
			tmdb_id: parseInt(id),
		},
	};
};