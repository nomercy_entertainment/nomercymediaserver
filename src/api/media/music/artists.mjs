import { createTitleSort } from '../../../controllers/file/filenameParser.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import { sort_by } from '../../../helpers/stringArray.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const music = await prismaMedia.artist.findMany({
		where:{
			NOT:{
				track:{
					none:{}
				}
			}
		},
		include: {
			_count: {
				select: {
					track: true,
					album: true,
				},
			},
		},
	});

	if (music) {

		let result = {
			type: 'artists',
		};

		result.data = sort_by(music.filter(m => m._count.track > 0).map(m => {
			return {
				...m,
				type: 'artist',
				name: m.name.replace(/["'\[\]*]/g, ''),
				title_sort: createTitleSort(m.name.replace(/["'\[\]*]/g, '')),
				origin: config.server_id,
			};
		}), 'title_sort');

		return res.json(result);
	}
	else {

		return res.json({});
	}

};
