import { prismaMedia } from '../../../../config/database.mjs';
import { tokenParser } from '../../../../helpers/index.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

  const user_id = tokenParser(req)?.sub;

	if(await prismaMedia.playlist.findFirst({
		where: {
			name: req.body.name,
		}
	})){
		return res.status(401).json({
			success: false,
			message: 'This playlist already exists.',
		})
	}

	const music = await prismaMedia.playlist.create({
		data:{
			name: req.body.name,
			description: req.body.description,
			profile_id: user_id,
		}
	});

	return res.json(music);
	
};
