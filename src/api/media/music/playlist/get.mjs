import { createDataImageURL } from '../../../../helpers/imageHelper.mjs';
import fs from 'fs';
import { prismaMedia } from '../../../../config/database.mjs';
import { tokenParser } from '../../../../helpers/tokenParser.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const user_id = tokenParser(req)?.sub;

	if(isNaN(parseInt(req.params.id))){
		return res.status(400).json({
			success: false,
			message: "Invalid id",
		});
	}

	const music = await prismaMedia.playlist.findFirst({
		where: {
			id: parseInt(req.params.id),
			profile_id: user_id,
		},
		include: {
			playlistTrack: {
				include: {
					track: {
						include: {
							artist: true,
							album: true,
							favorite_track: {
								where: {
									profile_id: user_id
								}
							},
						}
					}
				},
				orderBy: {
					updated_at: 'asc'
				}
			},
		},
	});

	if (music) {
		let result = {
			type: 'playlist',
			...music,
			track: music.playlistTrack.map(t => {
				return {
					...t.track,
					cover: t.track.album[0].cover,
					type: 'playlist',
					origin: config.server_id,
					date: t.updated_at,
					favorite_track: t.track.favorite_track.length > 0,
					album: {
						id: t.track.album[0]?.id ?? null,
						name: t.track.album[0]?.name ?? null,
						artistId: t.track.album[0]?.artistId ?? null,
						cover: music.playlistTrack[0].track.album[0]?.cover ?? null,
						// cover: t.track.album[0]?.cover ? createDataImageURL(fs.readFileSync(`${config.mediaRoot}${t.track.album[0]?.cover}`), 'image/jpeg') : null,
						description: t.track.album[0]?.description ?? null,
					},
					artists: t.track.artist,
					artist: {
						id: t.track.artist[0]?.id ?? null,
						name: t.track.artist[0]?.name ?? null,
						artistId: t.track.artist[0]?.artistId ?? null,
						cover: music.playlistTrack[0].track.artist[0]?.cover ?? null,
						// cover: t.track.artist[0]?.cover ? createDataImageURL(fs.readFileSync(`${config.mediaRoot}${t.track.artist[0]?.cover}`), 'image/jpeg') : null,
						description: t.track.artist[0]?.description ?? null,
						folder: t.track.artist[0]?.folder ?? null,
					},
				};
			})
		};
		
		delete result.playlistTrack;

		return res.json(result);
	}
	else {

		return res.json({});
	}

};
