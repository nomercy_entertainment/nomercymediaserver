import { prismaMedia } from '../../../../config/database.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const music = await prismaMedia.playlist.delete({
		where: {
			id: parseInt(req.params.id),
		},

	});

	return res.json(music);
	
};
