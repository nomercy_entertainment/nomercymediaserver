import { createDataImageURL } from '../../../../helpers/imageHelper.mjs';
import fs from 'fs';
import { prismaMedia } from '../../../../config/database.mjs';
import { tokenParser } from '../../../../helpers/tokenParser.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	
  const user_id = tokenParser(req)?.sub;

	const music = await prismaMedia.playlist.findMany({
		where: {
			profile_id: user_id,
		},
		include: {
			_count: {
				select: { 
					playlistTrack: true,
				},
			},
		},
	});
	
	if (music) {
		let result = {
			type: 'playlists',
			data: music.map(m => {
				return {
					...m,
					origin: config.server_id,
					type: 'playlist',
					cover: m.cover ? createDataImageURL(fs.readFileSync(`${config.mediaRoot}${m.cover}`), 'image/jpeg') : null,
				}
			}),
		};

		return res.json(result);
	}
	else {

		return res.json({});
	}

};
