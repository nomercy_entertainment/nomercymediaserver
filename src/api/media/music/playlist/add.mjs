import { convertPath } from '../../../../helpers/system.mjs';
import { exec } from 'child_process';
import fs from 'fs';
import { imageHash } from 'image-hash';
import path from 'path';
import { platform } from 'os-utils';
import { prismaMedia } from '../../../../config/database.mjs';
import { tokenParser } from '../../../../helpers/index.mjs';
import { unique } from '../../../../helpers/stringArray.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const ffmpeg = path.join(__dirname, 'binaries', 'ffmpeg' + (platform() == 'win32' ? '.exe' : '')).replace(/\\/g, '\\');
	
	const tempDir = path.join(__dirname, '..', 'cache');

	const user_id = tokenParser(req)?.sub;

	let music = await prismaMedia.playlist.findFirst({
		where: {
			profile_id: user_id,
			id: parseInt(req.params.id),
		},
	});

	music = await prismaMedia.playlist.update({
		where: {
			playlist_unique: {
				profile_id: user_id,
				id: parseInt(req.params.id),
			}
		},
		data: {
			cover: `/Music/playlistCovers/${music.name}.jpg`,
			playlistTrack: {
				connectOrCreate: {
					where: {
						playlist_track_unique: {
							trackId: parseInt(req.body.id),
							playlistId: parseInt(req.params.id),
						}
					},
					create: {
						trackId: parseInt(req.body.id),
					}
				}
			}
		},
		include: {
			playlistTrack: {
				include: {
					track: {
						include: {
							artist: true,
							album: true,
							favorite_track: {
								where: {
									profile_id: user_id
								}
							},
						}
					}
				},
				orderBy: {
					updated_at: 'asc'
				}
			},
		},
	});

	const images = [];

	for (let i = 0; i < music.playlistTrack.length; i++) {
		const t = music.playlistTrack[i];

		if(!t.track.cover) continue;

		const image = `${config.mediaRoot}/${t.track.folder}/${t.track.cover}`;
		const hash = await createImageHash(image);

		images.push({
			hash,
			image,
			outputFile: convertPath(`${config.mediaRoot}/Music/playlistCovers/${music.name}.jpg`),
		});
		
	}

	const imageList = unique(images.sort((b, a) => a.image - b.image), 'hash');
	
	for (let i = 0; i < imageList.length; i++) {
		const t = imageList[i];

		fs.copyFileSync(t.image, `${tempDir}/thumb-${i}.jpg`);

	};

	if(!fs.existsSync(tempDir)){
		fs.mkdirSync(tempDir, { recursive: true });
	}

	if(!fs.existsSync(imageList?.[0]?.outputFile)){
		fs.rmSync(imageList[0].outputFile);
	}

	// let montageCommand = `"${ffmpeg}" -i "${tempDir}/thumb-%d.jpg" -frames:v 1 -filter_complex tile='2x2' -y "${imageList[0].outputFile}"`;

	let montageCommand = `montage "${tempDir}/thumb-*.jpg" -tile 2x2  -background '#000' "${imageList[0].outputFile}"`;
	
	exec(montageCommand, { maxBuffer: 1024 * 5000 }, (e) => {
		if (e) {
			console.error(`exec error: ${error}`);
			return;
		}
		fs.existsSync(tempDir) && fs.readdirSync(tempDir).forEach(f => fs.rmSync(`${tempDir}/${f}`));
	});
	
	return res.json(music);


};


const createImageHash = async (image) => {
	return new Promise((resolve, reject) => {
		imageHash(image, 25, true, (error, data) => {
			if (error) reject(error);

			resolve(data);
		});
	});
};