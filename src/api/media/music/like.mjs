import { prismaMedia } from '../../../config/database.mjs';
import { tokenParser } from '../../../helpers/index.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const user_id = tokenParser(req)?.sub;

	if(isNaN(parseInt(req.params.id))){
		return res.status(400).json({
			success: false,
			message: "Invalid id",
		});
	}

	let query;

	switch (req.body.value) {
		case true:
			query = {
				connectOrCreate: {
					where: {
						favorite_track_unique: {
							trackId: parseInt(req.params.id),
							profile_id: user_id,
						}
					},
					create: {
						profile_id: user_id,
					}
				}
			};
			break;

		default:
			query = {
				delete: {
					favorite_track_unique: {
						trackId: parseInt(req.params.id),
						profile_id: user_id,
					}
				}
			};
			break;
	}

		try {
			const music = await prismaMedia.track.update({
				where: {
					id: parseInt(req.params.id),
				},
				data: {
					favorite_track: query
				},
				include:{
					favorite_track: true,
				}
			});
	
			return res.json({
				...music,
				favorite_track: music.favorite_track.length > 0,
			});
			
		} catch (error) {

			const music = await prismaMedia.track.findFirst({
				where: {
					id: parseInt(req.params.id),
				},
				include:{
					favorite_track: true,
				}
			});
			
			return res.json({
				...music,
				favorite_track: music.favorite_track.length > 0,
			});
		}

};
