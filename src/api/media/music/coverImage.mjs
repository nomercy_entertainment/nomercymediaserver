import {downloadImage, storageArtistImageInDatabase} from "../../../helpers/artistImages.mjs";

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	
	const {name, image, storagePath} = req.body;
		
	downloadImage(image, config.mediaRoot + '/Music/' + storagePath)
		.then(async () => {

			await storageArtistImageInDatabase(name, storagePath);

			return res.json({
				success: true,
				message: 'Image successfully uploaded',
			});
			
		}).catch((reason) => {
			return res.status(400).json({
				success: false,
				message: 'Something went wring when downloading the image.',
				error: reason,
			});
		});
};
