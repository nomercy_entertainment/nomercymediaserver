import {artistImages} from "../../../helpers/artistImages.mjs";

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const { artist } = req.query;

    const urls = (await artistImages(artist));

    if(urls.length == 0){
        return res.status(404).json({message: "Can not find images for this artist"})
    }
    return res.json(urls);
}