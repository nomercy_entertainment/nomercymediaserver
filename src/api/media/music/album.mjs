import { trackSort, uniqBy } from '../../../helpers/stringArray.mjs';

import { prismaMedia } from '../../../config/database.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	if(isNaN(parseInt(req.params.id))){
		return res.status(400).json({
			success: false,
			message: "Invalid id",
		});
	}

	const music = await prismaMedia.album.findFirst({
		where: {
			id: parseInt(req.params.id),
		},
		include: {
			track: {
				include: {
					artist: true,
					favorite_track: true,
				}
			},
			artist: true,
		}
	});
	
	if (music) {

		let result = {
			...music,
			type: 'album',
			track: uniqBy(music.track.sort(trackSort),'name').map(t => {
				return {
					...t,
					type: 'album',
					updated_at: music.updated_at,
					favorite_track: t.favorite_track.length > 0,
					artistId: music.artistId,
					origin: config.server_id,
					cover: music.cover,
					album: {
						id: music.id,
						name: music.name,
						artistId: music.artistId,
						cover: music.cover,
						description: music.description,
					},
					artists: t.artist,
					artist: {
						id: t.artist[0]?.id,
						name: t.artist[0]?.name,
						artistId: t.artist[0]?.artistId,
						cover: t.artist[0]?.cover,
						description: t.artist[0]?.description,
						folder: t.artist[0]?.folder,
					},
				};
			})
		};
		return res.json(result);
	}
	else {

		return res.json({});
	}

};
