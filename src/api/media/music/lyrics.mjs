import lyricsFinder from 'lyrics-finder';
import { prismaMedia } from '../../../config/database.mjs';
import Logger from '../../../loaders/logger.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	let lyrics = '';
	let song;
	const request = JSON.parse(req.query.song ?? {});

	try {
	
		song = await prismaMedia.track.findFirst({
			where: {
				id: request.id,
			},
		});
	} catch (err) {
		//
	}
	
	if (!song?.lyrics) {
		lyrics = await lyricsFinder(request.artist.name ?? req.query.artist, request.name ?? req.query.song);
	}

	if (lyrics) {
		try {
			song = await prismaMedia.track.update({
				data: {
					lyrics,
				},
				where: {
					id: request.id,
				},
			});
		} catch (error) {

			Logger.log({
				level: 'info',
				name: 'App',
				color: 'red',
				message: 'Song not found',
				
			});
		}
	}

	if (lyrics == '' && song?.lyrics) {
		return res.json({
			message: 'No Lyrics found'
		});
	}

	return res.json({
		lyrics: lyrics ?? song?.lyrics,
	});

};