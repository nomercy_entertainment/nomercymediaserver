import { prismaMedia } from '../../../config/database.mjs';
import { tokenParser } from '../../../helpers/tokenParser.mjs';
import { createTitleSort } from '../../../controllers/file/filenameParser.mjs';
import { sort_by, uniqBy } from '../../../helpers/stringArray.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const user_id = tokenParser(req)?.sub;
    const {query, type} = req.params;

	let data;
    let result;
    
    switch (type) {
        case "artist":
            result = await prismaMedia.artist.findMany({
                where: {
                    name: {
                        contains: query,
                    },
                    NOT:{
                        track:{
                            none:{}
                        }
                    },
                },
                include: {
                    album: true,
                },
            });
            break;
        case "album":
            result = await prismaMedia.album.findMany({
                where: {
                    OR: [
                        {
                            name: {
                                contains: query,
                            },
                        },
                        {
                            AND:{
                                NOT:{
                                    description: {
                                        contains: 'Various Artists',
                                    },
                                },
                                artist: {
                                    some: {
                                        name: {
                                            contains: query,
                                        },
                                    }
                                },
                            }
                        },
                    ]
                },
            });
            break;
        case "playlist":
            result = await prismaMedia.playlist.findMany({
                where: {
					profile_id: user_id,
                    name: {
                        contains: query,
                    },
                },
            });
            break;
        default:
            break;
    }


	data = {
		data: sort_by(uniqBy(result?.map(t => {
			return {
				...t,
                type,
                year: t.description?.match?.(/\((\d{4})\)/)?.[1] ?? 9999,
                title_sort: createTitleSort((t.title ?? t.name)[0].toUpperCase() + (t.title ?? t.name).slice(1)),
                origin: config.server_id,
				cover: t.cover ? t.cover : t.album?.[0]?.cover ?? null,
			};
		}) ?? [], 'name'), "year"),
        type: type + 's',
	};

	res.json(data);

}