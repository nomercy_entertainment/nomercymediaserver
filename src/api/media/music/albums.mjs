import { createTitleSort } from '../../../controllers/file/filenameParser.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import { sort_by } from '../../../helpers/stringArray.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const music = await prismaMedia.album.findMany({
		where:{
			NOT:{
				track:{
					none:{}
				}
			}
		},
		include: {
			_count: {
				select: {
					track: true,
				},
			},
		},
	});

	if (music) {

		let result = {
			type: 'albums',
		};

		result.data = sort_by(music.map(m => {
			return {
				...m,
				type: 'album',
				name: m.name.replace(/["'\[\]*]/g, ''),
				title_sort: createTitleSort(m.name.replace(/["'\[\]*]/g, '')),
				origin: config.server_id,
			};
		}), 'title_sort');



		return res.json(result);
	}
	else {

		return res.json({});
	}

};
