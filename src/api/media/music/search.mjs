import { prismaMedia } from '../../../config/database.mjs';
import { tokenParser } from '../../../helpers/tokenParser.mjs';
import { uniqBy } from '../../../helpers/stringArray.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const user_id = tokenParser(req)?.sub;

	let data;

	const artists = await prismaMedia.artist.findMany({
		where: {
			OR: [
				{
					AND:{
						NOT:{
							track:{
								none:{}
							}
						},
						name: {
							contains: req.query.query,
						},
					}
				},
			]
		},
		include: {
			track: {
				include: {
					artist: true,
					album: true,
				}
			},
			album: true,
		},
		take: 8,
	});

	let tracks = await prismaMedia.track.findMany({
		where: {
			name: {
				contains: req.query.query,
			},
		},
		include: {
			artist: true,
			album: true,
			favorite_track: true,
		},
		orderBy: {
			date: 'desc',
		},
		take: 5,
	});

	const albums = await prismaMedia.album.findMany({
		where: {
			OR: [
				{
					name: {
						contains: req.query.query,
					},
				},
				{
					AND:{
						NOT:{
							description: {
								contains: 'Various Artists',
							},
						},
						artist: {
							some: {
								name: {
									contains: req.query.query,
								},
							}
						},
					}
				},
			]
		},
		include: {
			track: {
				include: {
					artist: true,
				},
				orderBy: {
					date: 'desc',
				},
			},
			artist: true,
		},
		take: 8,
	});

	const tracks2 = [];
	artists.map(t => t.track).map(a => {
		a.map(b => {
			tracks2.push(b);
		});
	});

	tracks = Object.values({
		...tracks,
		...tracks2,
	})
	.slice(0, 4);

	const playlists = await prismaMedia.playlistTrack.findMany({
		where: {
			OR: [
				{
					playlist: {
						profile_id: user_id
					},
					track: {
						artist: {
							some: {
								name: {
									contains: req.query.query,
								}
							},
						},
					}
				},
				{
					playlist: {
						profile_id: user_id
					},
					track: {
						name: {
							contains: req.query.query,
						}
					}
				},
			],
		},
		include: {
			playlist: true,
		},
		take: 8,
	});

	let best = {};

	if (artists.some(a => a.name.toLowerCase() == req.query.query.toLowerCase())) {
		best = artists.find(a => a.name.toLowerCase() == req.query.query.toLowerCase());
		best.type = 'artist';
	}
	else if (artists.some(a => a.name.toLowerCase().includes(req.query.query.toLowerCase()))) {
		best = artists.find(a => a.name.toLowerCase().includes(req.query.query.toLowerCase()));
		best.type = 'artist';
	}
	else if (tracks.some(a => a.name.toLowerCase() == req.query.query.toLowerCase())) {
		best = tracks.find(a => a.name.toLowerCase() == req.query.query.toLowerCase());
		best.type = 'track';
		best.cover = best.folder + '/' + best.cover;
	}
	else if (tracks.some(a => a.name.toLowerCase().includes(req.query.query.toLowerCase()))) {
		best = tracks.find(a => a.name.toLowerCase().includes(req.query.query.toLowerCase()));
		best.type = 'track';
		best.cover = best.folder + '/' + best.cover;
	}
	else if (albums.some(a => a.name.toLowerCase() == req.query.query.toLowerCase())) {
		best = albums.find(a => a.name.toLowerCase() == req.query.query.toLowerCase());
		best.type = 'album';
	}
	else if (albums.some(a => a.name.toLowerCase().includes(req.query.query.toLowerCase()))) {
		best = albums.find(a => a.name.toLowerCase().includes(req.query.query.toLowerCase()));
		best.type = 'album';
	}
	else if (playlists.some(a => a.playlist.name.toLowerCase() == req.query.query.toLowerCase())) {
		const playlist = playlists.find(a => a.playlist.name.toLowerCase() == req.query.query.toLowerCase());
		best = {
			...playlist,
			id: playlist.id,
			profile_id: playlist.profile_id,
			name: playlist.name,
			description: playlist.description,
			cover: playlist.cover,
			created_at: playlist.created_at,
			updated_at: playlist.updated_at,
			type: 'playlist',
		};
	}
	else if (playlists.some(a => a.playlist.name.toLowerCase().includes(req.query.query.toLowerCase()))) {
		const playlist = playlists.find(a => a.playlist.name.toLowerCase().includes(req.query.query.toLowerCase()));
		best = {
			...playlist,
			id: playlist.id,
			profile_id: playlist.profile_id,
			name: playlist.name,
			description: playlist.description,
			cover: playlist.cover,
			created_at: playlist.created_at,
			updated_at: playlist.updated_at,
			type: 'playlist',
		};
	}

	data = {
		best: best,
		artists: artists.map(t => {
			return {
				...t,
				type: 'artist',
				cover: t.cover ? t.cover : t.album[0]?.cover ?? null,
			};
		}),
		track: tracks.map(t => {
			return {
				...t,
				type: 'track',
				cover: t.folder + '/' + t.cover
			};
		}),
		albums: uniqBy(albums.map(a => {
			return {
				...a,
				type: 'album',
			};
		}), 'name'),
		playlists: uniqBy(playlists.map(p => {
			const playlist = p.playlist;
			delete p.playlist;
			return {
				...p,
				id: playlist.id,
				profile_id: playlist.profile_id,
				name: playlist.name,
				description: playlist.description,
				cover: playlist.cover,
				created_at: playlist.created_at,
				updated_at: playlist.updated_at,
				type: 'playlist',
			};
		}), 'playlistId'),
	};

	res.json(data);

}