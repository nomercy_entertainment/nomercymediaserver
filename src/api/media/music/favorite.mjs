import { prismaMedia } from '../../../config/database.mjs';
import tokenParser from '../../../helpers/tokenParser.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const user_id = tokenParser(req)?.sub;

	const music = await prismaMedia.favorite_track.findMany({
		where:{
			profile_id: user_id,
		},
		include: {
            track: {
                include: {
                    artist: true,
                    album: true,
                    favorite_track: true,
                }
            },
		},
	});

	if(!music) return;
	
	if (music) {
		let result = {
            cover: "favorites",
            description: null,
            name: "Songs you like",
			type: 'playlist',
			track: music.map((t) => {
				return {
					...t.track,
					type: 'track',
					date: t.updated_at,
					favorite_track: t.track.favorite_track.length > 0,
					artistId: t.track.artist[0].artistId,
					origin: config.server_id,
					artists: t.track.artist,
					cover: t.track.album[0].cover,
					artist: {
						id: t.track.artist[0].id,
						name: t.track.artist[0].name,
						artistId: t.track.artist[0].id,
						cover: t.track.artist[0].cover,
						description: t.track.artist[0].description,
						folder: t.track.artist[0].folder,
					},
					album: {
						id: t.track.album[0]?.id,
						name: t.track.album[0]?.name,
						albumId: t.track.album[0]?.albumId,
						cover: t.track.album[0]?.cover,
						description: t.track.album[0]?.description,
					},
				};
			})
		};
		return res.json(result);
	}
	else {

		return res.json({});
	}

}