import { trackSort, uniqBy } from '../../../helpers/stringArray.mjs';

import { prismaMedia } from '../../../config/database.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
	if(isNaN(parseInt(req.params.id))){
		return res.status(400).json({
			success: false,
			message: "Invalid id",
		});
	}

	const music = await prismaMedia.artist.findFirst({
		where: {
			id: parseInt(req.params.id),
		},
		include: {
			track: {
				distinct: 'name',
				include: {
					artist: true,
					album: true,
					favorite_track: true,
				}
			},
		}
	});
	
	if (music) {
		let result = {
			...music,
			type: 'artist',
			track: uniqBy(music.track.sort(trackSort),'name').map(t => {
				return {
					...t,
					type: 'artist',
					updated_at: music.updated_at,
					favorite_track: t.favorite_track.length > 0,
					artistId: music.artistId,
					origin: config.server_id,
					artists: t.artist,
					cover: t.album[0].cover,
					artist: {
						id: music.id,
						name: music.name,
						artistId: music.artistId,
						cover: music.cover,
						description: music.description,
						folder: music.folder,
					},
					album: {
						id: t.album[0]?.id,
						name: t.album[0]?.name,
						albumId: t.album[0]?.albumId,
						cover: t.album[0]?.cover,
						description: t.album[0]?.description,
					},
				};
			})
		};
		return res.json(result);
	}
	else {

		return res.json({});
	}


};
