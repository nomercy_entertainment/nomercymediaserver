import { searchMovie, searchTv } from '../../../providers/themoviedb/client/search.mjs';

import { fileURLToPath } from 'url';
import fs from 'fs';
import movie from '../../../providers/themoviedb/database/movie.mjs';
import path from 'path';
import { prismaMedia } from '../../../config/database.mjs';
import tv from '../../../providers/themoviedb/database/tv.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

	const titles = req.body.titles.split('\n');

	const list = [];
	const transaction = [];
	let index = 0;

	const special = await prismaMedia.special.upsert({
		where: {
			title: req.body.name,
		},
		create: {
			title: req.body.name,
			description: req.body.description,
			poster: req.body.poster,
			backdrop: req.body.backdrop,
		},
		update: {
			title: req.body.name,
			description: req.body.description,
			poster: req.body.poster,
			backdrop: req.body.backdrop,
		},
	});


	for (var i = 0; i < titles.length; i++) {
		const t = titles[i];

		if (t.match(/Season/gi)) {
			const match = /(?<show>[\w\s\d\.':_-]*\s)Season\s(?<season>\d{1,})\s\((?<year>\d{4})(?<note>[\s\w_-]*)?\)$/.exec(t);
			if (match) {

				let TV, id;
				const sTv = await searchTv(match.groups.show.replace('The ', ''));

				try {
					TV = await prismaMedia.tv.findFirst({
						where: {
							id: parseInt(sTv[0]?.id || sTv.id),
						},
					});
					if (!TV) {
						id = await tv(sTv[0]?.id || sTv.id);
						TV = await prismaMedia.tv.findFirst({
							where: {
								id: parseInt(sTv[0]?.id || sTv.id),
							},
						});
					}
				} catch (error) {
					console.error(error);
				}

				if (!TV?.id && !sTv[0]?.id && !sTv.id && !id) {
					console.error(t);
				}

				if (TV?.id || sTv[0]?.id || sTv.id || id) {
					let season = await prismaMedia.season.findFirst({
						where: {
							season_number: parseInt(match?.groups?.season),
							tvId: parseInt(TV?.id || sTv[0]?.id || sTv.id || id),
						},
						include: {
							episode: {
								orderBy: {
									episode_number: 'asc',
								},
								include: {
									video_file: true
								}
							},
						}
					});
					if (season?.episode.length > 0) {
						list.push(...season.episode.map(e => {

							const item = {
								specialId: special.id,
								type: 'episode',
								episodeId: e.id,
								order: index,
							};
							index += 1;
							transaction.push(prismaMedia.special_item.upsert({
								where: {
									episodeId: e.id,
								},
								create: item,
								update: item,
							}));
							return {
								showName: TV.title,
								title: e.title,
								id: e.id,
								season: e.season_number,
								episode: e.episode_number,
								image: e.still,
								type: 'episode',
							};
						}) || {});
					}
					else {
						list.push({
							id: null,
							type: 'episode',
						});
					}
				}
			}
		}
		else {
			const match = /^(?<title>(?!.*\b(Season)\b).*)\s\((?<year>\d{4})(?<note>[\s\w_-]*)?\)$/.exec(t);
			if (match) {

				let MOVIE, id;
				const sMovie = await searchMovie(match.groups.title.replace('The ', ''), match.groups.year);

				try {
					MOVIE = await prismaMedia.movie.findFirst({
						where: {
							id: parseInt(sMovie[0]?.id),
						},
						include: {
							video_file: true
						}
					});
					if (!MOVIE) {
						id = await movie(sMovie[0]?.id || sMovie.id);
						MOVIE = await prismaMedia.movie.findFirst({
							where: {
								id: parseInt(sMovie[0]?.id || sMovie.id || id),
							},
							include: {
								video_file: true
							}
						});

					}

				} catch (error) {
					console.error(error);
				}

				if (MOVIE?.video_file) {
					const item = {
						specialId: special.id,
						type: 'movie',
						movieId: MOVIE.id,
						order: index,
					};
					index += 1;
					transaction.push(prismaMedia.special_item.upsert({
						where: {
							movieId: MOVIE.id,
						},
						create: item,
						update: item,
					}));
					list.push({
						title: MOVIE.title,
						id: MOVIE.id,
						image: MOVIE.poster,
						type: 'movie',
					});
				}
				else {
					list.push({
						id: null,
						type: 'movie',
					});
				}
			}
			else {
				console.error(t);
			}
		}
	};

	try {
		await prismaMedia.$transaction(transaction);
	} catch (error) {
		try {
			await prismaMedia.$transaction(transaction);
		} catch (error) {
			try {
				await prismaMedia.$transaction(transaction);
			} catch (error) {
				//
			}
		}
	}

	if (!fs.existsSync(__dirname + '/../../databases/')) {
		fs.mkdirSync(__dirname + '/../../databases/', { recursive: true });
	}
	fs.writeFileSync(__dirname + '/../../databases/' + req.body.name.replace(/\s/g, '_') + '.json', JSON.stringify(list));


	prismaMedia.special.findFirst({
		where: {
			title: req.body.name,
		},
		include: {
			special_item: true,
		}
	}).then(special => {

		return res.json(special);

	});
};