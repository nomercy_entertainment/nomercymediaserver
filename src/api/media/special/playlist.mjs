import { convertToHuman, convertToSeconds } from '../../../helpers/dateTime.mjs';

import appApiClient from '../../../loaders/axios.mjs';
import fs from 'fs';
import i18next from '../../../config/i18next.mjs';
import { prismaMedia } from '../../../config/database.mjs';
import { sort_by } from '../../../helpers/stringArray.mjs';

const playlist = async ({ id, servers, language, country, user_id, server_id }) => {
	servers = servers?.filter(s => !s.includes(config.server_id)) ?? [];

	const list = [];
	const media = [];
	let special = {};
	const progress = [];

	let response;

	await prismaMedia.special.findFirst(specialQuery([id, country, language]))
		.then(async (data) => {
			special = data;
			
			const ids = data.special_item.map(s => s.episode?.tv || s.movie).map(s => s.id);

			await Promise.all([

				prismaMedia.media.findMany(mediaQuery(ids))
					.then((data) => media.push(data)),

				prismaMedia.video_progress.findMany(progressQuery({ id, user_id }))
					.then(data => progress.push(data))

			]);
		})
		.catch(async (err) => {

			await Promise.all(
				servers.map(async (s) => {
					await appApiClient.post(`${s}/api/special/${id}/playlist`)
						.then(res => {
							if (res?.[0]?.id) {
								response = res;
							}
						})
						.catch(err => null);
				})
			);
		});

	if (response) {
		return response;
	}

	const items = sort_by(special.special_item, 'order');
	let length = 0;

	for (let i = 0; i < items.length; i++) {
		if (items[i].movie?.id || items[i].episode?.id) {

			const item = items[i].movie ?? items[i].episode;
			
			const logo = media[0].filter(m => m.mediaable_id == item.tvId || m.mediaable_id == item.id);

			length += convertToSeconds(item.video_file[0]?.duration);

			list.push(createPlaylistItem({
				item: item,
				title: special.title,
				poster: special.poster,
				index: i,
				logo,
				progress,
				id,
			}));
		}
	}
	console.log(convertToHuman(length));

	return list;
}

export default playlist;

const createPlaylistItem = ({ item, title, poster, index, logo, progress, id }) => {
	item.description = item.overview;
	item.duration = item.video_file[0]?.duration;
	item.episode = index;
	item.navigationY = index;
	item.fonts = [];
	item.fontsfile = '';

	item.logo = logo?.[logo?.length - 1]?.src ?? null;

	item.origin = config.server_id;
	item.playlist_id = id;
	item.playlist_type = 'special';
	item.season = 0;
	item.season_image = poster;
	item.season_title = title;
	item.show = item.title;
	item.textTracks = [];
	item.tmdbid = item.id;
	item.uuid = item.id + (item.movie_id || '') + '' + (item.episode_id || '');
	item.video_type = item.season_number ? 'episode' : 'movie';
	item.year = (item.tv?.first_air_date || item.release_date).split('-')[0];
	
	item.poster = item.poster
		? item.poster
		: null;
	item.backdrop = item.backdrop
		? item.backdrop
		: null;

	item.image = (item.still || item.poster)
		? item.still || item.poster
		: null;

	if (item.tv) {
		item.title = item.tv.title + ' %S' + item.season_number + ': %E' + item.episode_number + ' ' + item.title;
	} else {
		item.title = item.title;
	}
	
	item.rating = (item.tv?.certification_tv || item.certification_movie || []).map(cr => {
		return {
			country: cr.iso_3166_1,
			rating: cr.certification.rating,
			meaning: cr.certification.meaning,
			image: `/${cr.iso_3166_1}/${cr.iso_3166_1}_${cr.certification.rating}.svg`
		};
	})?.[0] || {};

	item.progress = progress?.[0]?.find(p => p.video_id == item.id)?.time / convertToSeconds(item.video_file[0]?.duration) * 100 || null;

	item.sources = item.video_file[0] ? [{
		src: `/${item.video_file[0]?.share}/${item.video_file[0]?.folder}${item.video_file[0]?.filename}`,
		type: item.video_file[0]?.filename.includes('.mp4') ? 'video/mp4' : 'application/x-mpegURL',
	}] : [];


	item.tracks = item.video_file[0] ? [
		{
			file: `/${item.video_file[0]?.share}/${item.video_file[0]?.folder}previews.vtt`,
			kind: "thumbnails"
		},
		{
			file: `/${item.video_file[0]?.share}/${item.video_file[0]?.folder}chapters.vtt`,
			kind: "chapters"
		},
		{
			file: `/${item.video_file[0]?.share}/${item.video_file[0]?.folder}sprite.webp`,
			kind: "sprite"
		},
		{
			file: `/${item.video_file[0]?.share}/${item.video_file[0]?.folder}fonts.json`,
			kind: "fonts"
		}

	] : [];

	// let subtitles = [];
	// if (fs.existsSync(item.video_file?.[0]?.host_folder + 'subtitles')) {
	//   fs.readdirSync(item.video_file?.[0]?.host_folder + 'subtitles').filter(f => f.endsWith('.ass') || f.endsWith('.vtt')).filter(f => !f.match(/-\w{5,}\.\w{3}$/)).filter(f => !f.includes('#')).forEach(function (f) {
	//     subtitles.push(f);
	//   });
	// };

	// item.textTracks = subtitles.map(t => {
	//   const reg = /.*?[\\/]?(?<fileName>.*(?<lang>\w{3}).(?<type>\w{3,4}).(?<ext>\w{3}))$/.exec(t);
	//   return {
	//       label: i18next.t(`languages:${reg.groups?.lang}`) + (reg.groups?.type !== "full" ? ` ${reg.groups?.type}` : "") + (reg.groups?.ext === 'ass' ? ' (S)' : ''),
	//       src: `/${item.video_file[0]?.share}/${item.video_file[0]?.folder}subtitles/${t}`,
	//       srclang: i18next.t(`languages:${reg.groups?.lang}`),
	//       language: reg.groups.lang,
	//       kind: "subtitles",
	//   }
	// });

	let search = false;
	item.video_file[0]?.subtitles.split(',').filter(s => !s.match(/\d/)).forEach(sub => {
		const [lang, type, ext] = sub.split('.');

		if (ext == 'ass') {
			search = true;
		}
		item.textTracks.push({
			label: type,
			src: `/${item.video_file[0]?.share}/${item.video_file[0]?.folder}subtitles/${item.video_file[0]?.filename.replace(/\.mp4|\.m3u8/, '')}.${lang}.${type}.${ext}`,
			srclang: i18next.t(`languages:${lang}`),
			language: lang,
			kind: "subtitles",
		});
	});

	if (search && fs.existsSync(item.video_file[0]?.host_folder + 'fonts.json')) {
		item.fonts = JSON.parse(fs.readFileSync(item.video_file[0]?.host_folder + 'fonts.json', 'utf8'));
		item.fontsfile = `/${item.video_file[0]?.share}/${item.video_file[0]?.folder}fonts.json`;

		if (fs.existsSync(item.video_file?.[0]?.host_folder + 'fonts.json')) {
			item.fonts = JSON.parse(fs.readFileSync(item.video_file?.[0]?.host_folder + 'fonts.json', 'utf8'));
		};
	}

	delete item.certification_tv;
	delete item.certification_movie;
	delete item.video_file;
	delete item.tv;
	return item;

};

const specialQuery = ([id, country, language]) => {
	return {
		where: {
			id: parseInt(id),
		},
		include: {
			special_item: {
				include: {
					movie: {
						include: {
							certification_movie: {
								where: {
									iso_3166_1: {
										in: [country, language.toUpperCase()]
									}
								},
								include: {
									certification: true,
								},
							},
							video_file: true,
						}
					},
					episode: {
						include: {
							video_file: true,
							tv: {
								include: {
									certification_tv: {
										where: {
											iso_3166_1: {
												in: [country, language.toUpperCase()]
											}
										},
										include: {
											certification: true,
										},
									},
								},
							},
						}
					},
				}
			},
		}
	};
};

const mediaQuery = (ids) => {
	return {
		where: {
			mediaable_id: {
				in: ids,
			},
			type: 'logo',
		},
		orderBy: {
			vote_average: 'asc',
		},
	};
};
const progressQuery = ({ id, user_id }) => {
	return {
		where: {
			profile_id: user_id,
			type: "special",
			tmdb_id: parseInt(id),
		},
	};
};