import { prismaMedia } from '../../../config/database.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {

  let special = await prismaMedia.special.findFirst({
    where: {
      id: parseInt(req.params.id),
    },
    include: {
      special_item: {
        include: {
          movie: {
            include: {
              video_file: true,
            }
          },
          episode: {
            include: {
              video_file: true,
            }
          },
        }
      },
    },
  });

  return res.json(special);

};