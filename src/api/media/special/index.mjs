import { sort_by, unique } from '../../../helpers/stringArray.mjs';

import appApiClient from '../../../loaders/axios.mjs';
import { prismaMedia } from '../../../config/database.mjs';

/**
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 */
export default async function (req, res) {
  
  let external = [];
  let servers = req.body.servers?.filter(s => !s.includes(config.server_id));
  if (servers) {
    await Promise.all(
      servers.map(async (s) => {
        let response = await appApiClient.post(`${s}/api/specials`)
          .catch(err => {
					
				});
        if (response) {
          external.push(...response);
        }
      }));
  }

  let specials = await prismaMedia.special.findMany({
  });

  specials = specials
    .map(m => {
      return {
        id: m.id,
        backdrop: m.backdrop,
        poster: m.poster,
        overview: m.overview,
        title: m.title,
        title_sort: m.title,
        type: 'special',
        media_type: 'special',
        release_date: m.created_at,
      }
    })

  specials = unique([
    ...specials,
    ...external
  ], 'id');

  let body = sort_by(specials, 'title_sort');

  return res.json(body || []);

};