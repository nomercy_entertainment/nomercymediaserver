import { hasOwner, isAllowed, isOwner } from './permissions.mjs';

import Logger from '../../loaders/logger.mjs';
import configuration from '../../config/index.mjs';
import { fileURLToPath } from 'url';
import fs from 'fs';
import path from 'path';
import { setConfig } from '../../helpers/database.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export const check = async (req, res, next) => {
	const token = req.kauth.grant?.access_token;

	if (!token?.token) {
		return res.status(401).json({
			status: 'error',
			message: 'You must provide a Bearer token.',
		});
	}

	let ownerFile = __dirname + '/../../../config/serverOwner.json';
	if (!hasOwner(token) && ownerFile) {
		fs.writeFileSync(ownerFile, JSON.stringify([token.content.sub]));
		setConfig('owner', token.content.sub);

		await configuration();
	}

	if (isOwner(token) || isAllowed(token)) {
		
		Logger.log({
			level: 'http',
			name: 'Http',
			color: 'yellowBright',
			message: token.content.name +
			' -> ' +
			req.originalUrl
				.replace(/\?\w*=.*/, '')
				.replace(/\/{2,}/g, '/')
				.replace(/\/$/, ''),
		});

		return next();
	} else {
		return res.status(401).json({
			status: 'error',
			message: 'You do not have access to this server.',
		});
	}
};

export default check;
