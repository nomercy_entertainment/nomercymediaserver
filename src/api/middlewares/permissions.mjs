import Logger from '../../loaders/logger.mjs';
import appApiClient from '../../loaders/axios.mjs';
import refreshToken from '../../loaders/refreshToken.mjs';
import fs from 'fs';
import path from 'path';
const __dirname = path.resolve();

export const hasOwner = function () {
	if (!config.owner) {
		return false;
	}
	return true;
};

export const isOwner = (token) => {
	return config.owner == token.content.sub;
};

export const isModerator = (token) => {
	return moderators.some((m) => m.id === token.content.sub);
};

export const isAllowed = (token) => {
	let { sub, email } = token.content;

	return config.allowedUsers.includes(email) || moderators.some((m) => m.id === sub) || config.openServer;
};

/**
 * @param {import('express').Request} req
 */
export function staticPermissions(req, res, next) {
	next();
}

let mods = [];

if (fs.existsSync(__dirname + '/../../../.env') && !process.env.TOKEN) {
	await refreshToken();
}

await appApiClient
	.get('https://api.nomercy.tv/server/moderators')
	.then((response) => {
		mods = response.data
	})
	.catch((error) => {
		Logger.log({
			level: 'error',
			name: 'permissions',
			color: 'red',
			message: error.data.message,
		});
	});

export const moderators = mods;

export default {
	hasOwner,
	isAllowed,
	isModerator,
	isOwner,
	moderators: mods,
	staticPermissions,
};
