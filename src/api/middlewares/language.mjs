import i18next from '../../config/i18next.mjs';

/**
 * @param {import('express').Request} req
 */
function changeLanguage(req, res, next) {
	const locale = req.acceptsLanguages()[0] != 'undefined' ? req.acceptsLanguages()[0].split('-')[0] : 'en';
	i18next.changeLanguage(locale);
	next();
}

export default changeLanguage;
