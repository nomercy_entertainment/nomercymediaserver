import { hasOwner, isAllowed, isOwner, moderators } from './permissions.mjs';

import changeLanguage from './language.mjs';
import check from './check.mjs';
import language from './language.mjs';

export default {
	check,
	changeLanguage,
	hasOwner,
	isOwner,
	isAllowed,
	moderators,
	language,
};
