import { convertToHis } from '../../../helpers/dateTime.mjs';
import fs from 'fs';

const makeChaperfile = function (chapters, location = null) {
	const chapterFile = location + '/chapters.vtt';
	if (chapters.length > 0) {
		let data = ['WEBVTT'];

		chapters.map((c) => {
			data.push('', `Chapter ${c.index + 1}`, `${convertToHis(c.start_time)} --> ${convertToHis(c.end_time)}`, `${c.title}`);
		});
		fs.writeFileSync(chapterFile, data.join('\n'));
	}
};

export default makeChaperfile;
