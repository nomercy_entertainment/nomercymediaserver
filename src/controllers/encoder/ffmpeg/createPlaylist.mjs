import { unique } from '../../../helpers/stringArray.mjs';

const createPlaylist = function (ffprobe) {
	let hq = config.folderRoots.find(
		(f) =>
			f.path ==
			ffprobe.format.filename
				.replace(/[\\\/][\w\s\._\-?()]*$/, '')
				.replace(/[\\\/][\w\s\._\-?()]*$/, '')
				.replace(/\\/g, '/')
	)?.hq;
	if (!hq) {
		hq = config.folderRoots.find((f) => f.path == ffprobe.format.filename.replace(/[\\\/][\w\s\._\-?()]*$/, '').replace(/\\/g, '/'))?.hq;
	}

	if (config.alwaysCreatePlaylist) return true;
	else if (unique(ffprobe.streams.audio, 'language').length > 1) return true;
	else if (ffprobe.streams.video[0].width > 2000) return true;
	else if (ffprobe.streams.video[0].hdr) return true;

	return false;
};

export default createPlaylist;
