import { convertToHuman, convertToSeconds } from '../../../helpers/dateTime.mjs';
import { groupBy, pad } from '../../../helpers/stringArray.mjs';

import { fileURLToPath } from 'url';
import fs from 'fs';
import i18next from 'i18next';
import path from 'path';
import { prismaMedia } from '../../../config/database.mjs';
import readLastLines from 'read-last-lines';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const publicFolder = path.join(__dirname, '..', '..', '..', 'public');

const watchDir = publicFolder + '/working/';

if (!fs.existsSync(watchDir)) {
	fs.mkdirSync(watchDir, { recursive: true });
}

let files = fs.readdirSync(watchDir);
files.forEach(async (file) => {
	try {
		fs.rmSync(watchDir + file);
	} catch (error) {
		//
	}
});

let watcher, remover;
let index = 0;

export default (socket, io) => {
	clearInterval(watcher);
	clearInterval(remover);

	socket.emit('remove_all_progress');
	socket.off('disconnect', disconnect);

	socket.on('disconnect', () => {
		clearInterval(watcher);
		clearInterval(remover);
	});

	socket.on('stop_progress_listening', () => {
		socket.off('disconnect', disconnect);
		clearInterval(watcher);
		clearInterval(remover);
	});

	socket.on('start_progress_listening', () => {
		socket.off('disconnect', disconnect);
		clearInterval(watcher);
		clearInterval(remover);
		start();
	});

	function start() {
		fs.readdir(watchDir, function (err, files) {
			if (files.length == 0) socket.emit('remove_all_progress');
			files.forEach(async (file) => {
				loop(watchDir, file, index);
			});
		});
		watcher = setInterval(async () => {
			fs.readdir(watchDir, async function (err, files) {
				if (files.length == 0) {
					socket.emit('remove_all_progress');
				} else {
					let jobs = await prismaMedia.jobs.findMany({
						where: {
							attempts: null,
						},
					});
					socket.emit('jobs', jobs);
				}

				files.forEach(async (file) => {
					loop(watchDir, file, index);
				});
			});
		}, 1000);

		remover = setInterval(() => {
			socket.emit('remove_all_progress');
		}, 120000);
	}

	function loop(watchDir, file, index) {
		socket.off('disconnect', disconnect);

		const ffmpegLogFile = watchDir + file;
		if (fs.existsSync(ffmpegLogFile)) {
			let startLines = readlines(ffmpegLogFile, 2500);
			readLastLines.read(ffmpegLogFile, 30).then(async (endLines) => {
				if (/progress=end/.exec(endLines)) {
					if (fs.existsSync(ffmpegLogFile)) {
						try {
							fs.rmSync(ffmpegLogFile);
						} catch (error) {
							//
						}
					}
					socket.emit('remove_progress_id', parseInt(file.replace('main.txt', '').replace('subs.txt', '')));
				} else {
					let p = await progress(startLines, endLines, index, parseInt(file.replace('main.txt', '').replace('subs.txt', '')));
					if (p?.workerName) {
						socket.emit('progress', {
							id: parseInt(file.replace('main.txt', '').replace('subs.txt', '')),
							data: p,
						});
					}
				}
			});
		} else {
			socket.emit('remove_all_progress');
		}

		socket.on('disconnect', disconnect);
	}
	function disconnect() {
		if (io.engine.clientsCount == 0) {
			clearInterval(watcher);
			clearInterval(remover);
		}
	}
};

function readlines(file, maxLines) {
	let lines = fs.readFileSync(file, { encoding: 'utf8' });
	let arr = lines.split('\n').slice(0, maxLines);
	return arr.join('\n');
}

function makeLog(str) {
	const regex = /Stream\s#(?<output>\d{1,2}):(?<stream>\d{1,2})(\((?<lang>\w{3})\))?:\s(?<type>\w*):\s(?<codec>\w*)(.*(,\s(?<format>(\d{3,}x\d{3,}|\d{1}\.\d{1}))))?/gi;

	let arr = [];
	let regs = str.match(regex);

	regs.forEach((s) => {
		let regz = regex.exec(s)?.groups;
		if (regz) {
			arr.push(regz);
		} else {
			arr.push(regex.exec(s)?.groups);
		}
	});

	arr = arr.filter((s) => s.stream < 2);
	arr = arr.splice(2, arr.length).sort((a, b) => a.output - b.output);

	let video = arr.filter((s) => s.type == 'Video');
	let streams = arr.filter((s) => s.type != 'Video');

	let streams_grp = groupBy(streams, 'type', true);
	let video_grp = groupBy(video, 'type', true);

	let videoLog, audioLog, subLog;

	if (video_grp) {
		videoLog = Object.values(video_grp)[0]?.map((s, index) => {
			return (s.format != '0.1' ? 's:' + s.output + ':' : '') + s.format.replace(/^0.1$/, 'previews') + '' + (s.format != '0.1' ? ':' + Object.values(streams_grp.Audio || [])?.[index]?.lang : '');
		});

		if (videoLog?.length > 0) {
			videoLog = 'Video: ' + videoLog.join(', ') + '\n';
		}
	}
	if (streams_grp) {
		if (streams_grp.Audio) {
			audioLog = Object.values(streams_grp.Audio)
				.splice(Object.values(video_grp)[0]?.length, Object.values(streams_grp.Audio)?.length)
				.map((s) => {
					if (s.lang) return 's:' + s.output + ':' + s.lang;
				});
			if (audioLog.length > 0) {
				audioLog = audioLog != null && audioLog != '' ? 'Audio: ' + audioLog.join(', ') + '\n' : '';
			}
		}
		if (streams_grp.Subtitle) {
			subLog = Object.values(streams_grp.Subtitle)?.map((s) => {
				return 's:' + s.output + ':' + s.lang;
			});
			if (subLog.length > 0) {
				subLog = 'Subtitle: ' + subLog.join(', ') + '\n';
			}
		}
	}
	if (str.includes('bt2020')) {
		videoLog += ' HDR -> SDR ';
	}

	return (videoLog || '') + ' ' + (audioLog || '') + ' ' + (subLog || '');
}

function getCurrentTime(str) {
	let regex = /out_time=(?<time>\d{2}:\d{2}:\d{2})\.\d*/g;
	let arr = [];
	let m;
	while ((m = regex.exec(str)) !== null) {
		if (m.index === regex.lastIndex) {
			regex.lastIndex++;
		}
		m.forEach((match) => {
			let res = match.split('=')[1];
			if (res) {
				arr.push(res);
			}
		});
	}

	return arr[arr.length - 1];
}

async function progress(startLines, endines, index, id) {
	const reg_speed = /speed=(?<speed>\d*.\d*.)(?!.*\d.\d)x/g;
	const reg_title = /title\s*:\s(?<title>.*).NoMercy/gm;
	const reg_title2 = /title\s*:\s(?<title>[\w\.]*).NoMercy/gm;
	const reg_title3 = /title\s*: (?<title>.*)[.\n].*\n.*Video/gm;
	const reg_title4 = /\s+title\s*:\s(?<title>.*)\n\s*encoder/gm;
	const reg_title5 = /Output #\d{1,}, image2, to '.*\/(?<title>.*)\/thumbs\/thumb-%04d.jpg':/gm;
	const reg_title6 = /Output #\d{1,}, (sup|sub|webvtt), to '.*\/(?<title>.*)\.\w{3}.\w{3,}.\w{3}':/gm;
	const reg_title7 = /Output #\d{1,}, matroska, to '.+\/\/(?<title>[\w\.:\/\\()]+\))[\/\\].*/gm;
	const reg_img = /image\d, to '(?<img>.*)':/gm;
	const reg_duration = /Duration: (?<duration>\d{2}:\d{2}:\d{2}.\d{2})/g;

	let Speed = reg_speed.exec(endines);
	let Title = reg_title.exec(startLines);
	if (!Title?.groups) {
		Title = reg_title2.exec(startLines);
	}
	if (!Title?.groups) {
		Title = reg_title3.exec(startLines);
	}
	if (!Title?.groups) {
		Title = reg_title4.exec(startLines);
	}
	if (!Title?.groups) {
		Title = reg_title5.exec(startLines);
	}
	if (!Title?.groups) {
		Title = reg_title6.exec(startLines);
	}
	if (!Title?.groups) {
		Title = reg_title7.exec(startLines);
	}

	if (Speed) {
		let Img = reg_img.exec(startLines);
		let Duration = reg_duration.exec(startLines);

		let speed = Speed.groups.speed;
		let title = Title?.groups?.title.replace('NoMercy', '');
		let img = Img?.groups.img;

		let currentTime = convertToSeconds(getCurrentTime(endines));
		let duration = convertToSeconds(Duration?.groups?.duration);

		let log = makeLog(startLines);

		let remaining = Math.floor((duration - currentTime) / speed);
		let percent = Math.floor((currentTime / duration) * 100);
		let remainingHMS = convertToHuman(remaining);
		let remainingSplit = remainingHMS.split(':');

		let imgNumber = pad(Math.floor(currentTime / 10), 4);
		let image = img != null ? img.replace('%04d', imgNumber).replace(config.mediaRoot, '/Media/').replace(/\w\:/, '/Media/').replace(config.ripperOutFolder, '/Ripper/') : null;

		let workerName = config.workerName;
		let status = i18next.t('active');

		let days = remainingSplit[0] > 0 ? remainingSplit[0] + i18next.t('time:d') : null;
		let hours = remainingSplit[1] != '00' ? remainingSplit[1] + i18next.t('time:h') : null;
		let minutes = remainingSplit[2] != '00' ? remainingSplit[2] + i18next.t('time:m') : null;
		let seconds = (remainingSplit[3] || '00') + i18next.t('time:s');

		let res = {
			id,
			remainingHMS,
			speed,
			percent,
			index,
			workerName,
			status,
			title,
			log,
			img: image,
			currentTime,
			duration,
			remaining,
			days,
			hours,
			minutes,
			seconds,
		};

		return res;
	}
	return;
}
