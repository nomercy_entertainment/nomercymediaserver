import { sort_by, unique } from '../../../helpers/stringArray.mjs';
import { verify } from './verify.mjs';

import Logger from '../../../loaders/logger.mjs';
import createPlaylist from './createPlaylist.mjs';
import fs from 'fs';
import { isoToName } from './language.mjs';

const audio = async function (ffprobe, outputFolder) {
	let audioMap = [];
	let audio = [];

	if (createPlaylist(ffprobe)) {
		await Promise.all(
			unique(sort_by(ffprobe.streams.audio, 'channels', 'desc'), 'language').map(async (stream, index) => {
				let baseAudioFolder = outputFolder + 'audio_' + stream.language + '/';
				let manifestFile = baseAudioFolder + 'audio_' + stream.language + '.m3u8';
				if (fs.existsSync(manifestFile)) {
					let verified = await verify(manifestFile);

					if (!verified) {
						Logger.log({
							level: 'info',
							name: 'Encoder',
							color: 'cyanBright',
							message: 'File: ' + manifestFile + ' corrupt, deleting...',
						});
						if (fs.existsSync(baseAudioFolder)) fs.rmSync(baseAudioFolder, { recursive: true });
					}
				}

				if (!fs.existsSync(manifestFile)) {
					audioMap.push('-c:a aac');
					audioMap.push('-map 0:' + stream.index);
					audioMap.push('-ac ' + (stream.channels > 6 ? 6 : stream.channels));

					audioMap.push('-metadata:s:a:' + index + ' language="' + stream.language + '"');
					audioMap.push('-metadata:s:a:' + index + ' title="' + isoToName(stream.language) + '"');

					audioMap.push('-f segment');
					audioMap.push('-segment_list "' + manifestFile + '"');
					audioMap.push('-segment_time 10');
					audioMap.push('-segment_list_type m3u8');
					audioMap.push('-bsf:v h264_mp4toannexb');
					audioMap.push('"' + baseAudioFolder + 'audio_' + stream.language + '-%04d.ts"');

					fs.mkdirSync(baseAudioFolder, { recursive: true });

					audio.push({
						audio: stream.language,
						stream: stream.index,
					});
				}
			}),
			sort_by(ffprobe.streams.audio, 'channels', 'desc').map(async (stream, index) => {
				if (config.keepOriginal.audio) {
					let originalFolder = outputFolder + 'original/';
					let audioFile = originalFolder + 'audio_' + stream.language + (stream.codec_name != 'aac' ? '_' + stream.codec_name + '_' + stream.channels + 'ch' : '') + '.mkv';
					if (fs.existsSync(audioFile)) {
						let verified = await verify(audioFile);

						if (!verified) {
							Logger.log({
								level: 'info',
								name: 'Encoder',
								color: 'cyanBright',
								message: 'File: ' + audioFile + ' corrupt, deleting...',
							});
							if (fs.existsSync(originalFolder)) {
								try {
									fs.rmSync(originalFolder, { recursive: true });
								} catch (error) {}
							}
						}
					}

					if (!fs.existsSync(audioFile)) {
						fs.mkdirSync(originalFolder, { recursive: true });

						audioMap.push('-c:a copy');
						audioMap.push('-map 0:' + stream.index);
						audioMap.push('"' + audioFile + '"');

						audio.push({
							audio: stream.language,
							stream: stream.index,
						});
					}
				}
			})
		);
	}

	let log = audio
		.filter((l) => l.audio !== null)
		.map((l) => ' s:' + l.stream + ':' + l.audio)
		.join(' ');

	return {
		audioMap,
		log,
	};
};

export default audio;
