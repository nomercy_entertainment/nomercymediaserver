import { fileURLToPath } from 'url';
import fs from 'fs';
import path from 'path';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export const isoToName = function (iso) {
	let data = JSON.parse(fs.readFileSync(__dirname + '/../../../../config/languages.json'));

	let name = data.filter((n) => n.iso_639_2_b == iso);

	if (!name[0]) return 'und';

	return name[0].english_name;
};

export default {
	isoToName,
};
