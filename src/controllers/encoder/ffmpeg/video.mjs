import { execSync } from 'child_process';
import { sort_by, unique } from '../../../helpers/stringArray.mjs';
import { verify } from './verify.mjs';

import Logger from '../../../loaders/logger.mjs';
import createPlaylist from './createPlaylist.mjs';
import { fileURLToPath } from 'url';
import fs from 'fs';
import { getQualities } from './quality.mjs';
import { getSubtitleFiles } from './subtitle.mjs';
import { isoToName } from './language.mjs';
import path from 'path';
import { platform } from 'os-utils';
import { sortByPriorityKeyed } from '../../../helpers/stringArray.mjs';
import { convertCubePath } from '../../../helpers/system.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const ffmpeg = path.join(__dirname, '..', '..', '..', 'binaries', 'ffmpeg' + (platform() == 'win32' ? '.exe' : '')).replace(/\\/g, '\\\\');

export const video = async function (ffprobe, outputFolder, fileName = '') {
	let videoMap = [];
	let ext = [];
	let size = [];
	let audio = [];
	if (createPlaylist(ffprobe)) {
		await Promise.all([
			getQualities(ffprobe)
				.sort((a, b) => b.width - a.width)
				.forEach(async (quality, index) => {
					let stream = ffprobe.streams.video[0];

					let vf = await buildVideoFilter(ffprobe.format.filename, stream);

					let dir = outputFolder + 'video_' + quality.width + 'x' + quality.height;
					let dirBase = 'video_' + quality.width + 'x' + quality.height;
					let manifestFile = dir + '/video_' + quality.width + 'x' + quality.height + '.m3u8';
					let manifestFileBase = dirBase + '/video_' + quality.width + 'x' + quality.height + '.m3u8';

					if (fs.existsSync(manifestFile)) {
						let verified = await verify(manifestFile);

						if (!verified) {
							Logger.log({
								level: 'info',
								name: 'Encoder',
								color: 'cyanBright',
								message: 'File: ' + manifestFile + ' corrupt, deleting...',
								
							});
							if (fs.existsSync(manifestFile)) fs.rmSync(manifestFile, { recursive: true });

							if (ffprobe.chapters.length > 0) {
								if (fs.existsSync(outputFolder + 'chapters.vtt')) fs.rmSync(outputFolder + 'chapters.vtt', { recursive: true });
							}

							try {
								if (fs.existsSync(outputFolder + 'sprite.webp')) fs.rmSync(outputFolder + 'sprite.webp', { recursive: true });
								if (fs.existsSync(outputFolder + 'previews.vtt')) fs.rmSync(outputFolder + 'previews.vtt', { recursive: true });
								if (fs.existsSync(outputFolder + 'subtitles')) fs.rmSync(outputFolder + 'subtitles', { recursive: true });
								if (fs.existsSync(outputFolder + 'thumbs')) fs.rmSync(outputFolder + 'thumbs', { recursive: true });
							} catch (error) {
								
							}
							
						} else {
							getSubtitleFiles(ffprobe, outputFolder, fileName);
						}
					}

					if (!fs.existsSync(manifestFile)) {
						const fr = stream.r_frame_rate.split('/');
						let rate = fr[0] / fr[1];
						rate = Math.floor(rate);

						if (quality?.options) {
							videoMap.push(...quality.options.split('-').map(o => `-${o}`));
						}
						if (
							stream.codec_name == 'h264'
							&& stream.width == quality.width
							&& stream.height == quality.height
						) {
							videoMap.push('-c:v copy');
						} else {
							videoMap.push('-c:v h264');
							videoMap.push(`-filter:v "${vf}"`);
							videoMap.push(`-keyint_min "${rate}"`);
							videoMap.push(
								`-x264opts "keyint=${rate}:min-keyint=${rate}:no-scenecut"`
							);
							videoMap.push(`-g "${rate}"`);
							videoMap.push(`-r:v "${rate}"`);
							videoMap.push(`-s ${quality.width}x${quality.height}`);
							if (quality.crf) {
								videoMap.push(`-crf ${quality.crf}`);
							}
							if (quality.bitrate) {
								videoMap.push(`-b:v ${quality.bitrate}`);
								videoMap.push('-bufsize 1M');
							}
							if (quality.maxrate) {
								videoMap.push(`-maxrate ${quality.maxrate}`);
							}
							videoMap.push('-pix_fmt yuv420p');
							videoMap.push('-profile:v high');
							videoMap.push('-preset slower');
							videoMap.push('-level 3.1');
						}
						videoMap.push(`-map 0:${stream.index}`);
						videoMap.push('-movflags faststart');
						videoMap.push('-map_metadata 0');
						videoMap.push(`-metadata title="${fileName}"`);
						
						unique(ffprobe.streams.audio, 'language').sort(sortByPriorityKeyed(config.prefferedOrder, 'language')).map((stream, index) => {
							if (index == 0) {
								videoMap.push('-c:a aac');
								videoMap.push('-map 0:' + stream.index);

								videoMap.push('-ac ' + (stream.channels > 6 ? 6 : stream.channels));

								videoMap.push('-metadata:s:a:' + index + ' language="' + stream.language + '"');
								videoMap.push('-metadata:s:a:' + index + ' title="' + isoToName(stream.language) + '"');
								audio.push({
									audio: stream.language,
									stream: stream.index,
								});
							}
						});

						videoMap.push('-f segment');
						videoMap.push('-segment_list "' + manifestFileBase + '"');
						videoMap.push('-segment_time 10');
						videoMap.push('-segment_list_type m3u8');
						videoMap.push('-bsf:v h264_mp4toannexb');
						videoMap.push('-use_wallclock_as_timestamps 1');
						videoMap.push('"' + dirBase + '/video_' + quality.width + 'x' + quality.height + '-%04d.ts"');

						fs.mkdirSync(dir, { recursive: true });

						ext.push('hls');
						size.push({
							stream: stream.index,
							size: quality.width + 'x' + quality.height,
						});

						ext.push(vf?.includes('bt709') ? ' HDR->SDR ' : '');
					}
				}),
		]);
	} else {
		await Promise.all(
			ffprobe.streams.video.map(async (stream, index) => {
				let mp4File = outputFolder + fileName + '.mp4';
				let mp4FileBase = fileName + '.mp4';

				let hq = config.folderRoots.find(
					(f) =>
						f.path ==
						ffprobe.format.filename
							.replace(/[\\\/][\w\s\._\-?()]*$/, '')
							.replace(/[\\\/][\w\s\._\-?()]*$/, '')
							.replace(/\\/g, '/')
				)?.hq;

				if (!hq) {
					hq = config.folderRoots.find((f) => f.path == ffprobe.format.filename.replace(/[\\\/][\w\s\._\-?()]*$/, '').replace(/\\/g, '/'))?.hq;
				}

				let quality = config.encodingQualities
					.filter((q) => q.enabled)
					.filter((q) => q.width < ffprobe.streams.video[0].width + 100)
					.find((q) => q.width < 2000 || hq);

				let vf = await buildVideoFilter(ffprobe.format.filename, stream);

				if (fs.existsSync(mp4File)) {
					let verified = await verify(mp4File);

					if (!verified) {
						Logger.log({
							level: 'info',
							name: 'Encoder',
							color: 'cyanBright',
							message: 'File: ' + mp4File + ' corrupt, deleting...',
							
						});
						try {
							if (fs.existsSync(mp4File)) fs.rmSync(mp4File, { recursive: true });
							
							if (ffprobe.chapters.length > 0) {
								if (fs.existsSync(outputFolder + 'chapters.vtt')) fs.rmSync(outputFolder + 'chapters.vtt', { recursive: true });
							}
	
							if (fs.existsSync(outputFolder + 'sprite.webp')) fs.rmSync(outputFolder + 'sprite.webp', { recursive: true });
							if (fs.existsSync(outputFolder + 'previews.vtt')) fs.rmSync(outputFolder + 'previews.vtt', { recursive: true });
							if (fs.existsSync(outputFolder + 'subtitles')) fs.rmSync(outputFolder + 'subtitles', { recursive: true });
							if (fs.existsSync(outputFolder + 'thumbs')) fs.rmSync(outputFolder + 'thumbs', { recursive: true });
						} catch (error) {
							
						}

					} else {
						getSubtitleFiles(ffprobe, outputFolder, fileName);
					}
				}

				if (!fs.existsSync(mp4File)) {
					const fr = stream.r_frame_rate.split('/');
					let rate = fr[0] / fr[1];
					rate = Math.floor(rate);

					if (
						stream.codec_name == 'h264'
						&& stream.width == quality.width
						&& stream.height == quality.height
					) {
						videoMap.push('-c:v copy');
					} else {
						videoMap.push('-c:v h264');
						videoMap.push(`-filter:v "${vf}"`);
						videoMap.push(
							`-s ${quality?.width || stream.width}x${quality?.height || stream.height
							}`
						);
						videoMap.push(`-keyint_min "${rate}"`);
						videoMap.push(
							`-x264opts "keyint=${rate}:min-keyint=${rate}:no-scenecut"`
						);
						videoMap.push(`-g "${rate}"`);
						videoMap.push(`-r:v "${rate}"`);

						videoMap.push('-pix_fmt yuv420p');
						videoMap.push('-preset slower');
						videoMap.push('-profile:v high');
						videoMap.push('-level 3.1');
					}
					videoMap.push(`-map 0:${stream.index}`);

					if (quality?.options) {
						videoMap.push(...quality.options.split('-').map(o => `-${o}`));
					}

					videoMap.push('-movflags faststart');

					videoMap.push('-map_metadata 0');
					videoMap.push(`-metadata title="${fileName}"`);

					unique(sort_by(ffprobe.streams.audio, 'channels', 'desc'), 'language').map((stream, index) => {
						if (index == 0) {
							videoMap.push('-c:a aac');
							videoMap.push('-map 0:' + stream.index);

							// videoMap.push('-ac ' + (stream.channels > 6 ? 6 : stream.channels));
							videoMap.push('-ac 2');
							videoMap.push('-b:a 128k');

							videoMap.push('-metadata:s:a:' + index + ' language="' + stream.language + '"');
							videoMap.push('-metadata:s:a:' + index + ' title="' + isoToName(stream.language) + '"');
							audio.push({
								audio: stream.language,
								stream: stream.index,
							});
						}
					});

					if (quality?.crf) {
						videoMap.push('-crf ' + quality.crf);
					}

					if (quality?.bitrate) {
						videoMap.push('-b:v ' + quality.bitrate);
						videoMap.push('-bufsize 1M');
					}
					if (quality?.maxrate) {
						videoMap.push('-maxrate ' + quality.maxrate);
					}

					videoMap.push('-bsf:v h264_mp4toannexb');
					videoMap.push('-use_wallclock_as_timestamps 1');
					videoMap.push('"' + mp4FileBase + '"');
					ext.push('mp4');
					size.push({
						stream: stream.index,
						size: stream.width + 'x' + stream.height,
					});
				}
			})
		);
	}

	let log = size
		.filter((l) => l.size !== null)
		.map((l) => ' s:' + l.stream + ':' + l.size)
		.join(' ');

	log += audio
		.filter((a) => a.audio !== null)
		.map((a) => ' s:' + a.stream + ':' + a.audio)
		.join(' ');

	log += ext.join(' ');

	return {
		videoMap,
		log,
	};
};

export const buildVideoFilter = async (fileIn, stream) => {
	
	let crop = execSync(`${ffmpeg} -ss 120 -i "${fileIn}" -max_muxing_queue_size 999 -vframes 1000 -vf cropdetect -t 1000 -f null - 2>&1`).toString();
	
	const counts = {};
	const regex = /crop=(\d+:\d+:\d+:\d+)$/gm;
	let m;
	while ((m = regex.exec(crop)) !== null) {
		if (m.index === regex.lastIndex) {
			regex.lastIndex++;
		}
		m.forEach((match, groupIndex) => {
			if (groupIndex == 1) {
				const crop = match;
				counts[crop] = (counts[crop] || 0) + 1;
			}
		});
	}
	let result = chooseCrop(counts);
	
	if(!result) return;

	if (stream.hdr == true) {
		return `crop=${result}${hdrMap()}`;
	}

	return `crop=${result}`;
};

export function chooseCrop(crops) {
	return Object.keys(crops).reduce(
		(res, key) => {
			if (res.max < crops[key]) {
				res.max = crops[key];
				res.key = key;
			}
			return res;
		},
		{ max: 0, key: '' }
	).key;
}

export const resolutionToHigh = function (stream) {
	if (config.encodingQualities.filter((q) => q.enabled).filter((q) => q.width == stream.width || (q.width > stream.width - 100 && q.width < stream.width + 100))) return false;
	return true;
};

export const hdrMap = function () {

	const lutFile = __dirname + '/../../../../config/HDRtoSDR.cube';
	console.log(convertCubePath(lutFile));

	if (fs.existsSync(lutFile)) {
		// return ",lut3d='" + convertCubePath(lutFile) + "'";
		// return ",lut3d='" + convertCubePath(lutFile) + "',zscale=p=bt709,zscale=t=bt709:m=bt709:r=tv,eq=saturation=0.95";
		// return ",lut3d='" + convertCubePath(lutFile) + "',zscale=t=linear:npl=100,format=gbrpf32le,zscale=p=bt709,tonemap=tonemap=hable:desat=0,zscale=t=bt709:m=bt709:r=tv,format=yuv420p";
	}

	return ',zscale=tin=smpte2084:min=bt2020nc:pin=bt2020:rin=tv:t=smpte2084:m=bt2020nc:p=bt2020:r=tv,zscale=t=linear:npl=100,format=gbrpf32le,zscale=p=bt709,tonemap=tonemap=hable:desat=0,zscale=t=bt709:m=bt709:r=tv,eq=saturation=0.85';
};

export default {
	video,
	resolutionToHigh,
};
