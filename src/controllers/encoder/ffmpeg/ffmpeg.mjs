import { cleanFileName, createBaseFolderName, createFileName, createRootFolderName, createShowFolderName, getFolderData, parseFileName } from '../../file/filenameParser.mjs';
import { convertSubToVtt, subtitle } from './subtitle.mjs';
import { convertToHis, dateFormat } from '../../../helpers/dateTime.mjs';
import { createThumbnails, thumbnails } from './preview.mjs';
import osu, { platform } from 'os-utils';
import { searchMovie, searchTv } from '../../../providers/themoviedb/client/search.mjs';

import Logger from '../../../loaders/logger.mjs';
import audio from './audio.mjs';
import { convertPath } from '../../../helpers/system.mjs';
import createManifest from './manifest.mjs';
import { exec } from 'child_process';
import { fileURLToPath } from 'url';
import fs from 'fs';
import { getVideoInfo } from './ffprobe.mjs';
import i18next from '../../../config/i18next.mjs';
import makeAttachmentsFile from './attatchments.mjs';
import makeChaperfile from './chapter.mjs';
import movie from '../../../providers/themoviedb/database/movie.mjs';
import path from 'path';
import { prismaMedia } from '../../../config/database.mjs';
import tv from '../../../providers/themoviedb/database/tv.mjs';
import { video } from './video.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const threads = osu.cpuCount();

const ffmpeg = path.join(__dirname, '..', '..', '..', 'binaries', 'ffmpeg' + (platform() == 'win32' ? '.exe' : '')).replace(/\\/g, '\\\\');

export const createFFmpegCommand = function (fileIn, prefix = '') {
	Logger.log({
		level: 'info',
		name: 'Encoder',
		color: 'cyanBright',
		message: 'Creating ffmpeg command for: ' + fileIn,
		
	});

	return new Promise(async (resolve, reject) => {
		await i18next.changeLanguage('en');

		let title, outputFolder, dbindex;

		let filenameInfo = parseFileName(fileIn);

		Logger.log({
			level: 'debug',
			name: 'Encoder',
			color: 'cyanBright',
			message: JSON.stringify(filenameInfo),
			
		});

		if ((filenameInfo.season_num && filenameInfo.ep_num) || (filenameInfo.type && filenameInfo.title && filenameInfo.year)) {
			let folderData = getFolderData(fileIn);
			if (!folderData) return resolve({});
		
			Logger.log({
				level: 'debug',
				name: 'Encoder',
				color: 'cyanBright',
				message: JSON.stringify(folderData),
				
			});

			let rootFolder = createRootFolderName(folderData); 
			if (!rootFolder) return resolve({});

			let data, episode;
			if (folderData.dbtype != 'movie') {
				data = await searchTv(filenameInfo.title, filenameInfo.year);
				if (!data) return resolve({});

				let TV = await prismaMedia.tv.findFirst({
					where: {
						id: data[0].id,
					},
				});

				if (!TV) {
					await tv(data[0].id, folderData.folder);
				}
			} else {
				data = await searchMovie(filenameInfo.title, filenameInfo.year);

				let MOVIE = await prismaMedia.movie.findFirst({
					where: {
						id: data[0].id,
					},
				});

				if (!MOVIE) {
					await movie(data[0].id, folderData.folder);
				}
			}

			if (!data[0]) return resolve({});
			
			
			Logger.log({
				level: 'debug',
				name: 'Encoder',
				color: 'cyanBright',
				message: JSON.stringify(data[0]),
				
			});


			let baseName = createBaseFolderName(data[0]); // Show.Name.(year)/
			if (!baseName) return resolve({});

			if (filenameInfo.season_num && filenameInfo.ep_num) {
				let subFileName = createShowFolderName(folderData.dbtype, data[0], filenameInfo); // Show.Name.S01E01
				if (!subFileName) return resolve({});

				outputFolder = rootFolder + baseName + subFileName + '/';

				episode = await prismaMedia.episode.findFirst({
					where: {
						tvId: parseInt(data[0].id),
						season_number: parseInt(filenameInfo.season_num),
						episode_number: parseInt(filenameInfo.ep_num),
					},
				});
				
				if (!episode) {
					await tv(data[0].id, folderData.folder);
				}
				
				episode = await prismaMedia.episode.findFirst({
					where: {
						tvId: parseInt(data[0].id),
						season_number: parseInt(filenameInfo.season_num),
						episode_number: parseInt(filenameInfo.ep_num),
					},
				});

				try {
					await prismaMedia.tv.update({
						where: {
							id: parseInt(data[0].id),
						},
						data: {
							folder: rootFolder + baseName.replace(/[\/\\]$/, '').replace(/\/{1,}/g, '/'),
						},
					});
				} catch (error) {
					try {
						await prismaMedia.tv.update({
							where: {
								id: parseInt(data[0].id),
							},
							data: {
								folder: rootFolder + baseName.replace(/[\/\\]$/, '').replace(/\/{1,}/g, '/'),
							},
						});
					} catch (error) {
						Logger.log({
							level: 'info',
							name: 'App',
							color: 'magentaBright',
							message: 'An error occured in updating the record for episodes in folder: ' + rootFolder + baseName.replace(/[\/\\]$/, '').replace(/\/{1,}/g, '/'),
							
						});
					}
				}
				
				if (!episode) return resolve({});

				dbindex = episode.id;
			} else if (filenameInfo.type && filenameInfo.title && filenameInfo.year) {
				outputFolder = rootFolder + baseName;
				dbindex = data[0].id;

				await prismaMedia.movie.update({
					where: {
						id: data[0].id,
					},
					data: {
						folder: rootFolder + baseName.replace(/[\/\\]$/, '').replace(/\/{1,}/g, '/'),
					},
				});
			}

			let fileName = createFileName(folderData.dbtype, data[0], episode); // Show.Name.S01E01.xxx, dbindex
			if (!fileName) return resolve({});
			
			Logger.log({
				level: 'info',
				name: 'Encoder',
				color: 'cyanBright',
				message: outputFolder,
				
			});

			let type = folderData.mediumType;
			let dbtype = folderData.dbtype;

			await ffmpegController(fileIn, outputFolder, fileName, type, dbtype, dbindex)
				.then((controller) => {
					return resolve(controller);
				})
				.catch((e) => {
					Logger.log({
						level: 'warn',
						name: 'Encoder',
						color: 'red',
						message: 'Controller: ' + e,
					});
				});
		} else {
			prefix = /\d{5,}/.exec(fileIn)?.[0];

			if (!prefix) {
				prefix = dateFormat(new Date(), 'yyyymmddHMM_');
			}

			title = prefix + '_' + fileIn.replace(/.*[\\\/]/, '').replace(/\.\w{3,}$/, '');

			let outputFolder = config.ripperOutFolder + title + '/';
		
			Logger.log({
				level: 'debug',
				name: 'Encoder',
				color: 'cyanBright',
				message: outputFolder,
				
			});

			await ffmpegController(fileIn, outputFolder, title, 'optical', 'optical', dbindex)
				.then((controller) => {
					return resolve(controller);
				})
				.catch((e) => {
					Logger.log({
						level: 'warn',
						name: 'Encoder',
						color: 'red',
						message: 'Controller: ' + e,
					});
				});
		}
	});
};

export const ffmpegController = function (fileIn, outputFolder, fileName, type, dbtype, dbindex) {
	return new Promise(async (resolve, reject) => {
		fs.mkdirSync(outputFolder, { recursive: true });

		let outputFileName = cleanFileName(fileName + '.NoMercy');

		let ffprobe = await getVideoInfo(fileIn).catch((reason) => {
			Logger.log({
				level: 'error',
				name: 'Encoder',
				color: 'red',
				message: 'Error while getting video info: ' + reason.file + ', reason: ' + (reason.error || 'unknown'),
			});
		});
		
		if (Object.entries(config.keepOriginal).map((k) => k) && ffprobe) {
			let originalFolder = outputFolder + 'original/';
			fs.mkdirSync(originalFolder, { recursive: true });
			fs.writeFileSync(originalFolder + fileName + '.ffprobe.json', JSON.stringify(ffprobe));
		}

		if (ffprobe == null || ffprobe.streams == null || ffprobe.streams.video.length == 0 || ffprobe.streams.audio.length == 0) return resolve('No streams to process');

		let hdr = ffprobe.streams.video.hdr;

		let videoMap = await video(ffprobe, outputFolder, outputFileName);
		let duration = convertToHis(ffprobe.format.duration);
		let audioMap = await audio(ffprobe, outputFolder);
		let subtitleMap = await subtitle(ffprobe, outputFolder, outputFileName);
		let thumbMap = thumbnails(ffprobe, outputFolder);

		let command = videoMap.videoMap.concat(audioMap.audioMap).concat(subtitleMap.subtitleMap).concat(thumbMap.thumbMap);

		let publicFolder = __dirname + '/../../../public/';
		let logFolder = publicFolder + 'working/';
		let encodingFolder = publicFolder + 'working/';
		fs.mkdirSync(logFolder, { recursive: true });

		dbindex = dbindex || Math.floor(Math.random() * 2000000000);

		let cuda = [
			`-threads ${Math.floor(threads * 2)}`,
		];

		if(process.env.GPU_ACCELERATION == "true"){
			cuda = [
				`-threads 0`,
				'-hwaccel auto',
			];
		}

		let ffmpegCommand1 = [
			ffmpeg,
			...cuda,
			'-progress - -nostats',
			'-async 1',
			'-vsync -1',
			'-analyzeduration 9999M',
			'-probesize 4092M',
			`-i "${fileIn}"`,
			...videoMap.videoMap,
			...audioMap.audioMap,
			...(videoMap.videoMap == '' && audioMap.audioMap == '' && subtitleMap.subtitleMap != '' ? subtitleMap.subtitleMap : ''),
			...thumbMap.thumbMap,
			'-y',
			`> "${convertPath(encodingFolder + dbindex + 'main.txt')}" 2>&1`,
		];

		let ffmpegCommand2 = [ffmpeg, `-threads ${Math.floor(threads * 0.5)}`, '-progress - -nostats', `-i "${fileIn}"`, ...subtitleMap.subtitleMap, '-y', `> "${convertPath(encodingFolder + dbindex + '0subs.txt')}" 2>&1`];

		let fontsFolder = publicFolder + 'fonts/';
		fs.mkdirSync(fontsFolder, { recursive: true });

		makeChaperfile(ffprobe.chapters, outputFolder);
		makeAttachmentsFile(ffprobe.streams.attachments, outputFolder);
		createManifest(ffprobe, outputFolder, outputFileName);

		if (command.length < 5) {
			await createThumbnails(ffprobe, outputFolder);
			await convertSubToVtt(outputFolder + 'subtitles/');
		} else {
		}

		let attatchmentsCommand = `"${ffmpeg}" -dump_attachment:t "" -i "${fileIn}" -y  -hide_banner -max_muxing_queue_size 9999 -async 1 -loglevel panic 2>&1`;
		exec(attatchmentsCommand, { cwd: fontsFolder, maxBuffer: 1024 * 5000 }, (e) => {
			if (e) {
				const attachmentsFile = outputFolder + '/fonts.json';
				if(!fs.existsSync(attachmentsFile)) return;
				
				JSON.parse(fs.readFileSync(attachmentsFile, 'utf8')).forEach((font) => {
					
					if(fs.existsSync(fontsFolder + font.file)){
						fs.renameSync(fontsFolder + font.file, fontsFolder + font.file.toLowerCase());
					}
				})
			}
		});

		let episode = await prismaMedia.episode.findFirst({
			where: {
				id: dbindex,
			},
			select: {
				tvId: true,
			},
		});

		let movie = await prismaMedia.movie.findFirst({
			where: {
				id: dbindex,
			},
			select: {
				id: true,
			},
		});

		let outputExtention = audioMap.audioMap.length > 1 ? '.m3u8' : '.mp4';

		let ffmpegCommand = command.length > 0 ? ffmpegCommand1.join(' ') : null;
		
		ffmpegCommand2 = subtitleMap.subtitleMap.length > 0 ? ffmpegCommand2.join(' ') : null;

		Logger.log({
			level: 'info',
			name: 'Encoder',
			color: 'cyanBright',
			message: 'Command: ' + ffmpegCommand,
		});
		Logger.log({
			level: 'info',
			name: 'Encoder',
			color: 'cyanBright',
			message: 'Command: ' + ffmpegCommand2,
		});
		
		if (ffmpegCommand != null) {
			await prismaMedia.jobs.create({
				data: {
					queue: 'encoder',
					payload: JSON.stringify({
						tmdb_id: episode || movie || null,
						ffmpegCommand,
						ffmpegCommand2,
						ffprobe,
						outputFolder,
						outputFileName,
						outputExtention,
						dbindex,
						logFile: dbindex,
						fileIn,
						fileName,
						type,
						dbtype,
					}),
				},
			});
		}

		return resolve({
			videoMap: videoMap.videoMap,
			audioMap: audioMap.audioMap,
			subtitleMap: subtitleMap.subtitleMap,
			thumbMap: thumbMap.thumbMap,
			outputFileName,
			duration,
			hdr,
			fileIn,
			outputFolder,
			outputExtention,
			fileName,
			type,
			dbtype,
			dbindex,
			ffprobe,
			ffmpegCommands: {
				main: ffmpegCommand,
			},
		});
	});
};

export default {
	createFFmpegCommand,
	ffmpegController,
};
