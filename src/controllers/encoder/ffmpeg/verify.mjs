import Logger from '../../../loaders/logger.mjs';
import fs from 'fs';
import { getVideoInfo } from './ffprobe.mjs';

export const verifyMkv = async function (file, inputFfprobe) {
	if (config.debug) {
		Logger.log({
			level: 'info',
			name: 'Encoder',
			color: 'cyanBright',
			message: 'Verifying: ' + file.replace(/.*[\\\/]/, ''),
		});
	}

	if (!file.endsWith('.mkv')) {
		return false;
	}

	let ffprobe = await getVideoInfo(file).catch((reason) => {
		Logger.log({
			level: 'error',
			name: 'Encoder',
			color: 'red',
			message: 'Error while getting video info: ' + reason.file + ', reason: ' + (reason.error || 'unknown'),
		});
	});


	if (!ffprobe || ffprobe.format.duration == 'N/A') {
		if (config.debug) {
			Logger.log({
				level: 'info',
				name: 'Encoder',
				color: 'cyanBright',
				message: "File doesn't have a duration: " + file.replace(/.*[\\\/]/, ''),
			});
		}
		return false;
	}

	if(inputFfprobe && inputFfprobe.format && (ffprobe.format.duration != inputFfprobe?.format?.duration)) {
		if (config.debug) {
			Logger.log({
				level: "info",
				name: "Encoder",
				color: "cyanBright",
				message:
					"File duration is too " + (ffprobe.format.duration < inputFfprobe?.format?.duration ? 'short: ' : 'long: ') + 
					file.replace(/.*[\\\/]/, ""),
			});
			Logger.log({
				level: "info",
				name: "Encoder",
				color: "cyanBright",
				message:
					"File input " + ffprobe.format.duration + " output: " + inputFfprobe?.format?.duration + 
					file.replace(/.*[\\\/]/, ""),
			});
		}
		// return false;
	}

	Logger.log({
		level: 'info',
		name: 'Encoder',
		color: 'cyanBright',
		message: 'File: ' + file.replace(/.*[\\\/]/, '') + ' Ok',
	});

	return true;
};

export const verifyMp4 = async function (file, inputFfprobe) {
	if (config.debug) {
		Logger.log({
			level: 'info',
			name: 'Encoder',
			color: 'cyanBright',
			message: 'Verifying: ' + file.replace(/.*[\\\/]/, '')
		});
	}

	if (!file.endsWith('.mp4')) {
		if (config.debug) {
			Logger.log({
				level: 'info',
				name: 'Encoder',
				color: 'cyanBright',
				message: 'file is not a mp4 file so is not playable on all devices: ' + file.replace(/.*[\\\/]/, ''),
			});
		}
		return false;
	}

	let ffprobe = await getVideoInfo(file).catch((reason) => {
		Logger.log({
			level: 'error',
			name: 'Encoder',
			color: 'red',
			message: 'Error while getting video info: ' + reason.file + ', reason: ' + (reason.error || 'unknown'),
		});

	});


	if (!ffprobe || ffprobe.format.duration == 'N/A') {
		if (config.debug) {
			Logger.log({
				level: 'info',
				name: 'Encoder',
				color: 'cyanBright',
				message: "File doesn't have a duration: " + file.replace(/.*[\\\/]/, ''),
			});
		}
		return false;
	}
	if(inputFfprobe && inputFfprobe.format && (ffprobe.format.duration != inputFfprobe?.format?.duration)) {
		if (config.debug) {
			Logger.log({
				level: "info",
				name: "Encoder",
				color: "cyanBright",
				message:
					"File duration is too " + (ffprobe.format.duration < inputFfprobe?.format?.duration ? 'short: ' : 'long: ') + 
					file.replace(/.*[\\\/]/, ""),
			});
			Logger.log({
				level: "info",
				name: "Encoder",
				color: "cyanBright",
				message:
					"File input " + ffprobe.format.duration + " output: " + inputFfprobe?.format?.duration + 
					file.replace(/.*[\\\/]/, ""),
			});
		}
		// return false;
	}

	if (ffprobe.streams.video.level > 40) {
		if (config.debug) {
			Logger.log({
				level: 'info',
				name: 'Encoder',
				color: 'cyanBright',
				message: 'File is not playable on all devices: ' + file.replace(/.*[\\\/]/, ''),
			});
		}
		return false;
	}

	if (config.debug) {
		Logger.log({
			level: 'info',
			name: 'Encoder',
			color: 'cyanBright',
			message: 'File: ' + file.replace(/.*[\\\/]/, '') + ' Ok',
		});
	}

	return true;
};

export const verifyPlaylist = async function (file, inputFfprobe) {
	if (config.debug) {
		Logger.log({
			level: 'info',
			name: 'Encoder',
			color: 'cyanBright',
			message: 'Verifying playlist: ' + file.replace(/.*[\\\/]/, ''),
		});
	}

	if (!file.endsWith('.m3u8')) {
		if (config.debug) {
			Logger.log({
				level: 'info',
				name: 'Encoder',
				color: 'cyanBright',
				message: 'playlist is not a playlist: ' + file.replace(/.*[\\\/]/, '')
			});
		}
		return false;
	}

	if (!file.endsWith('NoMercy.m3u8')) {
		let content = fs.readFileSync(file, 'utf8');
		let match = /(?<file>(\w*-\d{1,5}\.\w{2,3}))\n#EXT-X-ENDLIST/.exec(content);
		if (!match) {
			if (config.debug) {
				Logger.log({
					level: 'info',
					name: 'Encoder',
					color: 'cyanBright',
					message: 'playlist is incomplete: ' + file.replace(/.*[\\\/]/, ''),
				});
			}
			return false;
		}
	}

	if (config.debug) {
		Logger.log({
			level: 'info',
			name: 'Encoder',
			color: 'cyanBright',
			message: 'Verifying stream.: ' + file.replace(/.*[\\\/]/, ''),
		});
	}
	let ffprobe = await getVideoInfo(file).catch((reason) => {
		Logger.log({
			level: 'error',
			name: 'Encoder',
			color: 'red',
			message: 'Error while getting video info: ' + reason.file + ', reason: ' + (reason.error || 'unknown'),
		});

	});


	if (!ffprobe || ffprobe.format.duration == 'N/A') {
		if (config.debug) {
			Logger.log({
				level: 'info',
				name: 'Encoder',
				color: 'cyanBright',
				message: "playlist doesn't have a duration: " + file.replace(/.*[\\\/]/, ''),
			});
		}
		return false;
	}
	if(inputFfprobe && inputFfprobe.format && (ffprobe.format.duration != inputFfprobe?.format?.duration)) {
		if (config.debug) {
			Logger.log({
				level: "info",
				name: "Encoder",
				color: "cyanBright",
				message:
					"Output playlist item duration is too " + (ffprobe.format.duration < inputFfprobe?.format?.duration ? 'short: ' : 'long: ') + 
					file.replace(/.*[\\\/]/, ""),
			});
			Logger.log({
				level: "info",
				name: "Encoder",
				color: "cyanBright",
				message:
					"File input " + ffprobe.format.duration + " output: " + inputFfprobe?.format?.duration + 
					file.replace(/.*[\\\/]/, ""),
			});
		}
		// return false;
	}


	if (ffprobe.streams.video && ffprobe.streams.video.level > 40) {
		if (config.debug) {
			Logger.log({
				level: 'info',
				name: 'Encoder',
				color: 'cyanBright',
				message: 'playlist is not playable on all devices: ' + file.replace(/.*[\\\/]/, ''),
			});
		}
		return false;
	}

	if (config.debug) {
		Logger.log({
			level: 'info',
			name: 'Encoder',
			color: 'cyanBright',
			message: 'playlist: ' + file.replace(/.*[\\\/]/, '') + ' Ok',
		});
	}
	return true;
};

export const verify = async function (file, inputFfprobe) {
	if (file.endsWith('.mkv')) {
		return await verifyMkv(file, inputFfprobe);
	} else if (file.endsWith('.mp4')) {
		return await verifyMp4(file, inputFfprobe);
	} else if (file.endsWith('.m3u8')) {
		return await verifyPlaylist(file, inputFfprobe);
	}
};

export const remove = function (obj) {
	if (obj.files) {
		obj.files.map((f) => {
			if (config.debug) {
				Logger.log({
					level: 'info',
					name: 'Encoder',
					color: 'cyanBright',
					message: 'deleting file: ' + f,
				});
			}
			if (fs.existsSync(f)) fs.rmSync(f, { force: true });
		});
	}

	if (obj.folders) {
		obj.files.map((f) => {
			if (config.debug) {
				Logger.log({
					level: 'info',
					name: 'Encoder',
					color: 'cyanBright',
					message: 'deleting folder: ' + f,
				});
			}
			if (fs.existsSync(f)) fs.rmSync(f, { recursive: true });
		});
	}

	return;
};

export default {
	verify,
	verifyMp4,
	verifyMkv,
	verifyPlaylist,
	remove,
};
