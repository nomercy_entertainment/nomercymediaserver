import createPlaylist from './createPlaylist.mjs';
import fs from 'fs';
import { getQualities } from './quality.mjs';
import { isoToName } from './language.mjs';
import { unique } from '../../../helpers/stringArray.mjs';

const createManifest = function (ffprobe, location, filename) {
	if (createPlaylist(ffprobe)) {
		let languages = [];

		if (!fs.existsSync(location)) {
			fs.mkdirSync(location);
		}

		const manifestFile = location + filename + '.m3u8';
		console.log('Creating manifest file: ' + manifestFile);

		let m3u8_content = [];
		let def = 'YES';

		m3u8_content.push('#EXTM3U');
		m3u8_content.push('');

		unique(ffprobe.streams.audio, 'language').map(async (stream, index) => {
			m3u8_content.push(
				'#EXT-X-MEDIA:TYPE=AUDIO,GROUP-ID="stereo",LANGUAGE="' + stream.language + '",NAME="' + isoToName(stream.language) + '",DEFAULT=' + def + ',AUTOSELECT=YES,URI="audio_' + stream.language + '/audio_' + stream.language + '.m3u8' + '"'
			);
			def = 'NO';
			languages.push(stream.language);
		});

		ffprobe.streams.video.map(async (stream) => {
			getQualities(ffprobe).forEach((quality) => {
				m3u8_content.push('');
				m3u8_content.push('#EXT-X-STREAM-INF:PROGRAM-ID=1,BANDWIDTH=' + getBitrate(ffprobe, quality) + ',CODECS="avc1.4d4015,mp4a.40.2",AUDIO="stereo",RESOLUTION=' + quality.width + 'x' + quality.height) +
					',LABEL=' +
					quality.height +
					'P';
				m3u8_content.push('video_' + quality.width + 'x' + quality.height + '/video_' + quality.width + 'x' + quality.height + '.m3u8');
			});
		});

		fs.writeFileSync(manifestFile, m3u8_content.join('\n'));
	}
};

function getBitrate(ffprobe, quality) {
	
	let rate;
	if (quality.width >= 600 && quality.width < 1200) {
		rate = 1024 * 5;
	} else if (quality.width >= 1200 && quality.width < 1900) {
		rate = 1024 * 4;
	} else if (quality.width >= 1900 && quality.width < 2000) {
		rate = 1024 * 3;
	} else if (quality.width >= 2000 && quality.width < 3000) {
		rate = 1024 * 2;
	} else if (quality.width >= 3000) {
		rate = 1024 * 1;
	}

	if (quality.bitrate && ffprobe.format.duration) return Math.floor((ffprobe.format.duration * quality.bitrate) / 8 / rate);
	
	else if (ffprobe.format.bit_rate && ffprobe.format.bit_rate != 'N/A' && ffprobe.format.duration) return Math.floor((ffprobe.format.duration * ffprobe.format.bit_rate) / 8 / rate);
	
	else if (ffprobe.format.duration) {
	  let size = getTotalSize(ffprobe.format.filename.replace(/[\\\/][^\\\/]+(?=.*\w*)$/, ''));
	  return Math.floor(size / ffprobe.format.duration / 8 / rate);
	}

	return 520929;
}

const getTotalSize = function (dir) {
	let totalSize = 0;
	if (fs.existsSync(dir) && fs.statSync(dir).isDirectory()) {
		let files = fs.readdirSync(dir);
		files.forEach(function (file) {
			totalSize += fs.statSync(dir + '/' + file).size;
		});
	}
	return totalSize;
};

export default createManifest;
