export const getQualityTag = function (ffprobe) {
	let sizes = ffprobe.streams.video.map((s) => {
		if (s.width >= 600 && s.width < 1200) {
			return 'SD';
		} else if (s.width >= 1200 && s.width < 1900) {
			return 'HD720p';
		} else if (s.width >= 1900 && s.width < 2000) {
			return 'HD1080p';
		} else if (s.width >= 2000 && s.width < 3000) {
			return '2K';
		} else if (s.width >= 3000) {
			return '4K';
		}
		return 'Unknown';
	});

	return sizes.join(',');
};

export const getQualities = function (ffprobe) {

	let hq = config.folderRoots.find((f) =>
		f.path.replace(/\/$/, '') == ffprobe.format.filename
			.replace(/[\\\/][\w\s\._\-?()']*$/, '')
			.replace(/[\w\s\._\-?()']*$/, '')
			.replace(/\\/g, '/')
			.replace(/\/$/, '')
	)?.hq;

	if (!hq) {
		hq = config.folderRoots.find((f) => f.path.replace(/\/$/, '') == ffprobe.format.filename.replace(/[\\\/][\w\s\._\-?()]*$/, '').replace(/\\/g, '/').replace(/\/$/, ''))?.hq;
	}

	if(!hq && ffprobe.format.filename.match(/Marvel/i)) {
		console.log(ffprobe.format.filename
			.replace(/[\\\/][\w\s\._\-?()']*$/, '')
			.replace(/[\w\s\._\-?()']*$/, '')
			.replace(/\\/g, '/')
			.replace(/\/$/, ''));

		console.log(ffprobe.format.filename.replace(/[\\\/][\w\s\._\-?()']*$/, '').replace(/\\/g, '/').replace(/\/$/, ''));
	}
	
	let qualities = config.encodingQualities
		.filter((q) => q.enabled)
		.filter((q) => q.width == ffprobe.streams.video[0].width || q.width < ffprobe.streams.video[0].width + 100)
		.filter((q) => (q.width < 2000 && !hq) || hq);

	if (qualities.length == 0) {
		qualities = config.encodingQualities
			.filter((q) => q.width == ffprobe.streams.video[0].width 
				|| (q.width > ffprobe.streams.video[0].width - 100 && q.width < ffprobe.streams.video[0].width + 100));
	}
	if (qualities.length == 0) {
		qualities = config.encodingQualities
			.filter((q) => q.enabled)
			.filter((q) => q.height == ffprobe.streams.video[0].height 
				|| (q.height > ffprobe.streams.video[0].height - 100 && q.height < ffprobe.streams.video[0].height + 100));
	}
	// if(qualities.length == 0){
	//     qualities = config.encodingQualities.filter(q => q.height == ffprobe.streams.video[0].height || (q.height > ffprobe.streams.video[0].height - 100 && q.height < ffprobe.streams.video[0].height + 100))
	// }
	
	return qualities;
};

export default {
	getQualityTag,
	getQualities,
};
