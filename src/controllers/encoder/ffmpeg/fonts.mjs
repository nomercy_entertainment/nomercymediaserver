import fs from 'fs';
import path from 'path';

const __dirname = path.resolve();

export const renameFontsToLowerCase = () => {
	
	let publicFolder = __dirname + '/src/public/';
	let fontsFolder = publicFolder + 'fonts/';

	fs.readdirSync(fontsFolder).forEach((font) => {

		if(fs.existsSync(fontsFolder + font)){
			fs.renameSync(fontsFolder + font, fontsFolder + font.toLowerCase());
		}
	})

}

export default renameFontsToLowerCase;