import { createTimeInterval } from '../../../helpers/dateTime.mjs';
import { exec } from 'child_process';
import { fileURLToPath } from 'url';
import fs from 'fs';
import { pad } from '../../../helpers/stringArray.mjs';
import path from 'path';
import { platform } from 'os-utils';
import { hdrMap } from './video.mjs';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export const thumbSize = {
	w: 158,
	h: 90,
};

const ffmpeg = path.join(__dirname, '..', '..', '..', 'binaries', 'ffmpeg' + (platform() == 'win32' ? '.exe' : '')).replace(/\\/g, '\\\\');

export const thumbnails = function (ffprobe, outputFolder) {
	const thumbailsFolder = outputFolder + 'thumbs/';
	const spriteFile = outputFolder + 'sprite.webp';
	let thumbMap = [];
	let log = [];
	let vf = buildVideoFilter(ffprobe.streams.video[0]);

	if (!fs.existsSync(spriteFile) && !fs.existsSync(thumbailsFolder + 'thumb-' + pad(Math.floor(Math.floor(ffprobe.format.duration) / 10) - 1, 4) + '.jpg')) {
		fs.mkdirSync(thumbailsFolder, { recursive: true });
		thumbMap.push('-c:v mjpeg');
		thumbMap.push(vf);
		thumbMap.push('-ss 1');
		thumbMap.push(`-s ${thumbSize.w}x${thumbSize.h}`);
		thumbMap.push('"' + outputFolder + 'thumbs/thumb-%04d.jpg"');

		log.push(' thumbnails ');
	}

	return {
		thumbMap,
		log,
	};
};

export const createThumbnails = async function (ffprobe, outputFolder) {
	const thumbailsFolder = outputFolder + 'thumbs/';
	const previewFiles = outputFolder + 'previews.vtt';
	const spriteFile = outputFolder + 'sprite.webp';

	if (!fs.existsSync(spriteFile) && fs.existsSync(thumbailsFolder)) {
		let thumbWidth = thumbSize.w;
		let thumbHeight = thumbSize.h;
		let imageFiles = fs.readdirSync(thumbailsFolder).sort();
		let thumb_content = [];
		let imageLine = Math.floor(16000 / thumbWidth) - 1;
		let maxLines = Math.floor(16000 / thumbHeight) - 1;
		let dst_x = 0;
		let dst_y = 0;
		let interval = 10;

		if (imageFiles.length > 0) {
			let montageCommand = `"${ffmpeg}" -i "${thumbailsFolder}thumb-%04d.jpg" -filter_complex tile='${imageLine}x${Math.ceil(imageFiles.length / imageLine)}' -y "${spriteFile}"  2>&1`;
			imageFiles = imageFiles.sort();

			let times = createTimeInterval(ffprobe.format.duration, interval);

			let jpg = 1;
			let line = 1;

			thumb_content.push('WEBVTT');
			times.forEach((time, index) => {
				thumb_content.push(jpg);
				thumb_content.push(time + ' --> ' + times[index + 1]);
				thumb_content.push('sprite.webp#xywh=' + dst_x + ',' + dst_y + ',' + thumbWidth + ',' + thumbHeight);
				thumb_content.push('');
				if (line <= maxLines) {
					if (jpg % imageLine == 0) {
						dst_x = 0;
						dst_y += thumbHeight;
					} else {
						dst_x += thumbWidth;
					}
					jpg++;
				}
			});

			fs.writeFileSync(previewFiles, thumb_content.join('\n'));

			await new Promise((resolve, reject) => {
				exec(montageCommand, { maxBuffer: 1024 * 5000 }, (e) => {
					fs.existsSync(thumbailsFolder) && fs.rmSync(thumbailsFolder, { recursive: true });
				});
				resolve();
			});
		}
	}
};

export const buildVideoFilter = function (stream) {
	if (stream.hdr == true) {
		return '-vf fps=fps=1/10' + hdrMap();
	}
	return '-vf fps=fps=1/10';
};

export default {
	thumbnails,
	createThumbnails,
};
