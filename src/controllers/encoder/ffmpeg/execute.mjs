import chalk from 'chalk';
import { convertSubToVtt } from './subtitle.mjs';
import { createThumbnails } from './preview.mjs';
import { exec } from 'child_process';
import { fileURLToPath } from 'url';
import fs from 'fs';
import path from 'path';

let publicFolder = __dirname + '/../../../../public/';
let logFolder = publicFolder + 'working';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let activeWorkers = 0;

let exec_commands = async (commands, ffprobes, outputFolders, filesIn, dbindexes, cb = () => null) => {
	for (let i = 0; i < config.workers; i++) {
		activeWorkers += 1;
		let command = commands.shift();
		let ffprobe = ffprobes.shift();
		let outputFolder = outputFolders.shift();
		let fileIn = filesIn.shift();
		let dbindex = dbindexes.shift();

		if (command) {
			exec(
				command,
				{
					cwd: outputFolder,
					maxBuffer: 1024 * 5000,
				},
				async (error, stdout, stderr) => {
					if (error) {
						console.error(chalk.red(error));
					}
					await convertSubToVtt(outputFolder + 'subtitles/');
					await createThumbnails(ffprobe, outputFolder);

					if (fs.existsSync(logFolder + `/${dbindex}` + '.txt')) {
						fs.rmSync(logFolder + `/${dbindex}` + '.txt');
					}

					activeWorkers -= 1;
					console.log('Active workers: ' + activeWorkers);

					if (commands.length > 0 && activeWorkers < config.workers) {
						await exec_commands(commands, ffprobes, outputFolders, filesIn, dbindexes);
					}

					if (activeWorkers == 0) {
						console.log(outputFolder.replace(/[\\\/][\w\.]*[\\\/]$/, ''));

						const child = fork('./src/controllers/forks/processAllFiles.mjs', {
						});
						child.send({ force: true, folder: item.baseFolder.to.replace(/[\\\/][\w\.]*[\\\/]$/, '') });

						child.on('message', (data) => {
							return res.json({
								success: true,
								message: 'Updated successfully',
								data: data,
							});
						});
						console.log('removing: ' + fileIn);
						cb();
					}
				}
			);
		}
	}
};

export default exec_commands;
