import fs from 'fs';

const makeAttachmentsFile = function (attachments, location = null) {
	const attachmentsFile = location + '/fonts.json';
	if (attachments.length > 0) {
		let data = [];

		attachments.map((c) => {
			data.push({
				file: c.filename.toLowerCase(),
				mimeType: c.mimetype,
			});
		});
		fs.writeFileSync(attachmentsFile, JSON.stringify(data));
	}
};

export default makeAttachmentsFile;
