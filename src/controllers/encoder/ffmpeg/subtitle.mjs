import { exec } from 'child_process';
import { execSync } from 'child_process';
import { fileURLToPath } from 'url';
import forkit from 'node-forkit';
import fs from 'fs';
import { isoToName } from './language.mjs';
import path from 'path';
import { unique } from '../../../helpers/stringArray.mjs';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const subtitleEdit = __dirname + `/../../../binaries/SubtitleEdit/SubtitleEdit.exe`;

export const subtitle = async function (ffprobe, outputFolder, fileName) {
	let subtitleMap = [];
	let subtitle = [];
	let subEdit = false;

	let dir = outputFolder + 'subtitles/';
	fs.mkdirSync(dir, { recursive: true });
	convertSubToVtt(dir);
	
	// rename(dir, fileName, () => {
	//     if (ffprobe.format.filename.includes('Ripper')) {
	//         if(fs.existsSync(ffprobe.format.filename)){
	//             fs.rmSync(ffprobe.format.filename);
	//         }
	//         let folder = ffprobe.format.filename.replace(/[\\\/][\w\s_-]*\.\w{3,}$/, '')
	//         if (fs.readdirSync(folder).length == 0) {
	//             fs.rmSync(folder, { recursive: true });
	//         }
	//     }
	// });

	ffprobe.streams.subtitle.map((stream, index) => {
		let ext = getExtension(stream.codec_name);
		let type = getSubType(stream.title, index);

		if(!stream.language){
			stream.language = 'eng';
		}
		
		let subFileBase = 'subtitles/' + fileName + '.' + stream.language + '.' + type + '.' + ext;
		let subFile = dir + fileName + '.' + stream.language + '.' + type + '.' + ext;

		if (ext == 'sup' && config.keepOriginal.subtitles && !fs.existsSync(subFile.replace('/subtitles', '/original'))) {
			subtitleMap.push('-map 0:' + stream.index);
			subtitleMap.push('-c:s copy');
			subtitleMap.push('"' + subFileBase + '"');
			subtitle.push({
				type: ext,
				lang: stream.language,
				stream: stream.index,
			});
		} else if (!fs.existsSync(subFile) && ext.match(/ass|vtt/)) {
			subtitleMap.push('-map 0:' + stream.index);

			if (ext.match(/ass|sub/)) {
				subtitleMap.push('-c:s copy');
			}
			subtitleMap.push('"' + subFileBase + '"');
			subtitle.push({
				type: ext,
				lang: stream.language,
				stream: stream.index,
			});
		} else if (!fs.existsSync(subFile) && !ext.match(/ass|vtt/)) {
			subtitle.push({
				type: 'Subtitle Edit',
				lang: stream.language,
				stream: stream.index,
			});
			subEdit = true;
		}
	});

	// if (subEdit) {
	//     exec(`${subtitleEdit} /convert "${ffprobe.format.filename}" WebVtt /outputfolder:"${dir}" /ocrengine:tesseract`, () => {
	//         rename(dir, fileName, () => {
	//             if (ffprobe.format.filename.includes('Ripper')) {
	//             }
	//         });
	//     });
	// }
	// else {
	//     rename(dir, fileName, () => {
	//         if (ffprobe.format.filename.includes('Ripper')) {
	//         }
	//     });
	// }

	let log = subtitle
		.filter((l) => l.audio !== null)
		.map((l) => ' s:' + l.stream + ':' + l.lang + '.' + l.type + ' ')
		.join(' ');

	return {
		subtitleMap,
		log,
	};
};

export const getSubtitleFiles = function (ffprobe, outputFolder, fileName) {
	let subtitles = [];

	let dir = outputFolder + 'subtitles/';

	ffprobe.streams.subtitle.forEach((stream, index) => {
		let ext = getExtension(stream.codec_name);
		let type = getSubType(stream.title, index);
		let subFile = dir + fileName + '.' + stream.language + '.' + type + '.' + ext;

		if (ext.match(/ass|vtt|sup/)) {
			subtitles.push(subFile);
		}
	});

	return subtitles;
};

export const rename = (dir, fileName, cb = () => null) => {
	let files = fs.readdirSync(dir).filter((f) => !f.match(/full|sign|sdh|forced/i));
	let arr = [];
	files.forEach((f, index) => {
		let reg = /(?<lang>\w{3})\.\w{3}$/.exec(f);
		if (reg.groups) {
			arr[reg.groups.lang] = [];
		}
	});
	// console.log(arr);

	files.forEach((f, index) => {
		let reg = /(?<lang>\w{3})\.\w{3}$/.exec(f);
		if (reg.groups) {
			arr[reg.groups.lang].push(f);
		}
	});

	arr = arr.sort(function (a, b) {
		return fs.statSync(dir + b).size - fs.statSync(dir + a).size;
	});

	Object.values(arr).forEach((a) => {
		a.forEach((f, index) => {
			let reg = /(?<lang>\w{3})\.\w{3}$/.exec(f);
			if (reg.groups) {
				if (index == 0) {
					fs.renameSync(dir + f, dir + fileName + '.' + reg.groups.lang + '.full.vtt');
				}
				if (index == 1) {
					fs.renameSync(dir + f, dir + fileName + '.' + reg.groups.lang + '.sign.vtt');
				}
				if (index == 2) {
					fs.renameSync(dir + f, dir + fileName + '.' + reg.groups.lang + '.sdh.vtt');
				}
				if (index == 3) {
					fs.renameSync(dir + f, dir + fileName + '.' + reg.groups.lang + '.forced.vtt');
				}
			}
		});
	});
	cb();
};

export const getExtension = function (codec_name) {
	let extension;
	switch (codec_name) {
		case 'ass':
		case 'ssa':
			extension = 'ass';
			break;
		case 'hdmv_pgs_subtitle':
		case 'pgs_subtitle':
			extension = 'sup';
			break;
		case 'dvdsub':
		case 'dvd_subtitle':
			extension = 'sub';
			break;
		default:
			extension = 'vtt';
			break;
	}
	return extension;
};

export const getSubType = function (title, index = 0) {
	if (!title && index == 0) return 'full';
	// else if(!title && index == 1) return 'sign'
	else if (!title) return 'full';
	else if (title.match(/sign/i)) return 'sign';
	else if (title.match(/forced/i)) return 'forced';
	else if (title.match(/sdh/i)) return 'sdh';
	else if (title.match(/full/i)) return 'full';
	return 'full';
};

export const convertSubToVtt = async function (folder) {
	if (fs.existsSync(folder)) {
		//TODO: convert sup first and then convert ass if sup doesnt exist.
		let files = fs.readdirSync(folder);
		for(let i = 0; i < files.length; i++) {
		    let s = files[i];
				if (
					(!s.match(/.ass$|.vtt$/) && !fs.existsSync(folder + s.replace(/\.\w{3}$/, '.vtt'))) 
					|| (config.assToVtt && !s.endsWith('.vtt') && !fs.existsSync(folder + s.replace(/\.\w{3}$/, '.vtt')))) {
					try {
						await execSync(`${subtitleEdit} /convert "${folder + '/' + s}" WebVtt`);
						if (config.keepOriginal.subtitles && fs.existsSync(folder + s) && !s.match(/.ass$|.vtt$/)) {
							try {
								fs.renameSync(folder + s, folder + '../original/' + s);
							} catch (error) {}
						} else if (fs.existsSync(folder + s) && !s.match(/.ass$|.vtt$/)) {
							try {
								fs.removeSync(folder + s);
							} catch (error) {}
						}
					} catch (error) {}
				}
			}
	}
};

export const getExistingSubtitles = function (folder) {
	if (!fs.existsSync(folder)) return [];

	let files = fs.readdirSync(folder);
	let arr = [];
	files
		.filter((f) => !f.match(/-\w{5,}\.\w{3}$/))
		.filter((f) => f.match(/.ass$|.vtt$/))
		.forEach((f) => {
			const reg = /(?<lang>\w{3}).(?<type>\w{3,4}).(?<ext>\w{3})$/.exec(f);
			if (reg?.groups) {
				arr.push({
					language: reg.groups.lang,
					type: reg.groups.type,
					ext: reg.groups.ext,
				});
			}
		});

		return arr;
};

export default {
	subtitle,
	convertSubToVtt,
	getSubtitleFiles,
	getExistingSubtitles,
	rename,
};
