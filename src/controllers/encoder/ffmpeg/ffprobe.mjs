import Logger from '../../../loaders/logger.mjs';
import configuration from '../../../config/index.mjs';
import fffmpeg from 'fluent-ffmpeg';
import { fileURLToPath } from 'url';
import fs from 'fs';
import path from 'path';
import { platform } from 'os-utils';
import { sortByPriorityKeyed } from '../../../helpers/stringArray.mjs';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const ffprobe = path.join(__dirname, '..', '..', '..', 'binaries', 'ffprobe' + (platform() == 'win32' ? '.exe' : '')).replace(/\\/g, '\\\\');

export const getVideoInfo = (file) => {
	return new Promise(async (resolve, reject) => {
		await configuration();
		
		fffmpeg.setFfprobePath(ffprobe);

		Logger.log({
			level: 'verbose',
			name: 'Encoder',
			color: 'cyanBright',
			message: 'Getting file info from: ' + file,
		});

		return fffmpeg.ffprobe(file, ['-show_chapters'], async (error, videoInfo) => {
			if (error) {
				fs.appendFileSync(config.errorLog, error.toString().replace(/[\w\s\d\W\D\S\n\r]*\n/, '') + '\n');
				return reject({ file, error });
			}
			let video = [];
			let audio = [];
			let subtitle = [];
			let format = [];
			let chapters = [];
			let attachments = [];

			await Promise.all(
				videoInfo.streams.map((stream) => {
					if (stream.codec_name !== 'mjpeg' && stream.codec_type == 'video') {
						video.push({
							index: stream.index,
							codec_name: stream.codec_name,
							codec_long_name: stream.codec_long_name,
							profile: stream.profile,
							codec_type: stream.codec_type,
							width: stream.width,
							height: stream.height,
							coded_width: stream.coded_width,
							coded_height: stream.coded_height,
							has_b_frames: stream.has_b_frames,
							sample_aspect_ratio: stream.sample_aspect_ratio,
							display_aspect_ratio: stream.display_aspect_ratio,
							pix_fmt: stream.pix_fmt,
							level: stream.level,
							color_range: stream.color_range,
							color_space: stream.color_space,
							color_transfer: stream.color_transfer,
							color_primaries: stream.color_primaries,
							chroma_location: stream.chroma_location,
							field_order: stream.field_order,
							refs: stream.refs,
							is_avc: stream.is_avc,
							nal_length_size: stream.nal_length_size,
							r_frame_rate: stream.r_frame_rate,
							avg_frame_rate: stream.avg_frame_rate,
							time_base: stream.time_base,
							bits_per_raw_sample: stream.bits_per_raw_sample,
							size: stream.tags?.[`NUMBER_OF_BYTES-${stream.tags?.language || 'eng'}`],
							bit_rate: stream.bit_rate,
							language: stream.tags?.language,
							hdr: stream.color_space.includes('bt2020') || stream.color_primaries.includes('bt2020'),
						});
					}
					if (stream.codec_type == 'audio') {
						audio.push({
							index: stream.index,
							codec_name: stream.codec_name,
							codec_long_name: stream.codec_long_name,
							codec_type: stream.codec_type,
							codec_time_base: stream.codec_time_base,
							codec_tag_string: stream.codec_tag_string,
							sample_fmt: stream.sample_fmt,
							sample_rate: stream.sample_rate,
							channels: stream.channels,
							channel_layout: stream.channel_layout,
							bits_per_sample: stream.bits_per_sample,
							dmix_mode: stream.dmix_mode,
							ltrt_cmixlev: stream.ltrt_cmixlev,
							ltrt_surmixlev: stream.ltrt_surmixlev,
							loro_cmixlev: stream.loro_cmixlev,
							loro_surmixlev: stream.loro_surmixlev,
							time_base: stream.time_base,
							bit_rate: stream.bit_rate,
							size: stream.tags?.[`NUMBER_OF_BYTES-${stream.tags?.language || 'eng'}`],
							language: stream.tags?.language?.replace('fra', 'fre') || 'und',
							title: stream.tags?.title,
						});
					}
					if (stream.codec_type == 'subtitle') {
						subtitle.push({
							index: stream.index,
							codec_name: stream.codec_name,
							codec_long_name: stream.codec_long_name,
							codec_type: stream.codec_type,
							codec_time_base: stream.codec_time_base,
							codec_tag_string: stream.codec_tag_string,
							codec_tag: stream.codec_tag,
							time_base: stream.time_base,
							size: stream.tags?.[`NUMBER_OF_BYTES-${stream.tags?.language || 'eng'}`],
							language: stream.tags?.language,
							title: stream.tags?.title,
						});
					}
					if (stream.codec_type == 'attachment') {
						attachments.push({
							index: stream.index,
							codec_name: stream.codec_name,
							codec_long_name: stream.codec_long_name,
							codec_type: stream.codec_type,
							codec_time_base: stream.codec_time_base,
							codec_tag_string: stream.codec_tag_string,
							codec_tag: stream.codec_tag,
							time_base: stream.time_base,
							filename: stream.tags?.filename,
							mimetype: stream.tags?.mimetype,
						});
					}
				}),
				videoInfo.chapters.map((chapter, index) => {
					chapters.push({
						index: index,
						title: chapter['TAG:title'],
						start_time: chapter.start_time,
						end_time: chapter.end_time,
					});
				}),
				(format = {
					filename: videoInfo.format.filename,
					nb_streams: videoInfo.format.nb_streams,
					nb_programs: videoInfo.format.nb_programs,
					format_name: videoInfo.format.format_name,
					format_long_name: videoInfo.format.format_long_name,
					start_time: videoInfo.format.start_time,
					duration: videoInfo.format.duration,
					size: videoInfo.format.size,
					bit_rate: videoInfo.format.bit_rate,
					probe_score: videoInfo.format.probe_score,
					title: videoInfo.format.tags?.title,
					encoder: videoInfo.format.tags?.encoder,
					creation_time: videoInfo.format.tags?.creation_time,
				})
			);

			if (video.length == 0 && !file.includes('audio')) {
				fs.appendFileSync(config.errorLog, file + ' has no video stream\n');
				return reject(file + ' has no video stream');
			}

			return resolve({
				streams: {
					video,
					audio: audio.sort(sortByPriorityKeyed(config.prefferedOrder, 'language')),
					// audio: audio.sort(sortByPriorityKeyed(channelOrder, 'channels')).sort(sortByPriorityKeyed(config.prefferedOrder, 'language')),
					subtitle,
					attachments,
				},
				format,
				chapters,
			});
		});
	});
};

export const getAudioInfo = (file) => {
	return new Promise(async (resolve, reject) => {
		await configuration();

		fffmpeg.setFfprobePath(ffprobe);

		Logger.log({
			level: 'verbose',
			name: 'Encoder',
			color: 'cyanBright',
			message: 'Getting file info from: ' + file,
		});

		return fffmpeg.ffprobe(file, async (error, audioInfo) => {
			if (error) {
				fs.appendFileSync(config.errorLog, error.toString().replace(/[\w\s\d\W\D\S\n\r]*\n/, '') + '\n');
				return reject({ file, error });
			}
			let audio = [];
			let format = {};
			let tags = {};
			
		
			Logger.log({
				level: 'debug',
				name: 'Encoder',
				color: 'cyanBright',
				message: JSON.stringify(audioInfo),
				
			});

			await Promise.all(
				audioInfo.streams.map((stream) => {
					if (stream.codec_type == 'audio') {
						audio = {
							index: stream.index,
							codec_name: stream.codec_name,
							codec_long_name: stream.codec_long_name,
							codec_type: stream.codec_type,
							codec_time_base: stream.codec_time_base,
							codec_tag_string: stream.codec_tag_string,
							sample_fmt: stream.sample_fmt,
							sample_rate: stream.sample_rate,
							channels: stream.channels,
							channel_layout: stream.channel_layout,
							bits_per_sample: stream.bits_per_sample,
							time_base: stream.time_base,
							bit_rate: stream.bit_rate,
						};
					}
				}),
				(format = {
					filename: audioInfo.format.filename,
					nb_streams: audioInfo.format.nb_streams,
					nb_programs: audioInfo.format.nb_programs,
					format_name: audioInfo.format.format_name,
					format_long_name: audioInfo.format.format_long_name,
					start_time: audioInfo.format.start_time,
					duration: audioInfo.format.duration,
					size: audioInfo.format.size,
					bit_rate: audioInfo.format.bit_rate,
				}),
				(tags = {
					acoustid_id: audioInfo.format.tags['Acoustid Id'] || audioInfo.format.tags.ACOUSTID_ID,
					MusicBrainz_album_artist_id: audioInfo.format.tags['MusicBrainz Album Artist Id'] || audioInfo.format.tags.MUSICBRAINZ_ALBUMARTISTID,
					MusicBrainz_album_id: audioInfo.format.tags['MusicBrainz Album Id'] || audioInfo.format.tags.MUSICBRAINZ_ALBUMID,
					MusicBrainz_album_release_country: audioInfo.format.tags['MusicBrainz Album Release Country'] || audioInfo.format.tags.RELEASECOUNTRY,
					MusicBrainz_album_status: audioInfo.format.tags['MusicBrainz Album Status'] || audioInfo.format.tags.RELEASESTATUS,
					MusicBrainz_album_type: audioInfo.format.tags['MusicBrainz Album Type'] || audioInfo.format.tags.RELEASETYPE,
					MusicBrainz_release_group_id: audioInfo.format.tags['MusicBrainz Release Group Id'] || audioInfo.format.tags.MUSICBRAINZ_RELEASEGROUPID,
					MusicBrainz_release_track_id: audioInfo.format.tags['MusicBrainz Release Track Id'] || audioInfo.format.tags.MUSICBRAINZ_RELEASETRACKID,
					MusicBrainz_work_id: audioInfo.format.tags['MusicBrainz Work Id'],
					'artist-sort': audioInfo.format.tags['artist-sort'] || audioInfo.format.tags.ARTISTSORT,

					ARTISTS: audioInfo.format.tags.ARTISTS?.split(';').map(a => a.trim()) || audioInfo.format.tags.ALBUMARTISTSORT?.split(';').map(a => a.trim()),
					ASIN: audioInfo.format.tags.ASIN,
					BARCODE: audioInfo.format.tags.BARCODE,
					CATALOGNUMBER: audioInfo.format.tags.CATALOGNUMBER || audioInfo.format.tags.CATALOGNUMBER,
					MusicBrainz_artist_ids: audioInfo.format.tags['MusicBrainz Artist Id']?.split(';').map(a => a.trim()) || audioInfo.format.tags.MUSICBRAINZ_ARTISTID?.split(';').map(a => a.trim()),
					SCRIPT: audioInfo.format.tags.SCRIPT || audioInfo.format.tags.SCRIPT,
					TDOR: audioInfo.format.tags.TDOR,
					TIPL: audioInfo.format.tags.TIPL,
					TIT1: audioInfo.format.tags.TIT1,
					TMED: audioInfo.format.tags.TMED,
					TSO2: audioInfo.format.tags.TSO2,
					TSRC: audioInfo.format.tags.TSRC,
					Writer: audioInfo.format.tags.Writer,
					album: audioInfo.format.tags.album || audioInfo.format.tags.ALBUM,
					album_artist: audioInfo.format.tags.album_artist,
					artist: audioInfo.format.tags.artist || audioInfo.format.tags.ARTIST,
					date: audioInfo.format.tags.date || audioInfo.format.tags.DATE,
					disc: audioInfo.format.tags.disc,
					genre: audioInfo.format.tags.genre?.split(';').map(g => g.trim()) || audioInfo.format.tags.GENRE?.split(';').map(g => g.trim()),
					language: audioInfo.format.tags.language,
					originalyear: audioInfo.format.tags.originalyear || audioInfo.format.tags.ORIGINALYEAR,
					publisher: audioInfo.format.tags.publisher,
					title: audioInfo.format.tags.title || audioInfo.format.tags.TITLE,
					track: audioInfo.format.tags.track,
				})
			);
			return resolve({
				audio,
				format,
				tags,
			});
		});
	});
};

export default {
	getVideoInfo,
	getAudioInfo,
};
