// import youtubedl from 'youtube-dl-exec';

/**
 * downloads a youtube video
 * @param {string} url full url or Id.
 * @param {boolean} audioOnly if set to true it extracts the audio to mp3.
 * @param {string} folder folder inside media root.
 * @returns video or audio file.
 */
const download = function (url, audioOnly, folder, cb) {
	// youtubedl(url, {
	// 	noWarnings: true,
	// 	noCallHome: true,
	// 	noCheckCertificate: true,
	// 	youtubeSkipDashManifest: true,
	// 	noPostOverwrites: true,
	// 	restrictFilenames: true,
	// 	writeThumbnail: true,
	// 	writeInfoJson: true,
	// 	preferFfmpeg: true,
	// 	extractAudio: audioOnly ? true : null,
	// 	audioFormat: audioOnly ? 'mp3' : 'aac',
	// 	format: audioOnly ? 'bestaudio' : 'mp4',
	// 	audioQuality: 0,
	// 	addMetadata: true,
	// 	embedThumbnail: true,
	// 	ignoreErrors: true,
	// 	geoBypass: true,
	// 	ffmpegLocation: __dirname + '/../../../binaries/ffmpeg',
	// 	o: '/../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../../' + folder + '/%(uploader)s/%(title)s.%(ext)s',
	// })
	// 	.then(cb)
	// 	.catch((output) => console.log(output));
};

const link = async function (url) {
	return new Promise((resolve, reject) => {
		// let data = youtubedl(url, {
		// 	getUrl: true,
		// 	ignoreErrors: true,
		// 	geoBypass: true,
		// 	noWarnings: true,
		// 	noCallHome: true,
		// 	noCheckCertificate: true,
		// 	youtubeSkipDashManifest: true,
		// });

		// resolve(data);
	}).catch((e) => e);
};

export default {
	download,
	link,
};
