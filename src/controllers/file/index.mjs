import { addToDatabase, getFileList, getFiles, getFolderList, getFolders, getParsedFileList, getParsedFolderList, scanAllFolders } from './fileController.mjs';
import { anime_regexes, dvd_regexes, movie_regexes, normal_regexes } from './nameRegexes.mjs';
import { cleanFileName, createBaseFolderName, createFileName, createRootFolderName, createShowFolderName, getEpisodeIndex, getFileData, getFolderData, parseFileName } from './filenameParser.mjs';

export default {
	addToDatabase,
	getFiles,
	getFileList,
	getFolders,
	getFolderList,
	getParsedFolderList,
	scanAllFolders,
	parseFileName,
	getFolderData,
	getFileData,
	createRootFolderName,
	createBaseFolderName,
	createShowFolderName,
	createFileName,
	cleanFileName,
	getEpisodeIndex,
	getParsedFileList,
	normal_regexes,
	anime_regexes,
	dvd_regexes,
	movie_regexes,
};
