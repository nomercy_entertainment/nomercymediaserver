import { convertToHuman, fileChangedAgo } from '../../helpers/dateTime.mjs';
import { groupBy, sort_by } from '../../helpers/stringArray.mjs';

import Logger from '../../loaders/logger.mjs';
import configuration from '../../config/index.mjs';
import { fileURLToPath } from 'url';
import fs from 'fs';
import { getExistingSubtitles } from '../encoder/ffmpeg/subtitle.mjs';
import { getQualityTag } from '../encoder/ffmpeg/quality.mjs';
import { getVideoInfo } from '../encoder/ffmpeg/ffprobe.mjs';
import path from 'path';
import { prismaMedia } from '../../config/database.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export const getFiles = async function (dirPath, arrayOfFiles, folderFile, filter = ['.mp4', '.mkv', '.avi', '.ogv', '.NoMercy.m3u8']) {
	if (!fs.existsSync(dirPath)) {
		Logger.log({
			level: 'info',
			name: 'App',
			color: 'magentaBright',
			message: 'Folder: ' + dirPath + " Doesn't exist.",
		});
		return;
	}

	let files = fs.readdirSync(dirPath);
	
	arrayOfFiles = arrayOfFiles || [];

	files.forEach(function (file) {
		let folder = path.join(dirPath, '/', file);
		
		if (fs.existsSync(folder)) {
			let isDir = fs.statSync(folder).isDirectory();

			if (!/video\_.*|audio\_.*|subtitles.*|scans.*|cds.*/i.exec(file) && isDir) {
				getFiles(folder, arrayOfFiles, folderFile, filter);
			} else if (!isDir) {
				if (filter.some((v) => file.includes(v))) {
					arrayOfFiles.push(path.join(dirPath, '/', file));
				}
			}
		}
	});
	return arrayOfFiles.sort();
};

export const getFileList = async function (folder, filter) {
	let allFiles = [];
	
	if(!fs.existsSync(path.join(__dirname, '..', '..', '..', 'cache'))){
		fs.mkdirSync(path.join(__dirname, '..', '..', '..', 'cache'), {recursive: true});
	}

	let folderFile = path.join(__dirname, '..', '..', '..', 'cache', folder.replace(/[\/\\]/g, '_').replace(':', '_') + '.json');
	if (fs.existsSync(folderFile) && fileChangedAgo(folderFile, 'minutes') < 5) {
		return JSON.parse(fs.readFileSync(folderFile));
	} else if (fs.existsSync(folder)) {
		Logger.log({
			level: 'verbose',
			name: 'App',
			color: 'magentaBright',
			message: 'Starting File Listing ' + folder,
		});

		let result = sort_by(await getFiles(folder, allFiles, folderFile, filter), 'OutputFileName');

		fs.writeFileSync(folderFile, JSON.stringify(result));

		Logger.log({
			level: 'verbose',
			name: 'App',
			color: 'magentaBright',
			message: 'FileListing done',
		});
		return result;
	} else {
		Logger.log({
			level: 'info',
			name: 'App',
			color: 'magentaBright',
			message: 'Folder: ' + folder + " Doesn't exist.",
		});
		return [];
	}
};

export const getFolders = async function (dirPath, arrayOfFiles) {
	let files = fs.readdirSync(dirPath);

	arrayOfFiles = arrayOfFiles || [];

	for (let i = 0; i < files.length; i++) {
		let file = files[i];
		if (fs.statSync(dirPath + '/' + file).isDirectory()) {
			arrayOfFiles.push(dirPath + '/' + file);
		}
	}

	return arrayOfFiles;
};

export const getFolderList = async function (folder, allFolders) {
	allFolders = allFolders || [];
	const result = await getFolders(folder, allFolders);

	return result;
};

export const getParsedFolderList = async function (folder) {
	let folders = [];

	const list = await getFolders(folder, folders);

	return new Promise((resolve, reject) => {
		let res = list.map((data) => {
			let reg = /.*[\\\/](?<type>[\w\.\s]*)[\\\/][\w\.\s]*[\\\/](?<name>.*)[\.\s]\((?<year>\d{4})\)$/i.exec(data);
			if (reg) {
				let folderData = config.folderRoots.find((r) => r.path.includes(/(?<type>Anime|Films|TV.Shows)[\\\/](?<direction>Anime|Films|TV.Shows|Download)/.exec(data)?.[0]));
				if(folderData){
					return {
						folder: reg[0].replace(/\/{1,}/g, '/'),
						type: folderData.type,
						title: reg.groups.name
							.replace(/([^\.]{2,})(\.)/g, '$1 ')
							.replace(/\.$/, '')
							.replace(/([^\.]{2,})/g, ' $1')
							.replace(/^\s*/, ''),
						year: reg.groups.year,
						share: folderData.share,
						share_root: folderData.share_root,
					};
				}
			}
			if (!reg) {
				reg = /.*[\\\/](?<type>.*)[\\\/].*[\\\/](?<name>.+).(?<year>\d{4}).(?<quality>3640p|1080p|720p|480p).+.(?<ext>mp4|avi|mkv)$/.exec(data);
				if (reg) {
					let folderData = config.folderRoots.find((r) => r.path.includes(/(?<type>Anime|Films|TV.Shows)[\\\/](?<direction>Anime|Films|TV.Shows)/.exec(data)?.[0]));
					if(folderData){
						res.push({
						folder: reg[0].replace(/\/{1,}/g, '/'),
							type: folderData.type,
							title: reg.groups.name
								.replace(/([^\.]{2,})(\.)/g, '$1 ')
								.replace(/\.$/, '')
								.replace(/([^\.]{2,})/g, ' $1')
								.replace(/^\s*/, ''),
							year: reg.groups.year,
							share: folderData.share,
							share_root: folderData.share_root,
						});
					}
				}
			}
			if (!reg) {
				reg = /.*[\\\/](?<type>.*)[\\\/](?<direction>films|movies|download)[\\\/]?.*?[\\\/](?<name>.+)(\s|\.|\()(?<year>(19|20)[0-9][0-9])(\)|.*|(?!p)).(?<quality>3640p|1080p|720p|480p)?.+.(?<ext>mp4|avi|mkv)$/i.exec(data);
				if (reg) {
					let folderData = config.folderRoots.find((r) => r.path.includes(/(?<type>Anime|Films|TV.Shows)[\\\/](?<direction>Anime|Films|TV.Shows)/.exec(data)?.[0]));
					if(folderData){
						res.push({
						folder: reg[0].replace(/\/{1,}/g, '/'),
							type: folderData.type,
							title: reg.groups.name
								.replace(/([^\.]{2,})(\.)/g, '$1 ')
								.replace(/\.$/, '')
								.replace(/([^\.]{2,})/g, ' $1')
								.replace(/^\s*/, ''),
							year: reg.groups.year,
							share: folderData.share,
							share_root: folderData.share_root,
						});
					}
				}
			}
		});
		resolve(res);
	});
};

export const getParsedFileList = async function (folder, filter) {
	let res = [];
	const list = await getFileList(folder, filter);
	Logger.log({
		level: 'verbose',
		name: 'App',
		color: 'magentaBright',
		message: list,
		
	});
	if (list) {
		list.forEach(function (data) {
			let reg = /.*[\\\/](?<type>[\w\.\s]*)[\\\/][\w\.\s]*[\\\/](?<folder>.*[\.\s]\((?<year>\d{4})\))([\\\/](?<foldername>.*))?[\\\/](?<filename>.*S(?<season>\d{1,3})E(?<episode>\d{1,3}).*)\.(?<ext>\w{3,4})/.exec(data);

			if (reg) {
				res.push({
					file: reg[0],
					type: reg.groups.type,
					folder: reg.groups.folder,
					show:
						/(.*)\.\(\d{4}\)/.exec(reg.groups.folder) && reg[1] != 'Films'
							? /(.*)\.\(\d{4}\)/
									.exec(reg.groups.folder)[1]
									.replace(/([^\.]{2,})(\.)/g, '$1 ')
									.replace(/\.$/, '')
									.replace(/([^\.]{2,})/g, ' $1')
									.replace(/^\s*/, '')
							: null,
					title: /.*S\d{2}E\d{2}\.(.*).NoMercy/.exec(reg.groups.foldername)
						? /.*S\d{2}E\d{2}\.(.*).NoMercy/
								.exec(reg.groups.foldername)[1]
								.replace(/([^\.]{2,})(\.)/g, '$1 ')
								.replace(/\.$/, '')
								.replace(/([^\.]{2,})/g, ' $1')
								.replace(/^\s*/, '')
						: /(.*)\.\(\d{4}\)/.exec(reg.groups.folder)
						? /(.*)\.\(\d{4}\)/
								.exec(reg.groups.folder)[1]
								.replace(/([^\.]{2,})(\.)/g, '$1 ')
								.replace(/\.$/, '')
								.replace(/([^\.]{2,})/g, ' $1')
								.replace(/^\s*/, '')
						: null,
					year: reg.groups.year,
					foldername: reg.groups.foldername,
					filename: reg.groups.filename,
					season: reg.groups.season,
					episode: reg.groups.episode,
					ext: reg.groups.ext,
				});
			} else {
				if (!reg) reg = /.*[\\\/](?<type>films|movies)[\\\/](?<direction>films|movies|download)[\\\/](?<foldername>.*)[\\\/](?<filename>(?<title>.+)(\s|\.|\()(?<year>(19|20)[0-9][0-9])(\)|.*|(?!p))).(?<ext>mp4|avi|mkv|m3u8)$/i.exec(data);
				if (reg) {
					res.push({
						file: reg[0],
						type: reg.groups.type,
						foldername: reg.groups.foldername,
						filename: reg.groups.filename,
						title: reg.groups.title,
						year: reg.groups.year,
						ext: reg.groups.ext,
					});
				}
			}
		});
		Logger.log({
			level: 'verbose',
			name: 'App',
			color: 'magentaBright',
			message: JSON.stringify(res),
		});
		return res;
	}
};

export const scanAllFolders = async function (direction) {
	await configuration();

	let allSearches = [];
	let allRoots;
	if(!direction){
		allRoots = config.folderRoots.filter((r) => r.enabled);
	}
	else{
		allRoots = config.folderRoots.filter((r) => r.direction == direction && r.enabled);
	}

	for (let i = 0; i < allRoots.length; i++) {
		let r = allRoots[i];

		let title = await getParsedFolderList(r.path);

		for (let i = 0; i < title.length; i++) {
			let t = title[i];
			allSearches.push(t);
		}
	}

	return allSearches;
};

export const findMovieFiles = async function (id, t) {
	
	
	if (!id) {
		let movie = await prismaMedia.movie.findFirst({
			where: {
				OR: [
					{
						folder: t.folder.replace(/\/{1,}/g, '/').replace(/\/$/,''),
					},
					{
						folder: t.folder.replace(/\/$/,''),
					},
					{
						folder: t.folder,
					},
				],
			},
			select: {
				id: true,
			},
		});
		id = movie?.id;
	}

	let transaction = [];
	if (id) {
		let files = await getParsedFileList(t.folder, ['.NoMercy.mp4', '.NoMercy.mkv', '.NoMercy.m3u8']);
		for (let i = 0; i < files.length; i++) {
			let f = files[i];

			let ffprobe = await getVideoInfo(f.file).catch((reason) => {
				Logger.log({
					level: 'error',
					name: 'Encoder',
					color: 'red',
					message: 'Error while getting video info: ' + reason.file + ', reason: ' + (reason.error || 'unknown'),
				});
			});

			if (ffprobe?.format && ffprobe?.streams) {
				const duration = convertToHuman(ffprobe.format.duration).replace(/^00:/, '');
				const languages = ffprobe.streams.audio
					.filter((a) => a.language != null && a.language != 'undefinded' && a.language != 'und')
					.map((a) => a.language)
					.join(',');
				const subtitles = getExistingSubtitles(t.folder + '/subtitles')
					.map((s) => s.language + '.' + s.type + '.' + s.ext)
					.join(',');

				let videoInsert = {
					movieId: parseInt(id),
					host_folder: f.file.replace(/[\\\/][^\\\/]+(?=.*\w*)$/, ''),
					duration: ffprobe.format ? duration : null,
					languages: ffprobe.format ? languages : null,
					subtitles: ffprobe.format ? subtitles : null,
					quality: getQualityTag(ffprobe),
					share: config.share,
					folder: f.type + '/' + f.type + '/' + f.foldername + '/',
					filename: f.filename + '.' + f.ext,
				};

				transaction.push(
					prismaMedia.video_file.upsert({
						where: {
							movieId: parseInt(id),
						},
						update: videoInsert,
						create: videoInsert,
					})
				);
			}
		}
	}

	try {
		await prismaMedia.$transaction(transaction);
	} catch (error) {
		try {
			await prismaMedia.$transaction(transaction);
		} catch (error) {
			try {
				await prismaMedia.$transaction(transaction);
			} catch (error) {
				//
			}
		}
	}
};

export const contains = function (array, value) {
	var found = false;
	for (var i = 0; i < array.length; i++) {
		if (array[i].includes(value)) {
			found = true;
			break;
		}
	}
};

export const removeDuplicateEpisodes = async (files) => {
	let Files = groupBy(files, 'foldername');

	let result = files;

	await Promise.all(
		Object.values(Files).map(async (f) => {
			if (f.length > 1) {
				let sorted = f
					.filter(g => fs.existsSync(g.file))
					.map((g) => ({
						name: g.file,
						time: fs.statSync(g.file).mtime.getTime(),
					}))
					.sort((a, b) => a.time - b.time);

				let filtered = sorted.slice(0, sorted.length - 1).map((file) => file.name);

				if (!contains(filtered, '.mp4')) {
					filtered.forEach((f) => {
						if (f.includes('.m3u8')) {
							result = result.filter((F) => F.file.includes(f));
							fs.rmSync(f);
						}
					});
				}
			}
		})
	);
	return result;
};

export const findEpisodeFiles = async function (id, t) {
	Logger.log({
		level: 'info',
		name: 'App',
		color: 'magentaBright',
		message: 'Searching episodes in folder: ' + t.folder,
		
	});
	
	if (!id) {
		let tv = await prismaMedia.tv.findFirst({
			where: {
				OR: [
					{
						folder: t.folder.replace(/\/{1,}/g, '/').replace(/\/$/,''),
					},
					{
						folder: t.folder.replace(/\/$/,''),
					},
					{
						folder: t.folder,
					},
				],
			},
			select: {
				id: true,
			},
		});
		id = tv?.id;
	}
	if (id) {
		let files = await getParsedFileList(t.folder, ['.NoMercy.mp4', '.NoMercy.mkv', '.NoMercy.m3u8']);
		files = await removeDuplicateEpisodes(files);
		files = files.reverse();

		let have_episodes = 0;
		let transaction = [];

		for (let i = 0; i < files.length; i++) {
			let f = files[i];

			let episode = await prismaMedia.episode.findFirst({
				where: {
					tvId: id,
					season_number: parseInt(f.season),
					episode_number: parseInt(f.episode),
				},
			});

			if (episode) {
				let ffprobe = await getVideoInfo(f.file).catch((reason) => {
					Logger.log({
						level: 'error',
						name: 'Encoder',
						color: 'red',
						message: 'Error while getting video info: ' + reason.file + ', reason: ' + (reason.error || 'unknown'),
					});
					
				});

				if (ffprobe?.format && ffprobe?.streams) {
					let duration = convertToHuman(ffprobe.format.duration).replace(/^00:/, '');

					let languages = ffprobe.streams.audio
						.filter((a) => a.language != null && a.language != 'undefinded' && a.language != 'und')
						.map((a) => a.language)
						.join(',');
					let subtitles = getExistingSubtitles(t.folder + '/' + f.foldername + '/subtitles')
						.map((s) => s.language + '.' + s.type + '.' + s.ext)
						.join(',');

					if (ffprobe.format.duration != 'N/A' && parseInt(f.season) != 0) {
						have_episodes++;
					}

					let videoInsert = {
						episodeId: episode.id,
						host_folder: f.file.replace(/[\\\/][^\\\/]+(?=.*\w*)$/, '/'),
						duration: ffprobe.format ? duration : null,
						languages: ffprobe.format ? languages : null,
						subtitles: ffprobe.format ? subtitles : null,
						quality: getQualityTag(ffprobe),
						share: config.folderRoots.filter((r) => r.path.includes(f.type + '/' + f.type)).share,
						folder: f.type + '/' + f.type + '/' + f.folder + '/' + f.foldername + '/',
						filename: f.filename + '.' + f.ext,
					};

					transaction.push(
						prismaMedia.video_file.upsert({
							where: {
								episodeId: episode.id,
							},
							update: videoInsert,
							create: videoInsert,
						})
					);
				}
			}
		}
		
		let Type = t.media_type;
		if (t.folder) {
			const reg = /.*[\\\/](?<type>[\w\.]*)[\\\/](?<direction>[\w\.]*)[\\\/].*\(\d{4}\)$/;
			const regResult = reg.exec(t.folder);
			if (regResult?.groups) {
				Type = regResult.groups.type.toLowerCase().split('.')[0];
			}
		}

		let tv = await prismaMedia.tv.findFirst({
			where: {
				id: parseInt(id),
			},
		});

		if (tv) {
			try {
				await prismaMedia.tv.update({
					where: {
						id: parseInt(id),
					},
					data: {
						have_episodes: have_episodes,
						media_type: Type,
					},
				});
			} catch (error) {
				try {
					await prismaMedia.tv.update({
						where: {
							id: parseInt(id),
						},
						data: {
							have_episodes: have_episodes,
							media_type: Type,
						},
					});
				} catch (error) {
					Logger.log({
						level: 'info',
						name: 'App',
						color: 'magentaBright',
						message: 'An error occured in updating the record for episodes in folder: ' + t.folder,
						
					});
				}
			}
					
			if(tv.number_of_episodes < have_episodes){
				Logger.log({
					level: 'info',
					name: 'App',
					color: 'redBright',
					message: `Finished adding ${tv.title}, but it has too many episode files`,
				});
			}
			else {
				Logger.log({
					level: 'info',
					name: 'App',
					color: 'magentaBright',
					message: `Finished adding ${tv.title}`,
				});
				Logger.log({
					level: 'info',
					name: 'App',
					color: 'magentaBright',
					message: `The show has ${have_episodes} episodes and is ${tv.number_of_episodes - have_episodes == 0 ? 'complete' : `missing ${tv.number_of_episodes - have_episodes} episodes`}`,
				});
			}
		}

		try {
			await prismaMedia.$transaction(transaction);
		} catch (error) {
			try {
				await prismaMedia.$transaction(transaction);
			} catch (error) {
				try {
					await prismaMedia.$transaction(transaction);
				} catch (error) {
					//
				}
			}
		}
	}
	return;
};

export const addToDatabase = async function (t, seasonNumber, episode) {
	let files = await getParsedFileList(t.folder);

	let data = files.find((f) => f.season == seasonNumber && f.episode == episode.episode_number);

	let have_episodes = 0;

	if (data) {
		if (config.debug) {
			Logger.log({
				level: 'info',
				name: 'App',
				color: 'magentaBright',
				message: 'Checking file ' + data.file,
				
			});
		}

		let ffprobe = await getVideoInfo(data.file).catch((reason) => {
			Logger.log({
				level: 'error',
				name: 'Encoder',
				color: 'red',
				message: `Error while getting video info for ${reason.file}, reason: ${reason.error || 'unknown'}`,
			});			
		});

		if (ffprobe.format) {
			duration = convertToHuman(ffprobe.format.duration).replace(/^00:/, '');
			languages = ffprobe.streams.audio
				.filter((a) => a.language != null && a.language != 'undefinded' && a.language != 'und')
				.map((a) => a.language)
				.join(',');
			subtitles = getExistingSubtitles(t.folder + '/' + data.foldername + '/subtitles')
				.map((s) => s.language + '.' + s.type + '.' + s.ext)
				.join(',');

			if (ffprobe.format.duration != 'N/A' && seasonNumber != 0) {
				have_episodes++;
			}

			let videoInsert = {
				episodeId: episode.id,
				host_folder: data.file.replace(/[\\\/][^\\\/]+(?=.*\w*)$/, '/'),
				duration: duration,
				languages: languages,
				subtitles: subtitles,
				folder: data.type + '/' + data.type + '/' + data.folder + '/' + data.foldername + '/',
				filename: data.filename + '.' + data.ext,
			};

			await prismaMedia.video_file.upsert({
				where: {
					video_file_unique: {
						episodeId: episode.id,
						movie: null,
					},
				},
				update: videoInsert,
				create: videoInsert,
			});

			Logger.log({
				level: 'info',
				name: 'App',
				color: 'magentaBright',
				message: JSON.stringify(videoInsert),
				
			});
		}
	}
	if(tv.number_of_episodes < have_episodes){
		Logger.log({
			level: 'info',
			name: 'App',
			color: 'redBright',
			message: `Finished adding ${data.title}, but it has too many episode files`,
		});
	}
	else {
		Logger.log({
			level: 'info',
			name: 'App',
			color: 'magentaBright',
			message: `Finished adding ${data.title}, it has ${have_episodes} episodes and its missing ${tv.number_of_episodes - have_episodes} episodes`,
		});
	}


	return;
};

export default {
	addToDatabase,
	getFiles,
	getFileList,
	getFolders,
	getFolderList,
	getParsedFolderList,
	getParsedFileList,
	scanAllFolders,
	findMovieFiles,
	findEpisodeFiles,
	removeDuplicateEpisodes,
};
