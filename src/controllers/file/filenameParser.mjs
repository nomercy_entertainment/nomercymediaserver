import { anime_regexes, dvd_regexes, movie_regexes, normal_regexes } from './nameRegexes.mjs';

import { pad } from '../../helpers/stringArray.mjs';
import { parseYear } from '../../helpers/dateTime.mjs';

export const parseFileName = function (filename) {
	let reg = /(.*[\\\/])(?<fileName>.*)/.exec(filename);

	let yearReg = /(\s|\.|\()(?<year>(19|20)[0-9][0-9])(\)|.*|(?!p))/.exec(filename);

	let fileName = reg.groups.fileName;

	let match = [];
	let res = {};

	for (let i = 0; i < movie_regexes.length; i += 1) {
		reg = movie_regexes[i].exec(filename);
		if (reg && match.length == 0) {
			reg.groups.year = yearReg?.groups?.year;
			reg.groups.title = reg.groups.title?.replace(/\[\w*\][\s\.]*/g, '').replace(/[\[\]]]/g, '');
			match.push(reg.groups);
			break;
		}
	}

	for (let i = 0; i < normal_regexes.length; i += 1) {
		reg = normal_regexes[i].exec(fileName);
		if (reg && match.length == 0) {
			reg.groups.year = yearReg?.groups?.year;
			reg.groups.title = reg.groups.title?.replace(/\[\w*\][\s\.]*/g, '').replace(/[\[\]]]/g, '');
			match.push(reg.groups);
			break;
		}
	}

	for (let i = 0; i < anime_regexes.length; i += 1) {
		reg = anime_regexes[i].exec(fileName);
		if (reg && match.length == 0) {
			reg.groups.year = yearReg?.groups?.year;
			reg.groups.title = reg.groups.title?.replace(/\[\w*\][\s\.]*/g, '').replace(/[\[\]]]/g, '');
			match.push(reg.groups);
			break;
		}
	}

	for (let i = 0; i < dvd_regexes.length; i += 1) {
		reg = dvd_regexes[i].exec(fileName);
		if (reg && match.length == 0) {
			reg.groups.part = reg.groups.part.replace(/-/g, '');
			reg.groups.year = yearReg?.groups?.year;
			reg.groups.title = reg.groups.title?.replace(/\[\w*\][\s\.]*/g, '').replace(/[\[\]]]/g, '');
			match.push(reg.groups);
			break;
		}
	}

	if (match[0]) {
		Object.keys(match[0]).forEach((i) => {
			res[i] = match[0][i];
		});
	}

	return res;
};

export const getFolderData = function (fileIn) {
	let reg = /.*[\\\/](?<type>TV\.Shows|Anime|Films|Marvels)[\\\/](?<direction>Download|TV\.Shows|Anime|Films)[\\\/].*(\s|\.|\()(?<year>(19|20)[0-9][0-9])(\)|.*|(?!p)).*[\\\/].*$/.exec(fileIn);

	if (!reg) {
		reg = /.*[\\\/](?<type>TV\.Shows|Anime|Films|Marvels)[\\\/](?<direction>Download|TV\.Shows|Anime|Films)[\\\/].*(\s|\.|\()(?<year>(19|20)[0-9][0-9])(\)|.*|(?!p)).*.(?<ext>\w{3,4})$/.exec(fileIn);
	}
	if (!reg) {
		reg = /.*[\\\/](?<type>TV\.Shows|Anime|Films|Marvels)[\\\/](?<direction>Download|TV\.Shows|Anime|Films)[\\\/].*.(?<ext>\w{3,4})$/.exec(fileIn);
	}

	let direction;
	let dbtype;

	switch (reg.groups.direction) {
		case 'Download':
		case 'download':
			direction = 'in';
			break;
		default:
			direction = 'out';
	}

	switch (reg.groups.type) {
		case 'Anime':
		case 'anime':
			dbtype = 'tv';
			break;
		case 'Films':
		case 'films':
		case 'Movies':
		case 'movies':
			dbtype = 'movie';
			break;
		case 'TV.Shows':
		case 'tv.shows':
		case 'tv shows':
		case 'shows':
			dbtype = 'tv';
			break;
		case 'HQ':
		case 'hq':
			dbtype = 'tv';
			break;
	}

	return {
		mediumType: reg.groups.type.replace('Marvels', 'TV.Shows'),
		subType: reg.groups.direction,
		dbtype: dbtype,
		direction: direction,
		year: reg.groups.year || '',
		folder: null,
	};
};

export const getFileData = function (fileIn) {
	let reg = /[\\\/](?<type>TV\.Shows|Anime|Films)[\\\/](?<direction>Download|TV\.Shows|Anime|Films)[\\\/](?<name>.*)\.\((?<year>\d{4})\)[\\\/](?<file>(?<filename>.*)\.(?<ext>mkv|avi|mp4|m3u8|mov|webm))$/.exec(fileIn);

	let result = reg.groups;

	return result;
};

export const createRootFolderName = function (folderData) {
	let rootFolder = config.folderRoots.find((f) => f.path.includes(folderData.mediumType + '/' + folderData.mediumType)).path + '/';

	return rootFolder;
};

export const createBaseFolderName = function (showData) {
	let baseName = (showData.title || showData.name) + '.(' + parseYear(showData.first_air_date || showData.release_date) + ')/';

	return cleanFileName(baseName);
};

export const getEpisodeIndex = function (showData, filenameInfo) {
	let data = episodeData(showData, filenameInfo);

	return data ? data.id : null;
};

export const createShowFolderName = function (type, showData, filenameInfo) {
	let showName;

	if (type != 'movie') {
		showName = (showData.title || showData.name) + '.S' + pad(filenameInfo.season_num, 2) + 'E' + pad(filenameInfo.ep_num, 2);
	} else {
		showName = (showData.title || showData.name) + '.(' + parseYear(showData.release_date) + ')';
	}
	return cleanFileName(showName);
};

export const createFileName = function (type, showData, episode) {
	let showName, fileName;

	let title = episode?.title.substring(0, 100) || showData.name?.substring(0, 100) || showData.title?.substring(0, 100);

	if (type != 'movie') {
		showName = (showData.title || showData.name) + '.S' + pad(episode.season_number, 2) + 'E' + pad(episode.episode_number, 2);
		fileName = showName + (episode ? '.' + title : '').replace(/\//g, '.');
	} else {
		fileName = (showData.title || showData.name) + '.(' + parseYear(showData.release_date) + ')';
	}

	return cleanFileName(fileName);
};

export const episodeData = function (showData, filenameInfo) {
	let season = showData.seasons.find((s) => s.season_number == filenameInfo.season_num);
	let episodeData = season?.episodes.find((e) => e.episode_number == filenameInfo.ep_num);

	return episodeData;
};

export const cleanFileName = function (name) {
	return name
		.replace(/:\s/g, '.')
		.replace(/\?  /g, '.')
		.replace(/\? /g, '.')
		.replace(/,\./g, '.')
		.replace(/, /g, '.')
		.replace(/`/g, '')
		.replace(/'/g, '')
		.replace(/"/g, '')
		.replace(/,/g, '.')
		.replace(/"/g, "'")
		.replace(/\.{2,}/, '.')
		.replace(/\s/g, '.')
		.replace(/&/g, 'and')
		.replace(/#/g, '%23')
		.replace(/!/g, '')
		.replace(/\*/g, '-')
		.replace(/\.\./g, '.')
		.replace(/,\./g, '.')
		.replace(/\.{2,}/g, '.')
		.replace(/:/g, '.')
		.replace(/'|\?|\.\s|-\.|\.\(\d{1,3}\)|[^[:print:]]|[^-_.[:alnum:]]/gi, '');
};

export const createTitleSort = function (title) {
	const newTitle = cleanFileName(
		title
			.replace(/^The[\s]*/, '')
			.replace(/^An[\s]{1,}/, '')
			.replace(/^A[\s]{1,}/, '')
			.replace(/\./g, ' ')
	);
	return newTitle.toLowerCase();
};

export default {
	parseFileName,
	getFolderData,
	getFileData,
	createRootFolderName,
	createBaseFolderName,
	createShowFolderName,
	createFileName,
	cleanFileName,
	getEpisodeIndex,
	createTitleSort,
};
