import { convertToHuman } from '../../helpers/dateTime.mjs';
import { getPlatform } from '../../helpers/system.mjs';
import nosu from 'node-os-utils';
import os from 'os';
import osu from 'os-utils';
import { execSync } from 'child_process';
const memTotal = os.totalmem();
const threads = osu.cpuCount();

const cpuIAverage = function (i) {
	var cpu, cpus, idle, len, total, totalIdle, totalTick, type;
	totalIdle = 0;
	totalTick = 0;
	cpus = os.cpus();
	cpu = cpus[i];
	for (type in cpu.times) {
		totalTick += cpu.times[type];
	}
	totalIdle += cpu.times.idle;

	idle = totalIdle / cpus.length;
	total = totalTick / cpus.length;
	return {
		idle: idle,
		total: total,
	};
};

const cpuILoadInit = function () {
	var index = arguments[0];
	return function () {
		var start = cpuIAverage(index);
		return function (dif = {}) {
			var end;
			end = cpuIAverage(index);
			dif.cpu = index;
			dif.idle = Math.round(end.idle - start.idle);
			dif.total = Math.round(end.total - start.total);
			let percent = Math.round(100 - (dif.idle / dif.total) * 100);
			dif.percent = isNaN(percent) ? 0 : percent;
			return dif;
		};
	};
};

const cpuILoad = function (info) {
	var cpus = os.cpus();
	for (let i = 0, len = cpus.length; i < len; i++) {
		var a = cpuILoadInit(i)();
		info.push(a);
	}
	return function () {
		var res = [],
			cpus = os.cpus();
		for (let i = 0, len = cpus.length; i < len; i++) {
			res.push(info[i]());
		}
		return res;
	};
};

let info = [];
setInterval(() => {
	info = info.slice(info.length - threads, info.length);
}, 1000);

let watcher;

export default (socket, io) => {
	clearInterval(watcher);

	socket.on('disconnect', () => {
		clearInterval(watcher);
	});

	socket.on('stop_system_listening', () => {
		clearInterval(watcher);
	});

	socket.on('start_system_listening', () => {
		start();
	});

	function start() {
		clearInterval(watcher);
		watcher = setInterval(async () => {
			let stat = await cpuStats();
			socket.emit('system', stat);
		}, config.updateInterval || 1000);
	}
};

const cpuStats = async () => {
	const cpu = await nosu.cpu;
	const memTotal = os.totalmem();
	const memFree = os.freemem();
	const mem = memTotal - memFree;
	const memUsage = getPlatform() == 'linux' 
		? linuMemoryUsage() 
		: Math.round((mem / memTotal) * 100);

	const operatingSystem = await nosu.os.oos();
	const dist = operatingSystem != 'not supported' ? operatingSystem : os.type().split('_')[0];
	const coreUtil = await cpuILoad(info)();

	return {
		model: cpu.model().replace(/\s{2,}/g, ''),
		os: dist,
		threads,
		platform: osu.platform(),
		memUsage,
		memTotal,
		sysUptime: convertToHuman(Math.round(osu.sysUptime())),
		Uptime: convertToHuman(Math.round(osu.processUptime())),
		coreUtil,
	};
};


const linuMemoryUsage = () => {

	const results = [];
    const stats = execSync('cat /proc/meminfo').toString().split('\n');
	const regex = /(?<key>[\w]+)(\(\w+\))?:\s+(?<val>\d+)/gi;

	for (let i = 0; i < stats.length; i += 1) {
		let stat = stats[i];
		let groups = regex.exec(stat)?.groups;
		if(groups?.key){
			results.push({
				key: groups?.key,
				val: groups?.val,
			});
		}
	}

	let memoryTotal = 0;
	let memoryAvailable = 0;
	let returnMemPercentage = 0;

    if (results) {
			memoryTotal = results.find(r => r.key == 'MemTotal').val;
			memoryAvailable = results.find(r => r.key == 'MemAvailable').val;

        returnMemPercentage = Math.abs(((memoryAvailable / memoryTotal) * 100) - 100).toFixed(2);
    }
	return returnMemPercentage
}
