import { cpuILoad, cpuStats } from './cpu';

export default {
	cpuILoad,
	cpuStats,
};
