import { addMovie, addTv } from '../../providers/themoviedb/database/index.mjs';
import { findEpisodeFiles, findMovieFiles, scanAllFolders } from '../file/fileController.mjs';

import Logger from '../../loaders/logger.mjs';
import configuration from '../../config/index.mjs';
import i18next from '../../config/i18next.mjs';

process.send('ready');

process.on('message', async ({ force = false, folder, reversed = false }) => {
	await configuration();

	let allFolders = [];

	allFolders = await scanAllFolders('out');
	if (folder) {
		allFolders = allFolders.filter((f) => f?.folder == folder.charAt(0).toUpperCase() + folder.slice(1));
	}
	if(reversed){
		allFolders = allFolders.reverse();
	}

	let delay = 0;
	let index = 0;
	let Continue = true;

	if (allFolders.length > 0) {
		let interval = setInterval(async function () {
			if (Continue) {
				Continue = false;
				let t = allFolders[index];
				if (t) {
					i18next.changeLanguage('en');
					let type = t.type != 'movie' ? 'tv' : 'movie';

					if (type == 'movie') {
						let id = await addMovie(t, force);
						if (id) {
							await findMovieFiles(id, t);
						}
						Continue = true;
					} else if (type == 'tv') {
						let id = await addTv(t, force);
						if (id) {
							await findEpisodeFiles(id, t);
						}
						Continue = true;
					}
				} else {
					Logger.log({
						level: 'info',
						name: 'App',
						color: 'magentaBright',
						message: "Can't process folder: " + allFolders[index],
						
					});
					Continue = true;
				}

				if (++index === allFolders.length) {
					clearInterval(interval);
					Logger.log({
						level: 'info',
						name: 'App',
						color: 'magentaBright',
						message: 'done.',
						
					});

					if (typeof process.send == 'function') {
						process.send('ok');
						process.exit(0);
					}
				}
			}
		}, delay);
	} else {
		Logger.log({
			level: 'info',
			name: 'App',
			color: 'magentaBright',
			message: 'Nothing to do, there are no files to process.',
		});
		if (typeof process.send == 'function') {
			process.send('ok');
			process.exit(0);
		}
	}
});
