import { getParsedFileList, removeDuplicateEpisodes } from '../file/FileController';

import Logger from '../../loaders/logger.mjs';
import configuration from '../../config/index.mjs';
import { convertToHuman } from '../../helpers/index.mjs';
import { getExistingSubtitles } from '../encoder/ffmpeg/subtitle.mjs';
import { getQualityTag } from '../encoder/ffmpeg/quality.mjs';
import { getVideoInfo } from '../encoder.mjs';
import { prismaMedia } from '../../config/database.mjs';

process.on('message', async function ({ id, t }) {
	await configuration();

	Logger.log({
		level: 'info',
		name: 'App',
		color: 'magentaBright',
		message: 'Searching episodes in folder: ' + t.folder,
		
	});

	let files = await getParsedFileList(t.folder);
	files = await removeDuplicateEpisodes(files);

	let have_episodes = 0;
	let transaction = [];

	await Promise.all(
		files.map(async (f) => {
			let episode = await prismaMedia.episode.findFirst({
				where: {
					tvId: id,
					season_number: parseInt(f.season),
					episode_number: parseInt(f.episode),
				},
			});

			if (episode) {
				let ffprobe = await getVideoInfo(f.file).catch((reason) => {
					Logger.log({
						level: 'error',
						name: 'Encoder',
						color: 'red',
						message: 'Error while getting video info: ' + reason.file + ', reason: ' + (reason.error || 'unknown'),
					});
				});

				if (ffprobe?.format && ffprobe?.streams) {
					duration = convertToHuman(ffprobe.format.duration).replace(/^00:/, '');
					languages = ffprobe.streams.audio
						.filter((a) => a.language != null && a.language != 'undefinded' && a.language != 'und')
						.map((a) => a.language)
						.join(',');
					subtitles = getExistingSubtitles(t.folder + '/' + f.foldername + '/subtitles')
						.map((s) => s.language + '.' + s.type + '.' + s.ext)
						.join(',');

					if (ffprobe.format.duration != 'N/A' && parseInt(f.season) != 0) {
						have_episodes++;
					}

					let videoInsert = {
						episodeId: episode.id,
						host_folder: f.file.replace(/[\\\/][^\\\/]+(?=.*\w*)$/, '/'),
						duration: ffprobe.format ? duration : null,
						languages: ffprobe.format ? languages : null,
						subtitles: ffprobe.format ? subtitles : null,
						quality: getQualityTag(ffprobe),
						share: config.folderRoots.filter((r) => r.path.includes(f.type + '/' + f.type)).share,
						folder: f.type + '/' + f.type + '/' + f.folder + '/' + f.foldername + '/',
						filename: f.filename + '.' + f.ext,
					};

					transaction.push(
						prismaMedia.video_file.upsert({
							where: {
								episodeId: episode.id,
							},
							update: videoInsert,
							create: videoInsert,
						})
					);
				}
			}
		})
	);

	let tv = await prismaMedia.tv.findFirst({
		where: {
			id: parseInt(id),
		},
	});

	if (tv) {

		try {
			await prismaMedia.tv.update({
				where: {
					id: parseInt(id),
				},
				data: {
					have_episodes: have_episodes,
				},
			});
		} catch (error) {
			try {
				await prismaMedia.tv.update({
					where: {
						id: parseInt(id),
					},
					data: {
						have_episodes: have_episodes,
					},
				});
			} catch (error) {
				Logger.log({
					level: 'info',
					name: 'App',
					color: 'magentaBright',
					message: 'An error occured in updating the record for episodes in folder: ' + t.folder,
					
				});
			}
		}

		Logger.log({
			level: 'info',
			name: 'App',
			color: 'magentaBright',
			message: 'Found : ' + have_episodes + ' episodes in folder: ' + t.folder,
		});
		if (tv.number_of_episodes > have_episodes) {
			Logger.log({
				level: 'info',
				name: 'App',
				color: 'magentaBright',
				message: 'Missing: ' + (tv.number_of_episodes - have_episodes) + ' episodes',
				
			});
		}

		if (tv.number_of_episodes < have_episodes) {
			Logger.error('To many episode files for: ' + t.folder);
		}
	}

	try {
		await prismaMedia.$transaction(transaction);
	} catch (error) {
		try {
			await prismaMedia.$transaction(transaction);
		} catch (error) {
			try {
				await prismaMedia.$transaction(transaction);
			} catch (error) {
				//
			}
		}
	}

	if (typeof process.send == 'function') {
		process.send('done');
		process.exit(0);
	}
});
