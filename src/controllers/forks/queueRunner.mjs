import { exec, execSync } from 'child_process';
import { findEpisodeFiles, findMovieFiles, scanAllFolders } from '../file/fileController.mjs';

import configuration from '../../config/index.mjs';
import { convertSubToVtt } from '../encoder/ffmpeg/subtitle.mjs';
import { createThumbnails } from '../encoder/ffmpeg/preview.mjs';
import { fileURLToPath } from 'url';
import fs from 'fs';
import moment from 'moment';
import path from 'path';
import { prismaMedia } from '../../config/database.mjs';
import { randomUUID } from 'crypto';
import { verify } from '../encoder/ffmpeg/verify.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let running = false;

async function start() {
	await configuration();

	let publicFolder = __dirname + '/public/';
	let logFolder = publicFolder + 'working';

	let job = await prismaMedia.jobs.findFirst({
		where: {
			attempts: null,
		},
	});

	if (job) {
		let message = 'Job ' + job.id + ' started';
		process.send(message);

		try {
			await prismaMedia.jobs.update({
				where: { id: job.id },
				data: {
					attempts: job.attempts + 1 || 1,
					available_at: !job.available_at ? moment(new Date()).toDate() : job.available_at,
					reserved_at: !job.reserved_at ? moment(new Date()).toDate() : job.reserved_at,
				},
			});
		} catch (error) {
			try {
				await prismaMedia.jobs.update({
					where: { id: job.id },
					data: {
						attempts: job.attempts + 1 || 1,
						available_at: !job.available_at ? moment(new Date()).toDate() : job.available_at,
						reserved_at: !job.reserved_at ? moment(new Date()).toDate() : job.reserved_at,
					},
				});								
			} catch (error) {
				prismaMedia.failed_jobs.create({
					data: {
						uuid: randomUUID(),
						connnection: 'default',
						queue:  job,queue,
						payload: job.payload,
						exception: 'Can\'t convert this file.'
					}
				});

				await prismaMedia.jobs.delete({
					where: { id: job.id },
				});
		  	running = false;
			}
		}

		let payload = JSON.parse(job.payload);

		if (job.queue == 'encoder') {
			if (payload.ffmpegCommand) {
				process.send('Job ' + job.id + ' started, output folder: ' + payload.outputFolder);

				exec(
					payload.ffmpegCommand,
					{
						cwd: payload.outputFolder,
						maxBuffer: 1024 * 5000,
					},
					async (error, stdout, stderr) => {
						if (error) {
							let message = 'Job ' + job.id + ' failed';
							process.send(message);

							await prismaMedia.failed_jobs.create({
								data: {
									uuid: randomUUID(),
									connnection: 'default',
									queue: job.queue,
									payload: job.payload,
									exception: JSON.stringify(error),
								},
							});

							try {
								await prismaMedia.jobs.delete({
									where: { id: job.id },
								});
							} catch (e) {}
							running = false;
						} else {
							await createThumbnails(payload.ffprobe, payload.outputFolder);

							if (fs.existsSync(logFolder + `/${payload.logFile}` + 'main.txt')) {
								fs.rmSync(logFolder + `/${payload.logFile}` + 'main.txt');
							}

							if (verify(payload.outputFolder + payload.outputFileName + payload.outputExtention, payload.ffprobe)) {
								if (config.deleteInput && payload.dbtype == 'optical') {
									fs.rmSync(payload.fileIn);
								}
							}

							if (fs.existsSync(payload.fileIn.replace(/[\\\/].*$/, '')) && fs.readdirSync(payload.fileIn.replace(/[\\\/].*$/, '')).length == 0) {
								if (config.deleteInput && payload.dbtype == 'optical') {
									fs.rmSync(payload.fileIn.replace(/[\\\/].*$/, ''));
								}
							}

							let folder = payload.outputFolder.replace(/[\\\/][\w\s\._\-?()]*$/, '').replace(/[\\\/][\w\s\._\-?()]*$/, '');
							let allFolders = await scanAllFolders('out');

							if (folder && allFolders) {
								const t = allFolders.find((f) => f?.folder == folder);
								if (payload.dbtype == 'tv' && t) {
									await findEpisodeFiles(null, t).catch(async () => {
										running = false;
										try {
											await prismaMedia.jobs.delete({
												where: { id: job.id },
											});
										} catch (error) {}
										process.send('Job ' + job.id + ' failed');
									});
								}
								if (payload.dbtype == 'movie' && t) {
									await findMovieFiles(null, t).catch(async () => {
										running = false;
										try {
											await prismaMedia.jobs.delete({
												where: { id: job.id },
											});
										} catch (error) {}
										process.send('Job ' + job.id + ' failed');
									});
								}
							}

							try {
								await prismaMedia.jobs.delete({
									where: { id: job.id },
								});
							} catch (error) {}
						}

						running = false;
						process.send('Job ' + job.id + ' finished');
					}
				);
			}

			if (payload.ffmpegCommand2) {
				process.send('Job ' + job.id + ' subtitle extraction started');
				exec(
					payload.ffmpegCommand2,
					{
						cwd: payload.outputFolder,
						maxBuffer: 1024 * 5000,
					},
					async (error, stdout, stderr) => {
						if (error) {
							let message = 'Job ' + job.id + ' failed';
							process.send(message);

							await prismaMedia.failed_jobs.create({
								data: {
									uuid: randomUUID(),
									connnection: 'default',
									queue: job.queue,
									payload: job.payload,
									exception: JSON.stringify(error),
								},
							});
						} else {
							if (payload.ffmpegCommand2.includes('subtitles')) {
								process.send('Job ' + job.id + ' subtitle converting started');
								await convertSubToVtt(payload.outputFolder + 'subtitles/');
							}

							if (fs.existsSync(logFolder + `/${payload.logFile}` + 'subs.txt')) {
								fs.rmSync(logFolder + `/${payload.logFile}` + 'subs.txt');
							}
							process.send('Job ' + job.id + ' subtitle extraction finished');
						}
					}
				);
			}
		}
	} else {
		running = false;
	}
}

async function loop() {
	if (!running) {
		running = true;

		await start();
	}
}

setInterval(() => {
	loop();
}, 1000);

if (typeof process.send == 'function') {
	process.send('Queue runner started');
}

export default loop;
