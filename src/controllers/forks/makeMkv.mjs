import { exec, execSync } from 'child_process';

import Logger from '../../loaders/logger.mjs';
import configuration from '../../config/index.mjs';
import { createFFmpegCommand } from '../encoder/ffmpeg/ffmpeg.mjs';
import csv2json from 'csvjson-csv2json';
import { dateFormat } from '../../helpers/dateTime.mjs';
import { fileURLToPath } from 'url';
import fs from 'fs';
import os from 'os';
import path from 'path';
import { verifyMkv } from '../encoder/ffmpeg/verify.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let drives;
let makeMkv;

if (os.platform() == 'win32') {
	drives = execSync('powershell (New-Object -com "WMPlayer.OCX.7").cdromcollection.count').toString();
	makeMkv = __dirname + '/../../binaries/MakeMKV/makemkvcon64.exe';
} else {
}

if (drives == 0) {
	Logger.log({
		level: 'error',
		name: 'ripper',
		color: 'red',
		message: 'No optical drives found',
		
	});
	process.exit(0);
}

let running = false;

function state() {
	if (os.platform() == 'win32') {
		return execSync(`powershell (New-Object -com "WMPlayer.OCX.7").openState`).toString() == 6;
	} else {
		return true;
	}
}
function eject() {
	if (os.platform() == 'win32') {
		exec('powershell (New-Object -com "WMPlayer.OCX.7").cdromcollection.item(0).eject()');
	} else {
		exec('eject -r');
	}
}

async function start() {
	await configuration();
	exec(`${makeMkv} -r --cache=1 info disc:0 --directio=true --minlength=${config.minlength.toString() || 60}`, async function (error, stdout, stderr) {
		const outputFolder = config.ripperFolder;
		fs.mkdirSync(outputFolder, { recursive: true });

		let list = [];
		let results = [];

		if (error) {
			Logger.log({
				level: 'error',
				name: 'ripper',
				color: 'magentaBright',
				message: error,
				
			});
		}
		if (stderr) Logger.error(stderr.toString());
		if (stdout) Logger.verbose(stdout.toString());

		const json = csv2json('MSG,stream,key,value,x\n\n' + stdout, { parseNumbers: true });

		let err = json.filter((j) => j.MSG.includes('MSG') && j.value.includes('Failed'))[0];
		if (err) {
			running = false;
			message = 'Nothing to do.';
			Logger.log({
				level: 'info',
				name: 'ripper',
				color: 'magentaBright',
				message: 'Nothing to do.',
				
			});
			return;
		}

		let res = json.filter((j) => j.MSG.includes('TINFO'));
		let names = json.filter((j) => j.MSG.includes('TINFO') && j.value.toString().includes('mkv'));

		if (json.some((j) => j.MSG.includes('MSG:5055'))) {
			Logger.log({
				level: 'error',
				name: 'ripper',
				color: 'red',
				message: json.find((j) => j.MSG.includes('MSG:5055'))?.value,
				
			});
			process.exit();
		}

		if (json.some((j) => j.MSG.includes('MSG:5021'))) {
			Logger.log({
				level: 'error',
				name: 'ripper',
				color: 'red',
				message: json.find((j) => j.MSG.includes('MSG:5021'))?.value,
				
			});
			running = false;
			process.exit();
		}

		let discName =
			dateFormat(new Date(), 'yyyymmddHMM_') +
			json
				.filter((j) => j.MSG.includes('CINFO:2'))
				.map((n) => n.key)[0]
				?.replace(/[\s:;]/g, '-');

		let items = res.filter((r) => r.stream == 25 && r.value <= config.maxParts);
		if (items.length > 0) {
			Logger.log({
				level: 'info',
				name: 'ripper',
				color: 'magentaBright',
				message: 'Copying files from disc...',
				
			});
		}

		items.forEach(async (i, index) => {
			let name = names.filter((j) => j.MSG.split(':')[1] == i.MSG.split(':')[1])[0].value.replace(/[\\\r\n]/g, '');
			let filename = outputFolder + discName + '/' + name;

			if (fs.existsSync(filename)) {
				let verified = await verifyMkv(filename);

				if (!verified) {
					Logger.log({
						level: 'info',
						name: 'ripper',
						color: 'magentaBright',
						message: 'File corrupt, deleting...',
						
					});
					fs.rmSync(filename);
				} else {
					list.push(filename);
					Logger.log({
						level: 'info',
						name: 'ripper',
						color: 'magentaBright',
						message: 'File OK.',
						
					});
				}
			}

			if (!fs.existsSync(filename)) {
				Logger.log({
					level: 'info',
					name: 'ripper',
					color: 'magentaBright',
					message: 'Ripping title: ' + i.MSG.split(':')[1] + ' to: ' + outputFolder + discName + '/' + name,
					
				});

				if (!fs.existsSync(outputFolder + discName)) {
					fs.mkdirSync(outputFolder + discName);
				}

				fs.writeFileSync(outputFolder + discName + '/streams.json', JSON.stringify(res));

				execSync(`${makeMkv} mkv disc:0 ${i.MSG.split(':')[1]} --decrypt -r  --cache=16 --noscan --directio=true --progress=null ${outputFolder + discName}`);

				Logger.log({
					level: 'info',
					name: 'ripper',
					color: 'magentaBright',
					message: 'Verifying output file.',
					
				});
				let verified = await verifyMkv(filename);
				if (!verified) {
					Logger.log({
						level: 'info',
						name: 'ripper',
						color: 'magentaBright',
						message: 'File corrupt, deleting...',
						
					});
					fs.rmSync(filename);

					message = 'Trying title: ' + i.MSG.split(':')[1] + ' again.';
					Logger.log({
						level: 'info',
						name: 'ripper',
						color: 'magentaBright',
						message: 'Trying title: ' + i.MSG.split(':')[1] + ' again.',
						
					});

					execSync(`${makeMkv} mkv disc:0 ${i.MSG.split(':')[1]} --decrypt -r  --cache=16 --noscan --directio=true --progress=null ${outputFolder + discName}`);

					Logger.log({
						level: 'info',
						name: 'ripper',
						color: 'magentaBright',
						message: 'Verifying output file.',
						
					});
					let verified = await verifyMkv(filename);
					if (!verified) {
						Logger.log({
							level: 'info',
							name: 'ripper',
							color: 'magentaBright',
							message: 'File corrupt, you have rip this title manually..',
							
						});

						execSync(`${makeMkv} mkv disc:0 ${i.MSG.split(':')[1]} --decrypt -r  --cache=16 --noscan --directio=true --progress=null ${outputFolder + discName}`);

						let verified = await verifyMkv(filename);
						if (!verified) {
							Logger.log({
								level: 'info',
								name: 'ripper',
								color: 'magentaBright',
								message: 'File corrupt, you have rip this title manually..',
								
							});
						} else {
							Logger.log({
								level: 'info',
								name: 'ripper',
								color: 'magentaBright',
								message: 'Title ' + i.MSG.split(':')[1] + ' done.',
								
							});
							list.push(filename);
						}
					} else {
						Logger.log({
							level: 'info',
							name: 'ripper',
							color: 'magentaBright',
							message: 'Title ' + i.MSG.split(':')[1] + ' done.',
							
						});
						list.push(filename);
					}
				}

				if (items.length - 1 == index) {
					await Promise.all(
						list.map(async (fileIn) => {
							await createFFmpegCommand(fileIn, discName + '_');
						}),
						Logger.log({
							level: 'info',
							name: 'ripper',
							color: 'magentaBright',
							message: 'Job complete you may insert a new disc.',
							
						}),
						eject(),
						(running = false)
					).catch((error) => {
						Logger.log({
							level: 'info',
							name: 'ripper',
							color: 'magentaBright',
							message: error,
						});
					});

				}
			}
		});

		if (items.length == 0) {
			running = false;
			return;
		}
	});
}

setInterval(async () => {
	if (state() && !running && makeMkv) {
		running = true;

		await start();
	}
}, 10 * 1000);

process.send('Auto ripper started');

export default this;
