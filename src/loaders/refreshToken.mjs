import Logger from './logger.mjs';
import axios from 'axios';
import fs from 'fs';
import { keycloak_key } from '../config/keycloak.mjs';
import path from 'path';
import qs from 'qs';
import authenticate from './authenticate.mjs';

const __dirname = path.resolve();

const configDir = __dirname + "/config/";
const tokenFile = configDir + "tokens.json";

export default async () => {
	await refresh();
	await refreshLoop();
};

const refreshLoop = async() => {
	setTimeout(async () => {
		await refresh();
		refreshLoop();
	}, (JSON.parse(fs.readFileSync(tokenFile, 'utf8')).expires_in - 120) * 1000);
}

const refresh = async () => {
	Logger.log({
		level: "info",
		name: "setup",
		color: "blueBright",
		message: "Refreshing offline token",
	});

	const keycloakData = () => qs.stringify({
		client_id: "nomercy-ui",
		grant_type: "refresh_token",
		client_secret: keycloak_key,
		scope: "openid offline_access",
		refresh_token: JSON.parse(fs.readFileSync(tokenFile, "utf-8")).refresh_token,
	});

	let keycloakTokenResponse;
	
	await axios.post("https://auth.nomercy.tv/auth/realms/NoMercyTV/protocol/openid-connect/token", keycloakData())
		.then((response) => {
			Logger.log({
				level: "info",
				name: "setup",
				color: "blueBright",
				message: "Offline token refreshed",
			});

			keycloakTokenResponse = response;
		})
		.catch(async () => {

			await authenticate();

			await axios.post('https://auth.nomercy.tv/auth/realms/NoMercyTV/protocol/openid-connect/token', keycloakData())
			.then((response) => {
				Logger.log({
					level: "info",
					name: "token",
					color: "blueBright",
					message: "Offline token refreshed",
				});
				
				keycloakTokenResponse = response;
			})
			.catch((err) => {
				Logger.log({
					level: 'error',
					name: 'keycloak',
					color: 'red',
					message: err.response?.data?.error_description ?? err,
				});
				
				process.exit(1);
			});
		});

	fs.writeFileSync(tokenFile,JSON.stringify(keycloakTokenResponse.data));

	process.env.TOKEN = keycloakTokenResponse.data.access_token;
}