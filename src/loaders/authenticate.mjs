import rax from 'retry-axios';
import { keycloak_key } from "../config/keycloak.mjs";
import Logger from "./logger.mjs";
import { get_ip } from "./networking.mjs";
import axios from "axios";
import fs from "fs";
import QueryString from "qs";
import { random_string } from '../helpers/stringArray.mjs';
import path from 'path';
const __dirname = path.resolve();

export default async () => {
	const configDir = __dirname + "/config/";
	const tokenFile = configDir + "tokens.json";
	const confFile = configDir + "config.json";

	const token = random_string(20);
	const internal_ip = get_ip();
	const external_ip = await axios.get("https://api.ipify.org/");

	const serverData = {
		token,
		server_id: JSON.parse(fs.readFileSync(confFile, "utf8"))?.server_id,

		internal_ip: internal_ip,
		external_ip: external_ip.data,
	};

	await axios
		.post("https://api.nomercy.tv/server/auth", serverData, {
			headers: { Accept: "application/json" },
		})
		.catch((error) => {
			if (error.response) {
				console.log(error.response.data.message);
				process.exit(1);
			}
		});

	Logger.log({
		level: "info",
		name: "setup",
		color: "blueBright",
		message: `You need to authenticate this server because it has been offline too long and your refresh token is invalid.\n\r Visit https://app.nomercy.tv/#/serverauth?token=${token} to re-authenticate this server to your account`,
	});
    
    rax.attach(axios);
    const conf = {
        headers: {
            Accept: "application/json",
        },
        timeout: 1000 * 60,
        raxConfig: {
            retry: 50,
            noResponseRetries: 50,
        },
    };

	let linkServerResponse = await axios.get(`https://api.nomercy.tv/server/link?token=${token}`, conf).catch((error) => {
		if (error.response) {
			Logger.log({
				level: "info",
				name: "setup",
				color: "blueBright",
				message: error.response.data.message,
			});

			process.exit(1);
		}
	});

    // console.log(linkServerResponse.data);

	Logger.log({
		level: "info",
		name: "setup",
		color: "blueBright",
		message: "Obtaining offline token",
	});

	const keycloakData = QueryString.stringify({
		client_id: "nomercy-ui",
		grant_type: "refresh_token",
		client_secret: keycloak_key,
		scope: "openid offline_access",
		refresh_token: linkServerResponse.data.refresh_token,
	});

	const keycloakTokenResponse = await axios.post("https://auth.nomercy.tv/auth/realms/NoMercyTV/protocol/openid-connect/token", keycloakData).catch((err) => {
		Logger.log({
			level: "error",
			name: "keycloak",
			color: "red",
			message: err.response?.data?.error_description ?? err.response?.data,
		});

		process.exit(1);
	});

    // console.log(keycloakTokenResponse.data);

	fs.writeFileSync(tokenFile,JSON.stringify(keycloakTokenResponse.data));
    

	process.env.TOKEN = keycloakTokenResponse.data.access_token;
	process.env.REFRESH_TOKEN = keycloakTokenResponse.data.refresh_token;
};
