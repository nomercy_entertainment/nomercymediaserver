let copyText = 'By Echel0n';

import chalk from 'chalk';

const b = () => chalk.hex('#0057A3');
const o = () => chalk.hex('#097efb');
const t = () => chalk.hex('#9AC3F7');

export default () => {
	console.log(b()`  ╔═════════════════════════════════════════════════════════════════════════════════════════════════╗ `);
	console.log(b()`  ║                                                                                                 ║ `);
	console.log(b()`  ║`, o()`    .d8888b.  `, t()`d8b   .d8888b.   888    d8P  `, o()`8888888b.  `, t()`       d8888   .d8888b.   888888888   `, b()`║`);
	console.log(b()`  ║`, o()`   d88P  Y88b `, t()`Y8P  d88P  Y88b  888   d8P   `, o()`888   Y88b `, t()`      d88888  d88P  Y88b  888         `, b()`║`);
	console.log(b()`  ║`, o()`   Y88b.      `, t()`     888    888  888  d8P    `, o()`888    888 `, t()`     d88P888  888    888  888         `, b()`║`);
	console.log(b()`  ║`, o()`    "Y888b.   `, t()`888  888         888d88K     `, o()`888   d88P `, t()`    d88P 888  888         888         `, b()`║`);
	console.log(b()`  ║`, o()`       "Y88b. `, t()`888  888         8888888b    `, o()`8888888P"  `, t()`   d88P  888  888  88888  8888888     `, b()`║`);
	console.log(b()`  ║`, o()`         "888 `, t()`888  888    888  888  Y88b   `, o()`888 T88b   `, t()`  d88P   888  888    888  888         `, b()`║`);
	console.log(b()`  ║`, o()`   Y88b  d88P `, t()`888  Y88b  d88P  888   Y88b  `, o()`888  T88b  `, t()` d8888888888  Y88b  d88P  888         `, b()`║`);
	console.log(b()`  ║`, o()`    "Y8888P"  `, t()`888   "Y8888P"   888    Y88b `, o()`888   T88b `, t()`d88P     888   "Y8888PP"  888888888   `, b()`║`);
	console.log(b()`  ║                     ` + createCopy(copyText), b()`   ║`);
	console.log(b()`  ╚═════════════════════════════════════════════════════════════════════════════════════════════════╝ `);
};

function createCopy(copyText) {
	let spacing = [];
	for (let i = 72; i > copyText.length; i--) {
		spacing.push('');
	}
	spacing.push(copyText);
	let copy = spacing.join(' ');

	return copy;
}
