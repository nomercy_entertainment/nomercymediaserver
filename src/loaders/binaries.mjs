import Download from '../helpers/Download.mjs';
import Logger from '../loaders/logger.mjs';
import { fileChangedAgo } from '../helpers/dateTime.mjs';
import { fileURLToPath } from 'url';
import fs from 'fs';
import { getPlatform } from '../helpers/system.mjs';
import path from 'path';
import request from 'request';
import unzipper from 'unzipper';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const folder = __dirname + '/../';

if (!fs.existsSync(folder + '/binaries')) {
	fs.mkdirSync(folder + '/binaries', { recursive: true });
}

export default async () => {

	for (let i = 0; i < cdn.downloads[getPlatform()].length; i++) {

		let program = cdn.downloads[getPlatform()][i];

		const name = path.basename(program.url);

		if (!fs.existsSync(folder + '/binaries/' + name) || fileChangedAgo(folder + '/binaries/' + name, 'days') > 7) {

			Logger.log({
				level: 'info',
				name: 'binaries',
				color: 'blueBright',
				message: 'Downloading: ' + program.name,
			});

			await new Promise(async (resolve) => {

				await request.head(program.url, () => {
					request(program.url)
						.pipe(fs.createWriteStream(folder + '/binaries/' + name)
							.on('finish', async () => {

								if (fs.existsSync(folder + '/binaries/' + name)) {

									Logger.log({
										level: 'info',
										name: 'binaries',
										color: 'blueBright',
										message: 'Unpacking: ' + program.name,
									});

									const buffer = fs.readFileSync(folder + '/binaries/' + name);
									const directory = await unzipper.Open.buffer(buffer);

									let folders = directory.files.filter(f => f.type === 'Directory');
									let files = directory.files.filter(f => f.type === 'File');

									if (program.filter) {
										folders = directory.files.filter(f => f.type === 'Directory').filter(f => f.path.includes(program.filter));
										files = directory.files.filter(f => f.type === 'File').filter(f => f.path.includes(program.filter)).map(f => {
											return {
												...f,
												path: f.path.replace(/.*\//, ''),
											};
										});
									}

									for (const f of folders) {
										fs.mkdirSync(folder + program.path + '/' + f.path, { recursive: true });
									}

									for (const f of files) {
										const content = await f.buffer();

										if (!fs.existsSync((folder + program.path + '/' + f.path).replace(/\/[\w\d\s_\.()-]+$/, ''))) {
											fs.mkdirSync((folder + program.path + '/' + f.path).replace(/\/[\w\d\s_\.()-]+$/, ''), { recursive: true });
										}

										fs.writeFileSync(folder + program.path + '/' + f.path, content);
										fs.chmodSync(folder + program.path + '/' + f.path, 711);
									}

									resolve();
								}
							}));
				});
			});
		}
	};


};
