import Logger from './logger.mjs';
import axios from 'axios';

const cdn = async () => {

	let response = await axios.get('https://cdn.nomercy.tv/info')
		.catch((err) => {
			Logger.log({
				level: 'error',
				name: 'cdn',
				color: 'red',
				message: 'Something went wrong while fetching the CDN info.',
			});

			process.exit(1);
		});
		
	global.cdn = response.data.data;

	process.env.TMDB_API_KEY = response.data.data.keys.tmdb_key;

	return response.data.data;
};

export default cdn;