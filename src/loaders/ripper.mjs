import Logger from './logger.mjs';
import { fork } from 'child_process';

async function ripper() {
	let child = fork('./src/controllers/forks/makeMkv.mjs', {
		windowsHide: true,
	});

	child.on('message', (message) => {
		Logger.log({
			level: 'info',
			name: 'ripper',
			color: 'greenBright',
			message: message,
		});
	});

	child.send('START');

	return;
}

export default ripper;
