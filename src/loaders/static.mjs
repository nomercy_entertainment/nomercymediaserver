process.on('message', (app) => {
	app.use('/' + config.share, express.static(config.mediaRoot));

	if (typeof process.send == 'function') {
		process.send('Static folder routed');
	}

	process.send('message', app);

	return app;
});

export default app;
