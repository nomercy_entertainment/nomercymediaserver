import fs from 'fs';

export default () => {
	config.folderRoots.map((f) => {
		try {
			fs.mkdirSync(f.path, { recursive: true });
		} catch (error) {}
	});
};
