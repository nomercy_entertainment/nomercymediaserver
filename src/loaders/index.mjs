import {EventEmitter} from 'events';
import Logger from './logger.mjs';
import binaries from './binaries.mjs';
import { createShortcut } from '../helpers/createShortcut.mjs';
import express from './express.mjs';
import folder from './folder.mjs';
import getCertificate from './getCertificate.mjs';
import ping from './ping.mjs';
import { portMap } from './networking.mjs';
import queue from './queue.mjs';
import ripper from './ripper.mjs';
import server from './server.mjs';
import refreshToken from './refreshToken.mjs';
import fs from 'fs';
import path from 'path';
const __dirname = path.resolve();

EventEmitter.prototype.setMaxListeners(100);

const configDir = __dirname + "/config/";

async function loaders(app) {
	await folder();

	await binaries();
	
	await refreshToken();

	setInterval(async () => {
		await ping();
	}, 1 * 60 * 1000);
	await ping();

	Logger.log({
		level: 'info',
		name: 'App',
		color: 'magentaBright',
		message: 'Starting background runners...',
	});

	if (config.ripper) {
		await ripper();
	}
	if (config.queue) {
		await queue();
	}

	if (config.port_forward) {
		await portMap();
	}

	await express(app);

	await server(app);
	
	createShortcut();

}

export default loaders;
