import chalk from 'chalk';

export const serverRunning = () => {
	console.log(chalk.hex('#00a10d')`╔════════════════════════════════════════════════╗`);

	console.log(chalk.hex('#00a10d')`║     `, chalk.hex('#5ffa71')`Secure server running: on port:`, chalk.bold.hex('#c3c3c3')`${config.secureInternalPort}`.replace(', ', ''), chalk.hex('#00a10d')`     ║`);
	console.log(chalk.hex('#00a10d')`║       `, chalk.hex('#cccccc')`visit:`, chalk.bold.underline.hex('#c3c3c3')`https://app.nomercy.tv`, chalk.hex('#00a10d')`          ║`);

	console.log(chalk.hex('#00a10d')`╚════════════════════════════════════════════════╝ `);
};

export const unsecureServerRunning = () => {
	console.log(chalk.hex('#b50404')`╔════════════════════════════════════════════════╗`);

	console.log(chalk.hex('#b50404')`║    `, chalk.hex('#fa5f5f')`Unsecure server running: on port:`, chalk.bold.hex('#c3c3c3')`${config.unsecureInternalPort}`.replace(', ', ''), chalk.hex('#b50404')`    ║`);
	console.log(chalk.hex('#b50404')`║      `, chalk.hex('#cccccc')`visit:`, chalk.bold.underline.hex('#c3c3c3')`http://unsecure.nomercy.tv`, chalk.hex('#b50404')`       ║`);

	console.log(chalk.hex('#b50404')`╚════════════════════════════════════════════════╝ `);
};

export default {
	serverRunning,
	unsecureServerRunning,
}