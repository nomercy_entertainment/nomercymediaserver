import fs from 'fs';
import { getDrives } from '../helpers/system.mjs';
import path from 'path';
import { prismaMedia } from '../config/database.mjs';

async function seed() {

	let roots;
	if (!fs.existsSync(path.resolve('config/extraFolderRoots.mjs'))) {
		roots = () => [];
	}
	else {
		roots = (await import('../../config/extraFolderRoots.mjs')).roots;
	}

	const segmentTime = 15;
	const hasOpticalDrive = (await getDrives()).opticalDrives.length > 0;

	const data = {
		allowAdult: process.env.ALLOW_ADULT_CONTENT,
		allowedUsers: [],
		alwaysCreatePlaylist: false,
		assToVtt: true,
		channelOrder: {
			1: 8,
			2: 7,
			3: 6,
			4: 5,
			5: 4,
			6: 3,
			7: 2,
			8: 1,
		},
		debug: false,
		deleteInput: false,
		encodingQualities: [
			{
				width: 3840,
				height: 2160,
				crf: 23,
				// bitrate: 50000,
				enabled: true,
				required: false,
				options: '',
			},
			{
				width: 1920,
				height: 1080,
				crf: 23,
				// bitrate: 8000,
				enabled: true,
				required: true,
				options: '',
			},
			{
				width: 1280,
				height: 720,
				crf: 23,
				// bitrate: 5500,
				enabled: false,
				required: false,
				options: '',
			},
		],
		unsecureServer: process.env.UNSECURE_SERVER,
		unsecurePortForward: process.env.UNSECURE_PORT_FORWARD,
		unsecureInternalPort: process.env.UNSECURE_INTERNAL_PORT,
		unsecureExternalPort: process.env.UNSECURE_EXTERNAL_PORT,

		securePortForward: process.env.SECURE_PORT_FORWARD,
		secureInternalPort: process.env.SECURE_INTERNAL_PORT,
		secureExternalPort: process.env.SECURE_EXTERNAL_PORT,

		folderRoots: [
			{
				direction: 'out',
				enabled: true,
				hq: false,
				path: process.env.SHARE_ROOT + '/Anime/Anime/',
				share: 'Media',
				share_root: process.env.SHARE_ROOT + '/',
				type: 'anime',
				required: true,
			},
			{
				direction: 'in',
				enabled: true,
				hq: false,
				path: process.env.SHARE_ROOT + '/Anime/Download/',
				share: 'Media',
				share_root: process.env.SHARE_ROOT + '/',
				type: 'anime',
				required: true,
			},
			{
				direction: 'in',
				enabled: true,
				hq: false,
				path: process.env.SHARE_ROOT + '/Films/Download/',
				share: 'Media',
				share_root: process.env.SHARE_ROOT + '/',
				type: 'movie',
				required: true,
			},
			{
				direction: 'out',
				enabled: true,
				hq: false,
				path: process.env.SHARE_ROOT + '/Films/Films/',
				share: 'Media',
				share_root: process.env.SHARE_ROOT + '/',
				type: 'movie',
				required: true,
			},
			{
				direction: 'in',
				enabled: true,
				hq: false,
				path: process.env.SHARE_ROOT + '/TV.Shows/Download/',
				share: 'Media',
				share_root: process.env.SHARE_ROOT + '/',
				type: 'tv',
				required: true,
			},
			{
				direction: 'out',
				enabled: true,
				hq: false,
				path: process.env.SHARE_ROOT + '/TV.Shows/TV.Shows/',
				share: 'Media',
				share_root: process.env.SHARE_ROOT + '/',
				type: 'tv',
				required: true,
			},
		].concat(roots()),
		keepOriginal: {
			audio: true,
			subtitles: true,
		},
		loglevel: 'http',
		maxParts: 1,
		minlength: 60,
		mediaRoot: process.env.SHARE_ROOT,
		openServer: process.env.OPEN_SERVER,
		owner: undefined,
		prefferedOrder: {
			eng: 2,
			jpn: 3,
		},
		queue: true,
		ripper: hasOpticalDrive,
		ripperFolder: process.env.FAST_ROOT + '/Media/Ripper/',
		ripperOutFolder: process.env.FAST_ROOT + '/Media/Out/',
		segment: [
			'-f segment',
			`-hls_time ${segmentTime}`,
			'-hls_list_size 0',
			'-hls_time 15',
			'-hls_playlist_type vod',
			`-segment_time ${segmentTime}`,
			'-segment_format mpegts',
			'-segment_list_type m3u8',
			'-segment_list_size 0',
			'-segment_time_delta 0.1',
			`-force_key_frames "expr:if(isnan(prev_forced_t),gte(t,${segmentTime}),gte(t,prev_forced_t+${segmentTime}))"`,
		],
		updateInterval: 2000,
		segmentTime,
		workerName: 'NoMercy MediaServer',
		workers: 1,
		defaults: [
			'-profile:v baseline',
			'-level 3.0',
			'-pix_fmt yuv420p',
			'-tune film',
			'-sc_threshold 0',
			'-muxdelay 0',
			'-x264opts "keyint=48:min-keyint=48:no-scenecut"',
			'-movflags +faststart',
			'-max_muxing_queue_size 9999'
		],
		setupComplete: false,
	};

	let transaction = [];

	for (let [key, value] of Object.entries(data)) {

		if (key == 'owner') {
			value = value?.sub_id || value;
		}
		transaction.push(
			prismaMedia.configuration.upsert({
				where: {
					key: key.toString(),
				},
				update: {
					// key: key.toString(),
					// value: JSON.stringify(value)?.replace(/^"|"$/g, ''),
				},
				create: {
					key: key.toString(),
					value: JSON.stringify(value)?.replace(/^"|"$/g, ''),
				},
			})
		);
	}

	try {
		await prismaMedia.$transaction(transaction);
	} catch (error) {
		try {
			await prismaMedia.$transaction(transaction);
		} catch (error) {
			try {
				await prismaMedia.$transaction(transaction);
			} catch (error) {
				//
			}
		}
	}
}

export default seed;
