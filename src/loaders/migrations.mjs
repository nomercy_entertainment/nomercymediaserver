import Logger from './logger.mjs';
import certifications from '../providers/themoviedb/database/certifications.mjs';
import { execSync } from 'child_process';
import fs from 'fs';
import genres from '../providers/themoviedb/database/genres.mjs';
import path from 'path';
import { prismaMedia } from '../config/database.mjs';

const __dirname = path.resolve();

export default async () => {
	if (!fs.existsSync(__dirname + '/databases')) {
		fs.mkdirSync(__dirname + '/databases', { recursive: true });
	}

	let migrating = false;
	
	if (!fs.existsSync(__dirname + '/databases/media.db')) {
		Logger.log({
			level: 'info',
			name: 'App',
			color: 'magentaBright',
			message: 'Migrating media database',
		});
		migrating = true;
	}

	execSync('npx prisma migrate dev --name init');
	await prismaMedia.$queryRaw`PRAGMA journal_mode=WAL;`;

	// if (migrating) {
		await genres();
		await certifications();
	// }

};
