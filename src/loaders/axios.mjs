import axios from 'axios';
import https from 'https';
import i18next from '../config/i18next.mjs';
import keycloak from './keycloak.mjs';
import fs from 'fs';
import path from 'path';

let PENDING_REQUESTS = 0;

const __dirname = path.resolve();
const configDir = __dirname + '/config/';
const tokenFile = configDir + 'tokens.json';

const appApiClient = axios.create({
	httpsAgent: new https.Agent({ rejectUnauthorized: false }),
	timeout: process.env.REQUEST_TIMEOUT || 700,
});

appApiClient.interceptors.request.use(
	async (config) => {
		return new Promise((resolve) => {
			config.headers['Accept'] = 'application/json';
			config.headers['Accept-Language'] = `${i18next.language || 'en'}`;
			config.headers.Authorization = `Bearer ${process.env.TOKEN != '' ? process.env.TOKEN : JSON.parse(fs.readFileSync(tokenFile, 'utf-8')).access_token}`;
			resolve(config);
		});
	},
	function (error) {
		
		Logger.log({
			level: 'error',
			name: 'Http',
			color: 'red',
			message: error.response.message,
		});
		return Promise.reject(error.response.message);
	}
);

appApiClient.interceptors.response.use(
	async (response, error) => {
		const status = error && error.response ? error.response.status : null;
		// token expired
		if (status === 401) {
			keycloak.updateToken(5).then((refreshed) => {
				if (refreshed) {
					console.log('Auth Token Refreshed');
					error.config.headers.Authorization = `Bearer ${keycloak.token}`;
					return appApiClient.request(error.config);
				}
			});
		}
		PENDING_REQUESTS = Math.max(0, PENDING_REQUESTS - 1);
		if (response.data) {
			response = response.data;
		}

		return Promise.resolve(response);
	},
	async (error) => {
		PENDING_REQUESTS = Math.max(0, PENDING_REQUESTS - 1);
		return Promise.reject(error.response?.message || error.response);
	}
);
export default appApiClient;
