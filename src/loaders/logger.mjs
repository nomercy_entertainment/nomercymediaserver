import chalk from 'chalk';
import winston from 'winston';

const myCustomLevels = {
	error: 0,
	warn: 1,
	info: 2,
	http: 3,
	verbose: 4,
	debug: 5,
	silly: 6,
	socket: 3,
	cyan: 2,
};

winston.addColors({
	socket: 'magenta',
	cyan: 'cyan',
});

const transports = [];
// if (process.env.NODE_ENV !== 'development') {
// 	transports.push(new winston.transports.Console());
// } else {
	transports.push(
		new winston.transports.Console({
			timestamp: () => new Date(),
			format: winston.format.combine(
				winston.format.colorize(),
				winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
				winston.format.splat(),
				winston.format.json(),

				winston.format.printf((info) => `${spacer(info.color ? color(info.color, info.name) : info.level, 20)}: ${info.message}`)
			),
		})
	);
// }

function color(color, msg) {
	switch (color) {
		case 'black':
			return chalk.black(msg);
			break;
		case 'red':
			return chalk.red(msg);
			break;
		case 'green':
			return chalk.green(msg);
			break;
		case 'yellow':
			return chalk.yellow(msg);
			break;
		case 'blue':
			return chalk.blue(msg);
			break;
		case 'magenta':
			return chalk.magenta(msg);
			break;
		case 'cyan':
			return chalk.cyan(msg);
			break;
		case 'blackBright':
			return chalk.blackBright(msg);
			break;
		case 'redBright':
			return chalk.redBright(msg);
			break;
		case 'greenBright':
			return chalk.greenBright(msg);
			break;
		case 'yellowBright':
			return chalk.hex('#BFB900')(spacer(msg, 10));
			break;
		case 'blueBright':
			return chalk.blueBright(msg);
			break;
		case 'magentaBright':
			return chalk.magentaBright(msg);
			break;
		case 'cyanBright':
			return chalk.cyanBright(msg);
			break;
		case 'whiteBright':
			return chalk.whiteBright(msg);
			break;
		default:
			return chalk.white(msg);
			break;
	}
}

const Logger = winston.createLogger({
	level: process.env.LOG_LEVEL || 'http',
	levels: myCustomLevels,
	transports,
});

export default Logger;

function spacer(text, rightpadding) {
	let spacing = [];
	spacing.push(text);
	for (let i = 0; i < rightpadding - text.length; i++) {
		spacing.push('');
	}
	return spacing.join(' ');
}
