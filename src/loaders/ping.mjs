import appApiClient from './axios.mjs';
import Logger from './logger.mjs';
import { get_ip } from './networking.mjs';
import refreshToken from './refreshToken.mjs';

export default async () => {
	const internal_ip = get_ip();

	let data = {
		internal_ip: internal_ip,
		internal_port: config.secureInternalPort,
		external_port: config.secureExternalPort,
		server_name: config.workerName,
		server_id: config.server_id,
		online: true,
	};

	if(!internal_ip){
		delete data.internal_ip;
	}

	if (!process.env.TOKEN) {
		await refreshToken();
	}

	if (process.env.TOKEN != '') {
		await appApiClient
			.post('https://api.nomercy.tv/server/ping', data)
			.then((response) => {
				// console.log(response);
			})
			.catch((error) => {
				if(!error?.response?.data?.message){
					console.error(error);
				}
				else {
					Logger.log({
						level: 'error',
						name: 'ping',
						color: 'red',
						message: error?.response?.data?.message,
					});
					process.exit(1);
				}
			});
	}
};
