import Logger from './logger.mjs';
import { fork } from 'child_process';
import { prismaMedia } from '../config/database.mjs';
import { sleep } from '../helpers/dateTime.mjs';

async function queue() {
	await prismaMedia.jobs.updateMany({
		where: {
			id: {
				not: 0,
			},
		},
		data: {
			attempts: null,
			available_at: null,
			reserved_at: null,
		},
	});

	let queue = [];

	for (let i = 0; i < config.workers; i++) {
		queue[i] = fork('./src/controllers/forks/queueRunner.mjs', {
			windowsHide: true,
		});

		queue[i].on('message', (message) => {
			Logger.log({
				level: 'info',
				name: 'Queue',
				color: 'greenBright',
				message: message,
				
			});
		});

		queue[i].send('START');

		sleep(500);
	}

	global.queue = queue;
}

export default queue;
