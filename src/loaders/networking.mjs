import Logger from './logger.mjs';
import chalk from 'chalk';
import os from 'os';
import upnp from 'upnpjs';

export function get_ip() {
	let interfaces = os.networkInterfaces();
	let addresses = [];
	for (let k in interfaces) {
		for (let k2 in interfaces[k]) {
			let address = interfaces[k][k2];
			if (address.family === 'IPv4' && !address.internal) {
				addresses.push(address.address);
			}
		}
	}

	const address = addresses.filter((a) => !a.startsWith('127') && !a.startsWith('172'))?.[0];
	if(address){	
		return address;
	}
	else {
		return process.env.INTERNAL_IP
	}
}

export async function portMap() {
	
	Logger.log({
		level: 'info',
		name: 'App',
		color: 'magentaBright',
		message: `Trying to add the server to UPnP`,
		
	});
	const HOST = get_ip();

	return await upnp
		.discover((timeout = 10000))
		.then((igd) => {
			igd.addPortMapping({
				ip: HOST,
				internalPort: 7644,
				externalPort: 7644,
				description: 'NoMercy MediaServer',
			});
			igd.addPortMapping({
				ip: HOST,
				internalPort: config.secureInternalPort,
				externalPort: config.secureExternalPort,
				description: 'NoMercy MediaServer',
			});
		})
		.then(() => {
			Logger.log({
				level: 'info',
				name: 'App',
				color: 'magentaBright',
				message: `Successfully mapped the internal port: ${config.secureInternalPort} to external port: ${config.secureExternalPort}`,
				
			});
		})
		.catch((reason) => {
			let message =
				chalk.hex('#bd7b00')`Sorry, Your router does not let me add the record, you need to add a port forward manually.
         For more information, visit: ` +
				chalk.bold.underline.hex('#c3c3c3')`https://portforward.com/how-to-port-forward` +
				chalk.hex('#bd7b00')`
         You can only use the app on your local network until you forward port ` +
				config.secureInternalPort +
				chalk.hex('#bd7b00')` to ` +
				config.secureExternalPort +
				`.`;

			Logger.log({
				level: 'error',
				name: 'App',
				color: 'red',
				message: message,
				
			});
		});
}

export const allowedOrigins = [
	'https://nomercy.tv', 
	'https://app.nomercy.tv', 
	'https://dev.nomercy.tv',
	'http://localhost:3000',
	'http://192.168.2.201:3000',
	'https://tauri.localhost',
	'https://' + get_ip() + ':3000',
	'http://' + get_ip() + ':3000',
	'http://localhost:5000',
	'https://' + get_ip() + ':5000',
	'http://' + get_ip() + ':5000',
]; 

export default {
	allowedOrigins,
	get_ip,
	portMap,
};
