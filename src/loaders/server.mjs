import {serverRunning, unsecureServerRunning} from './serverRunning.mjs';

import Logger from './logger.mjs';
import { fileURLToPath } from 'url';
import fs from 'fs';
import http from 'http';
import https from 'https';
import path from 'path';
import {socket} from './socket.mjs';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const certDir = __dirname + '/../../certsFiles/';
const keyFile = certDir + 'key.pem';
const certFile = certDir + 'cert.pem';
const caFile = certDir + 'ca.pem';

export default async (app) => {
	let credentials;
	if (fs.existsSync(keyFile) && fs.existsSync(certFile) && fs.existsSync(caFile)) {
		credentials = {
			key: fs.readFileSync(keyFile),
			cert: fs.readFileSync(certFile),
			ca: fs.readFileSync(caFile),
		};
	}

	if (credentials) {
			
		let httpsServer = https.createServer(credentials, app);
		httpsServer
			.listen(config.secureInternalPort, '0.0.0.0', () => {
				serverRunning();
			})
			.on('error', (err) => {
				Logger.log({
					level: 'error',
					name: 'App',
					color: 'magentaBright',
					message: 'Sorry Something went wrong starting the secure server',
					
				});
				process.exit(1);
			});

		socket.connect(httpsServer);
	}
	
	if(config.unsecureServer || !credentials) {
		let httpServer = http.createServer(app);
		httpServer
			.listen(config.unsecureInternalPort, '0.0.0.0', () => {
				unsecureServerRunning();
			})
			.on('error', (err) => {
	
				Logger.log({
					level: 'error',
					name: 'App',
					color: 'magentaBright',
					message: 'Sorry Something went wrong starting the unsecure server',
					
				});
				process.exit(1);
			});
		socket.connect(httpServer);
	}
	return;
};
