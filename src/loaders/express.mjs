import Logger from './logger.mjs';
import { allowedOrigins } from './networking.mjs';
import changeLanguage from '../api/middlewares/language.mjs';
import check from '../api/middlewares/check.mjs';
import compression from 'compression';
import cors from 'cors';
import express from 'express';
import { fileURLToPath } from 'url';
import keycloak from './keycloak.mjs';
import methodOverride from 'method-override';
import path from 'path';
import routes from '../api/index.mjs';
import session from 'express-session';
import { session_config } from '../config/session.mjs';
import { staticPermissions } from '../api/middlewares/permissions.mjs';
import { unique } from '../helpers/stringArray.mjs'
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export default (app) => {

	const staticOptions = {
		dotfiles: 'deny',
		setHeaders: function (res, path, stat) {
			res.set('x-timestamp', Date.now())
		},
	}

	app.use(cors({
		origin: allowedOrigins
	}));

	app.use((req, res, next) => {
		res.set('X-Powered-By', 'NoMercy MediaServer');
		res.set('owner', config.owner);
		
		if(allowedOrigins.some(o => o == req.headers.origin)){
			res.set("Access-Control-Allow-Origin", req.headers.origin);
		}
		next();
	});

	app.use(session(session_config));
	app.use(keycloak.initKeycloak().middleware());

	app.enable('trust proxy');
	app.use(changeLanguage);
	app.use(methodOverride());
	app.use(express.json());
	app.use(express.urlencoded({ extended: true }));
	app.use(compression());

	app.get('/api/ping', async (req, res) => {
		return res.json({
			message: 'pong',
			setupComplete: config.setupComplete || false,
		});
	});

	app.get('/status', (req, res) => {
		res.status(200).end();
	});
	app.head('/status', (req, res) => {
		res.status(200).end();
	});
	
	unique(config.folderRoots, 'share_root').filter(r => r.share_root).map((r) => {
		app.use('/' + r.share, express.static(r.share_root.replace(/\/$/, ''), staticOptions));
	});

	app.use('/Ripper', staticPermissions, express.static(config.ripperOutFolder, staticOptions));
	app.use('/', staticPermissions, express.static(path.join(__dirname, '..', 'public'), staticOptions));

	app.get('/', staticPermissions, (req, res) => {
		res.redirect('https://app.nomercy.tv');
	});
	
	app.use('/api', keycloak.initKeycloak().checkSso(), check, changeLanguage, routes);

	Logger.log({
		level: 'info',
		name: 'App',
		color: 'magentaBright',
		message: 'Express loaded',
		
	});

	return app;
};
