import Logger from './logger.mjs';
import axios from 'axios';
import fs from 'fs';
import getCertificate from './getCertificate.mjs';
import { get_ip } from '../loaders/networking.mjs';
import { keycloak_key } from '../config/keycloak.mjs';
import path from 'path';
import qs from 'qs';
import { random_string } from '../helpers/stringArray.mjs';
import rax from 'retry-axios';
import readline from 'readline';

const __dirname = path.resolve();

export default async () => {
	const configDir = __dirname + '/config/';
	const certDir = __dirname + '/certsFiles/';
	if (!fs.existsSync(configDir)) {
		fs.mkdirSync(configDir, { recursive: true });
	}
	if (!fs.existsSync(certDir)) {
		fs.mkdirSync(certDir, { recursive: true });
	}

	const keyFile = certDir + 'key.pem';
	const certFile = certDir + 'cert.pem';
	const confFile = configDir + 'config.json';
	const tokenFile = configDir + 'tokens.json';

	if (!fs.existsSync(confFile) || !fs.existsSync(keyFile) || !fs.existsSync(certFile) || !fs.existsSync(tokenFile)) {
		const internal_ip = get_ip();
		const external_ip = await axios.get('https://api.ipify.org/').catch(({response}) => {console.log(response)});
		const token = random_string(20);
		
		if(!JSON.parse(fs.readFileSync(confFile, 'utf8'))?.server_id) {
			let serverData = {
				token,

				unsecure_internal_port: config.unsecureInternalPort,
				unsecure_external_port: config.unsecureExternalPort,
				
				internal_port: config.secureInternalPort,
				external_port: config.secureExternalPort,

				internal_ip: internal_ip,
				external_ip: external_ip.data,
			};
			Logger.log({
				level: 'info',
				name: 'register',
				color: 'blueBright',
				message: 'Registering server, this takes a moment...',
				
			});

			let registerServerResponse = await axios
				.post('https://api.nomercy.tv/server/register', serverData, {
					Accept: 'application/json',
				})
				.catch((error) => {
					if (error.response) {
						console.log(error.response.data.message);
						process.exit();
					}
				});

			if (registerServerResponse) {
				fs.writeFileSync(
					confFile,
					JSON.stringify({
						server_id: registerServerResponse.data.server_id,
					})
				);
				Logger.log({
					level: 'info',
					name: 'register',
					color: 'blueBright',
					message: `Visit https://app.nomercy.tv/#/link?token=${token} to link this server to your account`,
					
				});
			}

			rax.attach();
			const conf = {
				headers: {
					Accept: 'application/json',
				},
				timeout: 1000 * 60,
				raxConfig: {
					retry: 50,
					noResponseRetries: 50,
				},
			};

			let linkServerResponse = await axios.get(`https://api.nomercy.tv/server/link?token=${token}`, conf).catch((error) => {
				if (error.response) {
					Logger.log({
						level: 'info',
						name: 'register',
						color: 'blueBright',
						message: error.response.data.message,
						
					});

					process.exit();
				}
			});

			let { server_id } = JSON.parse(fs.readFileSync(confFile));
			Logger.log({
				level: 'info',
				name: 'register',
				color: 'blueBright',
				message: 'Obtaining SSL certificate...',
				
			});

			let serverCertificaqteResponse = await axios
				.get(`https://api.nomercy.tv/server/certificate?server_id=${server_id}`, {
					headers: {
						Accept: 'application/json',
						Authorization: `Bearer ${linkServerResponse.data.access_token}`,
					},
				})
				.catch((error) => {
					if (error.response) {
						Logger.log({
							level: 'info',
							name: 'register',
							color: 'blueBright',
							message: error.response.data.message,
							
						});

						process.exit();
					}
				});

			fs.writeFileSync(keyFile, serverCertificaqteResponse.data.private_key);
			fs.writeFileSync(certFile, serverCertificaqteResponse.data.certificate + '\n' + serverCertificaqteResponse.data.issuer_certificate);

			Logger.log({
				level: 'info',
				name: 'register',
				color: 'blueBright',
				message: 'Obtaining offline token',
				
			});

			let keycloakData = qs.stringify({
				client_id: 'nomercy-ui',
				grant_type: 'refresh_token',
				client_secret: keycloak_key,
				scope: 'openid offline_access',
				refresh_token: linkServerResponse.data.refresh_token,
			});

			let keycloakTokenResponse = await axios.post('https://auth.nomercy.tv/auth/realms/NoMercyTV/protocol/openid-connect/token', keycloakData)
			.catch((err) => {
				Logger.log({
					level: 'error',
					name: 'keycloak',
					color: 'red',
					message: err.response?.data?.error_description ?? err,
				});
	
				process.exit(1);
			});

			fs.writeFileSync(tokenFile,JSON.stringify(keycloakTokenResponse.data));

			process.env.TOKEN = keycloakTokenResponse.data.access_token;
			process.env.REFRESH_TOKEN = keycloakTokenResponse.data.refresh_token;

			Logger.log({
				level: 'info',
				name: 'register',
				color: 'blueBright',
				message: 'Setup ready.',
				
			});
		}
		else { 
			await getCertificate();
		}
	}
	else { 
		await getCertificate();
	}
};

function askQuestion(query, def = null) {
	const rl = readline.createInterface({
		input: process.stdin,
		output: process.stdout,
	});

	return new Promise((resolve) =>
		rl.question(query, (ans) => {
			rl.close();

			if (ans) {
				resolve(ans);
			}

			resolve(def);
		})
	);
}
