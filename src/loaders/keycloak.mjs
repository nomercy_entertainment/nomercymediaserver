import Keycloak from 'keycloak-connect';
import Logger from './logger.mjs';
import keycloakConfig from '../config/keycloak.mjs';
import session from 'express-session';

let _keycloak;

function initKeycloak() {
	if (_keycloak) {
		return _keycloak;
	} else {
		var memoryStore = new session.MemoryStore();
		_keycloak = new Keycloak(
			{
				store: memoryStore,
				scope: 'openid profile offline_access email',
			},
			keycloakConfig
		);

		Logger.log({
			level: 'info',
			name: 'App',
			color: 'magentaBright',
			message: 'Keycloak loaded',
		});
		return _keycloak;
	}
}

function getKeycloak() {
	if (!_keycloak) {
		Logger.log({
			level: 'error',
			name: 'App',
			color: 'magentaBright',
			message: 'Keycloak has not been initialized. Please called init first.',
		});
	}
	return _keycloak;
}

export default {
	Keycloak,
	initKeycloak,
	getKeycloak,
	keycloakConfig,
};
