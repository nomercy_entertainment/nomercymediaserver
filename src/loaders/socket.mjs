import Logger from './logger.mjs';
import { Server } from 'socket.io';
import base from '../api/sockets/base.mjs';
import socketioJwt from 'socketio-jwt';

let io = null;
let myClientList = [];

const updatedList = (socket) => myClientList.filter(c => c.sub == socket.decoded_token.sub).map((s) => {
	return {
		socket_id: s.id,
		id: s.client_id,
		type: s.client_type,
		name: s.client_name,
		secure_connection: s.secure_connection,
	};
});

export const socket = {
	listen: function (event, values) {
		if (io) {
			io.listen(event, values);
		}
	},
	emit: function (event, values) {
		if (io) {
			io.emit(event, values);
		}
	},
	use: function (event, values) {
		if (io) {
			io.sockets.use(event, values);
		}
	},
	on: function (event, values) {
		if (io) {
			socket.on(event, values);
		}
	},
	once: function (event, values) {
		if (io) {
			socket.once(event, values);
		}
	},
	connect: async function (server) {
		io = new Server(
			server,
			config.socketCors
		);
		io.setMaxListeners(300);

		io.use(
			socketioJwt.authorize({
				secret: config.key,
				timeout: 15000,
				handshake: true,
				auth_header_required: true,
			})
		);

		io.once("connection", (socket) => {
			socket.emit("update_content");

			socket.emit("notification", {
				id: 1,
				title: "NoMercy Mediaserver",
				body: "Your server has started.",
				silent: false,
				notify: true,
				read: false,
				from: "NoMercy Mediaserver",
				method: "add",
				to: "*",
				image: "https://cdn.nomercy.tv/img/favicon.ico",
				created_at: Date.now(),
			});
		});

		let uniqueFilter = "connection";

		io.on("connection", async (socket) => {
			if (uniqueFilter == "connection") {
				myClientList = myClientList.filter((s) => s.id != socket.id);
			} else if (uniqueFilter == "device") {
				myClientList = myClientList.filter(
					(s) => s.client_id != socket.handshake.headers.device_id
				);
			}

			myClientList.push({
				sub: socket.decoded_token.sub,
				email: socket.decoded_token.email,
				id: socket.id,
				address: socket.handshake.address,
				connected: socket.connected,
				disconnected: socket.disconnected,
				secure_connection: socket.handshake.secure,
				client_id: socket.handshake.headers.device_id,
				client_name: socket.handshake.headers.device_name,
				client_type: socket.handshake.headers.device_type,
				rooms: socket.adapter.rooms,
				socket,
			});

			socket.join(socket.decoded_token.sub);

			Logger.log({
				level: "http",
				name: "Socket",
				color: "yellow",
				message: `${socket.decoded_token?.name} connected, ${
					updatedList(socket).length
				} ${uniqueFilter}${updatedList(socket).length == 1 ? "" : "s"} ${
					uniqueFilter == "device" ? "connected" : "established"
				}.`,
			});
			socket.nsp
				.to(socket.decoded_token.sub)
				.emit("get_devices", updatedList(socket));
			
			
			socket.on("disconnect", () => {
				if (uniqueFilter == "connection") {
					myClientList = myClientList.filter(
						(s) => s.id != socket.id
					);
				} else if (uniqueFilter == "device") {
					myClientList = myClientList.filter(
						(s) => s.client_id != socket.handshake.headers.device_id
					);
				}
	
				Logger.log({
					level: "http",
					name: "Socket",
					color: "yellow",
					message: `${socket.decoded_token?.name} disconnected, ${
						updatedList(socket).length
					} ${uniqueFilter}${updatedList(socket).length == 1 ? "" : "s"} ${
						uniqueFilter == "device" ? "connected" : "established"
					}.`,
				});
	
				socket.nsp
					.to(socket.decoded_token.sub)
					.emit("get_devices", updatedList(socket));
			});

			await base(socket, io, updatedList);
		});
		

		// io.of("/").adapter.on("create-room", (room) => {
		// 	console.log(`room ${room} was created`);
		// });

		// io.of("/").adapter.on("join-room", (room, id) => {
		// 	if(room.includes(35)){
		// 		console.log(`socket ${id} has joined room ${room}`);
		// 	}
		// });

		// io.of("/").adapter.on("leave-room", (room, id) => {
		// 	if(room.includes(35)){
		// 		console.log(`socket ${id} has left room ${room}`);
		// 	}
		// });
	},
};

/**
 * @param  {import('express').Request} req
 * @param  {String} event
 * @param  {String} message
 * @description Send a custom socket event on the requestee socket connection.
 */

export const sendTo = (to, event, message = null) => {
	return myClientList.filter((c) => c.sub == to).forEach((c) => c.socket.emit(event, message));
};

/**
 * @param  {String} to User id or email
 * @param  {String} message
 * @description Send a message to a user on their socket connection.
 */
export const sendMessageTo = (to, message) => {
	return myClientList.filter((c) => c.email == to || c.sub == to).forEach((c) => c.socket.emit('message', message));
};

/**
 * @param  {String} to User id or email
 * @param  {String} message
 * @description Send a notification to a user on their socket connection.
 */
export const sendNotificationTo = (to, type = 'notification', message = null) => {
	return myClientList.filter((c) => c.email == to || c.sub == to).forEach((c) => c.socket.emit(type, message));
};

export default {
	socket,
	sendTo,
	sendMessageTo,
	sendNotificationTo,
};
