import Logger from './logger.mjs';
import axios from 'axios';
import certificateNeedsRenewal from '../helpers/certificateNeedsRenewal.mjs';
import fs from 'fs';
import path from 'path';
import authenticate from './authenticate.mjs';

const __dirname = path.resolve();

export default async () => {
	if (process.env.NO_CERTIFICATE) return;

	const configDir = __dirname + '/config/';
	const certDir = __dirname + '/certsFiles/';

	const keyFile = certDir + 'key.pem';
	const certFile = certDir + 'cert.pem';
	const authority = certDir + 'ca.pem';
	const confFile = configDir + 'config.json';
	const tokenFile = configDir + 'tokens.json';

	const { server_id } = JSON.parse(fs.readFileSync(confFile));
	const { access_token } = JSON.parse(fs.readFileSync(tokenFile));

	if (certificateNeedsRenewal(certFile)) {
		Logger.log({
			level: 'info',
			name: 'certificate',
			color: 'blueBright',
			message: 'Certificate is due renewal, Obtaining new SSL certificate...',
		});

		await axios
			.get(`https://api.nomercy.tv/server/renewcertificate?server_id=${server_id}`, {
				headers: {
					Accept: 'application/json',
					Authorization: `Bearer ${access_token}`,
				},
			})
			.then((newCertificates) => {
				console.log(newCertificates);
				fs.rmSync(keyFile, { force: true });
				fs.rmSync(certFile, { force: true });
				fs.rmSync(authority, { force: true });
				fs.writeFileSync(keyFile, newCertificates.data.private_key);
				fs.writeFileSync(certFile, newCertificates.data.certificate + '\n' + newCertificates.data.issuer_certificate);
				fs.writeFileSync(authority, newCertificates.data.certificate_authority);
			})
			.catch(async (error) => {
				console.log(error);
				if (error.response) {
					Logger.log({
						level: 'info',
						name: 'certificate',
						color: 'blueBright',
						message: error.response.data.message,
					});

					await authenticate();

					const { access_token } = JSON.parse(fs.readFileSync(tokenFile));

					await axios
						.get(`https://api.nomercy.tv/server/renewcertificate?server_id=${server_id}`, {
							headers: {
								Accept: 'application/json',
								Authorization: `Bearer ${access_token}`,
							},
						})
						.then((newCertificates) => {
							console.log(newCertificates);
							fs.rmSync(keyFile, { force: true });
							fs.rmSync(certFile, { force: true });
							fs.rmSync(authority, { force: true });
							fs.writeFileSync(keyFile, newCertificates.data.private_key);
							fs.writeFileSync(certFile, newCertificates.data.certificate + '\n' + newCertificates.data.issuer_certificate);
							fs.writeFileSync(authority, newCertificates.data.certificate_authority);
						})
						.catch(async (error) => {
							console.error(error);
							if (error.response) {
								Logger.log({
									level: 'info',
									name: 'certificate',
									color: 'blueBright',
									message: error.response.data.message,
								});

								process.exit(1);
							}
						});
				}
			});


		Logger.log({
			level: 'info',
			name: 'certificate',
			color: 'blueBright',
			message: 'New SSL certificate has been obtained',
		});
	}
};
